import React, { Fragment, useState } from "react";
import "react-dropzone-uploader/dist/styles.css";
import Dropzone from "react-dropzone-uploader";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CInputFile,
  CTooltip,
} from "@coreui/react";
import { FIELDS } from "../../constant";
import styled from "styled-components";
import { UploadDocSvgIcon, DownloadSampleDoc } from "../../../Comman/Icons";
import { URL_HOST } from "../../../Comman/constants";
import { ContentLoading } from "../../../Comman/components";

const styledComp = styled.div`
  .dzu-inputLabelWithFiles {
    display: flex;
    justify-content: center;

    align-self: flex-start;
    padding: 0 14px;
    min-height: 32px;
    background-color: #e6e6e6;
    color: #2484ff;
    border: none;
    font-family: "Helvetica", sans-serif;
    border-radius: 4px;
    font-size: 14px;
    font-weight: 600;
    margin-top: 63px;
    margin-left: 3%;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    cursor: pointer;
  }
`;

const commanCss = {
  width: "21%",
};
const iconCss = {
  color: "#d03431",
  fontSize: "19px",
};
const drop_zone_css = {
  height: "260px",
};
const button_css = {
  // fontWeight: '1000',
  fontFamily: "inherit",
};

const upload_css = {
  marginTop: "-41px",
  cursor: "pointer",
  marginRight: "22%",
  // fontWeight: '1000',
  fontFamily: "inherit",
};

var TotoalPercentageValue = 0;
var fileStatus = "";
var TotoalStatusSucess = "";
var lenght = "";
var TotoalPercentage = [];
var upoadFile = false;
let successMsg = "";
const Layout = ({
  input,
  previews,
  submitButton,
  dropzoneProps,
  files,
  extra: { maxFiles },
  extraProp,
  handleProcessFile,
  handleFileUploadALL,
  downloadFile,
}) => {
  lenght = files.length;
  TotoalStatusSucess = true;

  return (
    <Fragment>
      <div {...dropzoneProps}>{files.length < maxFiles && input}</div>

      <CCardHeader>{/* Header */}</CCardHeader>
      <CCardBody>
        {files.length > 0 && (
          <table className="ui celled table">
            <thead>
              <th style={commanCss}>Name</th>
              <th style={commanCss}>Size</th>
              <th style={commanCss}>Process</th>
              <th style={commanCss}>Status</th>
              <th style={commanCss}>Action</th>
            </thead>
            <tbody>
              {files.map((field, index) => {
                const { meta, xhr, file } = field;
                const { name, percent, status, id, size } = meta;
                TotoalPercentage[index] = percent;
                upoadFile = true;
                TotoalPercentageValue = (
                  TotoalPercentage.reduce((a, b) => a + b, 0) / lenght
                ).toFixed(2);
                let file_size = "";
                let response = "";

                file_size = parseFloat(size / 1024 / 1024).toFixed(4);
                let errorStatus = false;
                let errorMsg = "";
                console.log("status status = ", status);

                console.log("status file_size = ", file_size);
                console.log("status size = ", size);

                if (status === "done") {
                  console.log("status done ss= ", xhr);
                  response = JSON.parse(xhr.response);
                  console.log("status done = ", response);
                  if (response.status == "Error") {
                    fileStatus = "error_upload";
                    errorMsg = response.msg;
                  } else {
                    fileStatus = status;
                    successMsg = response.msg;

                    setTimeout(function () {
                      successMsg = "";
                    }, 4000);
                  }
                } else if (status === "error_upload") {
                  // errorMsgServer = JSON.parse(xhr);
                  fileStatus = status;
                  TotoalStatusSucess = false;
                  errorStatus = true;
                  console.log("status done xhdr= ", xhr);
                  console.log("status done xhr= ", xhr.responseText);
                  // console.log('status done xhr= ', errorMsgServer)
                  if (file_size > 2) {
                    errorMsg =
                      "File size is too large, please upload less than 2 MB";
                  } else {
                    errorMsg =
                      "We only accept CSV formatted file, please check your file. If you have further questions, please contact Bank Admin support. Thanks!";
                  }
                }

                console.log("fileStatus  ", fileStatus);

                return (
                  <Fragment key={index}>
                    <tr>
                      <td style={commanCss}>
                        <p>
                          <strong>{name}</strong>
                        </p>
                      </td>
                      <td style={commanCss}>{`${file_size} MB`}</td>
                      <td style={commanCss}>
                        {fileStatus == "error_upload" ? (
                          <Fragment>
                            <progress max={0} value={0} />
                            <span className="dzu-previewButton" />
                            &nbsp;&nbsp;
                            {"0%"}
                          </Fragment>
                        ) : (
                          <Fragment>
                            <progress max={100} value={Math.round(percent)} />
                            <span className="dzu-previewButton" />
                            &nbsp;&nbsp;
                            {percent == undefined
                              ? "0%"
                              : Math.round(percent) + "%"}
                          </Fragment>
                        )}
                      </td>
                      <td style={commanCss}>
                        <Fragment>
                          {fileStatus == "done" ? (
                            <Fragment> 
                              <CTooltip
                                content={`Successfully uploaded file `}
                                placement={"top-start"}
                              >
                                <CBadge color="success">Success</CBadge>
                              </CTooltip>
                            </Fragment>
                          ) : (
                            <Fragment>
                              <CTooltip
                                content={`${errorMsg}`}
                                placement={"top-start"}
                              >
                                <CBadge
                                  color={
                                    fileStatus == "uploading"
                                      ? "warning"
                                      : "danger"
                                  }
                                >
                                  {fileStatus == "uploading" ? (
                                    <div className="loadingDataSpan">
                                      Uploading
                                    </div>
                                  ) : (
                                    "Error"
                                  )}
                                </CBadge>
                              </CTooltip>
                            </Fragment>
                          )}
                        </Fragment>
                      </td>

                      <td>
                        <Fragment>
                          <CButton
                            type="submit"
                            size="mini"
                            color="danger"
                            className="remove_button btn-sm btn-pill "
                            onClick={(e) => submitButton(e, file, files, index)}
                          >
                            Remove
                          </CButton>

                          {fileStatus == "done" && (
                            <CButton
                              type="submit"
                              size="mini"
                              color="success"
                              className="remove_button btn-sm btn-pill"
                              onClick={(e) => handleProcessFile(e, file)}
                            >
                              Result
                            </CButton>
                          )}
                        </Fragment>
                      </td>
                    </tr>
                  </Fragment>
                );
              })}
            </tbody>
          </table>
        )}
      </CCardBody>
    </Fragment>
  );
};

export const UploadDocFile = (props) => {
  const {
    handleUploadNewDoc,
    downloadProcessedFile,
    proccessedFiles,
    successMessage,
    handleDeleteFile,
    deleted_msg,
    deleted_error_msg,
    hideProcessedFiles,
    errorMsg,
    isLoading,
    searchValue,
    handleFilterChange,
  } = props;

  const handleSubmitDownloadTemplate = () => {
    export_table_to_csv("End User Default Template.csv");
  };

  const export_table_to_csv = (filename) => {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    var header_row = [];
    header_row.push([
      "Download Template Date : " + new Date().toLocaleDateString(),
    ]);
    header_row.push(["End User Provision Default Template : "]);
    // header_row.push([''])
    header_row.push([
      "Title ( Mr. / Mrs ) ",
      "End Customer First Name",
      "End Customer Middle Name",
      "End Customer Last Name",
      "Regd Mobile",
      "Alternate Phone",
      "Current Status ( Registration / Verification )",
      "Preferred Langauge",
      "Loan Type",
      "Loan Acc No",
      "Branch Name",
      "Loan Amt",
      "Sales Agent",
      "Sale Agent Contact ",
      "Sales Team Leader",
      "Sales Team Leader Contact",
    ]);
    // header_row.push(['Time range : '])
    csv.push(header_row.join("\n"));
    // for (var i = 0; i < rows.length; i++) {
    //   var row = [],
    //     cols = rows[i].querySelectorAll("td, th");

    //   for (var j = 0; j < cols.length; j++) row.push(cols[j].innerText);

    //   csv.push(row.join(","));
    // }

    // Download CSV
    download_csv(csv.join("\n"), filename);
  };

  const download_csv = (csv, filename) => {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
  };

  const handleDeleteSubmit = (e, files, allFiles, index) => {
    e.preventDefault();

    allFiles[index].remove();
    TotoalPercentage.splice(index, 1);
    lenght = allFiles.length - 1;

    TotoalPercentageValue = (
      TotoalPercentage.reduce((a, b) => a + b, 0) / lenght
    ).toFixed(2);

    return true;
  };

  const handleProcessFileCustom = (e, uniqueId) => {
    e.preventDefault();
    handleUploadNewDoc(uniqueId);
  };

  // specify upload params and url for your files
  const getUploadParams = () => {
    return {
      url: `${URL_HOST}/api/document/upload`,
      headers: { Authorization: localStorage.getItem("access_token") },
    };
  };

  console.log("fileStatusfileStatusfileStatusfileStatus = ", fileStatus);
  console.log("successMsgsuccessMsg = ", successMsg);

  return (
    <Fragment>
      <CCardHeader>
        <b style={{ fontSize: "20px" }}>Upload File</b>
        <span>
          <div className="md-form mt-3">
            <span>
              <CButton
                type="submit"
                size="sm"
                color="primary"
                className="sample_doc button_style"
                onClick={() => handleSubmitDownloadTemplate()}
                style={button_css}
              >
                <DownloadSampleDoc /> Download Template
              </CButton>
            </span>
          </div>
        </span>
        <span>
          <styledComp>
            <Dropzone
              getUploadParams={getUploadParams}
              // onSubmit={handleSubmit}
              // accept="image/*,audio/*,video/*"
              inputContent="Drag & Drop your file or click to Browse"
              stype={drop_zone_css}
              LayoutComponent={(props) => (
                <Layout
                  {...props}
                  // onChangeStatus={handleChangeStatus}
                  handleProcessFile={handleProcessFileCustom}
                  submitButton={handleDeleteSubmit}
                  // handleFileUploadALL={handleFileUploadCustomALL}
                />
              )}
            />
          </styledComp>
        </span>

        {successMessage && (
          <CAlert color="success" className="msg_div">
            {successMessage}
          </CAlert>
        )}

        {deleted_msg && (
          <CAlert color="success" className="msg_div">
            {deleted_msg}
          </CAlert>
        )}
        {deleted_error_msg && (
          <CAlert color="danger" className="msg_div">
            {deleted_error_msg}
          </CAlert>
        )}

        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        {/* {(fileStatus=='done' && successMsg!='') && (
          <CAlert color="success" className="msg_div">
          {successMsg}
        </CAlert>
        )} */}

        {/* <span>
          <button
            type="button"
            className="btn btn-primary btn-block upload_doc"
            disabled={!upoadFile ? true :false}
          >
            Upload
          </button>
        </span> */}
        <span>
          {" "}
          <span>
            <b style={{ fontSize: "18px" }}>Processed Files :</b>
          </span>
          <span>
            <input
              className="form-control uploaded_search"
              type="text"
              placeholder="Search"
              aria-label="Search"
              value={searchValue}
              onChange={handleFilterChange}
            />
          </span>
        </span>
        <CCardBody>
          {isLoading && <ContentLoading content="Uploading..." />}
          <CDataTable
            items={proccessedFiles}
            fields={FIELDS}
            itemsPerPage={5}
            // loading={isLoading}

            pagination
            scopedSlots={{
              total_records: (item) => (
                <td>
                  <CTooltip
                    content={`Download all records (csv)`}
                    placement={"top-start"}
                  >
                    <a
                      href="#"
                      onClick={() => downloadProcessedFile(item.id, "total")}
                    >
                      {item.total_records}
                    </a>
                  </CTooltip>
                </td>
              ),
              success_records: (item) => (
                <td>
                  <CTooltip
                    content={`Download success records (csv)`}
                    placement={"top-start"}
                  >
                    <a
                      href="#"
                      onClick={() => downloadProcessedFile(item.id, "success")}
                    >
                      {item.success_records}
                    </a>
                  </CTooltip>
                </td>
              ),
              failed_records: (item) => (
                <td>
                  <CTooltip
                    content={`Download failed records (csv)`}
                    placement={"top-start"}
                  >
                    <a
                      href="#"
                      onClick={() => downloadProcessedFile(item.id, "failed")}
                    >
                      {item.failed_records}
                    </a>
                  </CTooltip>
                </td>
              ),

              Action: (item) => (
                <td>
                  <CButton
                    type="submit"
                    size="sm"
                    color="danger"
                    className="remove_button"
                    onClick={() => handleDeleteFile(item)}
                  >
                    <CTooltip content={`Delete Record`} placement={"top-start"}>
                      <i className="fa fa-trash" aria-hidden="true"></i>
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCardHeader>
    </Fragment>
  );
};
