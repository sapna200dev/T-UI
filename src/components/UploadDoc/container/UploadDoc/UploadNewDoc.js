import React, { Component, Fragment } from "react";
import "../../uploaddocStyle.css";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
  CCard,
  CDataTable,
} from "@coreui/react";
import { UploadDocFile } from "../../components/UploadDoc/index";
import {
  processFileDoc,
  getProcessedFiles,
  deleteProcessedFile,
} from "../../services/common";
import { URL_HOST } from "../../../Comman/constants";
import { getLoggedInUser } from "../../../Comman/functions";

class UploadNewDoc extends Component {
  state = {
    errorMsg: "",
    successMessage: "",
    successCount: [],
    failedCount: [],
    showModal: false,
    proccessedFiles: [],
    user_id: "",
    isDeleted: false,
    deletedId: "",
    deleted_error_msg: "",
    deleted_msg: "",
    isLoading: false,
    filterProccessedFiles: [],
    searchValue: "",
  };

  componentDidMount() {
    const userObj = getLoggedInUser("loggedInUser");
    let user = "";

    if (userObj) {
      user = JSON.parse(userObj);
      this.setState({ user_id: user.bank_id });
      this.getFiles(user.bank_id);
    }
  }

  handleUploadNewDoc = (file) => {
    this.setState({ isLoading: true });
    var formDataRequest = new FormData();
    formDataRequest.append("file", file);
    let _this = this;
    processFileDoc(formDataRequest)
      .then((response) => {
        console.log("response => ", response.data.msg);

        console.log("response asdfsddfhghjjhkjh=> ", response.data.msg);
        this.setState({
          errorMsg:
            response.data.msg && response.data.msg != ""
              ? response.data.msg
              : response.data.errormsg,
          successMessage: "",
          successCount: response.data.successArry,
          failedCount: response.data.failedArry,
          showModal: true,
          isLoading: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 10000);
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        console.log("file upload error => ", err.response);
      });
  };

  /**
   * download processed files
   */
  downloadProcessedFile = (file_id, type = "") => {
    const { user_id } = this.state;

    if (type == "total") {
      window.open(
        `${URL_HOST}/api/download/document/${file_id}/${user_id}`,
        "_blank"
      );
    } else if (type == "failed") {
      window.open(
        `${URL_HOST}/api/download/failed-document/${file_id}/${user_id}`,
        "_blank"
      );
    } else if (type == "success") {
      window.open(
        `${URL_HOST}/api/download/success-document/${file_id}/${user_id}`,
        "_blank"
      );
    }
  };

  /**
   * Handle cancel event and get all files
   */
  handleCancelModal = (delete_type = "") => {
    let _this = this;
    const { errorMsg } = this.state;

    if (delete_type == "processed_modal") {
      this.setState({ showModal: false });
      if (errorMsg != "") {
        // this.setState({ successMessage: "File Proccessed successfully" });
      }
      this.getFiles();

      this.hideProcessedFiles();
      setTimeout(function () {
        _this.setState({ successMessage: "" });
      }, 4000);
    } else if (delete_type == "delete_modal") {
      this.setState({ isDeleted: false, deletedId: "" });
      this.getFiles();
    } else if (delete_type == "error_modal") {
      this.setState({ showModal: false });
      this.getFiles();
    }
  };

  hideProcessedFiles = () => {};

  /**
   * Get all files from db
   */
  getFiles = () => {
    getProcessedFiles()
      .then((response) => {
        this.setState({
          proccessedFiles: response.data.data,
          filterProccessedFiles: response.data.data,
        });
        let _this = this;
      })
      .catch((err) => {
        this.setState({ errorMsg: err.response.data.msg });
      });
  };

  /**
   * Handle Delete file
   */
  handleDeleteFile = (item) => {
    this.setState({ isDeleted: true, deletedId: item });
  };

  handleDeleteConfirm = (id) => {
    deleteProcessedFile(id)
      .then((res) => {
        console.log("response delete = ", res);
        this.setState({
          deleted_msg: res.data.msg,
          deleted_error_msg: "",
          isDeleted: false,
        });
        let _this = this;
        setTimeout(function () {
          _this.setState({ deleted_msg: "" });
        }, 4000);

        this.getFiles();
      })
      .catch((err) => {
        console.log("erro => ", err.response);
        this.setState({
          deleted_error_msg: err.response.data.msg,
          deleted_msg: "",
        });
        let _this = this;
        setTimeout(function () {
          _this.setState({ deleted_error_msg: "" });
        }, 4000);
      });
  };
  /**
   * Handle set timeout msg
   */
  setTimeOutMsg = (type, seconds) => {
    let _this = this;
    setTimeout(function () {
      _this.setState({ type: "" });
    }, seconds);
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { proccessedFiles } = this.state;
    let arrayProcessFileLists = [];
    console.log("search value ddddd= ", data);
    if (data) {
      arrayProcessFileLists = this.handleFilterChangeVal(proccessedFiles, data);
    } else {
      arrayProcessFileLists = this.state.proccessedFiles;
    }

    this.setState({
      filterProccessedFiles: arrayProcessFileLists,
      searchValue: data,
    });
  };

  /**
   * Handle search event
   */
  handleFilterChangeVal = (processFileLists, value) => {
    let filesList = [];
    filesList = processFileLists.filter((item) => {
      return (
        (item.file_name &&
          item.file_name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.uploaded_date_time &&
          item.uploaded_date_time.toLowerCase().indexOf(value.toLowerCase()) !==
            -1)
      );
    });

    return filesList;
  };

  render() {
    const {
      errorMsg,
      successMessage,
      successCount,
      failedCount,
      showModal,
      proccessedFiles,
      deletedId,
      isDeleted,
      deleted_msg,
      deleted_error_msg,
      isLoading,
      searchValue,
      filterProccessedFiles,
    } = this.state;

    const data = [
      {
        id: 0,
        status: failedCount && failedCount.length == 0 ? "TRUE" : "False",
        success_count:
          successCount && successCount.length > 0 ? successCount.length : 0,
        failed_count:
          failedCount && failedCount.length > 0 ? failedCount.length : 0,
        total:
          (successCount && successCount.length > 0) ||
          (failedCount && failedCount.length > 0)
            ? successCount.length + failedCount.length
            : 0,
      },
    ];
    const fields = ["status", "success_count", "failed_count", "total"];

    return (
      <Fragment>
        <CCard>
          <UploadDocFile
            errorMsg={errorMsg}
            successMessage={successMessage}
            successCount={successCount}
            failedCount={failedCount}
            handleUploadNewDoc={this.handleUploadNewDoc}
            downloadProcessedFile={this.downloadProcessedFile}
            proccessedFiles={filterProccessedFiles}
            handleDeleteFile={this.handleDeleteFile}
            deleted_msg={deleted_msg}
            deleted_error_msg={deleted_error_msg}
            hideProcessedFiles={this.hideProcessedFiles}
            handleFilterChange={this.handleFilterChange}
            searchValue={searchValue}
          />
        </CCard>

        {showModal && (
          <CModal
            show={showModal}
            onClick={() =>
              errorMsg != ""
                ? this.handleCancelModal("error_modal")
                : this.handleCancelModal("processed_modal")
            }
          >
            <CModalHeader closeButton>
              <CModalTitle>Processed Files Status</CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CDataTable items={data} fields={fields} />
            </CModalBody>
            <CModalFooter>
              <button
                type="button"
                className={`btn btn-primary btn-block`}
                onClick={() => this.handleCancelModal("processed_modal")}
              >
                Ok
              </button>
            </CModalFooter>
          </CModal>
        )}

        {isDeleted && (
          <CModal
            show={isDeleted}
            onClose={() => this.handleCancelModal("delete_modal")}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this record <b>{deletedId.file_name}</b>
            </CModalBody>
            <CModalFooter>
              <CButton
                color="danger"
                onClick={() => this.handleDeleteConfirm(deletedId.id)}
              >
                Delete
              </CButton>{" "}
              <CButton
                color="secondary"
                onClick={() => this.handleCancelModal("delete_modal")}
              >
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}
      </Fragment>
    );
  }
}

export default UploadNewDoc;
