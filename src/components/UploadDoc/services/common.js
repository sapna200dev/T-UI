import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'


export const processFileDoc = (file) =>
axios.post(`${URL_HOST}/api/document/process`, file);

export const getProcessedFiles = () =>
axios.get(`${URL_HOST}/api/document/get-processed-files`, );


export const deleteProcessedFile = (id) =>
axios.get(`${URL_HOST}/api/document/delete-processed-file/${id}`, );




export const downloadFileFromDB = () => {
    axios.post(`${URL_HOST}/api/download/document`);
}
