import Profile from './Profile'
import  ProfilePage1 from './ProfilePage1'
import ProfilePage2 from './Profile2'

import MainProfilePage from './MainProfilePage'

export {
    Profile,
    ProfilePage1,
    ProfilePage2,

    MainProfilePage

}
