import React, { Fragment, useEffect, useState } from 'react'
import {
  CCol,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CAlert,
  CTooltip,
  CTabs,
  CButton,
  CInvalidFeedback,
  CInput
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import {
  ContentLoading,
  InvertContentLoader,
} from "../../../Comman/components";
import { URL_HOST } from "../../../Comman/constants";
import { getLoggedInUser, getUserObj } from "../../../Comman/functions";
import { Settings } from '../../../Comman/settings/Settings';

const img_profile = {
  width: "65%",
  height: "119px",
  borderRadius: " 8px",
};

const showPass = {
  marginLeft: "87%",
  zIndex: "10",
  /* margin-right: 7px; */
  cursor: "pointer",
  position: "initial",
  marginTop: "-24px",
  display: "block",
};


export const ProfilePage = (props) => {
  const {
    username,
    email,
    password,
    confirm_password,
    image,
    handleInputs,
    confirmPasswordError,
    usernameError,
    passwordError,
    emailError,
    handleUpdateProfile,
    loading,
    previewImage,
    blob,
    handleChange,
    resetMsg,
    errorMsg,
    is_change,
    role_name,
    role,
    showPW,
    handleShowPass,
    handleShowConfirmPass,
    showConfirmPW,
    clearUploadedImg,
    handleShowEdit,
    isEditable
  } = props;
  const [profile, setProfile] = useState(null);
  const [userObj, setUserObj] = useState({});
  const [userRole, setUserRole] = useState({});
  // let user_obj={};


  useEffect(() => {
    // Fetch user details
    const user_obj = getLoggedInUser();
    const user = JSON.parse(user_obj);
    let role = "";
    // Fetch user role details
    const user_role = getUserObj("role");

    if (user_role) {
      role = JSON.parse(user_role);
    }

    setProfile(user.profile);
    setUserObj(user);
    setUserRole(role);
  }, []);


  const  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

console.log('login user role = ')
console.log(userRole)
  return (
    <Fragment>


      <div className="">
        <div className="main-body">
          {/* Breadcrumb */}
          <nav aria-label="breadcrumb" className="main-breadcrumb">
          {resetMsg && resetMsg != "" && (
            <CAlert color="success" className="msg_div">
              {resetMsg}
            </CAlert>
          )}

          {errorMsg && (
            <CAlert color="danger" className="msg_div">
              {errorMsg}
            </CAlert>
          )}
          </nav>
          {/* /Breadcrumb */}
          <div className="row gutters-sm">
            <div className="col-md-4 mb-3">
              <div className="card">
                <div className="card-body">
                  <div className="d-flex flex-column align-items-center text-center">
                    {!previewImage && <img src={`${URL_HOST}${profile}`} alt="Admin" className="rounded-circle" width={150} />}
                    {previewImage && (
                      <div>
                        <img
                          src={previewImage}
                          className="rounded-circle"
                          style={img_profile}
                          width={150}
                        />
                        <span>
                          <button type="button" class="close" aria-label="Close" onClick={clearUploadedImg}>
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </span>
                      </div>
                    )}
                    <div className="mt-3">
                      <h4>{userObj.name}</h4>
                      <p className="text-secondary mb-1"><b>( 
                        {/* {typeof userObj.bank_name !== 'undefined' ? userObj.bank_name : 'Turant'} */}
                        {userObj.bank_name ? capitalizeFirstLetter(userObj.bank_name.toLowerCase()) : 'Turant'} {userRole.display_name} 
                         )</b></p>
                      <p className="text-muted font-size-sm"><b>Role -</b> 
                       {typeof userRole.display_name !== 'undefined' ? userRole.display_name  : 'Turant'}
                       
                       </p>
                      <div>
                        <input type="file" id="file" name="file-input" className="user_profile"
                          onChange={handleChange} />
                        {/* <CInputFile
                          id="file-input"
                          name="file-input"
                          onChange={handleChange}
                          type="file"
                        //style={{opacity:'0'}}
                        /> */}
                        <label for="file" >Upload</label>
                      </div>

                      {/* <button className="btn btn-primary">Follow</button>
                      <button className="btn btn-outline-primary">Message</button> */}
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="card mt-3">
                <ul className="list-group list-group-flush">
                  <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 className="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-globe mr-2 icon-inline"><circle cx={12} cy={12} r={10} /><line x1={2} y1={12} x2={22} y2={12} /><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z" /></svg>Website</h6>
                    <span className="text-secondary">https://bootdey.com</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 className="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-github mr-2 icon-inline"><path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22" /></svg>Github</h6>
                    <span className="text-secondary">bootdey</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 className="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-twitter mr-2 icon-inline text-info"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z" /></svg>Twitter</h6>
                    <span className="text-secondary">@bootdey</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 className="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-instagram mr-2 icon-inline text-danger"><rect x={2} y={2} width={20} height={20} rx={5} ry={5} /><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z" /><line x1="17.5" y1="6.5" x2="17.51" y2="6.5" /></svg>Instagram</h6>
                    <span className="text-secondary">bootdey</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 className="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-facebook mr-2 icon-inline text-primary"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z" /></svg>Facebook</h6>
                    <span className="text-secondary">bootdey</span>
                  </li>
                </ul>
              </div> */}
            </div>
            <div className="col-md-8">

              <CTabs>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink>
                      Profile
                  </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>
                      Layout
                  </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    {/* <CNavLink>
                      Profile 3
                  </CNavLink> */}
                  </CNavItem>
                </CNav>
                <CTabContent>
                  <CTabPane>
                    <div className="card mb-3">
                      <div >
                        <CButton
                          type="submit"
                          size="sm"
                          color="primary"
                          onClick={() => handleShowEdit(isEditable)}
                          // onClick={handleUpdateProfile}
                          // disabled={!is_change ? true : false}
                          className="editButton"
                        >
                          <CIcon name="cil-scrubber" /> {isEditable ? 'Close' : 'Edit'}{" "}
                          {loading && <InvertContentLoader />}
                        </CButton>
                      </div>
                      <div className="card-body">

                        <div className="row">
                          <div className="col-sm-3">
                            <h6 className="mb-0"><b>Username</b> <small className="required_lable">*</small></h6>
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <CInput
                                id="username"
                                name="username"
                                value={username}
                                placeholder="Username..."
                                onChange={handleInputs}
                                disabled
                              />
                            ) : (
                              username
                            )}


                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div className="col-sm-3">
                            <h6 className="mb-0"><b>Email</b> <small className="required_lable">*</small></h6>
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <Fragment>
                                <CInput
                                  type="email"
                                  id="email"
                                  name="email"
                                  value={email}
                                  placeholder="Enter Email"
                                  onChange={handleInputs}
                                  autoComplete="email"
                                  invalid={emailError ? true : false}
                                />
                                <CInvalidFeedback>{emailError}</CInvalidFeedback>
                              </Fragment>
                            ) : (
                              email
                            )}


                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div className="col-sm-3">
                            <h6 className="mb-0"><b>Role name</b> <small className="required_lable">*</small></h6>
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <Fragment>
                                <CInput
                                  type="role_name"
                                  id="role_name"
                                  name="role_name"
                                  value={role_name}
                                  placeholder="Role Name"
                                  onChange={handleInputs}
                                  autoComplete="role_name"
                                  disabled
                                />
                              </Fragment>
                            ) : (
                              role_name
                            )}
                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div className="col-sm-3">
                          <h6 className="mb-0"><b>Mobile No </b> <small className="required_lable">*</small></h6>
                          
                            {/* <h6 className="mb-0"><b>Level sss - Company Owner</b> <small className="required_lable">*</small></h6> */}
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <Fragment>
                                <CInput
                                  type="role_name"
                                  id="role_name"
                                  name="role_name"
                                  value={userObj.mobile_no}
                                  placeholder="Role Name"
                                  onChange={handleInputs}
                                  autoComplete="role_name"
                                  disabled
                                />
                              </Fragment>
                            ) : (
                              userObj.mobile_no
                            )}

                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div className="col-sm-3">
                          <h6 className="mb-0"><b>City</b> <small className="required_lable">*</small></h6>
                          
                            {/* <h6 className="mb-0"><b>Level sss - Company Owner</b> <small className="required_lable">*</small></h6> */}
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <Fragment>
                                <CInput
                                  type="role_name"
                                  id="role_name"
                                  name="role_name"
                                  value={userObj.city}
                                  placeholder="Role Name"
                                  onChange={handleInputs}
                                  autoComplete="role_name"
                                  disabled
                                />
                              </Fragment>
                            ) : (
                              userObj.city
                            )}

                          </div>
                        </div>

                        <hr />
                        <div className="row">
                          <div className="col-sm-3">
                          <h6 className="mb-0"><b>State</b> <small className="required_lable">*</small></h6>
                          
                            {/* <h6 className="mb-0"><b>Level sss - Company Owner</b> <small className="required_lable">*</small></h6> */}
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <Fragment>
                                <CInput
                                  type="role_name"
                                  id="role_name"
                                  name="role_name"
                                  value={userObj.state}
                                  placeholder="Role Name"
                                  onChange={handleInputs}
                                  autoComplete="role_name"
                                  disabled
                                />
                              </Fragment>
                            ) : (
                              userObj.state
                            )}

                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div className="col-sm-3">
                          <h6 className="mb-0"><b>Country</b> <small className="required_lable">*</small></h6>
                          
                            {/* <h6 className="mb-0"><b>Level sss - Company Owner</b> <small className="required_lable">*</small></h6> */}
                          </div>
                          <div className="col-sm-9 ">
                            {isEditable ? (
                              <Fragment>
                                <CInput
                                  type="role_name"
                                  id="role_name"
                                  name="role_name"
                                  value={userObj.country}
                                  placeholder="Role Name"
                                  onChange={handleInputs}
                                  autoComplete="role_name"
                                  disabled
                                />
                              </Fragment>
                            ) : (
                              userObj.country
                            )}

                          </div>
                        </div>
                        {isEditable ? (
                          <hr />
                        ) : null}
                        {isEditable ?
                          <div className="row">
                            <div className="col-sm-3">
                              <h6 className="mb-0"><b>Password</b> <small className="required_lable">*</small></h6>
                            </div>
                            <div className="col-sm-9 ">
                              <CInput
                                type={showPW ? "text" : "password"}
                                id="password"
                                name="password"
                                value={password}
                                placeholder="Password"
                                onChange={handleInputs}
                                autoComplete="new-password"
                                invalid={passwordError ? true : false}
                              />
                              {password && (
                                <Fragment>
                                  {showPW ? (
                                    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                                    <CTooltip content={`Hide Password`} placement={"top-start"}>
                                      <i
                                        className="fa fa-eye-slash"
                                        style={showPass}
                                        onClick={() => handleShowPass(false)}
                                      />
                                    </CTooltip>
                                  ) : (
                                    <CTooltip content={`Show Password`} placement={"top-start"}>
                                      <i
                                        className="fa fa-eye"
                                        style={showPass}
                                        onClick={() => handleShowPass(true)}
                                      />
                                    </CTooltip>
                                  )}
                                </Fragment>
                              )}

                              <CInvalidFeedback style={{ marginTop: "10px" }}>
                                {passwordError}
                              </CInvalidFeedback>

                            </div>
                          </div> : null}
                        {isEditable ? (
                          <hr />
                        ) : null}
                        {isEditable ?
                          <div className="row">
                            <div className="col-sm-3">
                              <h6 className="mb-0"><b> Confirm Password</b> <small className="required_lable">*</small></h6>
                            </div>
                            <div className="col-sm-9 ">
                              <CInput
                                type={showConfirmPW ? "text" : "password"}
                                id="confirm_password"
                                name="confirm_password"
                                value={confirm_password}
                                onChange={handleInputs}
                                placeholder="Confirm Password"
                                autoComplete="password"
                                invalid={confirmPasswordError ? true : false}
                              />
                              {confirm_password && (
                                <Fragment>
                                  {showConfirmPW ? (
                                    <CTooltip content={`Hide Password`} placement={"top-start"}>
                                      <i
                                        className="fa fa-eye-slash"
                                        style={showPass}
                                        onClick={() => handleShowConfirmPass(false)}
                                      />
                                    </CTooltip>
                                  ) : (
                                    <CTooltip content={`Show Password`} placement={"top-start"}>
                                      <i
                                        className="fa fa-eye"
                                        style={showPass}
                                        onClick={() => handleShowConfirmPass(true)}
                                      />
                                    </CTooltip>
                                  )}
                                </Fragment>
                              )}

                              <CInvalidFeedback style={{ marginTop: "10px" }}>
                                {confirmPasswordError}
                              </CInvalidFeedback>

                            </div>
                          </div> : null}
                      </div>
                    </div>
                    {isEditable && (
                        <div style={{float:'right'}}>
                          <CButton
                            type="submit"
                            size="sm"
                            color="primary"
                            onClick={handleUpdateProfile}
                            disabled={(!is_change) ? true : false}
                          >
                            <CIcon name="cil-scrubber" /> Update{" "}
                            {loading && <InvertContentLoader />}
                          </CButton>
                          <a href="#/dashboard" className="cancel_update">
                            <CButton
                              type="submit"
                              size="sm"
                              color="danger"
                              style={{ marginLeft: "10px" }}
                            >
                              <CIcon name="cil-scrubber" /> Cancel
                            </CButton>
                          </a>
                        </div>
                    )}
                  </CTabPane>
                  <CTabPane>
                    <Settings isFromProfile={true}/>
                  </CTabPane>
                  <CTabPane>

                  </CTabPane>
                </CTabContent>
              </CTabs>


              {/* <div className="row gutters-sm">
                <div className="col-sm-6 mb-3">
                  <div className="card h-100">
                    <div className="card-body">
                      <h6 className="d-flex align-items-center mb-3"><i className="material-icons text-info mr-2">assignment</i>Project Status</h6>
                      <small>Web Design</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '80%'}} aria-valuenow={80} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>Website Markup</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '72%'}} aria-valuenow={72} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>One Page</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '89%'}} aria-valuenow={89} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>Mobile Template</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '55%'}} aria-valuenow={55} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>Backend API</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '66%'}} aria-valuenow={66} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 mb-3">
                  <div className="card h-100">
                    <div className="card-body">
                      <h6 className="d-flex align-items-center mb-3"><i className="material-icons text-info mr-2">assignment</i>Project Status</h6>
                      <small>Web Design</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '80%'}} aria-valuenow={80} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>Website Markup</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '72%'}} aria-valuenow={72} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>One Page</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '89%'}} aria-valuenow={89} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>Mobile Template</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '55%'}} aria-valuenow={55} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <small>Backend API</small>
                      <div className="progress mb-3" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '66%'}} aria-valuenow={66} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>

    </Fragment>

  )
}