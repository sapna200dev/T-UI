import { take, put, call, all } from 'redux-saga/effects';
import * as actions from '../../actions/EndUser';
import axios from '../../../Comman/axiosConfig';
import { URL_HOST } from '../../../Comman/constants'



export const fetchLanguagesSaga = async () => {
    try {
      const response = await axios.get(`${URL_HOST}/api/languages/list`);

      return response.data;
    } catch (err) {
      return err.response.data;
    }
  };
  

  export function* fetchLanguages() {
    try {
      const response = yield fetchLanguagesSaga();
  
      if (response && response.status == 200) {

        yield all([
          put({
            type: actions.FETCH_LANGUAGES_SUCCESS, 
            response,
          })
       
        ]);
      } else {
  
        yield all([
          put({
            type: actions.FETCH_LANGUAGES_FAILURE,
            response,

          })
        ]);
      }
    } catch (err) {
      yield all([
        put({ type: actions.FETCH_LANGUAGES_FAILURE, err })
       
      ]);
    }
  }



  export function* watchFetchLanguagesSaga() {
    while (true) {
      const state = yield take(actions.FETCH_LANGUAGES);
      yield call(fetchLanguages);
    }
  }
  