import { take, put, call, all } from 'redux-saga/effects';
import * as actions from '../../actions/EndUser';
import axios from '../../../Comman/axiosConfig';
import { URL_HOST } from '../../../Comman/constants'


export const fetchTeamLeadsSaga = async () => {
    try {
      const response = await axios.get(`${URL_HOST}/api/common/team_leads`);

      return response.data;
    } catch (err) {
      return err.response.data;
    }
  };
  

  export function* fetchTeamLeads() {
    try {
      const response = yield fetchTeamLeadsSaga();
      if (response && response.status == 200) {

        yield all([
          put({
            type: actions.FETCH_TEAM_LEADS_SUCCESS, 
            response,
          })
       
        ]);
      } else {
  
        yield all([
          put({
            type: actions.FETCH_TEAM_LEADS_FAILURE,
            response,

          })
        ]);
      }
    } catch (err) {
      yield all([
        put({ type: actions.FETCH_TEAM_LEADS_FAILURE, err })
       
      ]);
    }
  }



  export function* watchFetchTeamLeadsSaga() {
    while (true) {
      const state = yield take(actions.FETCH_TEAM_LEADS);
      yield call(fetchTeamLeads);
    }
  }
  