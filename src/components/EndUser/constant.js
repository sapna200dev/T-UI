export const END_USER_STATE = {
  first_name: "",
  last_name: "",
  registered_mobile: "",
  loan_account_number: "",
  loan_amount: "",
  alternate_phone: "1234567896",
  preferred_langauge: "",
  loan_type: "",
  branch_id: "",
  sale_agent_id: "",
  team_leader: "",
  sales_agent_contact: "",
  team_leader_contact: "",
  call_portal: '1',
  call_status: "",
  current_status: "",
  call_action: "",
  verfications: [],
  registeration_trigger: "",
  verification_trigger: "",
  addEndUserErrors: {},
  errorMsg: "",
  successMsg: "",
  id: "",
  uploaded_by: "",
  isFatching: false,
  sales_agent: '',
  team_leader: '',
  created_at: '',
  user_logs: [],
  reason_for_change: '',
  isUpdateData: false,
  is_change: false,
  isCheckReason: false,
  isChangeAnyData: false,
  updatedArray: [],
  field_name: '',
  field_value: '',
  old_field_value: '',
  is_select: false,
  updateEndUserErrors: {},
  branch_name: '',
  middle_name: '',
  call_initiating: false,
  call_msg: '',
  isError: false,
  call_details: [],
  call_text: '',
  refreshing_call_info: false,
  call_failed_count: 0,
  fatchingLogs: false,
  fatchingNumbers: false,
  fatchingTLNumbers: false,
  is_registration: '',
  isSuccess: false,
  is_verification: '',
  playAudio1: false,
  playAudio2: false,
  playAudio3: false,
  playAudioFirst: '',
  playAudioSecond: '',
  playAudioThird: '',
  firstFileName: '',
  secondFileName: '',
  thirdFileName: '',
  firstFileExtension: '',
  secondFileExtension: '',
  thirdFileExtension: '',
  isResetLoading: false,
  isReset: false,
  showResetModal: false,
  isDisabledButton: false,
  callReference: '',
  call_status_code: 0,
  callHandling: false,
  display_msg_header: false,
  sales_agent_name: '',
  team_leader_name: '',
  loan_tenure:'',
  loan_emi:'',
  call_score:'',
  registration_status:'',
  verification_status:'',
};

export const END_USER_FORM_VALIDATIONS = {
  FIRST_NAME: "First name is mandatory",
  MIDDLE_NAME: 'Middle name id mandatory',
  VALID_CHARS: "Must be valid chars",
  LAST_NAME: "Last name is mandatory",
  VALID_MOBILE: "Please enter valid mobile number",
  VALID_FIELD: 'This field is mandatory and Please only enter numeric characters',
  REGISTERED_MOBILE: "This field is mandatory and Please only enter numeric characters",
  REGISTERED_MOBILE_DIGITS: "Please enter only 10 digits number",
  LOAN_ACCOUNT_NUMBER: "Loan account number is mandatory",
  LOAN_AMOUNT: "Field is mandatory",
  LOAN_AMOUNT_VALID: "Enter valid loan amount",
  LOAN_EMI_VALID: "Enter valid loan amount",
  LOAN_TENURE_VALID: "Enter valid loan amount",
  ASSIGN_SALE_AGENT: "Sale agent is mandatory",
  ASSIGN_TEMA_LEADER: "Team leader is mandatory",
  REGISTERED_TRIGGER: "Registeration trigger is mandatory",
  VERIFICATION_TRIGGER: "Verification trigger is mandatory",
  VALID_CHARS_LENGTH: "Name must be 2 to 20 chars",
  REASON_CHANGE: 'Reason is mandatory',
  LANGUAGE: 'Language is mandatory',
  LOAN_TYPE_ID: 'Loan Type is mandatory',
  BRANCH_NAME: 'Branch Name is mandatory',
};

export const FIELDS = [
  "name",
  "loan_amount",
  "registered_mobile",
  "current_state",
  "status",
  "preferred_language",
  "Action",
];


export const REASONS = [
  { id: 0, reason: 'To Correct Spelling Mistake' },
  { id: 1, reason: 'Change Langauge' },
  { id: 2, reason: 'Correct Alternate number ' },
  { id: 3, reason: 'Correct Loan Type' },
  { id: 4, reason: 'Change Sales Agent' },
  { id: 5, reason: 'Change Team Leader' },
];

export const call_portals = [
  { id: 0, text: 'Telnyx', value: 0 },
  { id: 1, text: 'Asterisk', value: 1 }
];




export const CALL_DETAILS_FEILDS = [
  "registration_1_call_date",
  "registration_1_call_duration",
  "registration_1_call_status",
  "registration_1_call_to",
  "registration_2_call_date",
  "registration_2_call_duration",
  "registration_2_call_status",
  "registration_2_call_to",
  "registration_3_call_date",
  "registration_3_call_status",
  "registration_3_call_to",
  "verification_1_call_date",
  "verification_1_call_duration",
  "verification_1_call_status",
  "verification_1_call_to",
  "verification_2_call_date",
  "verification_2_call_duration",
  "verification_2_call_status",
  "verification_2_call_to",
  "verification_3_call_date",
  "verification_3_call_duration",
  "verification_3_call_status",
  "verification_3_call_to",
  "uploaded_by",
  "created_at",
  "updated_at",
];


export const LOG_FIELDS = [
  "date_time",
  "field_changed",
  "old_data",
  "new_data",
  "reason_for_change",
  "changed_by",
  "level_role",
];



export const COULUMN_NAMES = [
  { id: 0, text: 'Name', value: 'name' },
  { id: 1, text: 'Loan Amount', value: 'loan_amount' },
  { id: 2, text: 'Registered Mobile', value: 'registered_mobile' },
  { id: 3, text: 'Current State', value: 'current_status' },
  { id: 4, text: 'Status', value: 'status' },
  { id: 5, text: 'Language', value: 'language_type' },

];


export const PAGINATION_COUNT = [
  { id: 0, text: '5', value: '5' },
  { id: 1, text: '10', value: '10' },
  { id: 2, text: '15', value: '15' },
  { id: 3, text: '20', value: '20' },
];

export const config = {
  page_size: 10,
  length_menu: [10, 20, 50],
  button: {
      excel: true,
      print: true,
      csv: true
  },
  show_filter:true,
  filename:'turant_endusers',
  language: {
      loading_text: "Please be patient while data loads..."
  }
  // pagination:'advance'
};




