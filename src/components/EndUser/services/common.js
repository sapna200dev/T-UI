import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'



/**
 * Fetch user mobile number
 * @param id 
 */
 export const fetchUserMobileNumber = (email) =>
 axios.post(`${URL_HOST}/api/end_user/get-number`, {
     email
 });

 


/**
 * Add new user (customer)
 * @param data 
 */
export const addNewEndUser = (data) =>
axios.post(`${URL_HOST}/api/end_user/add`, data);

/**
 * Get user details
 * @param id 
 */
export const getEndUserDetails = (id) =>
axios.get(`${URL_HOST}/api/end_user/${id}`);


/**
 * Get user log details
 * @param id 
 */
export const getEndUserLogData = (id) =>
axios.get(`${URL_HOST}/api/end_user/logs/${id}`);

/**
 * Get user log info
 * @param id 
 */
export const getEndUserInfo = (id) =>
axios.get(`${URL_HOST}/api/end_user/details/${id}`);

/**
 * Reset user call history
 * @param id 
 */
export const resetCallHistory = (id) =>
axios.get(`${URL_HOST}/api/end_user/reset-all-call-history/${id}`);

/**
 * Update user data
 * @param id 
 */
export const updateEndUserDetails = (id, data) =>
    axios.post(`${URL_HOST}/api/end_user/edit/${id}`, data);

/**
 * Delete user 
 * @param id 
 */
export const deleteEndUser = (id) =>
    axios.get(`${URL_HOST}/api/end_user/delete/${id}`);
   
/**
 * Trigger agent call api
 * @param id 
 */
export const handleAgentCallApi = (id, data) =>
    axios.post(`${URL_HOST}/api/call/register/${id}`, data );

/**
 * Trigger verify call api
 * @param id 
 */
export const handleVerifyCallApi = (id, data) =>
    axios.post(`${URL_HOST}/api/call/verify/register-call/${id}`, data);


/**
 * Fetch user mobile number
 * @param id 
 */
export const fetchMobileNumber = (id) =>
    axios.get(`${URL_HOST}/api/end_user/get-mobile-number/${id}`);

    


/**
 * Get user log info
 * @param id 
 */
 export const getUserCallDetails = (id) =>
 axios.get(`${URL_HOST}/api/end_user/get-call-history/${id}`);
    