import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from "@coreui/react";
import EndUserLists from "../../components/EndUser/EndUserLists";
import { fetchEndUser } from "../../actions/EndUser/index";
import { deleteEndUser } from "../../services/common";
import { InvertContentLoader } from "../../../Comman/components";
import { getUserObj } from "../../../Comman/functions";

let role = "";
class EndUsers extends Component {
  state = {
    endusers: [],
    deleteItem: "",
    isDelete: false,
    searchValue: "",
    filterEndUsers: [],
    isDeleteLoading:false,
    sort_by:'name',
    pagination_count:10
  };

  componentDidMount() {
    const { fetchEndUser } = this.props;

    const user_role = getUserObj("role");
 
    if (user_role) {
      role = JSON.parse(user_role);
    }

   let login_type = ( role.name == 'sales_agent') ? 'sales_agent' : ( role.name == 'team_lead') ? 'team_lead' :'admin';

    console.log('call api for sale agent ', login_type)
    fetchEndUser(login_type);
  }

  componentDidUpdate(prevProps) {
    const { sort_by } = this.state;

    const { endUsers } = this.props;
    if (prevProps.endUsers != endUsers) {
      let sorted_data = endUsers;

      sorted_data.sort(this.dynamicSort(sort_by));

      this.setState({
        endUsers,
        filterEndUsers: endUsers,
      });
    }
  }

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    this.setState({ isDeleteLoading:true })
    const { deleteItem } = this.state;

    deleteEndUser(deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
            isDeleteLoading:false
          });

          let login_type = ( role.name == 'sales_agent') ? true : false;

          console.log('call api for sale login_type ', login_type)
     

          this.props.fetchEndUser(login_type);
          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
            isDeleteLoading:false
          });
        }

        this.getAllEmployess();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isDeleteLoading:false
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { endUsers } = this.props;

    let arrayEmployeeLists = [];
    if (data) {
      arrayEmployeeLists = this.handleFilterChangeVal(endUsers, data);
    } else {
      arrayEmployeeLists = this.props.endUsers;
    }

    this.setState({
      filterEndUsers: arrayEmployeeLists,
      searchValue: data,
    });
  };

  handleFilterChangeVal = (endusers, value) => {
    let employeeList = [];
    employeeList = endusers.filter((item) => {
      return (
        (item.name &&
          item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.loan_amount &&
          item.loan_amount.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.registered_mobile &&
          item.registered_mobile.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.alternate_phone &&
          item.alternate_phone.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.language_type &&
          item.language_type.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return employeeList;
  };


      /**
   * Handle Bank inputs
   */
    handleInputValues = (e, result, type) => {
        const {filterEndUsers} = this.state;
        console.log('sort by na,type  = ')
        console.log(type)

       if(type == 'pagination') {
        const { name, value } = e.target;

        console.log('sort by na,result  = ')
        console.log(name)
        console.log(value)

        this.setState({ [name] : value });
       } else if(type == 'sort') {
          
       const { name, value } = e.target;
       console.log('sort by na,e = ')
       console.log(name)
       console.log(value)

          filterEndUsers.sort(this.dynamicSort(value));
  
          console.log('sort byproperty  filterEndUsers = ')
          console.log(filterEndUsers)
    
          this.setState({ filterEndUsers, sort_by:value });
       }

    };
 
       /**
        * Sort list
        * @param  property 
        * @returns 
        */
     dynamicSort = (property) =>  {
           var sortOrder = 1;
           if(property[0] === "-") {
               sortOrder = -1;
               property = property.substr(1);
           }
           return function (a,b) {
               /* next line works with strings and numbers, 
               * and you may want to customize it to your needs
               */
               var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
               return result * sortOrder;
           }
      }




  render() {
    const { endUsers, isLoading } = this.props;
    const {
      isDelete,
      deleteItem,
      successMsg,
      errorMsg,
      searchValue,
      filterEndUsers,
      isDeleteLoading,
      sort_by,
      pagination_count
    } = this.state;

    return (
      <Fragment>
        <EndUserLists
          endUsers={filterEndUsers}
          isLoading={isLoading}
          handleDeleteItem={this.handleDeleteItem}
          successMsg={successMsg}
          errorMsg={errorMsg}
          searchValue={searchValue}
          handleFilterChange={this.handleFilterChange}
          sort_by={sort_by}
          handleInputValues={this.handleInputValues}
          pagination_count={pagination_count}
        />

        {isDelete && (
          <CModal
            show={isDelete}
            onClose={this.handleCancelModal}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this End User <b>{deleteItem.name}</b>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={this.handleDeleteConfirm}>
                Delete {isDeleteLoading && (
                  <InvertContentLoader />
                )}
              </CButton>{" "}
              <CButton color="secondary" onClick={this.handleCancelModal}>
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}
      </Fragment>
    );
  }
}

export function mapStateToProps(state) {
  return {
    endUsers: state.fetchEndUserReducer.endUsers,
    isLoading: state.fetchEndUserReducer.isLoading,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchEndUser: (login_type) => dispatch(fetchEndUser(login_type)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EndUsers);

// export default EndUsers
