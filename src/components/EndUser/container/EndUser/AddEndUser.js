import React, { Component, Fragment } from "react";
import { connect } from "react-redux"; 
import AddEndUserModal from "../../components/EndUser/AddEndUserModal";
import {
  fetchLanguages,
  fetchLoanTypes,
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../actions/EndUser/index";
import { getBranches } from "../../../Comman/actions";
import { addNewEndUser, fetchMobileNumber, fetchUserMobileNumber } from "../../services/common";
import { END_USER_STATE, END_USER_FORM_VALIDATIONS } from "../../constant";
import { validateRequestParams, CreateReuqestPayload } from "../../functions";
import { getUserObj, getLoggedInUser } from "../../../Comman/functions";


let user='';
let role='';
let sale_user_id = ''

class AddEndUser extends Component {
  state = END_USER_STATE; 
  
  componentDidMount() {
 
    const {
      fetchLanguages,
      fetchLoanTypes,
      fetchSaleAgents,
      fetchTeamLeads,
      getBranches,
    } = this.props;

    fetchLanguages();
    fetchLoanTypes(); 
    fetchSaleAgents();
    fetchTeamLeads();
    getBranches(); 

    // Retrivr data from stroage
    const user_role = getUserObj("role");
    const login_user = getLoggedInUser("user");
   
    if (login_user) {
      user = JSON.parse(login_user);
    } // End if
  
    if (user_role) {
      role = JSON.parse(user_role);
    } // End if

    if(role.name == 'sales_agent') {
      // Fetch sale agent user number from DB  
      this.fetchUserEmail(user.email, true)
    } else if(role.name == 'team_lead') {
       // Fetch team leader user number from DB  
      this.fetchUserEmail(user.email, false)
    } else if(role.name == 'admin') {
      // Fetch team leader user number from DB  
     this.fetchUserEmail(user.email, false)
   }








  }


/**
 * Fetch User number from DB
 * @param  email 
 * @param  isSaleAgent 
 */
  fetchUserEmail = (email, isSaleAgent) => {
    fetchUserMobileNumber(email).then(res => {
      if(res.data.status == 200 ) {
        if(isSaleAgent) {
          this.setState({ sales_agent_contact : res.data.number, fatchingNumbers:false, fatchingTLNumbers:false })
         } else {
          this.setState({ team_leader_contact : res.data.number, fatchingNumbers:false, fatchingTLNumbers:false })
         }
      } else {
        this.setState({
          errorMsg : res.data.msg,
          fatchingNumbers:false,
          fatchingTLNumbers:false
        })

        setTimeout(() => {
          this.setState({ errorMsg:''})
        }, 4000 )
      }

      console.log('response = ', res)
    }).catch(err => {
      console.log('error => ', err)
      this.setState({
        errorMsg : err.response ? err.response.data.msg :'Somthing went wrong !',
        fatchingNumbers:false,
        fatchingTLNumbers:false
      })

      setTimeout(() => {
       this.setState({ errorMsg:''})
      }, 4000 )
    })
  }

  /**
   * Handle Bank inputs
   */ 
  handleInputValues = (e) => {
    const { name, value } = e.target;
    if(name == 'sale_agent_id' ) {
      console.log('sale_agent_id value = ', value)
      this.setState({ fatchingNumbers : true })
      this.getSalesAgentNumber(value, true);
      this.setState({ [name]: value, is_change: true });

    } else if(name == 'team_leader') {
      this.setState({ fatchingTLNumbers : true })
      this.getSalesAgentNumber(value, false);
      this.setState({ [name]: value, is_change: true });

    } else {
      if(name ==  'registered_mobile') {
        this.testValidateNumber(name, value, 'registered_mobile' )
  
      } else if(name == 'loan_account_number') { 
        this.testValidateNumber(name, value, 'loan_account_number' )
  
      } else if(name == 'loan_amount') {
        this.testValidateNumber(name, value, 'loan_amount' )
  
      } else if(name == 'alternate_phone') { 
        this.testValidateNumber(name, value, 'alternate_phone' )
  
      } else if(name ==  'sales_agent_contact') {
        this.testValidateNumber(name, value, 'sales_agent_contact' )
  
      } else if(name ==  'team_leader_contact') {
        this.testValidateNumber(name, value, 'team_leader_contact' )
      }  else {
        this.setState({ [name]: value, is_change: true });
      } 
    }
    // this.setState({ [name]: value });
  };

  /**
   * Get mobile number
   */
  getSalesAgentNumber = (sale_agent_id, isSaleAgent=false) => {
    fetchMobileNumber(sale_agent_id).then(res => {
      if(res.data.status == 200 ) {
        if(isSaleAgent) {
          this.setState({ sales_agent_contact : res.data.number, fatchingNumbers:false, fatchingTLNumbers:false })
         } else {
          this.setState({ team_leader_contact : res.data.number, fatchingNumbers:false, fatchingTLNumbers:false })
         }
      } else {
        this.setState({
          errorMsg : res.data.msg,
          fatchingNumbers:false,
          fatchingTLNumbers:false
        })

        setTimeout(() => {
          this.setState({ errorMsg:''})
        }, 4000 )
      }

      console.log('response = ', res)
    }).catch(err => {
      console.log('error => ', err)
      this.setState({
        errorMsg : err.response.data.msg,
        fatchingNumbers:false,
        fatchingTLNumbers:false
      })

      setTimeout(() => {
       this.setState({ errorMsg:''})
      }, 4000 )
    })
  }

  /**
   * Test validate number inputs
   */
  testValidateNumber = (name, value, field_name) => {
    let addEndUserErrors ={}

    if(!/^[0-9]+$/.test(value)){
      addEndUserErrors[field_name] = END_USER_FORM_VALIDATIONS.VALID_FIELD
      this.setState({ [name]: value,addEndUserErrors, is_change: false });
    } else {
      addEndUserErrors[field_name]='';
      this.setState({ [name]: value, is_change: true, addEndUserErrors });
    }

      return addEndUserErrors;
  } 


  /**
   * Handle bank validation
   */
  handleAddEndUserValidation = () => { 
    // Request validate
    let addEndUserErrors = validateRequestParams(this.state);
    this.setState({ addEndUserErrors });
    return addEndUserErrors;
  };

  handleAddEndUser = () => {
    console.log('handleAddEndUser = ')
    const errors = this.handleAddEndUserValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.addNewEndUser();
    }
  };
 
  addNewEndUser = () => { 
    this.setState({ loading: true });
    let payload = CreateReuqestPayload(this.state, this.props.sale_agents);
    let _this = this;

    addNewEndUser(payload)
      .then((response) => {
        if (response.data && response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            loading: false,
            errorMsg: "",
            first_name:'',
            last_name:'',
            registered_mobile:'',
            loan_account_number:'',
            loan_amount:'',
            alternate_phone:'',
            language_name:'',
            loan_type:'',
            branch_id:'',
            sale_agent_id:'',
            team_leader_id:'',
            registeration_trigger:'',
            verification_trigger:'',
            sales_agent_contact:'',
            team_leader_contact:'',
            reason_for_change:'',
            middle_name:'',
            loan_emi:'',
            loan_tenure:''
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } else {
          this.setState({
            errorMsg: response.data.msg,
            loading: false,
            successMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        console.log("error => ", err.response);
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  /**
   * Reset form
   */
  handleResetEvent = () => {
    this.setState({
      first_name: "",
      last_name: "",
      registered_mobile: "",
      loan_account_number: "",
      loan_amount: "",
      alternate_phone: "",
      preferred_langauge: "",
      loan_type: "",
      branch_id: "",
      sale_agent_id: "",
      team_leader_id: "",
      registeration_trigger: "",
      verification_trigger: "",
      sales_agent_contact:'',
      team_leader_contact:'',
      middle_name:''
    });
  };

  render() {
    const {
      sale_agents,
      team_leads,
      languages,
      loan_types,
      branches,
    } = this.props;
    const {
      first_name,
      last_name,
      registered_mobile,
      loan_account_number,
      loan_amount,
      alternate_phone,
      preferred_langauge_id,
      loan_type_id,
      branch_id,
      sale_agent_id,
      team_leader_id,
      registeration_trigger,
      verification_trigger,
      addEndUserErrors,
      successMsg,
      team_leader_contact,
      sales_agent_contact,
      errorMsg,
      loading,
      middle_name,
      fatchingNumbers,
      fatchingTLNumbers,
      loan_emi,
      loan_tenure
    } = this.state;

    let _this = this;

    sale_agents && sale_agents.map((item, index) => {
 
      console.log('load sales_agent_contact  = ', sales_agent_contact)
      if(item.mobile_number == sales_agent_contact ) {
        sale_user_id = item.id
        // _this.setState({
        //   sale_agent_id : item.id 
        // })
      }
    
    })

    console.log('load sale_user_id  = ', sale_user_id)

    return (
      <Fragment>
        <AddEndUserModal
          loan_types={loan_types}
          languages={languages}
          middle_name={middle_name}
          team_leads={team_leads}
          sale_agents={sale_agents}
          branches={branches}
          handleInputValues={this.handleInputValues}
          first_name={first_name}
          last_name={last_name}
          sales_agent_contact={sales_agent_contact}
          team_leader_contact={team_leader_contact}
          errorMsg={errorMsg}
          successMsg={successMsg}
          registered_mobile={registered_mobile}
          loan_account_number={loan_account_number}
          loan_amount={loan_amount}
          alternate_phone={alternate_phone}
          preferred_langauge={preferred_langauge_id}
          loan_type={loan_type_id}
          branch_id={branch_id}
          sale_agent_id={sale_agent_id}
          team_leader={team_leader_id}
          registeration_trigger={registeration_trigger}
          verification_trigger={verification_trigger}
          handleAddEndUser={this.handleAddEndUser}
          addEndUserErrors={addEndUserErrors}
          loading={loading}
          handleResetEvent={this.handleResetEvent}
          fatchingNumbers={fatchingNumbers}
          fatchingTLNumbers={fatchingTLNumbers}
          sale_user_id={sale_user_id}
          loan_emi={loan_emi}
          loan_tenure={loan_tenure}
        />
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    languages: state.fetchEndUserReducer.languages,
    loan_types: state.fetchEndUserReducer.loan_types,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: () => dispatch(fetchLanguages()),
    fetchLoanTypes: () => dispatch(fetchLoanTypes()),
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
    getBranches: () => dispatch(getBranches()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEndUser);
