import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import EndUserDetailView from "../../components/EndUser/EndUserDetailView";
import {
  fetchLanguages,
  fetchLoanTypes,
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../actions/EndUser/index";
import { getBranches } from "../../../Comman/actions";
import firebase from "../../../Comman/firebase";
import moment from "moment";
import {
  getEndUserInfo,
  getEndUserLogData,
  handleAgentCallApi,
  handleVerifyCallApi,
  resetCallHistory,
  getUserCallDetails
} from "../../services/common";
import { END_USER_STATE } from "../../constant";
import { ResetUserRecord } from "../../components/EndUser/ResetUserRecord";

import { getLoggedInUser } from "../../../Comman/functions";

let audio = "";


class EndUserDetails extends Component {
  state = END_USER_STATE;


  componentDidUpdate() {
    console.log("componentDidUpdate  fire ");


  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ id, isFatching: true, fatchingLogs: true });
    audio = document.querySelector("#audio");
    const getLoggedUser = getLoggedInUser("user");
    let date = moment().format("YYYY-MM-DD");

    const {
      fetchLanguages,
      fetchLoanTypes,
      fetchSaleAgents,
      fetchTeamLeads,
      getBranches,
    } = this.props;
    fetchLanguages();
    fetchLoanTypes();
    fetchSaleAgents();
    fetchTeamLeads();
    getBranches();

    this.getEndUserById(id);
    this.getEndUserLogs(id);
    this.getCallDetails(id);

    let _this = this;
    // setInterval(function(){  _this.getEndUserById(id); }, 7000);

    this.setFirebaseReference(getLoggedUser, id, date);
  }

  setFirebaseReference = (user, id, date) => {
    const { callReference, is_registration } = this.state;
    console.log("set firebase refrebnce = ", user);
    console.log("set firebase refrebnce is_registration = ", is_registration);
    let userData = JSON.parse(user);

    this.setState({
      callReference: firebase
        .database()
        .ref("turant-db/" + userData.bank_id + "/" + id + "/" + date),
    });
  };

  getCallDetails = (id) => {
    getUserCallDetails(id).then(response => {
      const score = response.data.data.verify_calls.length > 0 ? response.data.data.verify_calls[0].call_score : 0;
      this.setState({
        call_score: Number(score).toFixed(2), 
        registration_status: response.data.data.register_calls.length > 0 ? response.data.data.register_calls[0].registration_call_status :'NA',
        verification_status: response.data.data.verify_calls.length > 0 ? response.data.data.verify_calls[0].verification_call_status : 'NA',
      })
    }).catch(err => {

      let _this = this;
      console.log("error response => ", err);
      _this.setState({
        errorMsg: err.response
          ? err.response.data.message
          : err.response
            ? err.response.data.msg
            : "Somthing Went Worng !",
        isFatching: false,
      });
      setTimeout(function () {
        _this.setState({ errorMsg: "" });
      }, 4000);
    });
  }
  /**
   * Get user logs
   */
  getEndUserLogs = (id) => {
    getEndUserLogData(id)
      .then((response) => {
        console.log("response logs = ", response.data.data);
        this.setState({
          user_logs: response.data.data,
          fatchingLogs: false,
        });
      })
      .catch((err) => {
        console.log("error logs");
      });
  };

  /**
   * Get userid
   * @param  id 
   */
  getEndUserById = (id) => {
    getEndUserInfo(id)
      .then((res) => {
        let _this = this;
        let response = res.data.data;
        let call_info = res.data.call_details;

        console.log('sales_agent_name  response = ', response)

        console.log('sales_agent_name = ', response.sales_agent_name)

        if (res.data.status == 200) {
          this.setState({
            first_name: response.first_name,
            last_name: response.last_name,
            registered_mobile: response.registered_mobile,
            loan_account_number: response.loan_account_number,
            loan_amount: response.loan_amount,
            alternate_phone: response.alternate_phone,
            preferred_langauge: response.preferred_langauge_id,
            loan_type: response.loan_type_id,
            branch_id: response.branch_id,
            sale_agent_id: response.sale_agent_id,
            team_leader_id: response.team_leader_id,
            registeration_trigger: response.registeration_trigger,
            verification_trigger: response.verification_trigger,
            call_status: response.call_status,
            current_status: response.current_status,
            call_action: response.call_action,
            verfications: response.verifications,
            uploaded_by: response.uploaded_by,
            isFatching: false,
            sales_agent: response.sales_agents,
            team_leader: response.team_leads,
            created_at: response.created_at,
            sales_agent_contact: response.sales_agent_contact,
            middle_name: response.middle_name,
            team_leader_contact: response.team_leader_contact,
            call_details: call_info, //response.call_details,
            refreshing_call_info: false,
            call_failed_count: response.failed_call_count,
            is_registration: response.is_registration,
            is_verification: response.is_verification,
            sales_agent_name: response.sales_agent_name,
            team_leader_name: response.team_leader_name,
            loan_tenure: response.loan_tenure,
            loan_emi: response.loan_emi,
            // isDisabledButton:false
          });

          let _this = this;
          {
            call_info &&
              call_info.map((item, index) => {
                console.log("recoring details = ", item.recordings);

                // item.recordings && item.recordings.map((recording, index) => {
                //   console.log("recoring details recording = ", recording);
                // //if((item.recordings != null && (item.recordings[0] != null || item.recordings[1] != null || item.recordings[2] != null ))) {
                //   _this.setState({
                //     playAudioFirst:
                //     recording.length != 0 &&
                //       typeof recording !== 'undefined' && recording.turant_recording_urls,
                //     playAudioSecond:
                //       recording.length != 0 &&
                //       typeof recording !== 'undefined' &&recording.turant_recording_urls,
                //     playAudioThird:
                //       recording.length != 0 &&
                //       typeof recording !== 'undefined' &&recording.turant_recording_urls,
                //     firstFileName:
                //       recording.length != 0 &&
                //       this.getFileNameFromUrl(
                //         typeof recording !== 'undefined' && recording.turant_recording_urls
                //       ),
                //     secondFileName:
                //       recording.length != 0 &&
                //       this.getFileNameFromUrl(
                //         typeof recording !== 'undefined' &&recording.turant_recording_urls
                //       ),
                //     thirdFileName:
                //       recording.length != 0 &&
                //       this.getFileNameFromUrl(
                //         typeof recording !== 'undefined' && recording.turant_recording_urls
                //       ),
                //     firstFileExtension:
                //       recording.length != 0 &&
                //       this.get_url_extension(
                //         recording.length != 0 &&
                //         typeof recording !== 'undefined' &&recording.turant_recording_urls
                //       ),
                //     secondFileExtension:
                //       recording.length != 0 &&
                //       this.get_url_extension(
                //         recording.length != 0 &&
                //         typeof recording !== 'undefined' && recording.turant_recording_urls
                //       ),
                //     // thirdFileExtension:
                //     //   item.recordings.length != 0 &&
                //     //   this.get_url_extension(
                //     //     item.recordings.length != 0 &&
                //     //     typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls
                //     //   ),
                //     thirdFileExtension:'wav'
                //   });
                // //}


                // })

              });
          }
        } else {
          let _this = this;

          _this.setState({
            errorMsg: res.data
              ? res.data.data.message
              : res.data
                ? res.data.data.msg
                : "Somthing Went Worng !",
            isFatching: false,
          });
          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        let _this = this;
        console.log("error details => ", err);
        _this.setState({
          errorMsg: err.response
            ? err.response.data.message
            : err.response
              ? err.response.data.msg
              : "Somthing Went Worng !",
          isFatching: false,
        });
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  /**
   *
   * @param url
   * Get filename from url
   */
  getFileNameFromUrl = (url) => {
    const hashIndex = (url && url.indexOf("#"));
    let url1 = (url && hashIndex && hashIndex !== -1) ? url && url.substring(0, hashIndex) : url;
    if ((typeof url1 === 'string') && url1.includes('wav')) {
      return ((url1 && url1.split("/").pop()) || "").replace(/[\?].*$/g, "");
    } // End if
  };

  /**
   *
   * @param url
   * Get file extension from url
   */
  get_url_extension = (url) => {

    return url && url.split(/[#?]/)[0].split(".").pop().trim();
  };

  handleCallStand = () => {

    const {
      id,
      sales_agent_contact,
      registered_mobile,
      alternate_phone,
      sale_agent_id,
      call_action,
      is_registration,
      call_portal
    } = this.state;



    this.setState({
      call_initiating: true,
      isDisabledButton: true,
      callHandling: true,
      call_msg: is_registration != 1 ? 'Registration In Progress.....' : 'Verification  In Progress.....',
      display_msg_header: true
    });


    let find_str = call_action.includes("Registration");

    const payload = {
      id,
      from: sales_agent_contact != null ? sales_agent_contact : "",
      to:
        registered_mobile != "" && registered_mobile != null
          ? registered_mobile
          : alternate_phone,
      sale_agent_id,
      is_registration_call: find_str,
      portal: call_portal == 0 ? 0 : 1,
      secret_key:"Zrw6QrlHRSCV1WXgvyiSVDYpY7M093GFa7mJ3WwLqR3BhTt9mStUkIxVlzfPImMu"
    };

    // alert('You need to click on refresh icon for Excute Call ss!')
    if (is_registration === 1) {

      console.log('verification call hited = ', is_registration)

      handleVerifyCallApi(id, payload)
        .then((res) => {
          console.log("response verify= ", res);
          console.log("response verify status = ", res.status);
          if (res.status == 200) {
            var obj = JSON.parse(res.data.data);

            // Fetch live firebase verify call status
            this.fetchCallstatus(obj.sid.replace(/\./g, ''), 'Verification');
          } else {
            this.setState({
              call_initiating: true,
              call_msg: res.data
                ? res.data.message
                : "Error, Somthing went wrong !",
              isError: true,
              isDisabledButton: false,
              callHandling: false,
              display_msg_header: false
            });
          }
        })
        .catch((err) => {
          console.log("error => ", err);
          console.log("error s=> ", err.response);
          this.setState({
            call_initiating: true,
            call_msg: err.response
              ? err.response.data.message
              : "Error, Somthing went wrong !",
            isError: true,
            call_status_code: 3,
            isDisabledButton: false,
            callHandling: false,
            display_msg_header: false
          });
        });

      let _this = this;

      // setTimeout(function () {
      //   console.log("Refresh user detail page  after 15 seconds= ");
      //   // window.location.reload();
      //   // Fetch user data
      //   _this.getEndUserById(id);
      // }, 15000);


    } else {
 
      console.log('registration call hited = ', is_registration)
      handleAgentCallApi(id, payload)
        .then((res) => {
          console.log("response registration d= ", res);
          console.log("response registration v= ", res.status);
          if (res.status == 200) {
            var obj = JSON.parse(res.data.data);
            console.log("response registration d= ", obj);
            console.log("response registration sd= ", obj.sid);
            // Fetch live firebase register call status
            this.fetchCallstatus(obj.sid.replace(/\./g, ''), 'Registration');
          } else {
            this.setState({
              call_initiating: true,
              call_msg: res.data
                ? res.data.message
                : "Error, Somthing went wrong !",
              isError: true,
              isDisabledButton: false,
              callHandling: false,
              display_msg_header: false
            });
          }
        })
        .catch((err) => {
          console.log("errors=> ", err);
          console.log("error => ", err.response);
          this.setState({
            call_initiating: true,
            call_msg: err.response
              ? err.response.data.message
              : "Error, Somthing went wrong !",
            isError: true,
            call_status_code: 3,
            isDisabledButton: false,
            callHandling: false,
            display_msg_header: false
          });
        });
      let _this = this;

      // setTimeout(function () {
      //   console.log("Refresh user detail page  after 15 seconds= ");
      //   // window.location.reload();
      //   // Fetch user data
      //   _this.getEndUserById(id);
      // }, 15000);
    }

  };

  fetchCallstatus = (callControlId, type) => {
    const { callReference, is_registration, id } = this.state;
    let ref_calls = !is_registration
      ? "registration_calls"
      : "verification_calls";

    console.log("callReference callControlId ", callControlId);
    console.log("callReference ref_calls  ", ref_calls);

    callReference
      .child(ref_calls)
      .child(callControlId)
      .on("value", (snap) => {
        console.log("callReference realtimess", snap.val());
        if (snap.val() != null) {
          let _this = this;

          let values = snap.val();
          if (values.call_status_code == 0) {
            this.setState({
              call_initiating: true,
              call_msg: type + ' In Progress.....', //"Call " + values.status + " !",
              isError: false,
              call_status_code: 0,
            });

            // Fetch user data
            this.getEndUserById(id);
            this.getCallDetails(id);
          } else if (values.call_status_code == 1) {
            this.setState({
              call_initiating: true,
              call_msg: values.status,
              isError: false,
              call_status_code: 1,
              callHandling: false,
            });

            // Fetch user data
            this.getEndUserById(id);
            this.getCallDetails(id);
          } else if (values.call_status_code == 2) {
            this.setState({
              call_initiating: true,
              call_msg: "Call " + values.status,
              isError: false,
              call_status_code: 2,
              isDisabledButton: false,
              callHandling: false,
              display_msg_header: false
            });
            // Set timeout
            this.setTimeoutModal();

            // Fetch user data
            this.getEndUserById(id);
            this.getCallDetails(id);
          } else if (values.call_status_code == 3) {
            let status = values.status.split('_').join('');
            this.setState({
              call_initiating: true,
              call_msg: 'Call ' + status,
              isError: true,
              call_status_code: 3,
              isDisabledButton: false,
              callHandling: false,
              display_msg_header: false
            });

            // Set timeout
            this.setTimeoutModal();

            // Fetch user data
            this.getEndUserById(id);
            this.getCallDetails(id);
          } else if (values.call_status_code == 4) {
            this.setState({
              call_initiating: true,
              call_msg: 'Data is being Verified',
              isError: true,
              call_status_code: 4,
              isDisabledButton: true,
              callHandling: false,
            });

            // Fetch user data
            this.getEndUserById(id);
            this.getCallDetails(id);
          }


          console.log("callReference realtime", snap.val());
        } // End if
      });
  };

  setTimeoutModal = () => {
    let _this = this;
    setTimeout(function () {
      _this.setState({
        call_initiating: false,
        call_msg: "",
        isError: false,
        call_status_code: 0,
      });
    }, 7000);
  };

  /**
   *
   * @param
   * close call toast
   */
  handleCloseCallToast = () => {
    this.setState({
      call_initiating: false,
      call_msg: "",
      isError: false,
    });
  };

  /**
   *
   * @param
   * Get live user vcall details
   */
  getLiveUserCallDetails = () => {
    // this.setState({ isFatching:true });
    this.setState({ refreshing_call_info: true });
    const { id } = this.state;
    this.getEndUserById(id);
    this.getCallDetails(id)
  };

  /**
   *
   * @param url
   * Handle audio play audio event
   */
  handleAudioPlay = (play = true, data = null, type, value, index, dyna, audioRecording) => {
    let { playAudioFirst, playAudioSecond, playAudioThird } = this.state;

    if (type == "playAudioFirst") {
      let url = "";
      if (audioRecording != undefined && audioRecording.length != undefined) {
        url = audioRecording //playAudioFirst;
      } else {
        url = URL.createObjectURL(audioRecording != undefined && audioRecording);
      }


      if (value) {
        console.log('type audio url audio ')
        console.log(audio)
        if (audio) {
          audio.src = url;
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = url;
          audio.autoplay = true;
        }
      }

      this.setState({
        playAudio1: !value,
        playAudio2: false,
        playAudio3: false,
      });
    }

    if (type == "playAudioSecond") {
      let url = "";
      if (audioRecording != undefined && audioRecording.length != undefined) {
        url = audioRecording //playAudioSecond;
      } else {
        url = URL.createObjectURL(audioRecording);
      }
      if (value) {
        if (audio) {
          audio.src = url;
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = url;
          audio.autoplay = true;
        }
      }

      this.setState({
        playAudio2: !value,
        playAudio1: false,
        playAudio3: false,
      });
    }

    if (type == "playAudioThird") {
      let url = "";
      if (audioRecording != undefined && audioRecording.length != undefined) {
        url = audioRecording //playAudioThird;
      } else {
        url = URL.createObjectURL(audioRecording);
      }
      if (value) {
        if (audio) {
          audio.src = url;
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = url;
          audio.autoplay = true;
        }
      }
      this.setState({
        playAudio3: !value,
        playAudio2: false,
        playAudio1: false,
      });
    }
  };

  /**
   * Handle reset call user history event
   */
  handleResetAllEvent = () => {
    this.setState({ showResetModal: true });
  };

  /**
   * Handle close reset call user history
   */
  handleCancelModal = () => {
    this.setState({ showResetModal: false });
  };

  /**
   * Handle reset call user history
   */
  handleResetConfirm = () => {
    const { id } = this.state;
    this.setState({ isResetLoading: true, isFatching: true });

    /**
     * Call api for reset all user call history
     */
    resetCallHistory(id)
      .then((response) => {
        console.log("response = ", response.data);
        this.setState({ isFatching: true });

        let _this = this;
        // Fetch fresh user details
        this.getEndUserById(id);
        this.getCallDetails(id);
        
        if (response.data.status == 200) {
          this.setState({
            errorMsg: "",
            successMsg: response.data.msg,
            isResetLoading: false,
            showResetModal: false,
            isDisabledButton: false,
          });

          
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 4000);
        } else {
          this.setState({
            errorMsg: response.data.msg,
            successMsg: "",
            isResetLoading: false,
            showResetModal: false,
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        let _this = this;
        console.log("error response = ", err.response);
        this.setState({
          errorMsg: err.response
            ? err.response.data.msg
            : err.response.data.message,
          successMsg: "",
          isResetLoading: false,
          showResetModal: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  render() {
    const {
      sale_agents,
      team_leads,
      languages,
      loan_types,
      branches,
    } = this.props;
    const {
      first_name,
      last_name,
      registered_mobile,
      loan_account_number,
      loan_amount,
      alternate_phone,
      preferred_langauge,
      loan_type,
      branch_id,
      sale_agent_id,
      team_leader_id,
      registeration_trigger,
      verification_trigger,
      addEndUserErrors,
      successMsg,
      errorMsg,
      loading,
      call_status,
      current_status,
      call_action,
      verfications,
      uploaded_by,
      isFatching,
      sales_agent,
      team_leader,
      created_at,
      user_logs,
      sales_agent_contact,
      middle_name,
      team_leader_contact,
      call_initiating,
      call_msg,
      isError,
      call_details,
      call_text,
      refreshing_call_info,
      call_failed_count,
      fatchingLogs,
      is_registration,
      isSuccess,
      is_verification,
      playAudioFirst,
      playAudioSecond,
      playAudioThird,
      playAudio1,
      playAudio2,
      playAudio3,
      firstFileName,
      secondFileName,
      thirdFileName,
      firstFileExtension,
      secondFileExtension,
      thirdFileExtension,
      isResetLoading,
      isReset,
      showResetModal,
      isDisabledButton,
      call_status_code,
      callHandling,
      call_portal,
      display_msg_header,
      sales_agent_name,
      team_leader_name,
      loan_tenure,
      loan_emi,
      call_score,
      verification_status,
      registration_status
    } = this.state;

    return (
      <Fragment>
        <EndUserDetailView
          call_score={call_score}
          loan_types={loan_types}
          languages={languages}
          team_leads={team_leads}
          sale_agents={sale_agents}
          branches={branches}
          call_status={call_status}
          middle_name={middle_name}
          current_status={current_status}
          call_action={call_action}
          handleInputValues={this.handleInputValues}
          first_name={first_name}
          sales_agent_name={sales_agent_name}
          team_leader_name={team_leader_name}
          last_name={last_name}
          errorMsg={errorMsg}
          successMsg={successMsg}
          registered_mobile={registered_mobile}
          loan_account_number={loan_account_number}
          loan_amount={loan_amount}
          alternate_phone={alternate_phone}
          preferred_langauge={preferred_langauge}
          loan_type={loan_type}
          branch_id={branch_id}
          sale_agent_id={sale_agent_id}
          team_leader_id={team_leader_id}
          team_leader_contact={team_leader_contact}
          registeration_trigger={registeration_trigger}
          verification_trigger={verification_trigger}
          handleEditEndUser={this.handleEditEndUser}
          addEndUserErrors={addEndUserErrors}
          loading={loading}
          verfications={verfications}
          uploaded_by={uploaded_by}
          sales_agent={sales_agent}
          team_leader={team_leader}
          created_at={created_at}
          user_logs={user_logs}
          handleCallStand={this.handleCallStand}
          sales_agent_contact={sales_agent_contact}
          call_initiating={call_initiating}
          call_msg={call_msg}
          call_portal={call_portal}
          handleCloseCallToast={this.handleCloseCallToast}
          isError={isError}
          isFatching={isFatching}
          call_details={call_details}
          getLiveUserCallDetails={this.getLiveUserCallDetails}
          call_text={call_text}
          refreshing_call_info={refreshing_call_info}
          call_failed_count={call_failed_count}
          fatchingLogs={fatchingLogs}
          is_registration={is_registration}
          isSuccess={isSuccess}
          is_verification={is_verification}
          handleAudioPlay={this.handleAudioPlay}
          playAudioFirst={playAudioFirst}
          playAudioSecond={playAudioSecond}
          playAudioThird={playAudioThird}
          playAudio1={playAudio1}
          playAudio2={playAudio2}
          playAudio3={playAudio3}
          firstFileName={firstFileName}
          secondFileName={secondFileName}
          thirdFileName={thirdFileName}
          firstFileExtension={firstFileExtension}
          secondFileExtension={secondFileExtension}
          thirdFileExtension={thirdFileExtension}
          handleResetAllEvent={this.handleResetAllEvent}
          isDisabledButton={isDisabledButton}
          call_status_code={call_status_code}
          callHandling={callHandling}
          display_msg_header={display_msg_header}
          loan_tenure={loan_tenure}
          loan_emi={loan_emi}
          verification_status={verification_status}
          registration_status={registration_status}

        />

        {showResetModal && (
          <ResetUserRecord
            isResetLoading={isResetLoading}
            isReset={showResetModal}
            first_name={first_name}
            last_name={last_name}
            handleCancelModal={this.handleCancelModal}
            handleResetConfirm={this.handleResetConfirm}
            registered_mobile={registered_mobile}
          />
        )}
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    languages: state.fetchEndUserReducer.languages,
    loan_types: state.fetchEndUserReducer.loan_types,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: () => dispatch(fetchLanguages()),
    fetchLoanTypes: () => dispatch(fetchLoanTypes()),
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
    getBranches: () => dispatch(getBranches()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EndUserDetails);
