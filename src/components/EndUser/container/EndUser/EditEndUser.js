import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CInvalidFeedback,
} from "@coreui/react";
import UpdateEndUser from "../../components/EndUser/UpdateEndUser";
import {
  fetchLanguages,
  fetchLoanTypes,
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../actions/EndUser/index";
import { getBranches } from "../../../Comman/actions";
import { updateEndUserDetails, getEndUserDetails, fetchMobileNumber } from "../../services/common";
import {
  END_USER_STATE,
  REASONS,
  END_USER_FORM_VALIDATIONS,
} from "../../constant";
import {
  validateRequestParams, 
  CreateReuqestPayload,
  validateUpdateRequest,
} from "../../functions";
import { InvertContentLoader } from "../../../Comman/components";

let store_value = "";

class EditEndUser extends Component {
  state = END_USER_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ id, isFatching: true });

    const {
      fetchLanguages,
      fetchLoanTypes,
      fetchSaleAgents,
      fetchTeamLeads,
      getBranches,
    } = this.props;
    fetchLanguages();
    fetchLoanTypes();
    fetchSaleAgents();
    fetchTeamLeads();
    getBranches();

    this.getEndUserById(id);
  }

  getEndUserById = (id) => {
    getEndUserDetails(id)
      .then((res) => {
        let data = res.data.data;
        console.log('obje response  = t', data)


        let _this = this;
        this.setState({
          first_name: res.data.data.first_name,
          last_name: res.data.data.last_name,
          middle_name : res.data.data.middle_name,
          registered_mobile: res.data.data.registered_mobile,
          loan_account_number: res.data.data.loan_account_number,
          loan_amount: res.data.data.loan_amount,
          loan_tenure : res.data.data.loan_tenure,
          loan_emi : res.data.data.loan_emi,
          alternate_phone: res.data.data.alternate_phone,
          preferred_langauge: res.data.data.preferred_langauge_id,
          loan_type: res.data.data.loan_type_id,
          branch_id: res.data.data.branch_id,
          branch_name : res.data.data.branch_name,
          sale_agent_id: res.data.data.sale_agent_id,
          team_leader: res.data.data.team_leader_id,
          registeration_trigger: res.data.data.registeration_trigger,
          verification_trigger: res.data.data.verification_trigger,
          team_leader_contact: res.data.data.team_leader_contact,
          sales_agent_contact: res.data.data.sales_agent_contact,
          isFatching: false,
          reason_for_change: res.data.data.reason_for_change,
          updatedArray: res.data.data,
          loading:false
        });
      })
      .catch((err) => {
        let _this = this;
        console.log('error => ', err)
        _this.setState({
          errorMsg: err.response
            ? err.response.data.msg
            : err.response ? err.response.data.message : 'Error, Somthing went wrong',
          loading: false,
        });
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  /**
   * Handle End user inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;

    console.log('name = ', name)
    console.log('value = ', value)

    if(name == 'sale_agent_id' ) {

      this.getSalesAgentNumber(value, true);
      this.setState({ [name]: value, is_change: true });

    } else if(name == 'team_leader') {

      this.getSalesAgentNumber(value, false);
      this.setState({ [name]: value, is_change: true });

    } else {
      if(name ==  'registered_mobile') {
        this.testValidateNumber(name, value, 'registered_mobile' )
  
      } else if(name == 'loan_account_number') {
        this.testValidateNumber(name, value, 'loan_account_number' )
  
      } else if(name == 'loan_amount') {
        this.testValidateNumber(name, value, 'loan_amount' )
  
      } else if(name == 'loan_tenure') {
        this.testValidateNumber(name, value, 'loan_tenure' )
  
      }  else if(name == 'loan_emi') {
        this.testValidateNumber(name, value, 'loan_emi' )
  
      } else if(name == 'alternate_phone') {
        this.testValidateNumber(name, value, 'alternate_phone' )
  
      } else if(name ==  'sales_agent_contact') {
        this.testValidateNumber(name, value, 'sales_agent_contact' )
  
      } else if(name ==  'team_leader_contact') {
        this.testValidateNumber(name, value, 'team_leader_contact' )
      }  else {
        this.setState({ [name]: value, is_change: true });
      } 
    }
  };

  /**
   * Get mobile number
   */
  getSalesAgentNumber = (sale_agent_id, isSaleAgent=false) => {
    fetchMobileNumber(sale_agent_id).then(res => {
      if(res.data.status == 200 ) {
        if(isSaleAgent) {
          this.setState({ sales_agent_contact : res.data.number })
         } else {
          this.setState({ team_leader_contact : res.data.number })
         }
      } else {
        this.setState({
          errorMsg : res.data.msg
        })

        setTimeout(() => {
          this.setState({ errorMsg:''})
        }, 4000 )
      }

      console.log('response = ', res)
    }).catch(err => {
      console.log('error => ', err)
      this.setState({
        errorMsg : err.response.data.msg
      })

      setTimeout(() => {
       this.setState({ errorMsg:''})
      }, 4000 )
    })
  }


  /**
   * Test validate number inputs
   */
  testValidateNumber = (name, value, field_name) => {
    let addEndUserErrors ={}

    if(!/^[0-9]+$/.test(value)){
      addEndUserErrors[field_name] = END_USER_FORM_VALIDATIONS.VALID_FIELD
      this.setState({ [name]: value,addEndUserErrors, is_change: false });
    } else {
      addEndUserErrors[field_name]='';
      this.setState({ [name]: value, is_change: true, addEndUserErrors });
    }

      return addEndUserErrors;
  }

  /**
   * Handle reason inputs
   */
  handleReasonValues = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value, is_select: true });
  };

  /**
   * Handle On blur change
   */
  handleonBlur = (e) => {
    const { name, value } = e.target;
    const { updatedArray } = this.state;
    const { languages, loan_types, branches, sale_agents, team_leads } = this.props;
    console.log('obje nupdatedArray = t', updatedArray)
    if (!Object.values(updatedArray).includes(value)) {
      console.log('obje not fount')
      let fieldName = name.replace("_", " ");
      let fieldKey = this.capitalizeFirstLetter(fieldName);
        console.log('preferred_langauge = ', name)
        console.log('value = ', value)
      if(name == 'preferred_langauge') {
        let lang = languages.filter(item => item.id == value)
        console.log('preferred_langauge langlanglanglang  = ', lang)
        console.log('preferred_langauge languages   = ', languages)
        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: lang && lang[0].language,
          field_name: fieldKey,
          old_field_value: updatedArray.language_name,
        });
      } else if(name == 'branch_id') { 
        let lang = branches.filter(item => item.id == value)

        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: lang[0].branch_name,
          field_name: fieldKey,
          old_field_value: updatedArray.branch['id'],
        });
      } else if(name == 'sale_agent_id') {
       let sales_agent =  sale_agents.filter(item => item.id == value)
       console.log('obje nupdatedArray =sales_agent  t', sales_agent)
        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: sales_agent[0].first_name +' '+ sales_agent[0].last_name,
          field_name: fieldKey,
          old_field_value: updatedArray.sales_agents['first_name'] +' '+ updatedArray.sales_agents['last_name'],
        });
     
      } else if(name == 'team_leader') {
        let team_lead =  team_leads.filter(item => item.id == value)
        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: team_lead[0].first_name +' '+ team_lead[0].last_name,
          field_name: fieldKey,
          old_field_value: updatedArray.team_leads['first_name'] +' '+ updatedArray.team_leads['last_name'],
        });
      }  else {
        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: value,
          field_name: fieldKey,
          old_field_value: updatedArray[name],
        });
      }

    }  else {
      let fieldName = name.replace("_", " ");
      let fieldKey = this.capitalizeFirstLetter(fieldName);
      if(name == 'loan_type') {
        let lang = loan_types.filter(item => item.id == value)
        console.log('object loan_types', lang)
        console.log('object updatedArray', updatedArray)
        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: lang[0].loan_type,
          field_name: fieldKey,
          old_field_value: updatedArray.loan_type['loan_type'],
        });
      } else if (name == 'preferred_langauge') {
        let lang = languages.filter(item => item.id == value)
        console.log('preferred_langauge langlanglanglang  = ', lang)
        console.log('preferred_langauge languages   = ', languages)
        this.setState({
          isUpdateData: true,
          isCheckReason: false,
          field_value: lang && lang[0].language,
          field_name: fieldKey,
          old_field_value: updatedArray.language_name,
        });
      }
      console.log('object found', name)
    }
  };

  /**
   * Handle bank validation
   */
  handleEditEndUserValidation = () => {
    // Request validate
    let addEndUserErrors = validateUpdateRequest(this.state);
    this.setState({ addEndUserErrors });
    return addEndUserErrors;
  };

  /**
   * Show modal
   */
  handleEditEndUser = () => {
    const { is_change, reason_for_change, is_select } = this.state;

    if (!is_change || !is_select) {
      this.setState({ isChangeAnyData: true });
    } else {
      // this.setState({ isChangeAnyData: true, isCheckReason:false });
      this.handleUpdateConfirm();
    }
  };

  /**
   * Call edit user api
   */
  editNewEndUser = () => {
    this.setState({ loading: true });
    let payload = CreateReuqestPayload(this.state);
    const { id } = this.state;
    let _this = this;

    updateEndUserDetails(id, payload)
      .then((response) => {

        if (response.data && response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            loading: false,
            errorMsg: "",
            isUpdateData: false,
            is_change: false,
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } else {
          
          this.setState({
            errorMsg: response.data.msg,
            loading: false,
            successMsg: "",
            isUpdateData: false,
            is_change: false,
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        console.log("error => ", err.response);
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isUpdateData: false,
          is_change: false,
        });
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  /**
   * Handle cancel modal
   */
  handleCancelModal = (type) => {
    const { updatedArray, field_name, branch_id, loan_type } = this.state;
    const {branches, loan_types} = this.props;

    if (type == 1) {
      this.setState({ isCheckReason: false });
    } else if (type == 2) {
      var fieldName = field_name;
      let fieldKey = this.lowercaseFirstLetter(fieldName);
      let changedFieldKey = fieldKey.replace(" ", "_");

      if (Object.keys(updatedArray).includes(changedFieldKey)) {

        this.setState({
          [changedFieldKey]: updatedArray[changedFieldKey],
        });

      }

      this.setState({ isUpdateData: false });
    } else if (type == 3) {
      this.setState({ isChangeAnyData: false });
    }
  };

  /**
   * Convert first letter to uppercase
   */
  capitalizeFirstLetter = (string) => {
    let str = string.split(" ");

    for (var i = 0, x = str.length; i < x; i++) {
      str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }

    return str.join(" ");
  };

  /**
   * Convert first letter to lowercase
   */
  lowercaseFirstLetter = (string) => {
    let str = string.split(" ");

    for (var i = 0, x = str.length; i < x; i++) {
      str[i] = str[i][0].toLowerCase() + str[i].substr(1);
    }

    return str.join(" ");
  };

  /**
   * Handle confirm
   */
  handleUpdateConfirm = () => {
    const { is_select, reason_for_change } = this.state;
    let updateEndUserErrors = {};
    if (reason_for_change == "") {
      updateEndUserErrors.reason_for_change =
        END_USER_FORM_VALIDATIONS.REASON_CHANGE;

      this.setState({ updateEndUserErrors });
    } else {
      this.editNewEndUser();
    }
  };

  
  render() { 
    const {
      sale_agents,
      team_leads,
      languages,
      loan_types,
      branches,
    } = this.props;
    const {
      first_name,
      last_name,
      registered_mobile,
      loan_account_number,
      loan_amount,
      alternate_phone,
      preferred_langauge,
      loan_type,
      branch_id,
      sale_agent_id,
      team_leader,
      registeration_trigger,
      verification_trigger,
      addEndUserErrors,
      successMsg,
      errorMsg,
      loading,
      team_leader_contact,
      sales_agent_contact,
      isFatching,
      reason_for_change,
      isUpdateData,
      isCheckReason,
      isChangeAnyData,
      field_name,
      field_value,
      old_field_value,
      is_select,
      branch_name,
      updateEndUserErrors,
      middle_name,
      loan_tenure,
      loan_emi
    } = this.state;

    return (
      <Fragment>
        <UpdateEndUser
          loan_types={loan_types}
          languages={languages}
          team_leads={team_leads}
          sale_agents={sale_agents}
          branches={branches}
          handleInputValues={this.handleInputValues}
          first_name={first_name}
          last_name={last_name}
          middle_name={middle_name}
          errorMsg={errorMsg}
          successMsg={successMsg}
          registered_mobile={registered_mobile}
          loan_account_number={loan_account_number}
          loan_amount={loan_amount}
          alternate_phone={alternate_phone}
          preferred_langauge={preferred_langauge}
          loan_type={loan_type}
          branch_id={branch_id}
          branch_name={branch_name}
          sale_agent_id={sale_agent_id}
          team_leader={team_leader}
          registeration_trigger={registeration_trigger}
          verification_trigger={verification_trigger}
          handleEditEndUser={this.handleEditEndUser}
          addEndUserErrors={addEndUserErrors}
          loading={loading} 
          team_leader_contact={team_leader_contact}
          sales_agent_contact={sales_agent_contact}
          reason_for_change={reason_for_change}
          handleReasonValues={this.handleReasonValues}
          handleonBlur={this.handleonBlur}
          isFatching={isFatching}
          loan_tenure={loan_tenure}
          loan_emi={loan_emi}
        />

        {isUpdateData && (
          <CModal
            show={isUpdateData}
            onClose={() => this.handleCancelModal(2)}
            color="warning"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "#565151" }}>
                Update Confirmation
              </CModalTitle>
            </CModalHeader>
            <CModalBody>
              <p>
                Are you sure to update this field, please select reason for
                change !{" "}
              </p>
              <span>
                <CForm
                  action=""
                  method="post"
                  encType="multipart/form-data"
                  className="form-horizontal"
                >
                  <CFormGroup row>
                    <CCol md="6">
                      <CLabel htmlFor="first_name">Old {field_name}</CLabel>
                      <CInput
                        id="field_value"
                        name="field_value"
                        value={old_field_value}
                      />
                    </CCol>
                    <CCol md="6">
                      <CLabel htmlFor="first_name">New {field_name}</CLabel>
                      <CInput
                        id="field_value"
                        name="field_value"
                        value={field_value}
                      />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol xs="12" md="12">
                      <CLabel htmlFor="reason_for_change">
                        Reason For Change
                        <small className="required_lable">*</small>
                      </CLabel>
                      <CSelect
                        custom
                        name="reason_for_change"
                        id="reason_for_change"
                        invalid={updateEndUserErrors.reason_for_change}
                        onChange={this.handleReasonValues}
                        value={reason_for_change}
                      >
                        <option value="">Please select</option>
                        {REASONS ? (
                          REASONS.map((item) => (
                            <option
                              value={item.reason}
                            >{`${item.reason}`}</option>
                          ))
                        ) : (
                          <option value="">No Reasons</option>
                        )}
                      </CSelect>
                      <CInvalidFeedback>
                        {updateEndUserErrors.reason_for_change}
                      </CInvalidFeedback>
                    </CCol>
                  </CFormGroup>
                </CForm>
              </span>
            </CModalBody>
            <CModalFooter>
              <CButton
                style={{
                  backgroundColor: "#e6af39",
                  borderColor: "#e6af39",
                  color: "#4f5d73",
                }}
                onClick={this.handleUpdateConfirm}
                //  disabled={is_select ? false:true}
              >
                Update
                {loading && <InvertContentLoader />}
              </CButton>{" "}
              <CButton
                color="secondary"
                onClick={() => this.handleCancelModal(2)}
              >
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}

        {isCheckReason && (
          <CModal
            show={isCheckReason}
            onClose={() => this.handleCancelModal(1)}
            color="warning"
            // style={{ color: "#e6af39" }}
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "#565151" }}>
                Reason Confirmation
              </CModalTitle>
            </CModalHeader>
            <CModalBody>
              <p>Please select Reason For Change to update this record ! </p>
            </CModalBody>
            <CModalFooter>
              <CButton
                color="secondary"
                onClick={() => this.handleCancelModal(1)}
              >
                Ok
              </CButton>{" "}
              {/* <CButton color="secondary" onClick={this.handleCancelReasonModal}>
                  Cancel
                </CButton> */}
            </CModalFooter>
          </CModal>
        )}

        {isChangeAnyData && (
          <CModal
            show={isChangeAnyData}
            onClose={() => this.handleCancelModal(3)}
            color="warning"
            // style={{ color: "#e6af39" }}
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "#565151" }}>
                Update Confirmation
              </CModalTitle>
            </CModalHeader>
            <CModalBody>
              <p>No Changes were Noted ! </p>
            </CModalBody>
            <CModalFooter>
              <CButton
                color="secondary"
                onClick={() => this.handleCancelModal(3)}
              >
                Ok
              </CButton>{" "}
              {/* <CButton color="secondary" onClick={this.handleCancelReasonModal}>
                Cancel
              </CButton> */}
            </CModalFooter>
          </CModal>
        )}
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    languages: state.fetchEndUserReducer.languages,
    loan_types: state.fetchEndUserReducer.loan_types,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: () => dispatch(fetchLanguages()),
    fetchLoanTypes: () => dispatch(fetchLoanTypes()),
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
    getBranches: () => dispatch(getBranches()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEndUser);
