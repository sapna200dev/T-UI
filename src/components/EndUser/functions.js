
import { getLoggedInUser, getUserObj } from '../Comman/functions';
import {END_USER_FORM_VALIDATIONS} from './constant'


let role='';
let user='';

/**
 * Handle request
 * @param  props
 */
export const validateRequestParams = (props) => {
  const addEndUserErrors = {};
  const {
      first_name,
      last_name,
      registered_mobile,
      loan_account_number,
      loan_amount,
      alternate_phone,
      sale_agent_id,
      sales_agent_contact,
      team_leader_contact,
      reason_for_change,
      loan_tenure,
      loan_emi,
      preferred_langauge,
      loan_type,
      branch_id
  } = props;
  
  let role='';
  // Retrive data from storage
  const user_role = getUserObj("role");

  if (user_role) {
    role = JSON.parse(user_role);
  } // ENd if

    if (first_name.length < 1) {
      addEndUserErrors.first_name = END_USER_FORM_VALIDATIONS.FIRST_NAME;
    } else if (first_name.length < 2 || first_name.length > 10) {
        addEndUserErrors.first_name = END_USER_FORM_VALIDATIONS.VALID_CHARS_LENGTH;
    } else if (!/^[a-zA-Z]*$/g.test(first_name)) {
      addEndUserErrors.first_name = END_USER_FORM_VALIDATIONS.VALID_CHARS;
    }   else if (last_name.length < 1) {
      addEndUserErrors.last_name = END_USER_FORM_VALIDATIONS.LAST_NAME;
    } else if (last_name.length < 2 || last_name.length > 20) {
        addEndUserErrors.last_name = END_USER_FORM_VALIDATIONS.VALID_CHARS_LENGTH;
    } else if (!/^[a-zA-Z]*$/g.test(last_name))  {
      addEndUserErrors.last_name = END_USER_FORM_VALIDATIONS.VALID_CHARS;
    } else if (registered_mobile.length < 1 ) {
      addEndUserErrors.registered_mobile = END_USER_FORM_VALIDATIONS.REGISTERED_MOBILE;
    }  else if (registered_mobile.length < 10 && registered_mobile.length > 11 ) {
      addEndUserErrors.registered_mobile = END_USER_FORM_VALIDATIONS.REGISTERED_MOBILE_DIGITS;
    }  else if (sales_agent_contact && sales_agent_contact.length < 1) {
      addEndUserErrors.sales_agent_contact = END_USER_FORM_VALIDATIONS.REGISTERED_MOBILE;
    } else if (team_leader_contact && team_leader_contact.length < 1) {
      addEndUserErrors.team_leader_contact = END_USER_FORM_VALIDATIONS.REGISTERED_MOBILE;
    } else if (loan_account_number.length < 1) {
      addEndUserErrors.loan_account_number = END_USER_FORM_VALIDATIONS.LOAN_ACCOUNT_NUMBER;
    } else if (loan_amount.length < 1) { 
      addEndUserErrors.loan_amount = END_USER_FORM_VALIDATIONS.LOAN_AMOUNT;
    }  else if (!/^[0-9]+$/.test(loan_amount)) {
      addEndUserErrors.loan_amount = END_USER_FORM_VALIDATIONS.LOAN_AMOUNT_VALID;
    }  else if (loan_tenure.length < 1) {
      addEndUserErrors.loan_tenure = END_USER_FORM_VALIDATIONS.LOAN_AMOUNT;
    }  else if (!/^[0-9]+$/.test(loan_tenure)) {
      addEndUserErrors.loan_tenure = END_USER_FORM_VALIDATIONS.LOAN_TENURE_VALID;
    } else if (loan_emi.length < 1) {
      addEndUserErrors.loan_emi = END_USER_FORM_VALIDATIONS.LOAN_AMOUNT;
    } else if (!/^[0-9]+$/.test(loan_emi)) {
      addEndUserErrors.loan_emi = END_USER_FORM_VALIDATIONS.LOAN_EMI_VALID;
    }  else if (preferred_langauge.length < 1 ) {
      addEndUserErrors.preferred_langauge = END_USER_FORM_VALIDATIONS.LANGUAGE;
    }  else if (loan_type.length < 1 ) {
      addEndUserErrors.loan_type = END_USER_FORM_VALIDATIONS.LOAN_TYPE_ID;
    }   else if (branch_id.length < 1 ) {
      addEndUserErrors.branch_id = END_USER_FORM_VALIDATIONS.BRANCH_NAME;
    } 
    
    
    else if (!/^[0-9]+$/.test(alternate_phone)) {
      addEndUserErrors.alternate_phone = END_USER_FORM_VALIDATIONS.VALID_MOBILE;
    } 
    if(role.name != 'sales_agent') {
      if (sale_agent_id.length < 1) {
        addEndUserErrors.sale_agent_id = END_USER_FORM_VALIDATIONS.ASSIGN_SALE_AGENT;
      } 
    }


    return addEndUserErrors;
  };

/**
 * Update request validate payload
 * @param props 
 */
  export const validateUpdateRequest = (props) => {
    const updateEndUserErrors = {};
    const {
        reason_for_change
    } = props;

    if (reason_for_change.length < 1) {
      updateEndUserErrors.reason_for_change = END_USER_FORM_VALIDATIONS.REASON_CHANGE;
    } 

    return updateEndUserErrors;
  }


/**
 * Create request payload
 * @param props 
 */
  export const CreateReuqestPayload = (props, sale_agents) => {
    const {
        first_name,
        last_name,
        registered_mobile,
        loan_account_number,
        loan_amount,
        alternate_phone,
        preferred_langauge, 
        loan_type,
        branch_id,
        team_leader,
        registeration_trigger, 
        verification_trigger,
        sales_agent_contact,
        team_leader_contact,
        reason_for_change,
        sale_agent_id,
        middle_name,
        loan_emi,
        loan_tenure
      } = props;

      let sale_user_id1='';
      const user_role = getUserObj("role");
 
      if (user_role) {
        role = JSON.parse(user_role);
      } // ENd if

      if(role.name == 'sales_agent') {
        sale_agents && sale_agents.map((item, index) => {
  
          console.log('load sales_agent_contact  = ', sales_agent_contact)
          if(item.mobile_number == sales_agent_contact ) {
            sale_user_id1 = item.id
          }
        })
      }

      const payload = {
        first_name,
        last_name,
        registered_mobile,
        loan_account_number,
        loan_amount,
        alternate_phone:'1234567896',
        preferred_langauge_id: parseInt(preferred_langauge) ,
        loan_type_id:parseInt(loan_type),
        branch_id : parseInt(branch_id),
        sale_agent_id : sale_user_id1 != '' ? sale_user_id1: parseInt(sale_agent_id),
        team_leader_id : parseInt(team_leader),
        registeration_trigger,
        verification_trigger,
        sales_agent_contact,
        team_leader_contact,
        reason_for_change,
        middle_name,
        loan_emi,
        loan_tenure
      }

      return payload;
  }