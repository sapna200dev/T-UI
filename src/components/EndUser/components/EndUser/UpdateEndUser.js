import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ContentLoading } from "../../../Comman/components";

const error_msg = {
  fontSize: "10px",
  color: "#524444",
  fontFamily: "Roboto",
};

const UpdateEndUser = (props) => {
  const {
    sale_agents,
    team_leads,
    languages,
    loan_types,
    handleInputValues,
    first_name,
    last_name,
    registered_mobile,
    loan_account_number,
    loan_amount,
    alternate_phone,
    preferred_langauge,
    loan_type,
    branch_id,
    sale_agent_id,
    team_leader,
    handleEditEndUser,
    addEndUserErrors,
    branches,
    errorMsg,
    successMsg,
    loading,
    team_leader_contact,
    sales_agent_contact,
    isFatching,
    middle_name,
    handleonBlur,
    loan_tenure,
    loan_emi
  } = props;


  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> Edit End User</b>
          </h4>
        </CCardHeader>
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody> 

       
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
              {isFatching && <ContentLoading content="Fetching record.." />}
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="First Name"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.first_name}
                  onBlur={handleonBlur}
                  className="view-text-color"
                />
                <CInvalidFeedback>
                  {addEndUserErrors.first_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="middle_name">
                  Middle Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="middle_name"
                  name="middle_name"
                  value={middle_name}
                  placeholder=" Middle Name "
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.middle_name}
                  onBlur={handleonBlur}
                  className="view-text-color"
                />
                <CInvalidFeedback>
                  {addEndUserErrors.last_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder=" Last Name "
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.last_name}
                  onBlur={handleonBlur}
                  className="view-text-color"
                />
                <CInvalidFeedback>
                  {addEndUserErrors.last_name}
                </CInvalidFeedback>
              </CCol>
             
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="registered_mobile">
                  Registered Mobile <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="registered_mobile"
                  name="registered_mobile"
                  value={registered_mobile}
                  placeholder="Registered Mobile"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.registered_mobile}
                  onBlur={handleonBlur}
                  className="view-text-color  detail-text-color"
                />
                <CInvalidFeedback>
                  {addEndUserErrors.registered_mobile}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_account_number">
                  Loan Account Number{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_account_number"
                  name="loan_account_number"
                  value={loan_account_number}
                  placeholder="Loan Account Number"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_account_number}
                  onBlur={handleonBlur}
                  className="view-text-color"
                />
                <CInvalidFeedback>
                  {addEndUserErrors.loan_account_number}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_amount">
                  Loan Amount <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_amount"
                  name="loan_amount"
                  value={loan_amount}
                  placeholder=" Loan Amount"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_amount}
                  onBlur={handleonBlur}
                  className="view-text-color "
                />
                <CInvalidFeedback>
                  {addEndUserErrors.loan_amount}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>






            <CFormGroup row>
                <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_tenure">Loan Tenure ( months ) <small className="required_lable">*</small> </CLabel>
                  <CInput
                    id="loan_tenure"
                    name="loan_tenure"
                    value={loan_tenure}
                    placeholder="Loan Tenure"
                    onChange={handleInputValues}
                    invalid={addEndUserErrors.loan_tenure}
                    onBlur={handleonBlur}
                    // className="detail-text-color"
                  />
                  {/* <span style={error_msg}>We can't add alternate number !</span> */}
                  <CInvalidFeedback>
                    {addEndUserErrors.loan_tenure}
                  </CInvalidFeedback>
              </CCol>
              <CCol md="3">
              <CLabel htmlFor="loan_emi">Loan EMI <small className="required_lable">*</small></CLabel> 
                <CInput
                  id="loan_emi"
                  name="loan_emi"
                  value={loan_emi}
                  placeholder="Loan EMI"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_emi}
                  onBlur={handleonBlur}
                  // className="detail-text-color"
                />
               
                <CInvalidFeedback>
                  {addEndUserErrors.loan_emi}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
              <CLabel htmlFor="alternate_phone">Alternate Phone</CLabel>
                <CInput
                  id="alternate_phone"
                  name="alternate_phone"
                  value={'xxxxxxxxxx'}
                  placeholder="Alternate Phone"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.alternate_phone}
                  onBlur={handleonBlur}
                  className="view-text-color detail-text-color"
                  
                  
                />
                 {/* <span style={error_msg}>We can't update alternate number !</span> */}
                <CInvalidFeedback>
                  {addEndUserErrors.alternate_phone}
                </CInvalidFeedback>
              </CCol>
             
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
                <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">

                <CLabel htmlFor="preferred_langauge">Preferred Language</CLabel>
                  <CSelect
                    custom
                    name="preferred_langauge"
                    id="preferred_langauge"
                    invalid={addEndUserErrors.preferred_langauge}
                    onChange={handleInputValues}
                    value={preferred_langauge}
                    onBlur={handleonBlur}
                    className="view-text-color"
                  >
                    <option value="">Please select</option>
                    {languages ? (
                      languages.map((item) => (
                        <option value={item.id}>{item.language}</option>
                      ))
                    ) : (
                      <option value="">No Languages</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.first_name}
                  </CInvalidFeedback>



              </CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_type_id">Loan Type</CLabel>
                  <CSelect
                    custom
                    name="loan_type"
                    id="loan_type"
                    invalid={addEndUserErrors.loan_type_id}
                    onChange={handleInputValues}
                    value={loan_type}
                    onBlur={handleonBlur}
                    className="view-text-color"
                  >
                    <option value="">Please select</option>
                    {loan_types ? (
                      loan_types.map((item) => (
                        <option value={item.id}>{item.loan_type}</option>
                      ))
                    ) : (
                      <option value="">No Loan Types</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.loan_type}
                  </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">Branch</CLabel>
                  <CSelect
                    custom
                    name="branch_id"
                    id="branch_id"
                    invalid={addEndUserErrors.branch_id}
                    onChange={handleInputValues}
                    value={branch_id}
                    onBlur={handleonBlur}
                    className="view-text-color"
                  >
                    <option value="">Please select</option>
                    {branches ? (
                      branches.map((item) => (
                        <option value={item.id}>{item.branch_name}</option>
                      ))
                    ) : (
                      <option value="">No Branch</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.branch_id}
                  </CInvalidFeedback>
              </CCol>
             
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                  <CLabel htmlFor="sale_agent_id">
                      Sales Agent <small className="required_lable">*</small>
                    </CLabel>
                    <CSelect
                      custom
                      name="sale_agent_id"
                      id="sale_agent_id" 
                      invalid={addEndUserErrors.sale_agent_id}
                      onChange={handleInputValues}
                      value={sale_agent_id}
                      onBlur={handleonBlur}
                      className="view-text-color"
                    >
                      <option value="">Please select</option>
                      {sale_agents ? (
                        sale_agents.map((item) => (
                          <option
                            value={item.id}
                            selected={item.id === sale_agent_id ? "selected" : ""}
                          >{`${item.first_name}  ${item.last_name}`}</option>
                        ))
                      ) : (
                        <option value="">No Sales Agents</option>
                      )}
                    </CSelect>
                    <CInvalidFeedback>
                      {addEndUserErrors.sale_agent_id}
                    </CInvalidFeedback>

              </CCol>
              <CCol md="3">
                <CLabel htmlFor="sales_agent_contact">
                    Sales Agent Contact <small className="required_lable">*</small>
                  </CLabel>
                  <CInput
                    id="sales_agent_contact"
                    name="sales_agent_contact"
                    value={sales_agent_contact}
                    placeholder="Sales Agent Contact"
                    onChange={handleInputValues}
                    invalid={addEndUserErrors.sales_agent_contact}
                    onBlur={handleonBlur}
                    style={{pointerEvents:'none'}}
                    className="view-text-color"
                  />
                  <CInvalidFeedback>
                    {addEndUserErrors.sales_agent_contact}
                  </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                {/* <CLabel htmlFor="team_leader">
                    Team Leader <small className="required_lable">*</small>
                  </CLabel>
                  <CSelect
                    custom
                    name="team_leader"
                    id="team_leader"
                    invalid={addEndUserErrors.team_leader}
                    onChange={handleInputValues}
                    value={team_leader}
                    onBlur={handleonBlur}
                    className="view-text-color"
                  >
                    <option value="">Please select</option>
                    {team_leads ? (
                      team_leads.map((item) => (
                        <option
                          value={item.id}
                        >{`${item.first_name}  ${item.last_name}`}</option>
                      ))
                    ) : (
                      <option value="">No Team Leads</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.team_leader}
                  </CInvalidFeedback> */}
                </CCol>

              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                {/* <CLabel htmlFor="team_leader_contact">
                    Team Leader Contact <small className="required_lable">*</small>
                  </CLabel>
                  <CInput
                    id="team_leader_contact"
                    name="team_leader_contact"
                    value={team_leader_contact}
                    placeholder="Team Leader Contact"
                    onChange={handleInputValues}
                    invalid={addEndUserErrors.team_leader_contact}
                    onBlur={handleonBlur}
                    style={{pointerEvents:'none'}}
                    className="view-text-color"
                  />
                  <CInvalidFeedback>
                    {addEndUserErrors.team_leader_contact}
                  </CInvalidFeedback> */}
              </CCol>
              <CCol md="3">

              </CCol>


              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter>
        <span className="buttonRight">
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleEditEndUser}
          >
            <CIcon name="cil-scrubber" /> Update
            {/* {loading && <InvertContentLoader />} */}
          </CButton>
          <a href="#/end-user" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
          </span>
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};

export default UpdateEndUser;
