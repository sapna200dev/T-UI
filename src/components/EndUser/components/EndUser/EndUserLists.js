import React, { Fragment } from "react";
import ReactDatatable from '@ashvin27/react-datatable';
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip,
  CLabel,
  CSelect
} from "@coreui/react";
import { FIELDS, COULUMN_NAMES, PAGINATION_COUNT , config} from "../../constant";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile", 
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

const EndUserLists = (props) => {
  const {
    endUsers,
    isLoading,
    handleDeleteItem,
    successMsg,
    errorMsg,
    handleFilterChange,
    searchValue,
    sort_by,
    handleInputValues,
    pagination_count
  } = props;


  const columns = [
    {
      key: "name",
      text: "Name",
      className: "name",
      align: "left",
      sortable: true,
    },
    {
      key: "loan_amount",
      text: "Loan Amount",
      className: "loan_amount",
      align: "left",
      sortable: true
    },
    {
      key: "registered_mobile",
      text: "Registered mobile",
      className: "registered_mobile",
      align: "left",
      sortable: true
    },
    {
      key: "current_status",
      text: "Current state",
      className: "current_status",
      align: "left",
      sortable: true
    },
    {
      key: "call_status",
      text: "Status",
      className: "call_status",
      sortable: true,
      align: "left"
    },
    {
      key: "language_type",
      text: "Preferred language",
      className: "language_type",
      sortable: true,
      align: "left"
    },

    {
      key: "view_more",
      text: "View",
      className: "view_more",
      sortable: false,
      align: "left",
      cell: record => {
        return (
          <Fragment>

            <a
              href={`#/end-user-view/${record.id}`}
              className="cancel_bt"
            >
              <CButton
                type="submit"
                size="sm"
                color="primary"
                className="remove_button view_btn"
              >
                <CTooltip content={`View Record`} placement={"top-start"}>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </CTooltip>
                {/* View */}
              </CButton>
            </a>
          </Fragment>
        )
      }
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 100,
      align: "left",
      sortable: false,
      cell: record => {
        return (
          <Fragment>
            <a
              href={`#/end-user-edit/${record.id}`}
              className="cancel_bt"
            >
              <CTooltip
                content={`Edit Record`}
                placement={"top-start"}
              >
                <CButton type="submit" size="sm" color="primary">
                  <i className="fa fa-edit" aria-hidden="true"></i>
                  {/* Edit */}
                  {/* {loading && <ContentLoading />} */}
                </CButton>
              </CTooltip>


            </a>

            {/* <button
                      className="btn btn-primary btn-sm"
                      onClick={() => {
                          console.log('record = ', record)
                      }}
                      style={{marginRight: '5px'}}>
                      <i className="fa fa-edit"></i>
                  </button> */}

            <CButton
              type="submit"
              size="sm"
              color="danger"
              className="remove_button"
              onClick={() => handleDeleteItem(record)}
            >
              <CTooltip content={`Delete Record`} placement={"top-start"}>
                <i className="fa fa-trash" aria-hidden="true"></i>
              </CTooltip>
              {/* Delete */}
            </CButton>

          </Fragment>
        );
      }
    }
  ];

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> End Users</b>
          <span >



            <a href="#/end-user-add" className="button_style1">
              <CButton
                type="submit"
                size="sm"
                color="primary"
              // className="add_employee_button"
              // onClick={() => handleDeleteItem(item)}
              >
                {" "}
                <i className="fa fa-plus" aria-hidden="true"></i> End User
                </CButton>
            </a>
            {/* <span className="pagination_count">
            <CLabel htmlFor="last_name">
            Per page -
                </CLabel> {"   "}
          <CSelect
            custom
            name="pagination_count"
            id="pagination_count"
            width={50}
            onChange={(e, result) => handleInputValues(e, result, 'pagination')}
            value={pagination_count}
            className="view-text-color showing_pagination_count "
          >
            <option value="" style={{display:'none'}}>Please select</option>
            {PAGINATION_COUNT && PAGINATION_COUNT.length > 0 ? (
              PAGINATION_COUNT.map((item) => (
                <option value={item.value}  >{item.text} </option>
              ))
            ) : (
              <option value="">No Branches</option>
            )}
          </CSelect>
            </span> */}

          </span>
        </CCardHeader>
        <span className="sorting_lebal">
          {/* <CLabel htmlFor="last_name">
            Sort By -
                </CLabel> {"   "}
          <CSelect
            custom
            name="sort_by"
            id="sort_by"
            width={50}
            onChange={(e, result) => handleInputValues(e, result, 'sort')}
            value={sort_by}
            className="view-text-color showing_column"
          >
            <option value="">Please select</option>
            {COULUMN_NAMES && COULUMN_NAMES.length > 0 ? (
              COULUMN_NAMES.map((item) => (
                <option value={item.value}  >{item.text} {item.value == sort_by ? ' - ( Selected ) ' : ''}  </option>
              ))
            ) : (
              <option value="">No Branches</option>
            )}
          </CSelect> */}
          {/* <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />
 

            </div> */}
        </span>
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>

        <ReactDatatable
            config={config}
            records={endUsers}
            columns={columns}
            extraButtons={[]}
            loading={isLoading}
            // tHeadClassName={'custom-table-head'}
          />
          {/* <CDataTable
              items={endUsers}
              fields={FIELDS}
              itemsPerPage={pagination_count}
              loading={isLoading}
              pagination
              scopedSlots={{
                current_state: (item) => <td>{item.current_status}</td>,
                status: (item) => <td>{item.call_status}</td>,
                preferred_language: (item) => <td>{item.language_type}</td>,
                Action: (item) => (
                  <td> 
                    <a
                      href={`#/end-user-edit/${item.id}`}
                      className="cancel_bt"
                    >
                      <CTooltip
                        content={`Edit Record`}
                        placement={"top-start"}
                      >
                        <CButton type="submit" size="sm" color="primary">
                          <i className="fa fa-edit" aria-hidden="true"></i>

                        </CButton>
                      </CTooltip>

                     
                    </a>

                    <CButton
                      type="submit"
                      size="sm"
                      color="danger"
                      className="remove_button"
                      onClick={() => handleDeleteItem(item)}
                    >
                        <CTooltip content={`Delete Record`} placement={"top-start"}>
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </CTooltip>

                    </CButton>
                    <a
                      href={`#/end-user-view/${item.id}`}
                      className="cancel_bt"
                    >
                      <CButton
                        type="submit"
                        size="sm"
                        color="primary"
                        className="remove_button"
                      >
                        <CTooltip content={`View Record`} placement={"top-start"}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </CTooltip>
    
                      </CButton>
                    </a>
                  </td>
                ),
              }}
            /> */}
        </CCardBody>

      </CCard>
    </Fragment>
  );
};

export default EndUserLists;
