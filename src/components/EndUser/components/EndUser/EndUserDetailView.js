import React, { Fragment, useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CAlert,
  CToast,
  CToastBody,
  CToastHeader,
  CToaster,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ContentLoading } from "../../../Comman/components";

import { PlayAudio } from "../../../IVRSettings/componenets";
import { CallActivityDetails } from "./CallActivityDetails";
import { ProvisionedLogs } from "./ProvisionedLogs";
import {call_portals} from '../../constant'

const disabledCss = {
  display: "grid",
};

const toast_header = {
  backgroundColor: "rgb(86 140 208)",
  color: "antiquewhite",
  bordercolor: " cornflowerblue",
};

const EndUserDetailView = (props) => {
  const {
    sale_agents,
    team_leads,
    branches,
    languages,
    loan_types,
    first_name,
    last_name,
    registered_mobile,
    loan_account_number,
    loan_amount,
    alternate_phone,
    preferred_langauge,
    loan_type,
    branch_id,
    sale_agent_id,
    team_leader_id,
    call_status,
    current_status,
    call_action,
    isFatching,
    sales_agent,
    team_leader,
    user_logs,
    errorMsg,
    handleCallStand,
    sales_agent_contact,
    middle_name,
    team_leader_contact,
    call_portal,
    call_msg,
    call_initiating,
    handleCloseCallToast,
    isError,
    call_details,
    getLiveUserCallDetails,
    refreshing_call_info,
    fatchingLogs,
    is_registration,
    verification_status,
    registration_status,
    playAudio1,
    playAudio2,
    playAudio3,
    handleAudioPlay,
    playAudioFirst,
    playAudioSecond,
    playAudioThird,
    firstFileName,
    secondFileName,
    thirdFileName,
    firstFileExtension,
    secondFileExtension,
    thirdFileExtension,
    downloadRecording,
    handleResetAllEvent,
    successMsg,
    isDisabledButton,
    call_status_code,
    callHandling,
    display_msg_header,
    team_leader_name,
    sales_agent_name,
    loan_tenure,
    loan_emi,
    call_score
  } = props;

  const [accordion, setAccordion] = useState(""); 

  const setAccordionState = (index) => {
    setAccordion(accordion === index ? null : index);
  };

  return (
    <Fragment> 
      {display_msg_header && (
        <div className="call_message ">
          <div className="alert alert-danger alert_css ">
          <marquee className="stop" behavior="scroll" direction="left" scrollamount="5"><strong>Please !</strong> Do not refresh the page till call ends.</marquee>
            {/* <span>
            <strong>Please !</strong> Do not refresh the page till call ends.
            </span> */}
          </div>
        </div>
       )} 
       

      <CCard>
        <CCardHeader>
          <h4>
            <b> End User Details</b>
          </h4>
        </CCardHeader>
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}

        {call_initiating && (
          <CToaster position={"top-right"} style={{ marginTop: "46px" }}>
            <CToast
              show={true}
              // autohide={50000}
              fade={false}
              // className={
              //   isSuccess
              //     ? "success_msg"
              //     : isError
              //     ? "error_class"
              //     : "toast_class"
              // }
              className={
                call_status_code == 0
                  ? "toast_class"
                  : call_status_code == 1
                  ? "call_in_progress"
                  : call_status_code == 2
                  ? "call_success"
                  : call_status_code == 3 
                  ? "call_fail"
                  : call_status_code == 4
                  ? "call_verify"
                  : ""
              }
              // style={isError ? {backgroundColor:'rgb(210 87 40)', color:'antiquewhite'} : {backgroundColor:'rgb(86 140 208)', color:'antiquewhite'}}
            >
              <CToastHeader
                onClick={handleCloseCallToast}
                closeButton={false}
                // className={
                //   isSuccess
                //     ? "success_msg"
                //     : isError
                //     ? "error_class"
                //     : "toast_class"
                // }
                className={
                  call_status_code == 0
                    ? "toast_class"
                    : call_status_code == 1
                    ? "call_in_progress"
                    : call_status_code == 2
                    ? "call_success"
                    : call_status_code == 3
                    ? "call_fail"
                    : call_status_code == 4
                    ? "call_verify"
                    : ""
                }
                // style={isError ? {backgroundColor:'rgb(210 87 40)', color:'antiquewhite'} : toast_header}
              >
                <span>Call Status</span>
              </CToastHeader>
              <CToastBody>
                <span>
                  {" "}
                  {call_msg != "" ? (
                    <span>
                      {call_msg.replace("-", "  ")}{" "}
                      {(call_status_code == 1 || call_status_code == 4) && (
                        <span>
                          <span className="dot one">.</span>
                          <span className="dot two">.</span>
                          <span className="dot three">.</span>
                        </span>
                      )}
                    </span>
                  ) : (
                    <Fragment>
                      { "Call initiating... "}
                      <span>
                        {" "}
                        {registered_mobile != null
                          ? registered_mobile
                          : alternate_phone}
                      </span>
                    </Fragment>
                  )}
                </span>
              </CToastBody>
            </CToast>
          </CToaster>
        )}
        <CCardBody>
          {call_status_code == 1 && (
            <span>
              <div className="alert alert-success">
                <strong className="loading-dots">
                  <span>
                    {`${is_registration ==1 ? 'Verification in progress' : 'Registration in progress'}` } <span className="dot one">.</span>
                    <span className="dot two">.</span>
                    <span className="dot three">.</span>
                  </span>
                </strong>
              </div>
            </span>
          )}
          {call_status_code == 4 && (
            <span>
              <div className="alert alert-info"> 
                <strong className="loading-dots">
                  <span>
                    {/* {is_registration != 1 ? "Registration " : "Verification "}{" "}
                    call has been verifying  */}
                    {`Please wait... Data is being Verified`}
                    <span className="dot one">.</span>
                    <span className="dot two">.</span>
                    <span className="dot three">.</span>
                  </span>
                </strong>
              </div>
            </span>
          )}
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol md="3">
                {/* <ContentLoading content="Fetching record.." /> */}
                {(isFatching || refreshing_call_info) && (
                  <ContentLoading
                    content={
                      refreshing_call_info
                        ? "Please wait...Record Fetching !"
                        : "Fetching Record...!"
                    }
                  />
                )}

                {callHandling && <ContentLoading content={"Please wait..."} />}
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="First Name"
                  // disabled
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="middle_name">Middle Name</CLabel>
                <CInput
                  id="middle_name"
                  name="middle_name"
                  value={middle_name}
                  placeholder=" Middle Name "
                  // disabled
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder=" Last Name "
                  // disabled
                  className="detail-text-color"
                />
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="registered_mobile">
                  Registered Mobile <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="registered_mobile"
                  name="registered_mobile"
                  value={registered_mobile}
                  placeholder="Registered Mobile"
                  // disabled
                  className="detail-text-color"
                />
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_account_number">
                  Loan Account Number{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_account_number"
                  name="loan_account_number"
                  value={loan_account_number}
                  placeholder="Loan Account Number"
                  // disabled
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_amount">
                  Loan Amount <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_amount"
                  name="loan_amount"
                  value={loan_amount}
                  placeholder=" Loan Amount"
                  // disabled
                  className="detail-text-color"
                />
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>







            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
              <CLabel htmlFor="loan_tenure">Loan Tenure  ( months ) <small className="required_lable">*</small> </CLabel>
                  <CInput
                    id="loan_tenure"
                    name="loan_tenure"
                    value={loan_tenure}
                    placeholder="Loan Tenure"
                    className="detail-text-color"
                  />
              </CCol>
              <CCol md="3">
              <CLabel htmlFor="loan_emi">Loan EMI <small className="required_lable">*</small></CLabel> 
                <CInput
                  id="loan_emi"
                  name="loan_emi"
                  value={loan_emi}
                  placeholder="Loan EMI"
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">Alternate Phone</CLabel>
                  <CInput
                    id="alternate_phone"
                    name="alternate_phone"
                    value={"xxxxxxxxxx"}
                    placeholder="Alternate Phone"
                    // disabled
                    className="detail-text-color"
                  />
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>


            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="preferred_langauge">Preferred Language</CLabel>
                  <CSelect
                    custom
                    name="preferred_langauge"
                    id="preferred_langauge"
                    // disabled
                    className="detail-text-color"
                    value={preferred_langauge}
                  >
                    <option value="">Please select</option>
                    {languages ? (
                      languages.map((item) => (
                        <option value={item.id}>{item.language}</option>
                      ))
                    ) : (
                      <option value="">No Languages</option>
                    )}
                  </CSelect>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_type">Loan Type</CLabel>
                  <CSelect
                    custom
                    name="loan_type"
                    id="loan_type"
                    value={loan_type}
                    // disabled
                    className="detail-text-color"
                  >
                    <option value="">Please select</option>
                    {loan_types ? (
                      loan_types.map((item) => (
                        <option value={item.id}>{item.loan_type}</option>
                      ))
                    ) : (
                      <option value="">No Loan Types</option>
                    )}
                  </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                  <CLabel htmlFor="alternate_phone">Branch</CLabel>
                    <CSelect
                      custom
                      name="branch_id"
                      id="branch_id"
                      // disabled
                      className="detail-text-color"
                      value={branch_id}
                    >
                      <option value="">Please select</option>
                      {branches ? (
                        branches.map((item) => (
                          <option value={item.id}>{item.branch_name}</option>
                        ))
                      ) : (
                        <option value="">No Branch</option>
                      )}
                    </CSelect>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                  <CLabel htmlFor="sale_agent_id">
                      Sales Agent<small className="required_lable">*</small>
                    </CLabel>
                    <CSelect
                      custom
                      name="sale_agent_id"
                      id="sale_agent_id"
                      // disabled
                      className="detail-text-color"
                      value={sale_agent_id}
                    >
                      <option value="">Please select</option>
                      {sale_agents ? (
                        sale_agents.map((item) => (
                          <option
                            value={item.id}
                            selected={item.id === sale_agent_id ? "selected" : ""}
                          >{`${item.first_name}  ${item.last_name}`}</option>
                        ))
                      ) : (
                        <option value="">No Sales Agents</option>
                      )}
                    </CSelect>
              </CCol>
              <CCol md="3">
                  <CLabel htmlFor="sales_agent_contact">
                      Sales Agent Contact{" "}
                      <small className="required_lable">*</small>
                    </CLabel>
                    <CInput
                      id="sales_agent_contact"
                      name="sales_agent_contact"
                      value={sales_agent_contact}
                      placeholder="Sales Agent Contact"
                      // disabled
                      className="detail-text-color"
                    />
              </CCol>
              <CCol md="3">
                  {/* <CLabel htmlFor="team_leader_id">
                      Team Leader <small className="required_lable">*</small>
                    </CLabel>
                    <CSelect
                      custom
                      name="team_leader_id"
                      id="team_leader_id"
                      value={team_leader_id}
                      // disabled
                      className="detail-text-color"
                    >
                      <option value="">Please select</option>
                      {team_leads ? (
                        team_leads.map((item) => (
                          <option
                            value={item.id}
                          >{`${item.first_name}  ${item.last_name}`}</option>
                        ))
                      ) : (
                        <option value="">No Team Leads</option>
                      )}
                    </CSelect> */}
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                {/* <CLabel htmlFor="team_leader_contact">
                    Team Leader Contact{" "}
                    <small className="required_lable">*</small>
                  </CLabel>
                  <CInput
                    id="team_leader_contact"
                    name="team_leader_contact"
                    value={team_leader_contact}
                    placeholder="Team Leader Contact"
                    // disabled
                    className="detail-text-color"
                  /> */}
              </CCol>
              <CCol md="3">
               
              </CCol>

              {/* <CCol md="3">
                <CLabel htmlFor="call_portal">
                  Portal For Call{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="call_portal"
                  id="call_portal"
                  // disabled
                  className="detail-text-color"
                  value={call_portal}
                >
                  <option value="">Please select</option>
                  {call_portals ? (
                    call_portals.map((item) => (
                      <option
                        value={item.value}
                        selected={item.value === call_portal ? "selected" : ""}
                      >{`${item.text}  `}</option>
                    ))
                  ) : (
                    <option value="">No Sales Agents</option>
                  )}
                </CSelect>
              
              </CCol> */}
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CCardFooter className="divider_layout"></CCardFooter>
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="current_status">Current State</CLabel>
                <CInput
                  id="current_status"
                  name="current_status"
                  value={current_status}
                  placeholder="Current Status"
                  // disabled
                  style={disabledCss}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="call_status">Call Status</CLabel>
                <CInput
                  type="call_status"
                  id="call_status"
                  name="call_status"
                  value={call_status}
                  placeholder="Call Status"
                  // disabled
                  style={disabledCss}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3" style={{ display: "grid" }}>
                <CLabel htmlFor="call_action">Call Action</CLabel>
                <CButton
                  type="reset"
                  size="sm"
                  color="primary" 
                  className="remove_button"
                  // disabled={
                  //   isDisabledButton
                  //     ? true
                  //     : is_verification == 1 && is_registration == 1
                  //     ? true
                  //     : false
                  // }

                   disabled={
                    isDisabledButton
                      ? true
                      : false 
                  }
                  onClick={handleCallStand}
                >
                  {call_action}
                </CButton>
              </CCol>
              <CCol xs="12" md="2" style={{ marginTop: "30px" }}>
                <CLabel htmlFor="call_action"></CLabel>
                <CButton
                  type="reset"
                  size="sm"
                  color="danger"
                  disabled={isDisabledButton ? true : false}
                  onClick={handleResetAllEvent}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                    className="c-icon c-icon-2xl"
                    role="img"
                    style={{ width: "1rem", height: "1rem" }}
                  >
                    <path
                      fill="var(--ci-primary-color, currentColor)"
                      d="M495.473,197.262c0-73.061-43.651-136.118-106.242-164.462q-2-.9-4.021-1.762-4.046-1.715-8.19-3.235-2.071-.761-4.167-1.47-4.19-1.422-8.468-2.64a180.951,180.951,0,0,0-98.675,0q-4.278,1.218-8.469,2.64-2.094.71-4.166,1.47-4.143,1.519-8.19,3.235-2.023.857-4.021,1.762c-62.592,28.344-106.242,91.4-106.242,164.462h0V434.745L38.627,338.75,16,361.377,150.623,496,285.246,361.377,262.619,338.75l-96,96V197.263c0-72.891,52.814-133.678,122.186-146.1a149.419,149.419,0,0,1,52.479,0c69.371,12.426,122.186,73.213,122.186,146.1h32Z"
                      className="ci-primary"
                    ></path>
                  </svg>
                  <span style={{ marginLeft: "10px" }}>Reset All</span>
                </CButton>
              </CCol>
            </CFormGroup>

            <CFormGroup row>
              {/* <CCol xs="12" md="1"></CCol>
              <CCol md="9">
                <button className="btn btn-danger btn-sm btn-block" type="button">Reset all call history at once</button>
              </CCol>
              <CCol xs="12" md="1"></CCol> */}
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter></CCardFooter>

        {/***************************  Show Call Activity Details   *****************************  */}
        <CallActivityDetails
          call_details={call_details}
          accordion={accordion}
          getLiveUserCallDetails={getLiveUserCallDetails}
          handleAudioPlay={handleAudioPlay}
          firstFileName={firstFileName}
          firstFileExtension={firstFileExtension}
          playAudioFirst={playAudioFirst}
          playAudio1={playAudio1}
          secondFileName={secondFileName}
          secondFileExtension={secondFileExtension}
          playAudioSecond={playAudioSecond}
          playAudio2={playAudio2}
          thirdFileName={thirdFileName}
          thirdFileExtension={thirdFileExtension}
          playAudioThird={playAudioThird}
          playAudio3={playAudio3}
          setAccordion={setAccordionState}
          sales_agent={sales_agent}
          team_leader={team_leader}
          isFatching={isFatching}
          refreshing_call_info={refreshing_call_info}
          isDisabledButton={isDisabledButton}
          call_status_code={call_status_code}
          call_msg={call_msg}
          sales_agent_name = {sales_agent_name}
          team_leader_name={team_leader_name}
          call_score={call_score}
          verification_status={verification_status}
          registration_status={registration_status}
        />

        {/**************************  Show Call Logs   *****************************  */}

        <ProvisionedLogs user_logs={user_logs} fatchingLogs={fatchingLogs} />

        <CCardFooter>
          
          <a
            href="#/end-user"
            className="button_style"
            style={isDisabledButton ? { pointerEvents: "none" } : {}}
          >
            <CButton
              type="reset"
              size="sm"
              color="primary"
              className="remove_button"
              disabled={isDisabledButton ? true : false}
            >
              <CIcon name="cil-ban" /> Go Back
            </CButton>
          </a>
        </CCardFooter>
      </CCard>

      <div
        style={
          call_details.length != 0 ? { display: "block" } : { display: "none" }
        }
      >
        <PlayAudio />
      </div>
    </Fragment>
  );
};

export default EndUserDetailView;
