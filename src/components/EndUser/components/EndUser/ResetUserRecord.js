import React, {Fragment} from 'react'
import { InvertContentLoader } from "../../../Comman/components";
import {
    CButton,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
  } from "@coreui/react";


export const  ResetUserRecord = (props) => {
    const {isReset, first_name, last_name, handleCancelModal, handleResetConfirm, isResetLoading, registered_mobile} = props
    return (
        <Fragment>
            <CModal
                show={isReset}
                onClose={handleCancelModal}
                color="danger"
            >
                <CModalHeader closeButton>
                <CModalTitle style={{ color: "white" }}>Reset</CModalTitle>
                </CModalHeader>
                <CModalBody>
                Do you want to <b>Reset Call History</b> of this User <b>{ registered_mobile } ? </b>  All Data will be deleted !
                </CModalBody>
                <CModalFooter>
                    {/* <span className="reset_msg_note">
                        <span>
                            <b>Note: </b> can't revoke this call details.
                        </span>
                    </span> */}
                <CButton color="danger" onClick={handleResetConfirm}>
                    Reset {isResetLoading && (
                    <InvertContentLoader />
                    )}
                </CButton>{" "}
                <CButton color="secondary" onClick={handleCancelModal}>
                    Cancel
                </CButton>
                </CModalFooter>
            </CModal>
 
        </Fragment>
    )
}