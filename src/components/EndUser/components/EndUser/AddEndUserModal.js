import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { InvertContentLoader } from "../../../Comman/components";
import { getLoggedInUser, getUserObj } from "../../../Comman/functions";

const error_msg = {
  fontSize: "10px",
  color: "#524444",
  fontFamily: "Roboto",
};

let user='';
let role='';

const AddEndUserModal = (props) => {
  const {
    sale_agents,
    team_leads,
    languages,
    loan_types,
    handleInputValues,
    first_name,
    last_name,
    registered_mobile,
    loan_account_number,
    loan_amount,
    preferred_langauge,
    loan_type,
    branch_id,
    handleAddEndUser,
    addEndUserErrors,
    branches,
    errorMsg,
    successMsg,
    loading,
    team_leader_contact,
    sales_agent_contact,
    middle_name,
    fatchingNumbers,
    fatchingTLNumbers,
    sale_user_id,
    loan_emi,
    loan_tenure
  } = props;

  // Retrive data from storage
  const user_role = getUserObj("role");
  const login_user = getLoggedInUser("user");
 
  if (login_user) {
    user = JSON.parse(login_user);
  } // End if

  if (user_role) {
    role = JSON.parse(user_role);
  } // ENd if
  console.log('user email addEndUserErrors = ', addEndUserErrors)

  return ( 
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4> 
            <b> Add End User</b>
            
          </h4>
        </CCardHeader>
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert> 
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row> 
              <CCol xs="12" md="1"></CCol>
              <CCol md="3"> 
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="First Name"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.first_name}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.first_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="middle_name">
                  Middle Name 
                </CLabel>
                <CInput
                  id="middle_name"
                  name="middle_name"
                  value={middle_name}
                  placeholder="Middle Name "
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.middle_name}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.middle_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder=" Last Name "
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.last_name}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.last_name}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="registered_mobile">
                  Registered Mobile <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="registered_mobile"
                  name="registered_mobile"
                  value={registered_mobile}
                  placeholder="Registered Mobile"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.registered_mobile}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.registered_mobile}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_account_number">
                  Loan Account Number{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_account_number"
                  name="loan_account_number"
                  value={loan_account_number}
                  placeholder="Loan Account Number"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_account_number}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.loan_account_number}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_amount">
                  Loan Amount <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_amount"
                  name="loan_amount"
                  value={loan_amount}
                  placeholder=" Loan Amount"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_amount}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.loan_amount}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>





            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_tenure">Loan Tenure ( months ) <small className="required_lable">*</small> </CLabel>
                <CInput
                  id="loan_tenure"
                  name="loan_tenure"
                  value={loan_tenure}
                  placeholder="Loan Tenure"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_tenure}
                  // className="detail-text-color"
                />
                 {/* <span style={error_msg}>We can't add alternate number !</span> */}
                <CInvalidFeedback>
                  {addEndUserErrors.loan_tenure}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_emi">Loan EMI <small className="required_lable">*</small></CLabel> 
                <CInput
                  id="loan_emi"
                  name="loan_emi"
                  value={loan_emi}
                  placeholder="Loan EMI"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_emi}
                  // className="detail-text-color"
                />
               
                <CInvalidFeedback>
                  {addEndUserErrors.loan_emi}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">Alternate Phone</CLabel>
                  <CInput
                    id="alternate_phone"
                    name="alternate_phone"
                    value={'xxxxxxxxxx'}
                    placeholder="Alternate Phone"
                    onChange={handleInputValues}
                    invalid={addEndUserErrors.alternate_phone}
                    className="detail-text-color"
                  />
                  {/* <span style={error_msg}>We can't add alternate number !</span> */}
                  <CInvalidFeedback>
                    {addEndUserErrors.alternate_phone}
                  </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="preferred_langauge">Preferred Language  <small className="required_lable">*</small></CLabel> 
                  <CSelect
                    custom
                    name="preferred_langauge"
                    id="preferred_langauge"
                    invalid={addEndUserErrors.preferred_langauge}
                    onChange={handleInputValues}
                    value={preferred_langauge}
                  >
                    <option value="">Please select</option>
                    {languages ? (
                      languages.map((item) => (
                        <option value={item.id}>{item.language}</option>
                      ))
                    ) : (
                      <option value="">No Languages</option>
                    )}
                  </CSelect>
                
                  <CInvalidFeedback>
                    {addEndUserErrors.preferred_langauge}
                  </CInvalidFeedback>

              </CCol>
              <CCol md="3">
              <CLabel htmlFor="loan_type">Loan Type  <small className="required_lable">*</small></CLabel>
                <CSelect
                  custom
                  name="loan_type"
                  id="loan_type"
                  invalid={addEndUserErrors.loan_type}
                  onChange={handleInputValues}
                  value={loan_type}
                >
                  <option value="">Please select</option>
                  {loan_types ? (
                    loan_types.map((item) => (
                      <option value={item.id}>{item.loan_type}</option>
                    ))
                  ) : (
                    <option value="">No Loan Types</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.loan_type}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">Branch  <small className="required_lable">*</small></CLabel>
                  <CSelect
                    custom
                    name="branch_id"
                    id="branch_id"
                    invalid={addEndUserErrors.branch_id}
                    onChange={handleInputValues}
                    value={branch_id}
                  >
                    <option value="">Please select</option>
                    {branches ? (
                      branches.map((item) => (
                        <option value={item.id}>{item.branch_name}</option>
                      ))
                    ) : (
                      <option value="">No Branch</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.branch_id}
                  </CInvalidFeedback>



              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="sale_agent_id">
                    Sales Agent <small className="required_lable">*</small>
                  </CLabel>
                  <CSelect
                    custom 
                    name="sale_agent_id"
                    id="sale_agent_id"
                    invalid={addEndUserErrors.sale_agent_id}
                    onChange={handleInputValues}
                    className={`${role.name === 'sales_agent' ? 'detail-text-color' : ''}`}
                    disabled={`${role.name === 'sales_agent' ? true : ''}`}
                  >
                    <option value="">Please select</option>
                    {sale_agents ? (
                      sale_agents.map((item) => (
                        <option
                          value={sale_user_id && sale_user_id!= '' ? sale_user_id : item.id}
                          selected={ role.name == 'sales_agent' && item.email === user.email ? "selected" : ""}
                        >{`${item.first_name}  ${item.last_name}`}</option>
                      ))
                    ) : (
                      <option value="">No Sales Agents</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.sale_agent_id}
                  </CInvalidFeedback>
                  

              </CCol>
              <CCol md="3">
                <CLabel htmlFor="sales_agent_contact">
                    Sales Agent Contact <small className="required_lable">*</small>
                    {fatchingNumbers && <InvertContentLoader color={"black"} />}
                  </CLabel>
                  <CInput
                    id="sales_agent_contact"
                    name="sales_agent_contact"
                    value={sales_agent_contact}
                    placeholder="Select Sales Agent"
                    onChange={handleInputValues}
                    invalid={addEndUserErrors.sales_agent_contact}
                    style={{pointerEvents:'none'}}
                    className={`${role.name === 'sales_agent' ? 'detail-text-color' : ''}`}
                    disabled={`${role.name === 'sales_agent' ? true : ''}`}
                  />
                  <CInvalidFeedback>
                    {addEndUserErrors.sales_agent_contact}
                  </CInvalidFeedback>
                  
              </CCol>
              <CCol md="3">
                {/* <CLabel htmlFor="team_leader">
                    Team Leader 
                  </CLabel>
                  <CSelect
                    custom
                    name="team_leader"
                    id="team_leader"
                    invalid={addEndUserErrors.team_leader}
                    onChange={handleInputValues}
                    className={`${role.name == 'team_lead' ? 'detail-text-color' : ''}`}
                  >
                    <option value="">Please select</option>
                    {team_leads ? (
                      team_leads.map((item) => (
                        <option
                          value={item.id}
                          selected={ role.name == 'team_lead' && item.email === user.email ? "selected" : ""}
                        >{`${item.first_name}  ${item.last_name}`}</option>
                      ))
                    ) : (
                      <option value="">No Team Leads</option>
                    )}
                  </CSelect>
                  <CInvalidFeedback>
                    {addEndUserErrors.team_leader}
                  </CInvalidFeedback> */}
              </CCol>

            
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol xs="12" md="3">
                {/* <CLabel htmlFor="team_leader_contact">
                    Team Leader Contact 
                    {fatchingTLNumbers && <InvertContentLoader color={"black"} />}
                  </CLabel>
                  <CInput
                    id="team_leader_contact"
                    name="team_leader_contact"
                    value={team_leader_contact}
                    placeholder="Select Team Leader"
                    onChange={handleInputValues}
                    invalid={addEndUserErrors.team_leader_contact}
                    style={{pointerEvents:'none'}}
                    className={`${role.name == 'team_lead' ? 'detail-text-color' : ''}`}
                  />
                  <CInvalidFeedback>
                    {addEndUserErrors.team_leader_contact}
                  </CInvalidFeedback> */}
              </CCol>
              <CCol md="3">

              </CCol>
              {/* <CCol md="3">
                <CLabel htmlFor="registeration_trigger">
                  Registeration Trigger
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="registeration_trigger"
                  id="registeration_trigger"
                  invalid={addEndUserErrors.registeration_trigger}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.registeration_trigger}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="verification_trigger">
                  Verification Trigger{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="verification_trigger"
                  id="verification_trigger"
                  invalid={addEndUserErrors.verification_trigger}
                  onChange={handleInputValues}
                  disabled
                >
                  <option value="">Please select</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.verification_trigger}
                </CInvalidFeedback>
              </CCol> */}
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter>
        <span className="buttonRight">
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddEndUser}
          >
            <CIcon name="cil-scrubber" /> Save
            {loading && <InvertContentLoader />}
          </CButton>
          <a href="#/end-user" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
              </span>
          {/* <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
            onClick={handleResetEvent}
          >
            <CIcon name="cil-ban" /> Reset 
           
          </CButton> */}
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};

export default AddEndUserModal;
