import React, { Fragment } from "react";
import { CCardBody, CTooltip, CCollapse } from "@coreui/react";
import { PlaySvgIcon, StopSvgIcon } from "../../../Comman/Icons";

export const CallRecordingDetails = (props) => {
  const {
    item,
    handleAudioPlay,
    firstFileName,
    playAudio1,
    playAudio2,
    accordion,
    index,
  } = props;


    /**
   * 
   * @param url
   * Get filename from url 
   */
  const getFileNameFromUrl = (url) =>  {
    const hashIndex = url && url.indexOf('#')
    url = (hashIndex !== -1) ? (url && url.substring(0, hashIndex)) : url
    return (url && url.split('/').pop() || '').replace(/[\?].*$/g, '')
  } 

  return (
    <Fragment>
      <CCollapse show={accordion === index}>
        <CCardBody>
          <table className="table table-hover table-outline mb-0 d-none d-sm-table">
            <thead className="">
              <tr>
                <th className="text-center">Sr no</th>
                <th className="text-center"> Filename </th>
                {/* <th>Duration</th> */}
                <th className="text-center">Type</th>
                <th className="text-center">Action</th>
              </tr>

              {/* {
                item &&
                item.recordings &&
                item && 
                item.recordings.length != 0  && item.recordings.map()
              } */}
              {item &&
              item.recordings &&
              item && 
              item.recordings.length != 0 ? (
                <Fragment>
                  <tr>
                    <td className="text-center">#</td>
                    <td className="text-center">
                      {item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined'  && getFileNameFromUrl(item.recordings[0].turant_recording_urls) != undefined && item.recordings.length != 0 && getFileNameFromUrl(item.recordings[0].turant_recording_urls) != null ? (
                        <span style={{ cursor: "pointer" }}>
                          {item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined' && getFileNameFromUrl(item.recordings[0].turant_recording_urls).length > 11 ? (
                            <span
                              data-toggle="tooltip"
                              data-placement="top"
                              title={item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined'  && getFileNameFromUrl(item.recordings[0].turant_recording_urls)}
                            >
                              {item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined'  && getFileNameFromUrl(item.recordings[0].turant_recording_urls).substring(0, 11)} <br />
                              <small>More</small>
                            </span>
                          ) : (
                            <span>{item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined'  && getFileNameFromUrl(item.recordings[0].turant_recording_urls)}</span>
                          )}
                        </span>
                      ) : (
                        "NA"
                      )}
                    </td>
                    {/* <td className="text-center">1.25</td> */}
                    {/* <td className="text-center">{firstFileExtension}</td> */}
                    <td className="text-center">{'wav'}</td>
                    <td className="text-center">
                      {item && item.recordings && (
                        <Fragment>
                          {item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined' && item.recordings[0].turant_recording_urls &&
                            (playAudio1 ? (
                              <StopSvgIcon
                                type="playAudioFirst"
                                src={item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined' && item.recordings[0].turant_recording_urls}
                                handleAudioPlay={handleAudioPlay}
                                value={playAudio1}
                                audio={item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined' && item.recordings[0].turant_recording_urls}
                              />
                            ) : (
                              <PlaySvgIcon
                                type="playAudioFirst"
                                src={item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined' &&  item.recordings[0].turant_recording_urls}
                                handleAudioPlay={handleAudioPlay}
                                value={playAudio1}
                                audio={item.recordings.length != 0 && typeof item.recordings[0] !== 'undefined' && item.recordings[0].turant_recording_urls}
                              />
                            ))}
                        </Fragment>
                      )}
                      <span style={{ marginLeft: " 17px" }}>
                        <CTooltip
                          content={`Download Record File`}
                          placement={"top-start"}
                        >
                          <a
                            href={typeof item.recordings[0] !== 'undefined' && item.recordings[0].turant_recording_urls}
                            download={firstFileName}
                            target="_blank"
                          >
                            <i
                              className="fa fa-download"
                              aria-hidden="true"
                              // onClick={() => downloadRecording(playAudioFirst)}
                            ></i>
                            {/* <a href="audio.MP3" download="DownloadedFilenameHere.mp3">Download</a> */}
                          </a>
                        </CTooltip>
                      </span>
                    </td>
                  </tr>

                  <tr>
                    <td className="text-center">#</td>
                    <td className="text-center">
                    {item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined'  && getFileNameFromUrl(item.recordings[1].turant_recording_urls) != undefined && item.recordings.length != 0 && getFileNameFromUrl(item.recordings[1].turant_recording_urls) != null ? (
                        <span style={{ cursor: "pointer" }}>
                          {item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined'  && getFileNameFromUrl(item.recordings[1].turant_recording_urls).length > 11 ? (
                            <span
                              data-toggle="tooltip"
                              data-placement="top"
                              title={item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined'  && getFileNameFromUrl(item.recordings[1].turant_recording_urls)}
                            >
                              {item.recordings.length != 0 &&typeof item.recordings[1] !== 'undefined'  &&  getFileNameFromUrl(item.recordings[1].turant_recording_urls).substring(0, 11)} <br />
                              <small>More</small>
                            </span>
                          ) : (
                            <span>{item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined'  && getFileNameFromUrl(item.recordings[1].turant_recording_urls)}</span>
                          )}
                        </span>
                      ) : (
                        "NA"
                      )}
                    </td>
                    {/* <td className="text-center">0.23</td> */}
                    {/* <td className="text-center">{secondFileExtension}</td> */}
                    <td className="text-center">{'wav'}</td>
                    <td className="text-center">
                      {item && item.recordings && (
                        <Fragment>
                          {item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined' && item.recordings[1].turant_recording_urls &&
                            (playAudio2 ? (
                              <StopSvgIcon
                                type="playAudioSecond"
                                src={item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined' && item.recordings[1].turant_recording_urls}
                                handleAudioPlay={handleAudioPlay}
                                value={playAudio2}
                                audio={item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined' && item.recordings[1].turant_recording_urls}
                              />
                            ) : (
                              <PlaySvgIcon
                                type="playAudioSecond"
                                src={item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined' && item.recordings[1].turant_recording_urls}
                                handleAudioPlay={handleAudioPlay}
                                value={playAudio2}
                                audio={item.recordings.length != 0 && typeof item.recordings[1] !== 'undefined' && item.recordings[1].turant_recording_urls}
                              />
                            ))}
                        </Fragment>
                      )}
                      <span style={{ marginLeft: " 17px" }}>
                        <CTooltip
                          content={`Download Record File`}
                          placement={"top-start"}
                        >
                          <a href={typeof item.recordings[1] !== 'undefined' && item.recordings[1].turant_recording_urls} target="_blank">
                            <i
                              className="fa fa-download"
                              aria-hidden="true"
                              // onClick={() => downloadRecording(playAudioSecond)}
                            ></i>
                          </a>
                        </CTooltip>
                      </span> 
                    </td>
                  </tr>

                  {/* <tr>
                    <td className="text-center">#</td>
                    <td className="text-center">
                    {item.recordings.length != 0 && typeof item.recordings[2] !== 'undefined'  && getFileNameFromUrl(item.recordings[2].turant_recording_urls) != undefined && item.recordings.length != 0 && getFileNameFromUrl(item.recordings[2].turant_recording_urls) != null ? (
                        <span style={{ cursor: "pointer" }}>
                          {item.recordings.length != 0 && typeof item.recordings[2] !== 'undefined'  && getFileNameFromUrl(item.recordings[2].turant_recording_urls).length > 11 ? (
                            <span
                              data-toggle="tooltip"
                              data-placement="top"
                              title={item.recordings.length != 0 &&typeof item.recordings[2] !== 'undefined'  && getFileNameFromUrl(item.recordings[2].turant_recording_urls)}
                            >
                              {item.recordings.length != 0 && typeof item.recordings[2] !== 'undefined'  &&  getFileNameFromUrl(item.recordings[2].turant_recording_urls).substring(0, 11)} <br />
                              <small>More</small>
                            </span>
                          ) : (
                            <span>{item.recordings.length != 0 && typeof item.recordings[2] !== 'undefined'  && getFileNameFromUrl(item.recordings[2].turant_recording_urls)}</span>
                          )}
                        </span>
                      ) : (
                        "NA"
                      )}
                    </td>
                  
                    <td className="text-center">{'wav'}</td>
                    <td className="text-center">
                      {item && item.recordings && (
                        <Fragment>
                          {typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls &&
                            (playAudio3 ? (
                              <StopSvgIcon
                                type="playAudioThird"
                                src={typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls}
                                handleAudioPlay={handleAudioPlay}
                                value={playAudio3}
                                audio={item.recordings.length != 0 && typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls}
                              />
                            ) : (
                              <PlaySvgIcon
                                type="playAudioThird"
                                src={typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls}
                                handleAudioPlay={handleAudioPlay}
                                value={playAudio3}
                                audio={item.recordings.length != 0 && typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls}
                              />
                            ))}
                        </Fragment>
                      )}
                      <span style={{ marginLeft: " 17px" }}>
                        <CTooltip
                          content={`Download Record File`}
                          placement={"top-start"}
                        >
                          <a href={typeof item.recordings[2] !== 'undefined' && item.recordings[2].turant_recording_urls} target="_blank">
                            <i
                              className="fa fa-download"
                              aria-hidden="true"
                             
                            ></i>
                          </a>
                        </CTooltip>
                      </span>
                    </td>
                  </tr> */}
                </Fragment>
              ) : (
                <Fragment>
                  <tr className="no_call_details">
                    <td colspan="5" style={{ textAlign: "center" }}>
                      {" "}
                      Call Dropped abruptly due to network related issues !
                    </td>
                  </tr>
                </Fragment>
              )}
            </thead>
          </table>
        </CCardBody>
      </CCollapse>
    </Fragment>
  );
};
