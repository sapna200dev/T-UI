import React, { Fragment } from 'react'
import { CallRecordingDetails } from './CallRecordingDetails'
import Speedometer from "react-d3-speedometer";
import ReactSpeedometer from "react-d3-speedometer";
import Blink from 'react-blink-text';


import {
  CCardBody,
  CCardHeader,
  CBadge,
  CCollapse,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
} from "@coreui/react";


const customCss = {
  blinking_score:{
    animation: 'blinker-two 1.4s linear infinite',
    color: '#1c87c9',
    fontSize: '15px',
    fontWeight: 'bold',
    fontFamily: 'sans-serif'
  }, 
  textTitle:{
    // marginTop: '29px',
    // marginBottom: '-31px'
    marginLeft: '-90px'
  }
}


export const CallActivityDetails = (props) => {
  const { getLiveUserCallDetails, call_details,
    handleAudioPlay, accordion,
    firstFileName, firstFileExtension, playAudioFirst, playAudio1,
    secondFileName, secondFileExtension, playAudioSecond, playAudio2,
    thirdFileName, thirdFileExtension, playAudioThird, playAudio3,
    setAccordion,
    sales_agent,
    team_leader,
    isFatching,
    refreshing_call_info,
    isDisabledButton,
    call_status_code,
    call_score,
    team_leader_name,
    sales_agent_name,
    verification_status,
    registration_status
  } = props
  console.log('firant call_status_code = ', call_status_code)
  console.log('last registration_status = ', registration_status)
  console.log('call score from lsiting dddd  ', call_score)
  console.log('call score from lsiting bbb  ',  Number(call_score).toFixed(2))
 


  return (
    <Fragment>



      <div className="app" style={{marginLeft: '12px'}}>
        <CTabs>
          <CNav variant="tabs">
            <CNavItem>
              <CNavLink>Call Details</CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink>Speedo-Meter</CNavLink>
            </CNavItem>
          </CNav>
          <CTabContent>
            <CTabPane>

              <CCardHeader>
                <h5>
                  <b>Call Activity Details
             {/* <small>   
            {call_status_code == 1 && (
                               <span>  {call_msg}
                               </span>
                              )  }</small> */}

                  </b>
                </h5>
                <span>
                  <span style={isDisabledButton ? { pointerEvents: 'none' } : {}} onClick={getLiveUserCallDetails}>
                    <i className="fa fa-refresh refresh_icon"></i>
                  </span>
                </span>
              </CCardHeader>

              <CCardBody>
                <table className="table table-hover table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                    <tr>
                      <th>Activity Date / Time</th>
                      <th className="text-center">Call State</th>
                      <th>Called Number</th>
                      <th className="text-center">Call Duration (sec)</th>
                      <th className="text-center">Call Status</th>
                      <th className="text-center">Call Score</th>
                      <th className="text-center">Rating</th>
                      <th className="text-center">Call Failure Reason</th>
                      <th className="text-center">Sales Agent</th>
                      <th className="text-center">Team Leader</th>
                    </tr>
                  </thead>
                  <tbody style={isDisabledButton ? { pointerEvents: 'none' } : {}}>

                    {call_details.length != 0 &&
                      call_details.map((item, index) => {
                        return (
                          <Fragment>
                            {
                              <tr
                                onClick={() =>
                                  setAccordion(index)
                                }
                                style={{ cursor: "pointer" }}
                              >
                                <td>
                                  <div>
                                    {item.verification_call_date != null
                                      ? item.verification_call_date
                                      : item.registration_call_date}
                                  </div>
                                </td>
                                <td className="text-center">
                                  <div>
                                    <span>
                                      {/* {item.verification_call_text != null ?
                               item.verification_call_text : item.verification_call_text != null ? item.registration_call_text : 'NA'} */}
                                      {item.verification_call_to != null
                                        ? item.verification_call_text
                                        : item.registration_call_text}
                                    </span>
                                  </div>
                                </td>
                                <td>
                                  <div>
                                    {item.verification_call_to != null
                                      ? item.verification_call_to
                                      : item.registration_call_to}
                                  </div>
                                </td>
                                <td className="text-center">
                                  <div>
                                    <span>-</span>
                                    {/* {item.verification_call_duration != null
                                ? item.verification_call_duration
                                : item.registration_call_duration != null
                                ? item.registration_call_duration
                                : "-"} */}
                                  </div>
                                </td>
                                <td className="text-center">
                                  <div>
                                    {item.verification_call_status != null ? (
                                      item.verification_call_status == "Initiated" ? (
                                        <CBadge
                                          color="warning"
                                          style={{ fontSize: "13px" }}
                                        >
                                          {item.verification_call_status}
                                        </CBadge>
                                      ) : item.verification_call_status ==
                                        "Success" ? (
                                        <CBadge
                                          color="success"
                                          style={{ fontSize: "13px" }}
                                        >
                                           {Number(item.call_score).toFixed(2) > 59.99 ? 'Success' :
                                                      Number(item.call_score).toFixed(2) < 79.99 ? 'Success' : 'Failed'}

                                          {/* {item.verification_call_status} */}
                                        </CBadge>
                                      ) : item.verification_call_status ==
                                        "Call-has-been-verifying" ? (
                                        <CBadge
                                          color="info"
                                          style={{ fontSize: "13px" }}
                                        >
                                          {'Verify'}
                                          <span>
                                            <span className="dot one">.</span>
                                            <span className="dot two">.</span>
                                            <span className="dot three">.</span>
                                          </span>
                                        </CBadge>
                                      ) : item.verification_call_status ==
                                        "Registration in Progress" ? (
                                        <CBadge
                                          color="success"
                                          style={{ fontSize: "13px" }}
                                        >
                                          {'In-call'}
                                        </CBadge>
                                      ) : item.verification_call_status ==
                                        "Failed" ? (
                                        <CBadge
                                          color="danger"
                                          style={{ fontSize: "13px" }}
                                        >
                                          {item.verification_call_status}
                                        </CBadge>
                                      ) : (
                                        "NA"
                                      )
                                    ) : item.registration_call_status ==
                                      "Initiated" ? (
                                      <CBadge
                                        color="warning"
                                        style={{ fontSize: "13px" }}
                                      >
                                        {item.registration_call_status}
                                      </CBadge>
                                    ) : item.registration_call_status ==
                                      "Verification in Progress" ? (
                                      <CBadge
                                        color="success"
                                        style={{ fontSize: "13px" }}
                                      >
                                        {'In-call'}
                                      </CBadge>
                                    ) : item.registration_call_status ==
                                      "Call-has-been-verifying" ? (
                                      <CBadge
                                        color="info"
                                        style={{ fontSize: "13px" }}
                                      >
                                        {'Verify'}
                                        <span>
                                          <span className="dot one">.</span>
                                          <span className="dot two">.</span>
                                          <span className="dot three">.</span>
                                        </span>
                                      </CBadge>
                                    ) : item.registration_call_status == "Success" ? (
                                      <CBadge
                                        color="success"
                                        style={{ fontSize: "13px" }}
                                      >
                                        {item.registration_call_status}
                                      </CBadge>
                                    ) : item.registration_call_status == "Failed" ? (
                                      <CBadge
                                        color="danger"
                                        style={{ fontSize: "13px" }}
                                      >
                                        {item.registration_call_status}
                                      </CBadge>
                                    ) : (
                                      "NA"
                                    )}
                                  </div>
                                </td>
                                <td className="text-center">
                                  {item.is_verify == 1 ? (
                                       <Fragment>
                                          {item.call_score != null ? (
                                           <span classname="blink" style={customCss.blinking_score}>
                                              {/* <Blink color='#2eb85c' speed='2' text={`${call_score} %`} fontSize='20'>
                                                {call_score} %
                                              </Blink>  */}
                                              
                                              {Math.round(item.call_score) == call_score ? (
                                                     <Blink color={`${ Number(item.call_score).toFixed(2) < 60.00 ? 'red':
                                                     ( Number(item.call_score).toFixed(2) > 60.00 &&  Number(item.call_score).toFixed(2) < 79.99) ? 'green' :'red'}`}
                                                     
                                                    //  '#2eb85c'
                                                     
                                                     speed='2' text={`${Number(item.call_score).toFixed(2)} %`} fontSize='20'>
                                                     {/* {Math.round(item.call_score)} % */}
                                                    { Number(item.call_score).toFixed(2)} %
                                                    {/* 71.23143531056 > 59.99 ? 'Success' : 71.23143531056 < 79.99 ? 'Success' : 'Failed' */}
                                                     </Blink> 
                                              ): (
                                                <span>
                                                  {/* {Math.round(item.call_score)} % */}
                                                  {
                                                   Number(item.call_score).toFixed(2)
                                                  } %
                                                  {/* { Number(item.call_score).toFixed(2)} % */}

                                                  {/* {Number(item.call_score).toFixed(2) > 59.99 ? 'Success' :
                                                      Number(item.call_score).toFixed(2) < 79.99 ? 'Success' : 'Failed'}  % */}
                                                </span>
                                              )}
                                         
                                           </span>
                                          ) : '-'}
                                       </Fragment>
                                  ) : (
                                    <spna>
                                      NA
                                    </spna>
                                  ) }
                                </td>

                                <td className="text-center"> - </td>
                                <td className="text-center">
                                  <div>
                                    {item.call_failure_reason != undefined &&
                                      item.call_failure_reason != null ? (
                                      <span style={{ cursor: "pointer" }}>
                                        {item.call_failure_reason.length > 11 ? (
                                          <span
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title={item.call_failure_reason}
                                          >
                                            {item.call_failure_reason.substring(
                                              0,
                                              11
                                            )}{" "}
                                            <br />
                                            <small>More</small>
                                          </span>
                                        ) : (
                                          <span>{item.call_failure_reason}</span>
                                        )}
                                      </span>
                                    ) : (
                                      "NA"
                                    )}
                                  </div>
                                </td>
                                <td className="text-center">
                                  <div>

                                    {sales_agent_name}
                                  </div>
                                </td>
                                <td className="text-center">
                                  <div>
                                    {team_leader_name}
                                  </div>
                                </td>
                              </tr>
                            }

                            <Fragment>
                              {accordion === index && (
                                <tr>
                                  <td
                                    colspan="12"
                                    style={{ backgroundColor: "rgb(239 241 245)" }}
                                  >
                                    <CCollapse show={accordion === index}>
                                      <CCardBody>
 
                                        <CallRecordingDetails
                                          item={item}
                                          handleAudioPlay={handleAudioPlay}
                                          firstFileName={firstFileName}
                                          firstFileExtension={firstFileExtension}
                                          playAudioFirst={playAudioFirst}
                                          playAudio1={playAudio1}
                                          secondFileName={secondFileName}
                                          secondFileExtension={secondFileExtension}
                                          playAudioSecond={playAudioSecond}
                                          playAudio2={playAudio2}
                                          thirdFileName={thirdFileName}
                                          thirdFileExtension={thirdFileExtension}
                                          playAudioThird={playAudioThird}
                                          playAudio3={playAudio3}
                                          accordion={accordion}
                                          index={index}
                                          call_score={call_score}
                                        />
                                      </CCardBody>
                                    </CCollapse>
                                  </td>
                                </tr>
                              )}
                            </Fragment>
                          </Fragment>
                        );
                      })}
                  </tbody>
                  {/* isFatching || refreshing_call_info */}
                  {isFatching || refreshing_call_info ? (
                    <tbody>
                      <tr className="no_call_details">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td> Loading...</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tbody>
                  ) : (
                    <Fragment>
                      {call_details && call_details.length == 0 && (
                        <tbody>
                          <tr className="no_call_details">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td> No Activity Details Found</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      )}
                    </Fragment>
                  )}
                </table>
              </CCardBody>

            </CTabPane>
            <CTabPane>
              <div className="container">
                {/* <div style={customCss.textTitle}>
                  <h4><b>Last Verification Call Score : </b></h4>
                </div> */}
                <div className="row" style={{marginTop: '50px'}}>

                  <div className="col-sm-1"></div>
                  <div className="col-sm-5">
                    <div style={customCss.textTitle}>
                      <h5><b>Registration Call : </b></h5>
                    </div>

                    {/* <ReactSpeedometer
                      width={400}
                      // ringWidth={60}
                      needleHeightRatio={0.8}
                      value={777}
                      currentValueText="Success"
                      segments={2}
                      customSegmentLabels={[
                        {
                          text: "Failed",
                          position: "INSIDE",
                          color: "#555"
                        },
                        {
                          text: "Success",
                          position: "INSIDE",
                          color: "#555"
                        }
                      ]}
                      ringWidth={40}
                      needleTransitionDuration={3333}
                      needleTransition="easeElastic"
                      needleColor={"#90f2ff"}
                      textColor={"#d8dee9"}
                    /> */}


                    <Speedometer
                        minValue={0}
                        maxValue={100}
                        // maxSegmentLabels={12}
                        needleHeightRatio={0.8}
                        ringWidth={15}
                        segments={2}
                        value={ registration_status == 'Initiated' ? '50' : registration_status == 'NA' ? '0' : registration_status == 'Success' ? '70' : '30'}
                        currentValueText={`${registration_status == 'NA' ? 'Provisioned' :registration_status} `}
                        customSegmentLabels={[
                          {
                            text: "Failed",
                            position: "INSIDE",
                            color: "white"
                          },
                          {
                            text: "Success",
                            position: "INSIDE",
                            color: "white"
                          }
                        ]}
                        segmentColors={[
                          "#ec5555",
                          "#385828",

                          // "#385828",
                          // "#f2db5b ",
                          // "#b81414",
                          // "#ec5555",
                          // #7ab55c
                          // ""
                        ]}
                        // customSegmentLabels={[
                        //   {
                        //     text: "Very Bad",
                        //     position: "INSIDE",
                        //     color: "#555"
                        //   },
                        //   {
                        //     text: "Bad",
                        //     position: "INSIDE",
                        //     color: "#555"
                        //   },
                        //   {
                        //     text: "Ok",
                        //     position: "INSIDE",
                        //     color: "#555",
                        //     fontSize: "19px"
                        //   },
                        //   {
                        //     text: "Good",
                        //     position: "INSIDE",
                        //     color: "#555"
                        //   }
                        // ]}
                        // needleColor="#000080"
                        needleColor="rgb(135 140 132)"
                        // needleColor={`${registration_status === 'Success' ? '#385828' : "#ec5555"}`}
                      />
                  </div>

                  <div className="col-sm-5">
                    <div style={customCss.textTitle}>
                      <h5><b> Verification Call : </b></h5>
                    </div>
                    <Speedometer
                      minValue={0}
                      maxValue={100}
                      // maxSegmentLabels={12}
                      needleHeightRatio={0.8}
                      ringWidth={10}
                      segments={4}
                      value={call_score} 
                      currentValueText={`${registration_status == 'NA' ? 'Provisioned' :  Number(call_score).toFixed(2) < 60.00 ? 'Failed' :
                      ( Number(call_score).toFixed(2) > 60.00 &&  Number(call_score).toFixed(2) < 79.99) ? 'Success' : 'Failed'}- (${call_score} %)`}

                      // currentValueText={`${registration_status == 'NA' ? 'Provisioned' : verification_status}- (${call_score} %)`}
                      segmentColors={[
                        "#ec5555",
                        "#f2db5b",
                        "#385828",
                        "#b81414",
                        // "#385828",
                        // "#f2db5b ",
                        // "#b81414",
                        // "#ec5555",
                        // #7ab55c
                        // ""
                      ]}
                      // customSegmentLabels={[
                      //   {
                      //     text: "Very Bad",
                      //     position: "INSIDE",
                      //     color: "#555"
                      //   },
                      //   {
                      //     text: "Bad",
                      //     position: "INSIDE",
                      //     color: "#555"
                      //   },
                      //   {
                      //     text: "Ok",
                      //     position: "INSIDE",
                      //     color: "#555",
                      //     fontSize: "19px"
                      //   },
                      //   {
                      //     text: "Good",
                      //     position: "INSIDE",
                      //     color: "#555"
                      //   }
                      // ]}
                      // needleColor="#000080"
                      // needleColor="#385828"
                      needleColor={ verification_status == 'NA'  && call_score == 0 ? '#ec5555' : call_score > 0 && call_score < 25 ? '#ec5555' : 
                     call_score >25 && call_score < 50 ? '#f2db5b' 
                      :call_score > 50 && call_score < 75 ? '#385828' : call_score > 75 && call_score < 100 ? '#b81414' :'rgb(135 140 132)' }
                    />
                  </div>
                  <div className="col-sm-1">

                  </div>
                </div>
              </div>
            </CTabPane>
            <CTabPane>
              Tab tworr
              </CTabPane>
          </CTabContent>
        </CTabs>





        {/* <ReactSpeedometer
        width={400}
        // ringWidth={60}
        needleHeightRatio={0.8}
        value={777}
        currentValueText="Happiness Level"
        segments={4}
        customSegmentLabels={[
          {
            text: "Very Bad",
            position: "INSIDE",
            color: "#555"
          },
          {
            text: "Bad",
            position: "INSIDE",
            color: "#555"
          },
          {
            text: "Ok",
            position: "INSIDE",
            color: "#555",
            fontSize: "19px"
          },
          {
            text: "Good",
            position: "INSIDE",
            color: "#555"
          }
        ]}
        ringWidth={40}
        needleTransitionDuration={3333}
        needleTransition="easeElastic"
        needleColor={"#90f2ff"}
        textColor={"#d8dee9"}
      /> */}
      </div>





    </Fragment>
  )
}