import React, {Fragment} from 'react'
import {
    CCardBody,
    CCardHeader,
  } from "@coreui/react";


export const ProvisionedLogs = (props) => {
    const {user_logs, fatchingLogs} = props
    return (
        <Fragment>
        <CCardHeader>
          <h5>
            <b>Provisioned Data Changes logs</b>
          </h5>
        </CCardHeader>

        <CCardBody>
          <table className="table table-hover table-outline mb-0 d-none d-sm-table">
            <thead className="thead-light">
              <tr>
                <th>Date / Time</th>
                <th className="text-center">Field Changed</th>
                <th>Old Data</th>
                <th className="text-center">New Data</th>
                <th className="text-center">Reason For Change</th>
                <th className="text-center">Changed By</th>
                <th className="text-center">Level / Role</th>
              </tr>
            </thead>

            <tbody>
              {user_logs && user_logs.length != 0 && (
                <Fragment>
                  {user_logs.map((value) => {
                    return (
                      <tr>
                        <td>
                          <div>{value.created_at}</div>
                        </td>
                        <td className="text-center">
                          <div>{value.field}</div>
                        </td>
                        <td>
                          <div>{value.old_data}</div>
                        </td>
                        <td className="text-center">
                          <div>{value.new_data}</div>
                        </td>
                        <td className="text-center">
                          <div>{value.reason}</div>
                        </td>
                        <td className="text-center">
                          <div>{value.created_by.name}</div>
                        </td>
                        <td className="text-center">
                          <div>{value.role.display_name}</div>
                        </td>
                      </tr>
                    );
                  })}
                </Fragment>
              )}
            </tbody>

            {fatchingLogs ? (
              <tbody>
                <tr className="no_call_details">
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Loading...</td> 
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            ) : (
              <span>
                {user_logs.length == 0 && (
                  <tbody>
                    <tr className="no_call_details">
                      <td colspan="3"></td>
                      <td></td>
                      <td></td>
                      <td >No Provisioned Data Logs Found</td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                )}
              </span>
            )}
          </table>
        </CCardBody>


        </Fragment>
    )
}