import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';
import ReactLoading from 'react-loading';
import {
  CAlert,
  CButton,
  CCol,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend, 
  CInputGroupText,
  CRow,
  CSelect,
  CInvalidFeedback
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "../../containers/common.css";
import {
  REGEX__EMAIL,
  TEXT__VALIDATION_ERROR__USERNAME,
  TEXT__VALIDATION_ERROR__NAME_EMPTY,
  TEXT__VALIDATION_ERROR__PASSWORD_EMPTY,
  TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH
} from "../../../Comman/constants";
import firebase from '../../../Comman/firebase'
import {  ContentLoading, InvertContentLoader } from "../../../Comman/components";
import { registerUser, bankLists, login, resendActivationLinkAPI } from "../../servicers/common";
import { attemptLogin } from '../../actions/index'

const showPass = {
  fontSize: '15px',
    marginLeft: '-22px',
    zIndex: '10',
    marginTop: '9px',
    marginRight:'7px',
    cursor: 'pointer'
};

const handleType = {
  cursor:'pointer',
  // color: 'blue',
  textDecoration: 'revert'
};

const active_account = {
  cursor: 'pointer',
  textDecoration: 'underline',
  color: 'blue',
  marginLeft: '48px',
};



const errorMsgcSS={
  marginTop: '10px',
  marginBottom: '-26px'
}

const customCss = {
  footerCss:{
    marginTop: '28px',
    marginBottom: -'16px',
    textAlign: 'center'
  }
}


const loading={
  fill: 'black',
    width: '29px',
    marginLeft: '44%',
    zIndex: '9px',
    animation: 'rotate 1.1s cubic-bezier(0, 0, 0.56, 0.26) infinite',
    zIndex: '20',
    position: 'absolute',
    color: '#7e79a9 !important',
}
class LoginPage extends Component {
  state = {
    name:'',
    username: "",
    password: "",
    usernameError: "",
    c_passwordError:'',
    nameError:'',
    passwordError: "",
    errorMsg: "",
    loading: false,
    all_roles:[],
    all_branches:[],
    showPW:false,
    showCPW:false,
    isRegister:false,
    isLogin:true,
    c_password:'',
    c_passwordConfimError:'',
    user_type:'',
    userTypeError:'',
    banks:[],
    bank_id:'',
    bankError:'',
    inactive_account:false,
    isActivation:false
  };

  componentDidMount() {
    this.fetchBankList();
  }

  componentDidUpdate(prevProps) {
    const {loginPayload} = this.props
    if(prevProps.loginPayload != loginPayload) {
      console.log('login payload', loginPayload)
    }
  }


  fetchBankList = ()=> {
    bankLists().then(response => {
      console.log('bank list = ', response.data)
      console.log('bank list  d = ', response.data.data)
      this.setState({
        banks:response.data.data
      });
    }).catch(err => {
      console.log('bank error = ', err)
    })
  }

  validateInput = (e) => {
    const field = e.target.name;
    const val = e.target.value;

    console.log('nameError field =-', field)
    console.log('nameErrornameError  val = ', val)

    switch (field) {
      case "username":
        this.validateUsername(val, 'email');
        break;
      case "password":
        this.validatePassword(val, 'password');
        break;
      case "name":
        this.validateUsername(val, 'name');
        break;
      case "c_password":
        this.validatePassword(val, 'c_password');
        break;
        
        

      default:
        console.warn("validateInput was supplied an uncaught field case");
        break;
    }
  };

  /**
   * Validate input username
   */
  validateUsername = (val, type) => {
    console.log('nameError validateUsername =-', type)
      console.log('nameErrornameError = ', val)

    if(type == 'email') {
      if (!REGEX__EMAIL.test(val)) {
        this.setState({ usernameError: TEXT__VALIDATION_ERROR__USERNAME });
        return true;
      }
      this.setState({ usernameError: "" });
    } else if(type == 'name') {
      if (val.length < 0) {
        this.setState({ nameError: TEXT__VALIDATION_ERROR__NAME_EMPTY });
        return true;
      }
      this.setState({ nameError: "" });
    }
    return false;
  };

  /**
   * Validate input username
   */
   validateName = (val, type) => {
    if(type == 'name') { 
      if (val.length < 1) {
        this.setState({ nameError: 'Must provide a name!' });
        return true;
      }
      this.setState({ nameError: "" });
    }
    return false;
  };



  /**
   * Validate input passowrd
   */
  validatePassword = (val, type) => {
    const {password} = this.state;
    console.log('paswword ffff validatePassword = ', val)
    console.log('type validatePassword ', type)
    if(type == "password") {
      if (val.length < 1) {
        this.setState({ passwordError: TEXT__VALIDATION_ERROR__PASSWORD_EMPTY });
        return true;
      }
      this.setState({ passwordError: "" });
    } 
    // else {

    //   if (val.length < 1) {
    //     this.setState({ c_passwordError: TEXT__VALIDATION_ERROR__PASSWORD_EMPTY });
    //     return true;
    //   } else if(password != val) {
    //     this.setState({ c_passwordConfimError: TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH });
    //     return true;
    //   }
    //   this.setState({ c_passwordError: "", c_passwordConfimError:'' });
    // }

    return false;
  };


  /**
   * Validate input passowrd
   */
   validateCPassword = (val, type) => {
    const {password} = this.state;
  
    console.log('paswword validateCPassword = ', val)
    console.log('type validateCPassword ', type)
      if (val.length < 1) {
        this.setState({ c_passwordError: TEXT__VALIDATION_ERROR__PASSWORD_EMPTY });
        return true;
      } else if(password != val) {
        this.setState({ c_passwordConfimError: TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH });
        return true;
      }
      this.setState({ c_passwordError: "", c_passwordConfimError:'' });
  

    return false;
  };


  /**
     * Validate input username and password
     */
  validateRequiredForRegister = () => {
    const nameCheck = this.validateName(this.state.name, 'name');
    const usernameCheck = this.validateUsername(this.state.username, 'email');
    const passwordCheck = this.validatePassword(this.state.password, 'password');
    const c_passwordCheck = this.validateCPassword(this.state.c_password);
    

    if (usernameCheck || nameCheck || passwordCheck || c_passwordCheck) {
      return true;
    }
    return false;
  };


    /**
   * Validate input username and password
   */
     validateRequired = () => {
      const usernameCheck = this.validateUsername(this.state.username, 'email');
      const passwordCheck = this.validatePassword(this.state.password, 'password');
    
      if (usernameCheck || passwordCheck ) {
        return true;
      }
      return false;
    };

    
  /**
   * Validate input username and password
   */
  validateRegisterRequired = () => {
    const {name, user_type, bank_id } = this.state;
    const nameCheck = this.validateName(this.state.name, 'name');
    const usernameCheck = this.validateUsername(this.state.username, 'email');
    const passwordCheck = this.validatePassword(this.state.password, 'password');
    const c_passwordCheck = this.validateCPassword(this.state.c_password);
    
    if(user_type.length < 0 || user_type == '')  {
      this.setState({
        userTypeError:'Please select user type !'
      })
      return true;
    } else {
      this.setState({
        userTypeError:''
      });
     
    }

    if(user_type == 'int') {
      if(bank_id.length < 1) {
        this.setState({
          bankError:'Please select bank name !'
        })
        return true;
      } else {
        this.setState({
          bankError:''
        });
      
      }
    }



    if (nameCheck || usernameCheck || passwordCheck || c_passwordCheck) {
      return true;
    }
    return false;
  };

  /**
   * Handle inputs
   */
  handleChangeInput = (e) => {
    console.log('logine event xx= ')
    console.log(e.key)
    this.setState({ [e.target.name]: e.target.value });
  };

  handleLoginEvent = (e) => {
    console.log('handleLoginEventhandleLoginEvent')
    console.log(e.key)
    if(e.key == 'Enter') {
      this.submitForm(e)
    }
  }
  /**
   * Submit login form
   */
  submitForm = (e) => {
    e.preventDefault();
    console.log('logine event = ')
    console.log(e.keyCode)
    // proceed if no validation errors
    const { name, username, password, user_type, isLogin, bank_id } = this.state;
    if(isLogin) {
     
      const result = this.validateRequired();
  
      if (!result) {
        this.setState({ loading: true });
        const data = {
          email: username,
          password: password,
        };
  
        this.props.login(username, password);
        login(data)
          .then((res) => {
            console.log("Success response  fff = ", res.data);
  
            if (res.data && res.data.token) {
              // this.setState({ errorMsg: "", loading: false }); 
              // const userObj = JSON.stringify(res.data.roles);
              // const loggedInUser = {
              //   username:res.data.username,
              //   email:res.data.email,
              //   id: res.data.id,
              //   profile:res.data.profile
              // }
              // // Set user token in localstorage
              // console.log('user obj = ')
              // console.log(JSON.stringify(res.data))
              // settToken(res.data.token);
              // setAuthHeader(res.data.token)
              // settUser(res.data.username);
  
              // settUserObj(userObj);
  
              // setLoggedInUser(JSON.stringify(loggedInUser))
              // // after login success redirect to login
              // redirectToDashboard(true);
              console.log("login username = ", username);
              console.log("login password = ", password);
                     firebase.auth().signInWithEmailAndPassword(username, password)
                     .then((user1) => {
                       console.log('auth firebase login with email pass = ')
                       console.log(user1)
                     })
                     .catch((error) => {
                       var errorCode = error.code;
                       var errorMessage = error.message;                    
                       firebase.auth().createUserWithEmailAndPassword(username, password)
                       .then((user) => {
                         console.log('auth firebase create with email pass = ')
                         console.log(user)
                       })
                       .catch((error) => {
                         var errorCode = error.code;
                         var errorMessage = error.message;
                         console.log('auth firebase create with email pass  error= ')
                         console.log(errorCode)
                         console.log(errorMessage)
                       });
  
                     });
            } else {
              console.log("error dd dreponse = ", res.data);
              if(res.data.code == 404) {
                const mesg = res.data.msg;
                this.setState({ errorMsg: mesg, loading: false, inactive_account:true });

              } else {
                this.setState({ errorMsg: res.data.msg, loading: false, inactive_account:false });
              }
              
            }
          })
          .catch((err) => {
            console.log("error reponse = ", err.response);
            this.setState({
              errorMsg: err.response != undefined ? err.response.data.message != undefined ? err.response.data.message : 'Somthing went wrong !' : err.response ? err.response.message : 'Network issue ! ',
              loading: false,
            });
            let _this = this;
            setTimeout(function () {
              _this.setState({ errorMsg: "" });
            }, 4000);
  
          });
      }
  

    } else {
      // Register new user
      const result = this.validateRegisterRequired();
      console.log('register validateRegisterRequired = ')
      console.log(result)

      if (!result) {
        this.setState({ loading: true });
        const data = {
          name:name,
          email: username,
          password: password,
          user_type,
          bank_id
        };
  
          console.log('register payload = ')
          console.log(data)
        // this.props.login(username, password);
        registerUser(data)
          .then((res) => {
            console.log("Success response register = ", res.data);
  
            if (res.data && res.data.status == 200) {
              this.setState({ successMsg: res.data.msg, loading: false, isLogin:true, isRegister:false });
            } else {
              this.setState({ errorMsg: res.data.msg, loading: false });
            }
  
            let _this = this;
            setTimeout(function () {
              _this.setState({ errorMsg: "", successMsg:'' });
            }, 4000);
  
          })
          .catch((err) => {
            console.log("error reponse = ", err.response);
            this.setState({
              errorMsg: err.response != undefined ? err.response.data.message != undefined ? err.response.data.message : 'Somthing went wrong !' : err.response ? err.response.message : 'Network issue ! ',
              loading: false,
            });
            let _this = this;
            setTimeout(function () {
              _this.setState({ errorMsg: "" });
            }, 4000);
  
          });
      }
    }

  };


  /**
   * handle register user api
   * @param  type 
   */
  handleRegisterUser = (type) => {
    if(type == 'login') {
      this.setState({
        isLogin:true,
        isRegister:false,
        usernameError:'',
        passwordError:'',
        nameError:'',
        username: "",
        password: "",
        c_password:'',
        errorMsg:'',
        successMsg:'',
      })
    } else {
      this.setState({
        isLogin:false,
        isRegister:true,
        usernameError:'',
        passwordError:'',
        nameError:'',
        username: "",
        password: "",
        c_password:'',
        errorMsg:'',
        successMsg:'',
      })
    }
  }

  /**
   * Handle login type
   * @param  type 
   */
  handleLoginType = (type) => {
    console.log('handleLoginType = ', type)
    this.setState({
      user_type:type
    })
  }

  /**
   * Handle activation mail
   */
  handleActivationMail = () => {
    this.setState({
      isActivation:true
    })
    console.log('handleActivationMail')
    const {username} = this.state;
    const payload = {
      email:username
    }
    resendActivationLinkAPI(payload).then(res => {
      console.log('response activation link = ', res)
      console.log("error dd dreponse = ", res.data);
      if(res.data.status == 200) {
        this.setState({ successMsg:res.data.msg, isActivation: false, errorMsg:'', inactive_account:false });
      } else {
        if(res.data.code == 404) {
          const mesg = res.data.msg;
          this.setState({ errorMsg: mesg, isActivation: false, inactive_account:true });
  
        } else {
          this.setState({ errorMsg: res.data.msg, isActivation: false, inactive_account:false });
        }
  
      }
      let _this = this;
      setTimeout(function () {
        _this.setState({ errorMsg: "", successMsg:''});
      }, 4000);
    }).catch(err => {
      console.log('activation error = ', err)
      console.log("error reponse = ", err.response);
      this.setState({
        errorMsg: err.response != undefined ? err.response.data.message != undefined ? err.response.data.message : 'Somthing went wrong !' : err.response ? err.response.message : 'Network issue ! ',
        isActivation: false,
      });
      let _this = this;
      setTimeout(function () {
        _this.setState({ errorMsg: "" });
      }, 4000);
    })
  
  }
  render() {
    const {
      username,
      password,
      usernameError,
      passwordError,
      errorMsg,
      loading,
      isRegister,
      isLogin,
      showPW,
      showCPW,
      name, 
      nameError,
      c_password,
      c_passwordError,
      c_passwordConfimError,
      user_type,
      userTypeError,
      banks,
      bank_id,
      bankError,
      successMsg,
      inactive_account,
      isActivation
    } = this.state;
    const {handleForgotPass} = this.props

      console.log('nameError nameError = ', nameError)
    return (
      <div>
        <CForm>
          <h1 style={{textAlign: 'center'}}>{isLogin ? 'Login' :'Register'}</h1>

          <p className="text-muted"  style={{textAlign: 'center'}}> {isLogin ? 'Login to Your Own Voice Verify Engine  !' : 'Sign Up with new user !'}</p>
          {/* {isRegister ? (
            <Fragment>
              <p>You already have account <a  style={handleType}   onClick={() => this.handleRegisterUser('login')}><b>Login</b></a> with existing user !</p>
            </Fragment>
          ) : (
            <Fragment>
              <p>If don't have account You can <a style={handleType} onClick={() => this.handleRegisterUser('register')}><b  >Register</b></a> new user with <b>Us</b> !</p>
            </Fragment>
          ) } */}
          
          {errorMsg && (
            <CAlert color="danger">
              {/*eslint-disable-next-line*/}
             
              {inactive_account ? (
                <Fragment>
                     {`Error : ${errorMsg} `}  <br /> <a style={active_account} onClick={() => this.handleActivationMail()}>Resend Activation Link</a> 
                </Fragment>
              ) : (
                <Fragment>
                   {`Error : ${errorMsg}`}
                </Fragment>
              )}
            </CAlert>
          )}
            {successMsg && (
            <CAlert color="success">
              {/*eslint-disable-next-line*/}
              {`Success : ${successMsg}`}
            </CAlert>
          )}
  
  
          {isLogin ? (
            <Fragment>
              <CInputGroup className="mb-3 input_class">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    <CIcon name="cil-user" />
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  type="text"
                  placeholder="Username"
                  name="username"
                  autoComplete="username"
                  onChange={this.handleChangeInput}
                  value={username}
                  onBlur={(e) => {
                    if (username.length > 0) {
                      this.validateInput(e);
                    }
                  }}
                />
              </CInputGroup>
         
              <div
                 className={usernameError!= '' ? 'validationRegisterErrorMessagesStyles' : 'validationErrorMessagesStyles'}
                style={{ textAlign: "left" }}
              >
                {usernameError}
              </div>
              {isActivation && <ReactLoading type={'spin'} color={'black'}  className="loading" />}

              <CInputGroup className="mb-4 input_class">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    <CIcon name="cil-lock-locked" />
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  type={showPW ? 'text' : 'password'}
                  placeholder="Password"
                  name="password"
                  autoComplete="password"
                  onChange={this.handleChangeInput}
                  value={password}
                  onKeyPress={this.handleLoginEvent}
                />
    
                  {showPW ? (
                    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                    <i 
                      className="fa fa-eye-slash" style={showPass}
                      onClick={() => this.setState({ showPW: false })}
                    />
                  ) : (
                    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                    <i 
                      className="fa fa-eye" style={showPass}
                      onClick={() => this.setState({ showPW: true })}
                    />

                  )}
              </CInputGroup>
              <div
                   className={passwordError!= '' ? 'validationRegisterErrorMessagesStyles' : 'validationErrorMessagesStyles'}
                style={{ textAlign: "left" }}
              >
                {passwordError}
              </div>
              <CRow>
                <CCol xs="6">
                  <CButton
                    color="primary"
                    className="px-4 login_bt"
                    onClick={this.submitForm}
                  >
                    Login
                    {loading && <InvertContentLoader />}
                  </CButton>
                </CCol>
                <CCol xs="6" className="text-right">
                  <CButton color="link" className="px-0" onClick={handleForgotPass}>
                    Forgot Password?
                  </CButton>
                </CCol>
              </CRow>
              <span>
                      <p style={customCss.footerCss}>Don't have an account ? 
                      <CButton color="link" className="px-0" onClick={() => this.handleRegisterUser('register')}>
                        Register
                      </CButton>
                     
                        </p>
                    </span>

            </Fragment>
          ) : (
            <Fragment>
               <div
                      className="validationErrorMessagesStyles"
                      style={errorMsgcSS}
                    >
                      {userTypeError}
                    </div>
                <div className="form-check form-check-inline" style={{ marginTop: '35px'}}>
                  <input className="form-check-input" type="radio" onClick={() => this.handleLoginType('int')} name="flexRadioDefault" id="flexRadioDefault1"  />
                  <label className="form-check-label" for="flexRadioDefault1">
                  Enterprises
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" name="flexRadioDefault" onClick={() => this.handleLoginType('indi')}  id="flexRadioDefault2" />
                  <label className="form-check-label" for="flexRadioDefault2">
                    Individual Developer
                  </label>
                </div>
               
                
                <div style={{marginTop:'14px'}}>
                  <CInputGroup className="mb-3 input_class">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        placeholder="name"
                        name="name"
                        autoComplete="off"
                        onChange={this.handleChangeInput}
                        value={name}
                        onBlur={(e) => {
                          if (name.length > 0) {
                            this.validateInput(e);
                          }
                        }}
                      />
                    </CInputGroup>
                    <div
                       className={nameError!= '' ? 'validationRegisterErrorMessagesStyles' : 'validationErrorMessagesStyles'}
                      style={{ textAlign: "left" }}
                    >
                      {nameError}
                    </div>

                  <CInputGroup className="mb-3 input_class">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        placeholder="Username"
                        name="username"
                        autoComplete="username"
                        onChange={this.handleChangeInput}
                        value={username}
                        onBlur={(e) => {
                          if (username.length > 0) {
                            this.validateInput(e);
                          }
                        }}
                      />
                    </CInputGroup>
                    <div
                        className={usernameError!= '' ? 'validationRegisterErrorMessagesStyles' : 'validationErrorMessagesStyles'}
                      style={{ textAlign: "left" }}
                    >
                      {usernameError}
                    </div>

                    <CInputGroup className="mb-4 input_class">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type={showPW ? 'text' : 'password'}
                        placeholder="Password"
                        name="password"
                        autoComplete="password"
                        onChange={this.handleChangeInput}
                        value={password}
                        onKeyPress={this.handleLoginEvent}
                      />
          
                        {showPW ? (
                          // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                          <i 
                            className="fa fa-eye-slash" style={showPass}
                            onClick={() => this.setState({ showPW: false })}
                          />
                        ) : (
                          // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                          <i 
                            className="fa fa-eye" style={showPass}
                            onClick={() => this.setState({ showPW: true })}
                          />

                        )}
                    </CInputGroup>
                    <div
                      className={passwordError!= '' ? 'validationRegisterErrorMessagesStyles' : 'validationErrorMessagesStyles'}
                      style={{ textAlign: "left" }}
                    >
                      {passwordError}
                    </div>


                    <CInputGroup className="mb-4 input_class">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type={showCPW ? 'text' : 'password'}
                        placeholder="Confirm Password"
                        name="c_password"
                        autoComplete="c_password"
                        onChange={this.handleChangeInput}
                        value={c_password}
                        // onKeyPress={this.handleLoginEvent}
                      />
          
                        {showCPW ? (
                          // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                          <i 
                            className="fa fa-eye-slash" style={showPass}
                            onClick={() => this.setState({ showCPW: false })}
                          />
                        ) : (
                          // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                          <i 
                            className="fa fa-eye" style={showPass}
                            onClick={() => this.setState({ showCPW: true })}
                          />

                        )}
                    </CInputGroup>
                    <div
                     className={(c_passwordError!= '' || c_passwordConfimError != '') ? 'validationRegisterErrorMessagesStyles' : 'validationErrorMessagesStyles'}
                      style={{ textAlign: "left" }}
                    >
                      {c_passwordError}
                      {c_passwordConfimError}
                    </div>

                        {user_type == 'int' && (
                          <Fragment>
                            <CInputGroup className="mb-4 input_class">
                            <CInputGroupPrepend>
                              <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                              </CInputGroupText>
                            </CInputGroupPrepend>

                              <CSelect
                                custom
                                name="bank_id"
                                id="bank_id"
                                invalid={bankError}
                                onChange={this.handleChangeInput}
                                value={bank_id}
                              >
                                <option value="">Please select</option>
                                {banks ? (
                                  banks.map((item) => (
                                    <option value={item.id}>{item.bank_name}</option>
                                  ))
                                ) : (
                                  <option value="">No Banks</option>
                                )}
                              </CSelect>
                          </CInputGroup>
                          <CInvalidFeedback>
                            {bankError}
                          </CInvalidFeedback>
                          </Fragment>
                        )}




                    

                    <CRow>
                      <CCol xs="6">
                        <CButton
                          color="primary"
                          className="px-4 login_bt"
                          onClick={this.submitForm}
                        >
                          Register
                          {loading && <InvertContentLoader />}
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0" onClick={() => this.handleRegisterUser('login')}>
                          Login
                        </CButton>
                      </CCol>
                    </CRow>
                 
                    </div>

   
                    <span>
                      <p style={customCss.footerCss}>Already have an account ? <a style={handleType} onClick={() => this.handleRegisterUser('login')}><b  >Login</b></a> </p>
                    {/* <p>If don't have account You can <a style={handleType} onClick={() => this.handleRegisterUser('register')}><b  >Register</b></a> new user with <b>Us</b> !</p> */}
                    </span>

                    
            </Fragment>
          ) 

          }

        </CForm>
      </div>
    );
  }
}



export function mapStateToProps(state) {
  return {
    isAttemptingLogin: state.loginReducer.isAttemptingLogin,
    loginPayload: state.loginReducer.payload,
    loginError: state.loginReducer.loginError,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    login: (email, password) => dispatch(attemptLogin(email, password)),
  };
}



export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

