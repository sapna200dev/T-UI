export {
   login,
   resetPassword,
   registerUser,
   bankLists,
   validUser,
   resendActivationLinkAPI
  } from '/common';