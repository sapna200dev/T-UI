import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";


import {
  ContentLoading,
  ShowToastMessage,
  InvertContentLoader,
} from "../../Comman/components";


const customCss = {
    low_red:{
        color: '#f33737'
    }, 

    low_yellow:{
        color: 'orange'
    },

    low_green:{
        color: 'green'
    },

    ex_low_red:{
        color: 'darkred'
    },

    hide_text :{
        pointerEvents: 'none'
    }
}



export const ManageRatingEditForm = (props) => {
  const {
    handleInputValues,
    handleAddBank,
    manageRatingErrors,
    succesMsg,
    errorMsg,
    loading,
    isUpdating,
    handleUpdateManageRating,
    low_red,
    heigh_red,
    low_yellow,
    heigh_yellow,
    low_green,
    heigh_green,
    ext_low_red,
    ext_low_heigh,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b>Update Manage Rating</b>
          </h4>
        </CCardHeader>
        {succesMsg && (
          <CAlert color="success" className="msg_div">
            {succesMsg}
          </CAlert>
          // <ShowToastMessage
          //     success={true}
          //     header={'Success'}
          //     content={succesMsg}
          // />
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post" 
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol md="3">
                {loading && <ContentLoading content="Fetching record.." />}

                {isUpdating && <ContentLoading content="Updating record.." />}
                <CLabel htmlFor="low_red" style={customCss.low_red}>
                 Low <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="low_red"
                  name="low_red"
                  value={low_red}
                //   placeholder="Bank Name"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.low_red}
                  style={customCss.hide_text}
                />
                <CInvalidFeedback>{manageRatingErrors.low_red}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="branch_name" style={customCss.low_red}>
                  Heigh  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="heigh_red"
                  name="heigh_red"
                  value={heigh_red}
                //   placeholder="HO Location"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.heigh_red}
                />
                <CInvalidFeedback>{manageRatingErrors.heigh_red}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="address" style={customCss.low_yellow}>
                  Low  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="low_yellow"
                  name="low_yellow"
                  value={low_yellow}
                //   placeholder="Common Address"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.low_yellow}
                  style={customCss.hide_text}
                />
                <CInvalidFeedback>{manageRatingErrors.low_yellow}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">

              <CLabel htmlFor="status" style={customCss.low_yellow}>
                  Heigh  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="heigh_yellow"
                  name="heigh_yellow"
                  value={heigh_yellow}
                //   placeholder="Compnay Email"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.heigh_yellow}
                />
                <CInvalidFeedback>{manageRatingErrors.heigh_yellow}</CInvalidFeedback>
              </CCol>
            </CFormGroup>

            <CFormGroup row>
 
              <CCol xs="12" md="3">
                <CLabel htmlFor="low_green" style={customCss.low_green}>
                  Low  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="low_green"
                  name="low_green"
                  value={low_green}
                //   placeholder="Billing Status"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.low_green}
                  style={customCss.hide_text}
                />
                <CInvalidFeedback>
                  {manageRatingErrors.low_green}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="heigh_green"  style={customCss.low_green}>
                  Heigh  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="heigh_green"
                  name="heigh_green"
                  value={heigh_green}
                //   placeholder="Compnay Email"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.heigh_green}
                />
                <CInvalidFeedback>
                  {manageRatingErrors.heigh_green}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">

              <CLabel htmlFor="ext_low_red"  style={customCss.ex_low_red}>
                  Low  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ext_low_red"
                  name="ext_low_red"
                  value={ext_low_red}
                //   placeholder="Compnay Phone"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.ext_low_red}
                  style={customCss.hide_text}
                />
                <CInvalidFeedback>
                  {manageRatingErrors.ext_low_red}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="ext_low_heigh" style={customCss.ex_low_red}>
                 Heigh <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ext_low_heigh"
                  name="ext_low_heigh"
                  value={ext_low_heigh}
                //   placeholder="Contact Person"
                  onChange={handleInputValues}
                  invalid={manageRatingErrors.ext_low_heigh}
                  style={customCss.hide_text}
                />
                <CInvalidFeedback>
                  {manageRatingErrors.ext_low_heigh}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>

      
          </CForm>
        </CCardBody>
        <CCardFooter>
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleUpdateManageRating}
            disabled={isUpdating ? true : false}
          >
            <CIcon name="cil-scrubber" /> Update {isUpdating && <InvertContentLoader />}
          </CButton>
          <a href="#/manage-rating" className="cancel_bt">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>

          {/* <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
          >
            <CIcon name="cil-ban" /> Reset
          </CButton> */}
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
