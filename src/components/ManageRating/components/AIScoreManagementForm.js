import React, { Fragment } from "react";
import {
    CButton,
    CInput,
    CInvalidFeedback,
    CTooltip,
} from "@coreui/react";
import { InvertContentLoader } from "../../Comman/components";

const customCss = {
    low_red: {
        color: '#f33737'
    },

    low_yellow: {
        color: 'orange'
    },

    low_green: {
        color: 'green'
    },

    ex_low_red: {
        color: 'darkred'
    },

    hide_text: {
        pointerEvents: 'none'
    },

    inputForm: {
        border: '0',
        outline: '0',
        background: 'transparent',
        borderBottom: '1px solid black',
        width: '100%'

    }
}



export const AIScoreManagementForm = (props) => {
    const {
        handleInputValues,
        handleAddBank,
        manageRatingErrors,
        ai_heigh_ex_red,
        errorMsg,
        loading,
        isUpdatingAI,
        handleClose,
        ai_heigh_red,
        ai_heigh_yellow,
        ai_heigh_green,
        handleUpdateAIScore
    } = props;

    return (
        <Fragment>
            <tr>
                <th scope="row"></th>
               
                <td>
                    <CInput
                        id="ai_heigh_red"
                        name="ai_heigh_red"
                        value={ai_heigh_red}
                        //   placeholder="HO Location"
                        onChange={handleInputValues}
                        invalid={manageRatingErrors.ai_heigh_red}
                        style={customCss.inputForm}
                        type="number"
                    />
                    <CInvalidFeedback>
                        {manageRatingErrors.ai_heigh_red}
                    </CInvalidFeedback>
                </td>
                <td> </td>
                <td>
                    <CInput
                        id="ai_heigh_yellow"
                        name="ai_heigh_yellow"
                        value={ai_heigh_yellow}
                        //   placeholder="HO Location"
                        onChange={handleInputValues}
                        invalid={manageRatingErrors.ai_heigh_yellow}
                        style={customCss.inputForm}
                        type="number"
                    />
                    <CInvalidFeedback>
                        {manageRatingErrors.ai_heigh_yellow}
                    </CInvalidFeedback>
                </td>
                <td></td>
                <td>
                    <CInput
                        id="ai_heigh_green"
                        name="ai_heigh_green"
                        value={ai_heigh_green}
                        //   placeholder="HO Location"
                        onChange={handleInputValues}
                        invalid={manageRatingErrors.ai_heigh_green}
                        style={customCss.inputForm}
                        type="number"
                    />
                    <CInvalidFeedback>
                        {manageRatingErrors.ai_heigh_green}
                    </CInvalidFeedback>
                </td>
                <td></td>
               <td></td>
                
                <td>
                    <CTooltip
                        content={`Save Record`}
                        placement={"top-start"}
                    >
                        <CButton type="button" size="sm" color="primary" onClick={handleUpdateAIScore}>
                            <i className="fa fa-check" aria-hidden="true"></i>
                            {isUpdatingAI && <InvertContentLoader />}
                        </CButton>
                        {/* <CButton
                            type="submit"
                            size="sm"
                            color="primary"
                            onClick={handleUpdateBank}
                            disabled={isUpdating ? true : false}
                        >
                            <CIcon name="cil-scrubber" /> Update {isUpdating && <InvertContentLoader />}
                        </CButton> */}
                    </CTooltip>
                    <CTooltip
                        content={`Close Record`}
                        placement={"top-start"}
                    >
                        <CButton type="button" size="sm" color="danger"   onClick={() => handleClose('ai')} style={{ marginLeft: '12px' }}>
                            <i className="fa fa-times" aria-hidden="true"></i>

                        </CButton>
                    </CTooltip>

                </td>
            </tr>
        </Fragment>
    );
};
