import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip
} from "@coreui/react";
import { AIScoreManagement } from "./AIScoreManagement";
import { DMScoreManagement } from "./DMScoreManagement";
// import { ContentLoading } from "../../../Comman/components";

const fields = [
  "low",
  "high",
  "low",
  "high",
  "low",
  "high",
  "low",
  "high",
  "Action"
];


const tableCSS  = {
  width: '97%',
  marginBottom: '1rem',
  color: '#4f5d73',
  margin: '21px',
}


const customCss = {
  low_red:{
    color:'red'
  },
  low_yellow:{
    color:'orange'
  },
  low_green:{
    color:'green'
  },

  low_ex_red:{
    color:'darkred'
  },
}
export const ManageRatingList = (props) => {
  const {
    ai_score_lists,
    dm_score_lists,
    errorMsg,
    successMsg,
    loading,
    handleInputValues,
    handleAddBank,
    manageRatingErrors,
    succesMsg,
    isUpdating,

    ai_low_red,
    ai_heigh_red,
    ai_low_yellow,
    ai_heigh_yellow,
    ai_low_green,
    ai_heigh_green,
    ai_low_ex_red,
    ai_heigh_ex_red,

    dm_low_red,
    dm_heigh_red,
    dm_low_yellow,
    dm_heigh_yellow,
    dm_low_green,
    dm_heigh_green,
    dm_low_ex_red,
    dm_heigh_ex_red,
    handleUpdateAIScore,
    openEditSection,
    showEditSection,
    handleClose,
    isFatching,
    dmScoreErrors,

    handleDMInputValues,
    openDMEditSection,
    showDMEditSection,
    errorDMMsg,
    successDMMsg,
    handleUpdateDMScore,
    isUpdatingAI
  } = props;

  console.log('ai_score_lists  isFatching  errorDMMsg =  ', errorDMMsg)
  return (
    <Fragment>
      <AIScoreManagement 
      ai_score_lists={ai_score_lists} 
        errorMsg={errorMsg}
        manageRatingErrors={manageRatingErrors}
        successMsg={successMsg}
        loading={isFatching}
        handleInputValues={handleInputValues}
        isUpdatingAI={isUpdatingAI}
        ai_low_red={ai_low_red}
        ai_heigh_red={ai_heigh_red}
        ai_low_yellow={ai_low_yellow}
        ai_heigh_yellow={ai_heigh_yellow}
        ai_low_green={ai_low_green}
        ai_heigh_green={ai_heigh_green}
        ai_low_ex_red={ai_low_ex_red}
        ai_heigh_ex_red={ai_heigh_ex_red}
        handleUpdateAIScore={handleUpdateAIScore}
        openEditSection={openEditSection}
        showEditSection={showEditSection}
        handleClose={handleClose}
      />


      <DMScoreManagement 
        dm_score_lists={dm_score_lists}
        dm_low_red={dm_low_red}
        dm_heigh_red={dm_heigh_red}
        dm_low_yellow={dm_low_yellow}
        loading={isFatching}
        dm_heigh_yellow={dm_heigh_yellow}
        dm_low_green={dm_low_green}
        dm_heigh_green={dm_heigh_green}
        dm_low_ex_red={dm_low_ex_red}
        dm_heigh_ex_red={dm_heigh_ex_red}
        errorDMMsg={errorDMMsg}
        successMsg={successDMMsg}
        handleDMInputValues={handleDMInputValues}
        manageRatingErrors={dmScoreErrors}
        openDMEditSection={openDMEditSection}
        showDMEditSection={showDMEditSection}
        handleClose={handleClose}
        handleUpdateDMScore={handleUpdateDMScore}
       
      />
    </Fragment>
  );
};
