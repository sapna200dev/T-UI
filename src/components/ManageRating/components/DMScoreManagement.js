import React, { Fragment } from "react";
// import {
//   CBadge,
//   CCard,
//   CCardBody,
//   CCardHeader,
//   CCol,
//   CDataTable,
//   CRow,
//   CButton,
//   CAlert,
//   CTooltip
// } from "@coreui/react";

import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CForm,
    CFormGroup,
    CTextarea,
    CInput,
    CLabel,
    CAlert,
    CInvalidFeedback,
    CInputFile,
    CSelect,
    CTooltip
} from "@coreui/react";
import { AIScoreManagementForm } from "./AIScoreManagementForm";
import { ContentLoading } from "../../Comman/components";
import { DMScoreManagementForm } from "./DMScoreManagementForm";
// import { ContentLoading } from "../../../Comman/components";

const fields = [
    "low",
    "high",
    "low",
    "high",
    "low",
    "high",
    "low",
    "high",
    "Action"
];


const tableCSS = {
    width: '97%',
    marginBottom: '1rem',
    color: '#4f5d73',
    margin: '21px',
}


const customCss = {
    low_red: {
        color: 'red',
        width: '10%'
    },
    low_yellow: {
        color: 'orange',
        width: '10%'
    },
    low_green: {
        color: 'green',
        width: '10%'
    },

    low_ex_red: {
        color: 'darkred',
        width: '10%'
    },

    inputForm: {
        border: '0',
        outline: '0',
        background: 'transparent',
        borderBottom: '1px solid black',
        width: '100%'
    },
}
export const DMScoreManagement = (props) => {
    const {
        dm_score_lists,
        successMsg,
        handleDMInputValues,
        handleAddBank,
        manageRatingErrors,
        succesMsg,
        errorDMMsg,
        loading,
        isUpdating,

        ai_heigh_red,
        ai_heigh_yellow,
        ai_heigh_green,
        handleUpdateDMScore,
        openDMEditSection,
        showDMEditSection,
        handleClose,
        dm_heigh_red,
        dm_heigh_yellow,
        dm_heigh_green,
        dm_low_ex_red,
        dm_heigh_ex_red,
    } = props;

    console.log('dm_score_lists errorDMMsg = ', errorDMMsg)
    console.log('dm_score_lists  manageRatingErrors = ', manageRatingErrors)
    return (
        <Fragment>
            <CCard>
                <CCardHeader>
                    <b style={{ fontSize: "20px" }}>DM Score </b>
                    <span>
                        <div className="md-form mt-3">
                            {/* <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              /> */}
                            {/* <a href="#/bank-add" className="button_style">
              <CButton
                type="submit"
                size="sm"
                color="primary"
                className="add_employee_button"
                // onClick={() => handleDeleteItem(item)}
              >
               
                  {" "}
                  Add New
                  </CButton>
                </a> */}

                        </div>
                    </span>
                </CCardHeader>

                {successMsg && (
                    <CAlert color="success" className="msg_div">
                        {successMsg}
                    </CAlert>
                )}
                {errorDMMsg && (
                    <CAlert color="danger" className="msg_div">
                        {errorDMMsg}
                    </CAlert>
                )}
                {/* {loading &&  <ContentLoading content="Fetching record.." />} */}
                <CCardBody>
                    <table class="table " style={tableCSS}>
                        <thead class="">
                            {/* table-dark */}
                            <tr>
                                {/* <th scope="col" >#</th> */}
                                <th scope="col" style={customCss.low_red}>Low</th>
                                <th scope="col" style={customCss.low_red}>High</th>
                                <th scope="col" style={customCss.low_yellow}>Low</th>
                                <th scope="col" style={customCss.low_yellow} >High</th>
                                <th scope="col" style={customCss.low_green}>Low</th>
                                <th scope="col" style={customCss.low_green}>High</th>
                                <th scope="col" style={customCss.low_ex_red}>Low</th>
                                <th scope="col" style={customCss.low_ex_red}>High</th>
                                <th scope="col" >Action</th>
                            </tr>
                        </thead>
                        
                        
                        <tbody>
                            {dm_score_lists && dm_score_lists.map((item, index) => {
                                return <Fragment>
                                    <tr>
                                        {/* <th scope="row">1</th> */}
                                        <td>{item.low_red} %</td>
                                        <td>{ item.heigh_red} % </td>
                                        <td> {dm_heigh_red ? dm_heigh_red : item.low_yellow} %</td>
                                        <td>{ item.heigh_yellow} % </td>
                                        <td>{dm_heigh_yellow ? dm_heigh_yellow : item.low_green} % </td>
                                        <td>{item.heigh_green} %</td>
                                        <td>{dm_heigh_green ? dm_heigh_green : item.extra_low_red} % </td>
                                        <td>{item.extra_heigh_red} % </td>
                                        <td>

                                            {/* <a href={`#/rating-update/${item.id}`} className="cancel_bt"> */}
                                                <CTooltip
                                                    content={`Edit Record`}
                                                    placement={"top-start"}
                                                >
                                                    <CButton type="button" size="sm" color="primary" onClick={() => openDMEditSection(item.id)}>
                                                        <i className="fa fa-edit" aria-hidden="true"></i>
                                                        {/* Edit */}
                                                        {/* {loading && <ContentLoading />} */}
                                                    </CButton>
                                                </CTooltip>
                                            {/* </a> */}
                                        </td>
                                    </tr>

                                </Fragment>
                            })}  
                            {showDMEditSection && (
                                <DMScoreManagementForm
                                    manageRatingErrors={manageRatingErrors}
                                    succesMsg={succesMsg}
                                   
                                    loading={loading}
                                    handleDMInputValues={handleDMInputValues}
                                
                                    isUpdating={isUpdating}
                                    dm_heigh_red={dm_heigh_red}
                                    dm_heigh_yellow={dm_heigh_yellow}
                                    dm_heigh_green={dm_heigh_green}
                                    handleUpdateDMScore={handleUpdateDMScore}
                                    handleClose={handleClose}
                                />
                            )}


                            {/* <tr>
            <th scope="row">1</th>
                <td>0%</td>
                <td>20%</td>
                <td>20%</td>
                <td>35%</td>
                <td>35%</td>
                <td>45%</td>
                <td>45%</td>
                <td>100%</td>
                <td>

                <a href={`#/rating-update/5`} className="cancel_bt">
                      <CTooltip
                        content={`Edit Record`}
                        placement={"top-start"}
                      >
                        <CButton type="submit" size="sm" color="primary">
                          <i className="fa fa-edit" aria-hidden="true"></i>
                       
                        </CButton>
                      </CTooltip>
                  </a>
                </td>
            </tr> */}
                        </tbody>
                    </table>


                    {/* <AIScoreManagementForm 
                manageRatingErrors={manageRatingErrors}
                succesMsg={succesMsg}
                errorMsg={errorMsg}
                loading={loading}
                handleDMInputValues={handleDMInputValues}
                handleUpdateManageRating={handleUpdateManageRating}
             
                isUpdating={isUpdating}
        
                low_red={low_red}
                heigh_red={heigh_red}
                low_yellow={low_yellow}
                heigh_yellow={heigh_yellow}
                low_green={low_green}
                heigh_green={heigh_green}
                ext_low_red={ext_low_red}
                ext_low_heigh={ext_low_heigh}
            /> */}
                </CCardBody>
            </CCard>
        </Fragment>
    );
};
