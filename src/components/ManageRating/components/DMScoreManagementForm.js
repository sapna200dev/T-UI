import React, { Fragment } from "react";
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CForm,
    CFormGroup,
    CTextarea,
    CInput,
    CLabel,
    CAlert,
    CInvalidFeedback,
    CInputFile,
    CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";


import {
    ContentLoading,
    ShowToastMessage,
    InvertContentLoader,
} from "../../Comman/components";


const customCss = {
    low_red: {
        color: '#f33737'
    },

    low_yellow: {
        color: 'orange'
    },

    low_green: {
        color: 'green'
    },

    ex_low_red: {
        color: 'darkred'
    },

    hide_text: {
        pointerEvents: 'none'
    },

    inputForm: {
        border: '0',
        outline: '0',
        background: 'transparent',
        borderBottom: '1px solid black',
        width: '100%'

    }
}



export const DMScoreManagementForm = (props) => {
    const {
        handleDMInputValues,
        handleAddBank,
        manageRatingErrors,
        ai_heigh_ex_red,
        errorMsg,
        loading,
        handleClose,
        handleUpdateDMScore,

        dm_heigh_red,
 
        dm_heigh_yellow,

        dm_heigh_green,
        dm_low_ex_red,
        dm_heigh_ex_red,
    } = props;

    return (
        <Fragment>
            <tr>
                <th scope="row"></th>
               
                <td>
                    <CInput
                        id="dm_heigh_red"
                        name="dm_heigh_red"
                        value={dm_heigh_red}
                        //   placeholder="HO Location"
                        onChange={handleDMInputValues}
                        invalid={manageRatingErrors.dm_heigh_red}
                        style={customCss.inputForm}
                    />
                    <CInvalidFeedback>
                        {manageRatingErrors.dm_heigh_red}
                    </CInvalidFeedback>
                </td>
                <td> </td>
                <td>
                    <CInput
                        id="dm_heigh_yellow"
                        name="dm_heigh_yellow"
                        value={dm_heigh_yellow}
                        //   placeholder="HO Location"
                        onChange={handleDMInputValues}
                        invalid={manageRatingErrors.dm_heigh_yellow}
                        style={customCss.inputForm}
                    />
                    <CInvalidFeedback>
                        {manageRatingErrors.dm_heigh_yellow}
                    </CInvalidFeedback>
                </td>
                <td></td>
                <td>
                    <CInput
                        id="dm_heigh_green"
                        name="dm_heigh_green"
                        value={dm_heigh_green}
                        //   placeholder="HO Location"
                        onChange={handleDMInputValues}
                        invalid={manageRatingErrors.dm_heigh_green}
                        style={customCss.inputForm}
                    />
                    <CInvalidFeedback>
                        {manageRatingErrors.dm_heigh_green}
                    </CInvalidFeedback>
                </td>
                <td></td>
               <td></td>
                
                <td>
                    <CTooltip
                        content={`Save Record`}
                        placement={"top-start"}
                    >
                        <CButton type="button" size="sm" color="primary" onClick={handleUpdateDMScore}>
                            <i className="fa fa-check" aria-hidden="true"></i>

                        </CButton>
                    </CTooltip>
                    <CTooltip
                        content={`Close Record`}
                        placement={"top-start"}
                    >
                        <CButton type="button" size="sm" color="danger"   onClick={() => handleClose('dm')} style={{ marginLeft: '12px' }}>
                            <i className="fa fa-times" aria-hidden="true"></i>

                        </CButton>
                    </CTooltip>

                </td>
            </tr>
        </Fragment>
    );
};
