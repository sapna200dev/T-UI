export { addBank, bankLists, deleteBank, getBankRecord, updateBank,
    getATPortsAPI,
    getATIpAddressessAPI, 
    getATEnvironmentAPI,
    getATProtocolsAPI,
    getAIScoresAPI,
    getDMScoresAPI,
    updateAIScoreMangementAPI,
    getAIScoreById,
    updateDMScoreMangementAPI,
    getDMScoreById
} from './common'
