import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'


export const addBank = (data) =>
axios.post(`${URL_HOST}/api/bank/add`, data);


export const getAIScoresAPI = () =>
axios.get(`${URL_HOST}/api/score_mgmt/list`);

export const getAIScoreById = (id) =>
axios.get(`${URL_HOST}/api/score_mgmt/${id}`);

export const updateAIScoreMangementAPI = (id, data) =>
axios.post(`${URL_HOST}/api/score_mgmt/update/${id}`, data);

export const getDMScoresAPI = () =>
axios.get(`${URL_HOST}/api/score_mgmt/dm/list`);

export const getDMScoreById = (id) =>
axios.get(`${URL_HOST}/api/score_mgmt/dm/${id}`);

export const updateDMScoreMangementAPI = (id, data) =>
axios.post(`${URL_HOST}/api/score_mgmt/dm-update/${id}`, data);




// http://api.mturbine.com/api/manage_rating/update/1
export const bankLists = () =>
axios.get(`${URL_HOST}/api/bank/all`);

export const deleteBank = (id) =>
axios.post(`${URL_HOST}/api/bank/delete/${id}`);

export const getBankRecord = (id) =>
axios.get(`${URL_HOST}/api/bank/${id}`);

export const updateBank = (id, data) =>
axios.post(`${URL_HOST}/api/bank/update/${id}`, data);


export const getATPortsAPI = () =>
axios.get(`${URL_HOST}/api/at_settings/asterisk_ports`);

export const getATIpAddressessAPI = () =>
axios.get(`${URL_HOST}/api/at_settings/asterisk_ip_address`);


export const getATEnvironmentAPI = () =>
axios.get(`${URL_HOST}/api/at_settings/asterisk_environment`);


export const getATProtocolsAPI = () =>
axios.get(`${URL_HOST}/api/at_settings/asterisk_protocols`);