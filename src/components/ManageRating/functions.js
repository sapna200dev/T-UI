import { MANAGE_RATING_FORM_VALIDATIONS } from "./contants";

export const validateUpdateAIRequest = (props) => {
  const manageratingErrors = {};

  const {
    ai_heigh_red,
    ai_heigh_yellow,
    ai_heigh_green,
    heigh_ex_red,
  } = props;


  console.log('ai_heigh_red = ', ai_heigh_red)
  console.log('ai_heigh_yellow = ', ai_heigh_yellow)
  console.log('ai_heigh_green = ', ai_heigh_green)

  // 'Number must be less than 100'
  if (ai_heigh_red.length < 1) {

    manageratingErrors.ai_heigh_red = MANAGE_RATING_FORM_VALIDATIONS.HEIGH_RED;
  } else if (ai_heigh_red > 1) {
    manageratingErrors.ai_heigh_red = 'number must be greater that 0 and less than 1';
  } 
  // else if (ai_heigh_red > ai_heigh_yellow) {
  //   manageratingErrors.ai_heigh_yellow = 'number must be greater that high red';
  // } 
  else if (ai_heigh_yellow.length < 1) {

    manageratingErrors.ai_heigh_yellow = MANAGE_RATING_FORM_VALIDATIONS.HEIGH_YELLOW;
  } 
  else if (ai_heigh_yellow > 1) {
    manageratingErrors.ai_heigh_yellow = 'number must be greater that 0 and less than 1';
  } 
  else if (ai_heigh_green.length < 1) {

    manageratingErrors.ai_heigh_green = MANAGE_RATING_FORM_VALIDATIONS.GREEN_HEIGH;
  } else if (ai_heigh_green > 1) {
    manageratingErrors.ai_heigh_green = 'number must be greater that 0 and less than 1';
  } 
  // else if (ai_heigh_yellow > ai_heigh_green) {
  //   manageratingErrors.ai_heigh_yellow = 'number must be greater that high red';
  // }

  return manageratingErrors;
};



export const validateUpdateDMRequest = (props) => {
  const manageratingErrors = {};

  const {
    dm_heigh_red,
    dm_heigh_yellow,
    dm_heigh_green,
    heigh_ex_red,
  } = props;


  console.log('dm_heigh_red = ', dm_heigh_red)
  console.log('dm_heigh_yellow = ', dm_heigh_yellow)
  console.log('dm_heigh_green = ', dm_heigh_green)

  // 'Number must be less than 100'
  if (dm_heigh_red.length < 1) {

    manageratingErrors.dm_heigh_red = MANAGE_RATING_FORM_VALIDATIONS.HEIGH_RED;
  } else if (dm_heigh_red > 100) {
    manageratingErrors.dm_heigh_red = 'number must be greater that 0 and less than 100';
  } 
  // else if (dm_heigh_red > dm_heigh_yellow) {
  //   manageratingErrors.dm_heigh_yellow = 'number must be greater that high red';
  // } 
  else if (dm_heigh_yellow.length < 1) {

    manageratingErrors.dm_heigh_yellow = MANAGE_RATING_FORM_VALIDATIONS.HEIGH_YELLOW;
  } 
  else if (dm_heigh_yellow > 100) {
    manageratingErrors.dm_heigh_yellow = 'number must be greater that 0 and less than 100';
  } 
  else if (dm_heigh_green.length < 1) {

    manageratingErrors.dm_heigh_green = MANAGE_RATING_FORM_VALIDATIONS.GREEN_HEIGH;
  } else if (dm_heigh_green > 100) {
    manageratingErrors.dm_heigh_green = 'number must be greater that 0 and less than 100';
  } 
  // else if (dm_heigh_yellow > dm_heigh_green) {
  //   manageratingErrors.dm_heigh_yellow = 'number must be greater that high red';
  // }

  return manageratingErrors;
};




/**
* Makde form request params
* @param  props 
*/
export const prepareFormRequestParams = (props) => {
  const {
    ai_low_red,
    ai_heigh_red,
    ai_low_yellow,
    ai_heigh_yellow,
    ai_low_green,
    ai_heigh_green,
    ai_low_ex_red,
    ai_heigh_ex_red,
  } = props;

  var formDataRequest = new FormData();

  formDataRequest.append("low_red", ai_low_red);
  formDataRequest.append("heigh_red", ai_heigh_red);
  formDataRequest.append("low_yellow", ai_low_yellow);
  formDataRequest.append("heigh_yellow", ai_heigh_yellow);
  formDataRequest.append("low_green", ai_low_green);
  formDataRequest.append("heigh_green", ai_heigh_green);
  formDataRequest.append("extra_low_red", ai_low_ex_red);
  formDataRequest.append("extra_heigh_red", ai_heigh_ex_red);

  const payload = {
    low_red:ai_low_red,
    heigh_red:ai_heigh_red,
    low_yellow: ai_low_yellow,
    heigh_yellow: ai_heigh_yellow,
    low_green:ai_low_green,
    heigh_green: ai_heigh_green,
    extra_low_red: ai_low_ex_red,
    extra_heigh_red:ai_heigh_ex_red,
  }

  return payload;
};


/**
* Makde form request params
* @param  props 
*/
export const prepareFormRequestParamsfForDM = (props) => {
  const {
    dm_low_red,
    dm_heigh_red,
    dm_low_yellow,
    dm_heigh_yellow,
    dm_low_green,
    dm_heigh_green,
    dm_low_ex_red,
    dm_heigh_ex_red,
  } = props;

  var formDataRequest = new FormData();

  formDataRequest.append("low_red", dm_low_red);
  formDataRequest.append("heigh_red", dm_heigh_red);
  formDataRequest.append("low_yellow", dm_low_yellow);
  formDataRequest.append("heigh_yellow", dm_heigh_yellow);
  formDataRequest.append("low_green", dm_low_green);
  formDataRequest.append("heigh_green", dm_heigh_green);
  formDataRequest.append("extra_low_red", dm_low_ex_red);
  formDataRequest.append("extra_heigh_red", dm_heigh_ex_red);


  return formDataRequest;
};
