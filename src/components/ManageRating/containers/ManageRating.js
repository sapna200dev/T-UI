import React, { Component, Fragment } from "react";

import { ManageRatingList } from "../components/ManageRatingList";
import { getAIScoresAPI, getDMScoresAPI, getAIScoreById, updateAIScoreMangementAPI, updateDMScoreMangementAPI, getDMScoreById } from "../services";

import { prepareFormRequestParams, validateUpdateAIRequest, validateUpdateDMRequest, prepareFormRequestParamsfForDM } from "../functions";

export class ManageRating extends Component {
  state = {
    ai_score_lists: [],
    dm_score_lists: [],
    isDelete: false,
    deleteItem: "",
    successMsg: "",
    loading: false,
    errorMsg: "",
    searchValue: "",
    filterai_score_lists: [],
    filterdm_score_lists: [],
    manageRatingErrors: {},
    dmScoreErrors: {},
    isFatching: false,
    isUpdatingAI:false,
    ai_low_red: '0%',
    ai_heigh_red: '',
    ai_low_yellow: '',
    ai_heigh_yellow: '',
    ai_low_green: '',
    ai_heigh_green: '',
    ai_low_ex_red: '',
    ai_heigh_ex_red: '100%',

    dm_low_red: '0%',
    dm_heigh_red: '',
    dm_low_yellow: '',
    dm_heigh_yellow: '',
    dm_low_green: '',
    dm_heigh_green: '',
    dm_low_ex_red: '',
    dm_heigh_ex_red: '100%',
    showEditSection: false,
    showDMEditSection: false,
    errorDMMsg: '',
    successDMMsg: '',
    dm_id: ''
  };

  componentDidMount() {
    this.setState({ loading: true, isFatching: true });
    this.getAIScores();
    this.getDMScores();
  }

  /**
   * Get AI scores listing
   */
  getAIScores = () => {
    getAIScoresAPI()
      .then((res) => {
        console.log('AI response data = ', res.data)
        this.setState({
          ai_score_lists: res.data.data,
          filterai_score_lists: res.data.data,
          loading: false,
        });
      })
      .catch((err) => {

        console.log("AI list  error d ", err);
        this.setState({
          ai_score_lists: "",
          filterai_score_lists: '',
          loading: false,
          errorMsg: err.response ? err.response.data.msg : err
        });
      });
  };


  /**
   * Get DM scores listing
   */
  getDMScores = () => {
    getDMScoresAPI()
      .then((res) => {
        console.log('DM response data = ', res.data)
        this.setState({
          dm_score_lists: res.data.data,
          filterdm_score_lists: res.data.data,
          loading: false,
          isFatching: false
        });
      })
      .catch((err) => {
        console.log("DM list  error", err);
        this.setState({
          dm_score_lists: "",
          filterdm_score_lists: '',
          loading: false,
          isFatching: false
        });
      });
  };


  /**
   * Handle AI inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    // this.testValidateNumberForDM(name, value, name)
    this.testValidateNumberForAI(name, value, name)
  };


  /**
   * Handle DM  inputs
   */
  handleDMInputValues = (e) => {
    const { name, value } = e.target;
    this.testValidateNumberForDM(name, value, name)
  };

  /**
 * Test validate number inputs for DM
 */
  testValidateNumberForDM = (name, value, field_name) => {
    const { dm_heigh_red, dm_heigh_yellow, dm_heigh_green } = this.state;

    let dmScoreErrors = {}

    if (!/^[0-9]+$/.test(value)) {
      dmScoreErrors[field_name] = 'This field is mandatory and Please only enter numeric characters'
      this.setState({ [name]: value, dmScoreErrors, is_change: false });
    } else if (value > 100) {
      dmScoreErrors[field_name] = 'Number must be less than 100'
      this.setState({ [name]: value, dmScoreErrors, is_change: false });
    } else {
      dmScoreErrors[field_name] = '';

      if (name == 'dm_heigh_red') {
        this.setState({ ['dm_low_yellow']: value  });
      } else if (name == 'dm_heigh_yellow') {
        if (dm_heigh_red > value) {
          dmScoreErrors[field_name] = 'Number must be greater that high red !'
          this.setState({ [name]: value, dmScoreErrors, is_change: false });
        }
        this.setState({ ['dm_low_green']: value });
      } else if (name == 'dm_heigh_green') {
        if (dm_heigh_yellow > value) {
          dmScoreErrors[field_name] = 'Number must be greater that high red !'
          this.setState({ [name]: value, dmScoreErrors, is_change: false });
        }
        this.setState({ ['dm_low_ex_red']: value  });
      }

      this.setState({ [name]: value, is_change: true, dmScoreErrors });
    }

    return dmScoreErrors;
  }

  /**
* Test validate number inputs for AI
*/
  testValidateNumberForAI = (name, value, field_name) => {
    const { ai_heigh_red, ai_heigh_yellow, ai_heigh_green } = this.state;

    let manageRatingErrors = {}

    // if (!/^[0-9]+$/.test(value)) {
    //   manageRatingErrors[field_name] = 'This field is mandatory and Please only enter numeric characters'
    //   this.setState({ [name]: value, manageRatingErrors, is_change: false });
    // } 
     if (value > 1) {
      manageRatingErrors[field_name] = 'Number must be inbetween 0-1'
      this.setState({ [name]: value, manageRatingErrors, is_change: false });
    } else {
      manageRatingErrors[field_name] = '';
      const inputVal = value.split('.');
      console.log('value changed by  value = ', value)
      console.log('value changed by = ', inputVal)
      //  if(inputVal[1].length > 4) {
      //   manageRatingErrors[field_name] = 'Allow only 4 digits !'
      //   this.setState({ [name]: value,manageRatingErrors, is_change: false });
      //  }
      if (name == 'ai_heigh_red') {
        this.setState({ ai_low_yellow: value });
      } else if (name == 'ai_heigh_yellow') {
        if (ai_heigh_red > value) {
          manageRatingErrors[field_name] = 'Number must be greater that high red !'
          this.setState({ [name]: value, manageRatingErrors, is_change: false });
        }
        this.setState({ ai_low_green : value });
      } else if (name == 'ai_heigh_green') {
        if (ai_heigh_yellow > value) {
          manageRatingErrors[field_name] = 'Number must be greater that high red !'
          this.setState({ [name]: value, manageRatingErrors, is_change: false });
        }
        this.setState({ ai_low_ex_red : value });
      }

      this.setState({ [name]: value, is_change: true, manageRatingErrors });
    }

    return manageRatingErrors;
  }


  /**
   * Handle bank validation
   */
  handleAIValidation = () => {
    // Request validate
    let manageRatingErrors = validateUpdateAIRequest(this.state);

    this.setState({ manageRatingErrors });
    return manageRatingErrors;
  };

  /**
   * Handle  validations for AI Score
   */
  handleUpdateAIScore = () => {
    const errors = this.handleAIValidation();
    console.log('call api from backend errors ', Object.getOwnPropertyNames(errors).length)
    if (Object.getOwnPropertyNames(errors).length === 0) {
      console.log('call api from backend')
      this.setState({isUpdatingAI: true})
      this.updateAIScores();
    }
  };

  /**
   * Handle ai api
   */
  updateAIScores = () => {

    const { id } = this.state;
    console.log('call api from updateAIScores id ', id)
    // Make form request payload
    let formRequest = prepareFormRequestParams(this.state);

    let _this = this;
    updateAIScoreMangementAPI(id, formRequest)
      .then((res) => {
        console.log('call api from updateAIScoreMangementAPI res ', res.data)
        if (res.data && res.data.status == 200) {
          this.setState({
            succesMsg: res.data.msg,
            isUpdatingAI: false,
            errorMsg: "",
          });
          // _this.props.history.push("/bank-list");

          setTimeout(function () {
            _this.setState({ succesMsg: "" });
            // _this.props.history.push("/bank-list");
          }, 4000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            isUpdatingAI: false,
            succesMsg: "",
          });
          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          isUpdating: false,
        });
        setTimeout(function () {
          _this.setState({ succesMsg: "" });
        }, 4000);
      });
  };


  /**
   * Open ai edit section
   * @param  id 
   */
  openEditSection = (id) => {
    this.setState({
      showEditSection: true,
      isFatching: true,
      id: id
    })

    // get ai details api
    getAIScoreById(id).then((response) => {
      console.log('get by id response = ', response)
      const value = response.data.data
      this.setState({
        ai_low_red: value.low_red,
        ai_heigh_red: value.heigh_red,
        ai_low_yellow: value.low_green,
        ai_heigh_yellow: value.heigh_yellow,
        ai_low_green: value.low_green,
        ai_heigh_green: value.heigh_green,
        ai_low_ex_red: value.extra_low_red,
        ai_heigh_ex_red: value.extra_heigh_red,
        isFatching: false,
      })

    }).catch((err) => {
      console.log('get by id response  err = ', err)
      const error = err.response ? err.response.data.msg : err;
      this.setState({
        errorMsg: error,
        isUpdating: false,
      });
    })
  }

  /**
   * Handle close
   * @param  type 
   */
  handleClose = (type) => {
    const {
      ai_heigh_red,
      ai_heigh_yellow,
      ai_heigh_green,
      dm_heigh_red,
      dm_heigh_yellow,
      dm_heigh_green
    } = this.state;
    let _this = this;
    if (type == 'ai') {
      console.log('close AI section')
      this.setState({
        isFatching: false,
        showEditSection: false,
        // ai_low_yellow: ai_heigh_red,
        // ai_low_green: ai_heigh_yellow,
        // ai_low_ex_red: ai_heigh_green,
        manageRatingErrors: {}
      })
      // _this.getAIScoreById(id);
      this.getAIScores();
    } else if (type == 'dm') {
      console.log('close DM section')

      this.setState({
        isFatching: false,
        showDMEditSection: false,
        dm_low_yellow: dm_heigh_red,
        dm_low_green: dm_heigh_yellow,
        dm_low_ex_red: dm_heigh_green,
        dmScoreErrors: {}
      })
      // _this.getAIScoreById(id);
      this.getDMScores();
    }
  }

  /**
   * Open DM edit section
   * @param  id 
   */
  openDMEditSection = (id) => {
    this.setState({
      showDMEditSection: true,
      isFatching: true,
      dm_id: id
    })

    getDMScoreById(id).then((response) => {
      console.log('get by id response = ', response)
      const value = response.data.data
      this.setState({
        dm_low_red: value.low_red,
        dm_heigh_red: value.heigh_red,
        dm_low_yellow: value.low_green,
        dm_heigh_yellow: value.heigh_yellow,
        dm_low_green: value.low_green,
        dm_heigh_green: value.heigh_green,
        dm_low_ex_red: value.extra_low_red,
        dm_heigh_ex_red: value.extra_heigh_red,
        isFatching: false,
      })

    }).catch((err) => {
      console.log('get by id response  err = ', err)
      const error = err.response ? err.response.data.msg : err;
      this.setState({
        errorDMMsg: error,
        isUpdating: false,
      });
    })
  }

  /**
 * Handle bank validation
 */
  handleDMValidation = () => {
    // Request validate
    let dmScoreErrors = validateUpdateDMRequest(this.state);

    this.setState({ dmScoreErrors });
    return dmScoreErrors;
  };

  /**
   * Handle add bank with validations
   */
  handleUpdateDMScore = () => {
    const errors = this.handleDMValidation();
    console.log('call api from backend errors  handleUpdateDMScore ', Object.getOwnPropertyNames(errors).length)
    if (Object.getOwnPropertyNames(errors).length === 0) {
      console.log('call api from backend')
      this.updateDMScores();
    }
  };


  /**
* Handle add bank
*/
  updateDMScores = () => {
    this.setState({ isUpdating: true });
    const { dm_id } = this.state;
    console.log('call api from updateDMScores dm_id ', dm_id)
    // Make form request payload
    let formRequest = prepareFormRequestParamsfForDM(this.state);

    let _this = this;
    updateDMScoreMangementAPI(dm_id, formRequest)
      .then((res) => {
        console.log('call api from updateDMScoreMangementAPI res ', res.data)
        if (res.data && res.data.status == 200) {
          this.setState({
            successDMMsg: res.data.msg,
            isUpdating: false,
            errorDMMsg: "",

          });
          // _this.props.history.push("/bank-list");

          setTimeout(function () {
            _this.setState({ successDMMsg: "" });
            // _this.props.history.push("/bank-list");
          }, 4000);
        } else {
          this.setState({
            errorDMMsg: res.data.msg,
            isUpdating: false,
            successDMMsg: "",
          });
          setTimeout(function () {
            _this.setState({ errorDMMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        console.log('call api from updateDMScoreMangementAPI err ', err)
        console.log('call api from updateDMScoreMangementAPI err response ', err.response)
        // err.response.data.message
        this.setState({
          errorDMMsg:( err.response && err.response.data) ?  "Something went wrong !" : "Something went wrong !",
          isUpdating: false,
        });
        setTimeout(function () {
          _this.setState({ errorDMMsg: "" });
        }, 4000);
      });
  };




  render() {
    const {
      filterai_score_lists,
      errorMsg,
      successMsg,
      manageRatingErrors,
      succesMsg,
      loading,
      isUpdating,
      filterdm_score_lists,
      ai_low_red,
      ai_heigh_red,
      ai_low_yellow,
      ai_heigh_yellow,
      ai_low_green,
      ai_heigh_green,
      ai_low_ex_red,
      ai_heigh_ex_red,
      dm_low_red,
      dm_heigh_red,
      dm_low_yellow,
      dm_heigh_yellow,
      dm_low_green,
      dm_heigh_green,
      dm_low_ex_red,
      dm_heigh_ex_red,
      showEditSection,
      isFatching,

      dmScoreErrors,
      showDMEditSection,
      errorDMMsg,
      successDMMsg,
      isUpdatingAI
    } = this.state;

    return (
      <Fragment>
        <div>
          <ManageRatingList
            ai_score_lists={filterai_score_lists}
            dm_score_lists={filterdm_score_lists}
            errorMsg={errorMsg}
            successMsg={succesMsg}
            loading={loading}
            openEditSection={this.openEditSection}
            manageRatingErrors={manageRatingErrors}
            handleClose={this.handleClose}
            handleInputValues={this.handleInputValues}
            // handleUpdateManageRating={this.handleUpdateBank}
            handleUpdateAIScore={this.handleUpdateAIScore}
            isUpdating={isUpdating}
            isFatching={isFatching}
            ai_low_red={ai_low_red}
            ai_heigh_red={ai_heigh_red}
            ai_low_yellow={ai_low_yellow}
            ai_heigh_yellow={ai_heigh_yellow}
            ai_low_green={ai_low_green}
            ai_heigh_green={ai_heigh_green}
            ai_low_ex_red={ai_low_ex_red}
            ai_heigh_ex_red={ai_heigh_ex_red}
            showEditSection={showEditSection}

            dm_low_red={dm_low_red}
            dm_heigh_red={dm_heigh_red}
            dm_low_yellow={dm_low_yellow}
            dm_heigh_yellow={dm_heigh_yellow}
            dm_low_green={dm_low_green}
            dm_heigh_green={dm_heigh_green}
            dm_low_ex_red={dm_low_ex_red}
            dm_heigh_ex_red={dm_heigh_ex_red}
            dmScoreErrors={dmScoreErrors}
            handleDMInputValues={this.handleDMInputValues}
            openDMEditSection={this.openDMEditSection}
            showDMEditSection={showDMEditSection}
            errorDMMsg={errorDMMsg}
            successDMMsg={successDMMsg}
            handleUpdateDMScore={this.handleUpdateDMScore}
            isUpdatingAI={isUpdatingAI}

          />

          {/* {isDelete && (
            <CModal
              show={isDelete}
              onClose={this.handleCancelModal}
              color="danger"
            >
              <CModalHeader closeButton>
                <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
              </CModalHeader>
              <CModalBody>
                Are you sure to delete this Bank <b>{deleteItem.name}</b>
              </CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={this.handleDeleteConfirm}>
                  Delete
                </CButton>{" "}
                <CButton color="secondary" onClick={this.handleCancelModal}>
                  Cancel
                </CButton>
              </CModalFooter>
            </CModal>
          )} */}
        </div>
      </Fragment>
    );
  }
}
