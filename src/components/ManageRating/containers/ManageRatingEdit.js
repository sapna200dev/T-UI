import React, { Component, Fragment } from "react";

// import { UpdateBank } from "../components/Bank";
import { getBankRecord, updateBank } from "../services";
// import { BANK_STATE, validateUpdateAIRequest } from "../contants";
import { validateUpdateAIRequest, prepareFormRequestParams } from "../functions";
import { ManageRatingEditForm } from "../components";




export class ManageRatingEdit extends Component {
  state = {
    low1: '',
    high1: '',
    low2: '',
    high2: '',
    low3: '',
    high3: '',
    low4: '',
    high4: '',

    low_red: '0%',
    heigh_red: '',
    low_yellow: '',
    heigh_yellow: '',
    low_green: '',
    heigh_green: '',
    low_ex_red: '',
    heigh_ex_red: '100%',
    manageRatingErrors: {}
  };

  componentDidMount() {
    console.log(' manage componentDidMount id = ', this.props)

    const { id } = this.props.match.params;
    // this.setState({ id, loading: true });

    console.log(' manage rating id = ', id)
    // Get bank details by id
    // this.getBankDetails(id);
  }

  /**
   * Get bank details
   */
  getBankDetails = (id) => {
    getBankRecord(id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            loading: false,
            errorMsg: "",
            bank_name: res.data.data.bank_name,
            address: res.data.data.address,
            location: res.data.data.location,
            status: res.data.data.status,
            billing_status: res.data.data.billing_status,
            company_email: res.data.data.company_email,
            contact_email: res.data.data.contact_email,
            company_phone: res.data.data.company_phone,
            contact_name: res.data.data.contact_name,
            contact_phone: res.data.data.contact_phone,
            contact_location: res.data.data.contact_location,
            billing_cycle: res.data.data.billing_cycle,
            billing_cycle_from: res.data.data.billing_cycle_from,
            billing_cycle_to: res.data.data.billing_cycle_to,
            admin_username: res.data.data.admin_username,
            uploaded_1: res.data.data.upload_1,
            uploaded_2: res.data.data.upload_2,
            uploaded_3: res.data.data.upload_3,
            uploaded_4: res.data.data.upload_4,
            uploaded_5: res.data.data.upload_5,
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
          });
        }
      })
      .catch((err) => {
        console.log("error => ", err);
      });
  };

  /**
   * Handle Bank inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;

    this.testValidateNumber(name, value, name)


  };


  /**
 * Test validate number inputs
 */
  testValidateNumber = (name, value, field_name) => {
    let manageRatingErrors = {}

    if (!/^[0-9]+$/.test(value)) {
      manageRatingErrors[field_name] = 'This field is mandatory and Please only enter numeric characters'
      this.setState({ [name]: value, manageRatingErrors, is_change: false });
    } else {
      manageRatingErrors[field_name] = '';

      if (name == 'heigh_red') {
        this.setState({ ['low_yellow']: value + ' %' });
      } else if (name == 'heigh_yellow') {
        this.setState({ ['low_green']: value + ' %' });
      } else if (name == 'heigh_green') {
        this.setState({ ['low_ex_red']: value + ' %' });
      }



      this.setState({ [name]: value, is_change: true, manageRatingErrors });
    }

    return manageRatingErrors;
  }



  /**
   * Handle bank validation
   */
  handleAddBankValidation = () => {
    // Request validate
    let manageRatingErrors = validateUpdateAIRequest(this.state);

    this.setState({ manageRatingErrors });
    return manageRatingErrors;
  };

  /**
   * Handle add bank with validations
   */
  handleUpdateBank = () => {
    const errors = this.handleAddBankValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.updateBankRecod();
    }
  };

  /**
   * Handle add bank
   */
  updateBankRecod = () => {
    this.setState({ isUpdating: true });
    const { id } = this.state;

    // Make form request payload
    let formRequest = prepareFormRequestParams(this.state);

    let _this = this;
    updateBank(id, formRequest)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            succesMsg: res.data.msg,
            isUpdating: false,
            errorMsg: "",
          });
          // _this.props.history.push("/bank-list");

          setTimeout(function () {
            _this.setState({ succesMsg: "" });
            // _this.props.history.push("/bank-list");
          }, 4000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            isUpdating: false,
            succesMsg: "",
          });
          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          isUpdating: false,
        });
        setTimeout(function () {
          _this.setState({ succesMsg: "" });
        }, 4000);
      });
  };



  /**
   * Handle upload docs events
   */
  handleInputValuesUpload2 = (event, uploadType = null) => {
    if (uploadType == "coi_upload") {
      this.setState({
        upload_1: event.target.files[0],
      });
    } else if (uploadType == "gst_upload") {
      this.setState({
        upload_2: event.target.files[0],
      });
    } else if (uploadType == "pan_upload") {
      this.setState({
        upload_3: event.target.files[0],
      });
    } else if (uploadType == "agreement_upload") {
      this.setState({
        upload_4: event.target.files[0],
      });
    } else if (uploadType == "customer_logo_upload") {
      this.setState({
        upload_5: event.target.files[0],
      });
    }
  };

  render() {
    const {

      errorMsg,

      manageRatingErrors,
      succesMsg,
      loading,
      isUpdating,
      low_red,
      heigh_red,
      low_yellow,
      heigh_yellow,
      low_green,
      heigh_green,
      low_ex_red,
      heigh_ex_red,

    } = this.state;

    return (
      <Fragment>
        <div>
          <ManageRatingEditForm

            errorMsg={errorMsg}
            manageRatingErrors={manageRatingErrors}
            succesMsg={succesMsg}
            errorMsg={errorMsg}
            loading={loading}
            handleInputValues={this.handleInputValues}
            handleUpdateManageRating={this.handleUpdateBank}

            handleInputValuesUpload2={this.handleInputValuesUpload2}
            isUpdating={isUpdating}

            low_red={low_red}
            heigh_red={heigh_red}
            low_yellow={low_yellow}
            heigh_yellow={heigh_yellow}
            low_green={low_green}
            heigh_green={heigh_green}
            ext_low_red={low_ex_red}
            ext_low_heigh={heigh_ex_red}

          />
        </div>
      </Fragment>
    );
  }
}
