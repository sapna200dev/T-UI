import React, { Fragment } from "react";

import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ContentLoading, InvertContentLoader } from "../../Comman/components";

export const ShowBranchDetails = (props) => {
  const {
    sale_agents,
    team_leads,
    successMsg,
    errorMsg,
    branch_name,
    area,
    address,
    city_id,
    state_id,
    locality,
    phone,
    sale_agent_id,
    team_lead_id,
    pin_code,
    branch_manager_name,
    states,
    cities,
    country_id,
    countries,
    isFatchingData,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b>Branch View Details</b>
          </h4>
        </CCardHeader>
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                {isFatchingData && (
                  <ContentLoading content="Fetching record.." />
                )}

                <CLabel htmlFor="branch_name">
                  Branch Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="branch_name"
                  name="branch_name"
                  value={branch_name}
                  placeholder="Branch Name"
                  className="detail-text-color"
                />

              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="middle_name">
                  City <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="city_id"
                  id="city_id"
                  value={city_id}
                  className="detail-text-color"
                >
                  <option value="">Please Select State</option>
                  {cities ? (
                    cities.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    ))
                  ) : (
                    <option value="">No Cities</option>
                  )}
                </CSelect>

              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  States<small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="state_id"
                  id="state_id"
                  value={state_id}
                  className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {states ? (
                    states.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    ))
                  ) : (
                    <option value="">No States</option>
                  )}
                </CSelect>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Country Code<small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="country_id"
                  id="country_id"
                  value={country_id}
                  className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {countries ? (
                    countries.map((item) => (
                      <option value={item.value}>{item.text}</option>
                    ))
                  ) : (
                    <option value="">No Countries</option>
                  )}
                </CSelect>
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="address">
                  Address<small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="address"
                  name="address"
                  value={address}
                  placeholder="Address"
                  className="detail-text-color"
                  value={address}
                />
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="area">
                  Area <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="area"
                  name="area"
                  value={area}
                  placeholder="Area"
                  className="detail-text-color"
                  value={area}
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="locality">
                  Locality <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="locality"
                  name="locality"
                  value={locality}
                  placeholder="Locality"
                  className="detail-text-color"
                  value={locality}
                />
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="pin_code">Pin Code</CLabel>
                <CInput
                  id="pin_code"
                  name="pin_code"
                  value={pin_code}
                  placeholder="Pin Code"
                  className="detail-text-color"
                  value={pin_code}
                />

              </CCol>
              <CCol md="3">
                <CLabel htmlFor="branch_manager_name">
                  Branch Manager Name
                </CLabel>
                <CInput
                  id="branch_manager_name"
                  name="branch_manager_name"
                  value={branch_manager_name}
                  placeholder="Branch Manager Name"
                  className="detail-text-color"
                  value={branch_manager_name}
                />

              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="phone">Branch Phone</CLabel>
                <CInput
                  id="phone"
                  name="phone"
                  value={phone}
                  placeholder="Branch Phone"
                  className="detail-text-color"
                  value={phone}
                />
              </CCol>

              <CCol md="3">
                {/* <CLabel htmlFor="sale_agent_id">
                  Sales Agent<small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="sale_agent_id"
                  id="sale_agent_id"
                  className="detail-text-color"
                  value={sale_agent_id}
                >
                  <option value="">Please select</option>
                  {sale_agents ? (
                    sale_agents.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Sales Agents</option>
                  )}
                </CSelect> */}

              </CCol>
              <CCol md="3">
                {/* <CLabel htmlFor="team_lead_id">
                  Team Leader <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="team_lead_id"
                  id="team_lead_id"
                  className="detail-text-color"
                  value={team_lead_id}
                >
                  <option value="">Please select</option>
                  {team_leads ? (
                    team_leads.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Team Leads</option>
                  )}
                </CSelect> */}
              </CCol>
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter>
          <span className="buttonRight">
            <a href="#/manage-branches" className="button_style">
              <CButton
                type="reset"
                size="sm"
                color="danger"
                className="remove_button"
              >
                <CIcon name="cil-ban" /> Go Back
            </CButton>
            </a>
          </span>
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
