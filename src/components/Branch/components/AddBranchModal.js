import React, { Fragment } from "react";

import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { InvertContentLoader } from "../../Comman/components";

export const AddBranchModal = (props) => {
  const {
    handleInputValues,
    successMsg,
    errorMsg,
    branch_name,
    area,
    address,
    city_id,
    state_id,
    locality,
    phone,
    sale_agent_id,
    team_lead_id,
    pin_code,
    branch_manager_name,
    states,
    cities,
    isFatchingCities,
    citiDetails,
    fetchingDetails,
    country_id,
    country_code,
    isFatchingCountry,
    countries,
    handleAddBranch,
    handleErrors,
    branchCreating,
    showAddModal,
    add_type,
    fresh_login
  } = props;

  return (
    <Fragment>
      {/* {display_msg_header && ( */}
      <div className="call_message ">
        <div className="alert alert-warning alert_css ">
          {/* <marquee className="stop" behavior="scroll" direction="right" scrollamount="5"><strong>Please !</strong> Do not refresh the page till call ends.</marquee> */}
          <span>
            <strong>Note :</strong> To add a New City / State / Country, please follow the sequence. &ensp;
            {/* <br /> */}
            <span>
              {`( country -> state -> city )`}
            </span>
          </span>
        </div>
      </div>
      {/* //  )}  */}
      <CCard>
        <CCardHeader>
          <h4>
            <b> Add New Branch</b>
          </h4>
        </CCardHeader>
        {fresh_login && (
          <CAlert color="warning" className="msg_div">
            <span className="blink new_user">
              You are fresh login So you need to create branch first !
            </span>
          </CAlert>
        )}
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="branch_name">
                  Branch Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="branch_name"
                  name="branch_name"
                  value={branch_name}
                  placeholder="Branch Name"
                  onChange={handleInputValues}
                  invalid={handleErrors.branch_name}
                />
                <CInvalidFeedback>{handleErrors.branch_name}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3">


                <CLabel htmlFor="last_name">
                  Country Code <small className="required_lable">*</small>
                  {(fetchingDetails || isFatchingCountry) && (
                    <InvertContentLoader color={"black"} />
                  )}
                </CLabel>
                <CTooltip
                  content={`Add New Country`}
                  placement={"top-start"}
                >
                  <span className="add_more">
                    {" "}
                    <i
                      className="fa fa-plus"
                      aria-hidden="true"
                      onClick={() => showAddModal("country")}
                    ></i>
                  </span>
                </CTooltip>


                <CSelect
                  custom
                  name="country_id"
                  id="country_id"
                  //   invalid={handleErrors.country_id}
                  onChange={handleInputValues}
                  value={country_id}
                  disabled={city_id != "" ? false : true}
                >
                  <option value="">Please select</option>
                  {countries ? (
                    countries.map((item) => (
                      <option value={item.value}>{item.text}</option>
                    ))
                  ) : (
                    <option value="">No Countries</option>
                  )}
                </CSelect>
                <CInvalidFeedback>{handleErrors.country_id}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  State <small className="required_lable">*</small>
                  {fetchingDetails && <InvertContentLoader color={"black"} />}
                </CLabel>
                <CTooltip
                  content={`Add New State`}
                  placement={"top-start"}
                >
                  <span className="add_more">
                    {" "}
                    <i
                      className="fa fa-plus"
                      aria-hidden="true"
                      onClick={() => showAddModal("state")}
                    ></i>
                  </span>
                </CTooltip>

                <CSelect
                  custom
                  name="state_id"
                  id="state_id"
                  //   invalid={handleErrors.state_id}
                  onChange={handleInputValues}
                  value={state_id}
                  disabled={city_id != "" ? false : true}
                >
                  <option value="">Please select</option>
                  {states ? (
                    states.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    ))
                  ) : (
                    <option value="">No States</option>
                  )}
                </CSelect>
                <CInvalidFeedback>{handleErrors.state_id}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="middle_name">
                  City <small className="required_lable">*</small>
                  {isFatchingCities && <InvertContentLoader color={"black"} />}
                </CLabel>

                <CTooltip
                  content={`Add New City`}
                  placement={"top-start"}
                >
                  <span className="add_more">
                    {" "}
                    <i
                      className="fa fa-plus"
                      aria-hidden="true"
                      onClick={() => showAddModal("city")}
                    ></i>
                  </span>
                </CTooltip>

                <CSelect
                  custom
                  name="city_id"
                  id="city_id"
                  onChange={handleInputValues}
                  value={city_id}
                  // disabled={state_id != "" ? false : true}
                  data-show-subtext="true"
                  data-live-search="true"
                >
                  <option value="">Please Select State</option>
                  {cities ? (
                    cities.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    ))
                  ) : (
                    <option value="">No Cities</option>
                  )}
                </CSelect>

              </CCol>

              {/* <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Country Code<small className="required_lable">*</small>
                  {fetchingDetails && <InvertContentLoader color={"black"} />}
                </CLabel>
                <CSelect
                  custom
                  name="country_code"
                  id="country_code"
                  //   invalid={handleErrors.country_code}
                  onChange={handleInputValues}
                  value={country_code}
                  disabled={city_id != "" ? false : true}
                >
                  <option value="">Please select</option>
                  {states ? (
                    states.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    ))
                  ) : (
                    <option value="">No States</option>
                  )}
                </CSelect>
                <CInvalidFeedback>{handleErrors.country_code}</CInvalidFeedback>
              </CCol> */}

              <CCol xs="12" md="3">
                <CLabel htmlFor="address">
                  Address <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="address"
                  name="address"
                  value={address}
                  placeholder="Address"
                  onChange={handleInputValues}
                  invalid={handleErrors.address}
                  value={address}
                />
                <CInvalidFeedback>{handleErrors.address}</CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="area">
                  Area <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="area"
                  name="area"
                  value={area}
                  placeholder="Area"
                  onChange={handleInputValues}
                  invalid={handleErrors.area}
                  value={area}
                />
                <CInvalidFeedback>{handleErrors.area}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="locality">
                  Locality <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="locality"
                  name="locality"
                  value={locality}
                  placeholder="Locality"
                  onChange={handleInputValues}
                  invalid={handleErrors.locality}
                  value={locality}
                />
                <CInvalidFeedback>{handleErrors.locality}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="pin_code">Pin Code</CLabel>
                <CInput
                  id="pin_code"
                  name="pin_code"
                  value={pin_code}
                  placeholder="Pin Code"
                  onChange={handleInputValues}
                  invalid={handleErrors.pin_code}
                  value={pin_code}
                />

                <CInvalidFeedback>{handleErrors.pin_code}</CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="branch_manager_name">
                  Branch Manager Name
                </CLabel>
                <CInput
                  id="branch_manager_name"
                  name="branch_manager_name"
                  value={branch_manager_name}
                  placeholder="Branch Manager Name"
                  onChange={handleInputValues}
                  invalid={handleErrors.branch_manager_name}
                  value={branch_manager_name}
                />
                <CInvalidFeedback>
                  {handleErrors.branch_manager_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="phone">Branch Phone Number</CLabel>
                <CInput
                  id="phone"
                  name="phone"
                  value={phone}
                  placeholder="Branch Phone"
                  onChange={handleInputValues}
                  invalid={handleErrors.phone}
                  value={phone}
                />
                <CInvalidFeedback>{handleErrors.phone}</CInvalidFeedback>
              </CCol>

              <CCol md="3">
                {/* <CLabel htmlFor="sale_agent_id">
                  Sales Agent<small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="sale_agent_id"
                  id="sale_agent_id"
                
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  {sale_agents ? (
                    sale_agents.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Sales Agents</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                 
                </CInvalidFeedback> */}
              </CCol>
              <CCol md="3">
                {/* <CLabel htmlFor="team_lead_id">
                  Team Leader <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="team_lead_id"
                  id="team_lead_id"
                  //   invalid={handleErrors.team_lead_id}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  {team_leads ? (
                    team_leads.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Team Leads</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                
                </CInvalidFeedback> */}
              </CCol>
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter>
        <span className="buttonRight">
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddBranch}
          >
            <CIcon name="cil-scrubber" /> Save
            {branchCreating && <InvertContentLoader />}
          </CButton>
          <a href="#/manage-branches" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
                </span>
          {/* <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
            onClick={handleResetEvent}
          >
            <CIcon name="cil-ban" /> Reset
          </CButton> */}
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
