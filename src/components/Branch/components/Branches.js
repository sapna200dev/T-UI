import React, { Fragment } from "react";
import {

  CCard,
  CCardBody,
  CCardHeader,

  CButton,
  CAlert,
  
} from "@coreui/react";
import ReactDatatable from '@ashvin27/react-datatable';
import { columns, config } from "../constants";


export const Branches = (props) => {
  const {
    branches,
    isFatching,
    errorMsg,

  } = props;

  const handleClick = () => {
    console.log("handleClick");
  };


  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> Branch List</b>
          <span>
          <a href="#/add-new-branch" className="button_style1">
                <CButton
                  type="submit"
                  size="sm"
                  color="primary"
                  // className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                >
                  {" "}
                  <i className="fa fa-plus" aria-hidden="true"></i> New Branch
                </CButton>
              </a>
           
          </span>

          {errorMsg && (
            <CAlert color="danger" className="msg_div">
              {errorMsg}
            </CAlert>
          )}
        </CCardHeader>
        <CCardBody>
          {/* <PageNotFound /> */}
          <ReactDatatable
            config={config}
            records={branches}
            columns={columns}
            extraButtons={[]}
            loading={isFatching}
            // tHeadClassName={'custom-table-head'}
          />
          {/* <CDataTable
            items={branches}
            fields={BRANCH_FIELDS}
            itemsPerPage={pagination_count}
            loading={isFatching}
            pagination
            scopedSlots={{
              address: (item) => (
                <td>
                  {" "}
                  {item.address.substring(0, 25)}{" "}
                  <span>
                    {item.address.length > 30 ? (
                      <CPopover 
                        header="View More"
                        content={`${item.address}`}
                        placement={"top"}
                        interactive={true}
                        trigger="click"
                      >
                        <span onClick={handleClick}>
                          <ShowMoreSvgIcon />
                        </span>
                        
                      </CPopover>
                    ) : (
                      <span></span>
                    )}
                  </span>
                </td>
              ),
              country_code: (item) => (
                <td>
                  {" "}
                  + {item.country.country_code}{" "}
              
                </td>
              ),
              city: (item) => (
                <td>
                  {" "}
                  {item.city && item.city != null ? item.city.name :'-'}{" "}
              
                </td>
              ),
              no_of_digits: (item) => (
                <td>
                  {" "}
                  {item.city && item.city.no_of_digits != null ? item.city.no_of_digits :'-'}{" "}
              
                </td>
              ),

              Action: (item) => (
                <td>
                  <a href={`#/branch-details/${item.id}`} className="cancel_bt">
                    <CButton
                      type="submit"
                      size="sm"
                      color="primary"
                      className="remove_button"
                    >
                        <CTooltip content={`View Record`} placement={"top-start"}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </CTooltip>
                    </CButton>
                  </a>
                </td>
              ),
            }}
          /> */}
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
