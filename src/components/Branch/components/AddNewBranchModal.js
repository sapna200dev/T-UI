import React, { Fragment } from "react";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CLabel,
  CInput,
  CInvalidFeedback,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { InvertContentLoader } from "../../Comman/components";

export const AddNewBranchModal = (props) => {
  const {
    input_val,
    showmodal,
    add_type,
    handleInputValues,
    handleErrors,
    handleAddInfo,
    showAddModal,
    addingInfo,
    country_name,
    countries,
    states,
    country_id1,
    state_id1,
    fetchingDetails,
    city_id,
    isFatchingCountry,
    isSaving,
    no_of_digits
  } = props;

  return (
    <Fragment>
      <CModal show={showmodal} color="primary" className="modalContent">
        <CModalHeader className="modalHeader">
          <CModalTitle style={{ color: "white" }}>Add {add_type}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <div>
            <CLabel htmlFor="branch_name">
              {add_type == "Country" ? "Country Code" : `${add_type} Name`}{" "}
              <small className="required_lable">*</small>
            </CLabel>
            <CInput
              id="input_val"
              name="input_val"
              value={input_val}
              placeholder={`Enter ${add_type}`}
              onChange={handleInputValues}
              invalid={handleErrors.input_val}
            />
            <CInvalidFeedback>{handleErrors.input_val}</CInvalidFeedback>

            {add_type == "Country" && (
              <Fragment>
                <br />
                <CLabel htmlFor="branch_name">
                  {add_type} Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="country_name"
                  name="country_name"
                  value={country_name}
                  placeholder={`Enter ${add_type}`}
                  onChange={handleInputValues}
                  invalid={handleErrors.country_name}
                />

                <CInvalidFeedback>{handleErrors.country_name}</CInvalidFeedback>
              </Fragment>
            )}

            {add_type == "State" ? (
              <Fragment>
                <br />
                <CLabel htmlFor="last_name">
                  Country Code <small className="required_lable">*</small>
                  {(fetchingDetails || isFatchingCountry) && (
                    <InvertContentLoader color={"black"} />
                  )}
                </CLabel>

                <CSelect
                  custom
                  name="country_id1"
                  id="country_id1"
                    invalid={handleErrors.country_id1}
                  onChange={handleInputValues}
                  value={country_id1}
                  // disabled={city_id != "" ? false : true}
                >
                  <option value="">Please select</option>
                  {countries ? (
                    countries.map((item) => (
                      <option value={item.value}>{item.text}</option>
                    ))
                  ) : (
                    <option value="">No Countries</option>
                  )}
                </CSelect>
                <CInvalidFeedback>{handleErrors.country_id1}</CInvalidFeedback>
              </Fragment>
            ) : (
              <Fragment>
                <br />
                {add_type == "City" && (
                  <Fragment>
                    <CLabel htmlFor="last_name">
                      States <small className="required_lable">*</small>
                      {(fetchingDetails || isFatchingCountry) && (
                        <InvertContentLoader color={"black"} />
                      )}
                    </CLabel>

                    <CSelect
                      custom
                      name="state_id1"
                      id="state_id1"
                        invalid={handleErrors.state_id1}
                      onChange={handleInputValues}
                      value={state_id1}
                      // disabled={city_id != "" ? false : true}
                    >
                      <option value="">Please select</option>
                      {states ? (
                        states.map((item) => (
                          <option value={item.id}>{item.name}</option>
                        ))
                      ) : (
                        <option value="">No States</option>
                      )}
                    </CSelect>
                    <CInvalidFeedback>{handleErrors.state_id1}</CInvalidFeedback>
                        
                  
                  </Fragment>
                )}

                 
                  {add_type == "City" && (
                    <Fragment>
                       <br />
                    <CLabel htmlFor="branch_name" style={{marginTop:'18px'}}>
                     No of phone digits <small className="required_lable">*</small>
                    </CLabel>
                    <CInput
                      id="no_of_digits"
                      name="no_of_digits"
                      value={no_of_digits}
                      placeholder={`Enter no of phone number digits`}
                      onChange={handleInputValues}
                      invalid={handleErrors.no_of_digits}
                    />

                    <CInvalidFeedback>{handleErrors.no_of_digits}</CInvalidFeedback>

                    </Fragment>
                  )}

              </Fragment>
            )}
          </div>
        </CModalBody>
        <CModalFooter>
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddInfo}
            disabled={
              isSaving ? true:false
            }
          >
            <CIcon name="cil-scrubber" /> Add
            {isSaving && <InvertContentLoader />}
          </CButton>{" "}
          <CButton color="secondary" onClick={() => showAddModal("close")}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </Fragment>
  );
};
