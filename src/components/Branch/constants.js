

import React, { Fragment } from "react";
import {
  CButton,
  CTooltip,
} from "@coreui/react";


export const BRANCH_FIELDS = [
  "branch_name",
  "country_code",
  "area",
  "pin_code",
  "city",
  "no_of_digits",
  "address",
  "Action",
];


export const PIN_CODE = 'pin_code';
export const PHONE = 'phone';
export const INPUT_VAL = 'input_val';
export const COUNTRY_NAME = 'country_name';
export const NO_OF_DIGITS = 'no_of_digits'

export const TYPE_CITY = 'City';
export const TYPE_STATE = 'State';
export const TYPE_COUNTRY = 'Country';

export const CITY = 'city';
export const STATE = 'state';
export const COUNTRY = 'country';


export const DIGIT_ERROR_MSG = "Please enter no of digits !";
export const STATE_ERROR_MSG = "Please select state !";
export const COUNTRY_ERROR_MSG = "Please select country !";

export const ERROR_MSGS = {
  VALID_NUMBER: "Only allowed numbers (digits)",
  TEXT_ERROR: "Enter at least 1 chars",
  REQUIRED_FIELD: "Field is required ",
  DEFAULT_ERROR_MSG: "Somthing went worngs !",

}


export const COULUMN_NAMES = [
  { id: 0, text: 'Name', value: 'branch_name' },
  { id: 1, text: 'Area', value: 'area' },
  { id: 2, text: 'Pin Code', value: 'pin_code' },
  { id: 3, text: 'Address', value: 'address' },
];

export const PAGINATION_COUNT = [
  { id: 0, text: '5', value: '5' },
  { id: 1, text: '10', value: '10' },
  { id: 2, text: '15', value: '15' },
  { id: 3, text: '20', value: '20' },
];



export const columns = [
  {
    key: "branch_name",
    text: "Branch name",
    className: "branch_name",
    align: "left",
    sortable: true,
  },
  {
    key: "country_code",
    text: "Country code",
    className: "country_code",
    align: "left",
    sortable: true
  },
  {
    key: "area",
    text: "Area",
    className: "area",
    align: "left",
    sortable: true
  },
  {
    key: "pin_code",
    text: "Pin code",
    className: "pin_code",
    align: "left",
    sortable: true
  },
  {
    key: "city_name",
    text: "City",
    className: "city_name",
    sortable: true,
    align: "left"
  },
  {
    key: "no_of_digits",
    text: "No of digits",
    className: "no_of_digits",
    sortable: true,
    align: "left"
  },

  {
    key: "address",
    text: "Address",
    className: "address",
    sortable: true,
    align: "left"
  },
  {
    key: "view_more",
    text: "View",
    className: "view_more",
    sortable: false,
    align: "left",
    cell: record => {
      return (
        <Fragment>

          <a href={`#/branch-details/${record.id}`} className="cancel_bt">
            <CButton
              type="submit"
              size="sm"
              color="primary"
              className="remove_button view_btn"
            >
              <CTooltip content={`View Record`} placement={"top-start"}>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </CTooltip>
            </CButton>
          </a>
        </Fragment>
      )
    }
  },

];



export const config = {
  page_size: 10,
  length_menu: [10, 20, 50],
  button: {
    excel: true,
    print: true,
    csv: true
  },
  show_filter: true,
  filename: 'turant_branches',
  language: {
    loading_text: "Please be patient while data loads..."
  }
  // pagination:'advance'
};

