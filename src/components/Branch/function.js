
/**
 * Create request payload
 * @param props 
 */
export const CreateReuqestPayload = (props) => {
    const {
        branch_name,
        area,
        address,
        city_id,
        state_id,
        locality,
        phone,
        sale_agent_id,
        team_lead_id,
        pin_code,
        branch_manager_name,
        country_id
      } = props;

      console.log('payload  props= ',props)
 

      const payload = {

        branch_name,
        area,
        address,
        city_id,
        state_id,
        locality,
        branch_phone:phone,
        sales_agent_id : parseInt(sale_agent_id) ,
        team_leader_id:parseInt(team_lead_id),
        pin_code,
        manager_name:branch_manager_name,
        country_id
      }

      return payload;
  }