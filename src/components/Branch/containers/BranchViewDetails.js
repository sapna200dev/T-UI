import React, { Component, Fragment } from "react";
import { AddBranchModal } from "../components/index";
import { connect } from "react-redux";
import { getStates, createBranch, getCitiesList, getCityByName, getCountries, getBranchById } from "../services/index";
import {CreateReuqestPayload} from '../function'
import {ShowBranchDetails} from '../components/ShowBranchDetails'
import {
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../EndUser/actions/EndUser/index";

class BranchViewDetails extends Component {
  state = {
    branch_name: "",
    area: "",
    address: "",
    city_id: "",
    state_id: "",
    locality: "",
    phone: "",
    sale_agent_id: "",
    team_lead_id: "",
    pin_code: "",
    branch_manager_name: "",
    isAdding: false,
    errorMsg: "",
    successMsg: "",
    states: [],
    cities: [],
    isFatchingData:false,
    citiDetails:[],
    country_code:'',
    country_id:'',
    countries:[],
    id:'',
  };
  componentDidMount() {
    const { fetchSaleAgents, fetchTeamLeads } = this.props;
    this.setState({ isFatchingData: true });
    this.getStatesList();
    this.getCitiesList();
    this.getCountryList();

    fetchSaleAgents();
    fetchTeamLeads();

    const { id } = this.props.match.params;
    this.setState({ id, loading: true });
    
    // Get bank details by id
    this.getBranchDetails(id);


    // getBranchById
  }


  getBranchDetails = (id) => {
    getBranchById(id).then(response => {
        console.log('response = ', response)

        if(response.data.status == 200) {
            let data = response.data.data;

            this.setState({
                branch_name:data.branch_name,
                area:data.area,
                address:data.address,
                city_id:data.city.id,
                state_id:data.state.id,
                locality:data.locality,
                phone:data.branch_phone,
                // sale_agent_id:data.sales_agent.id,
                // team_lead_id:data.team_leader.id,
                pin_code:data.pin_code,
                branch_manager_name:data.manager_name,
                country_id:data.country.id,
                isFatchingData:false
            })
        } else {
            this.setState({
                errorMsg:response.data.msg,
                isFatchingData:false
            })
        }
    }).catch(err => {
        console.log('error get by id = ', err)
        this.setState({
            errorMsg:err.response ? err.response.data.msg : 'Somthign went wrong !',
            isFatchingData:false
        })
    })
  }
  

/**
 * Get all countries
 */
  getCountryList = () => {
    getCountries()
      .then((res) => {
        console.log("all getCountries = ", res);
        let countries = res.data.data;

        const getOptions = countries.map((item, index) => {
          return {
            key: index,
            text: `${item.country_code} - ${item.country_name}`,
            value: item.id,
          };
        });

        this.setState({
          isFatchingCountry: false,
          errorMsg: "",
          countries: getOptions,
        });
      })
      .catch((err) => {
        console.log("error dddd=> ", err.response.data);
        this.setState({
          isFatchingCountry: false,
          errorMsg: err.response.data.message,
        });
      });
  };


/**
 * Get all States
 */
  getStatesList = () => {
    getStates()
      .then((res) => {
        console.log("all states = ", res);
        this.setState({
          isFatchingState: false,
          errorMsg: "",
          states: res.data.data,
        });
      })
      .catch((err) => {
        console.log("error dddd=> ", err.response.data);
        this.setState({
          isFatchingState: true,
          errorMsg: err.response.data.message,
        });
      });
  };

  /**
   * Fetch all cities for current bank
   */
  getCitiesList = () => {
    // Call get city api
    getCitiesList()
      .then((res) => {
        console.log("all cities list = ", res);
        this.setState({
          isFatchingCities: false,
          errorMsg: "",
          cities: res.data.data,
        });
      })
      .catch((err) => {
        console.log("error dddd=> ", err.response.data);
        this.setState({
          isFatchingCities: true,
          errorMsg: err.response.data.message,
        });
      });
  };




  render() {
    const { sale_agents, team_leads } = this.props;
    const {
      isFatchingState,
      errorMsg,
      branch_name,
      area,
      address,
      city_id,
      state_id,
      locality,
      phone,
      sale_agent_id,
      team_lead_id,
      pin_code,
      branch_manager_name,
      states,
      cities,
      isFatchingCities,
      addBranchErrors,
      citiDetails,
      fetchingDetails,
      country_code,
      country_id,
      isFatchingCountry,
      countries,
      handleErrors,
      branchCreating,
      successMsg,
      isFatchingData
    } = this.state;


    return (
      <Fragment>
        <ShowBranchDetails
          isFatchingState={isFatchingState}
          errorMsg={errorMsg}
          branch_name={branch_name}
          area={area}
          address={address}
          city_id={city_id}
          state_id={state_id}
          locality={locality}
          phone={phone}
          sale_agent_id={sale_agent_id}
          team_lead_id={team_lead_id}
          pin_code={pin_code}
          branch_manager_name={branch_manager_name}
          states={states}
          team_leads={team_leads}
          cities={cities}
          sale_agents={sale_agents}
          isFatchingCities={isFatchingCities}
          addBranchErrors={addBranchErrors}
          citiDetails={citiDetails}
          fetchingDetails={fetchingDetails}
          country_id={country_id}
          country_code={country_code}
          countries={countries}
          isFatchingCountry={isFatchingCountry}
          handleErrors={handleErrors}
          branchCreating={branchCreating}
          successMsg={successMsg}
          isFatchingData={isFatchingData}
        />
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BranchViewDetails);
