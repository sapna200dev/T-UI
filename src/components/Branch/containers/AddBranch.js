import React, { Component, Fragment } from "react";
import { AddBranchModal } from "../components/index";
import { connect } from "react-redux";
import {
  getStates,
  createBranch,
  getCitiesList,
  getCityByName,
  getCountries,
  addBranchInfo,
} from "../services/index";
import { CreateReuqestPayload } from "../function";
import {
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../EndUser/actions/EndUser/index";
import { AddNewBranchModal } from "../components/index";
import { getLoggedInUser } from "../../Comman/functions";

import { INPUT_VAL, PHONE, PIN_CODE, COUNTRY_NAME, STATE, COUNTRY, CITY, TYPE_CITY, TYPE_COUNTRY, TYPE_STATE, ERROR_MSGS, STATE_ERROR_MSG, DIGIT_ERROR_MSG, COUNTRY_ERROR_MSG, NO_OF_DIGITS } from '../constants'

class AddBranch extends Component {
  state = {
    branch_name: "",
    area: "",
    address: "",
    city_id: "",
    state_id: "",
    locality: "",
    phone: "",
    sale_agent_id: "",
    team_lead_id: "",
    pin_code: "",
    branch_manager_name: "",
    isAdding: false,
    errorMsg: "",
    successMsg: "",
    isFatchingState: false,
    states: [],
    addBranchErrors: {},
    cities: [],
    fetchingDetails: false,
    citiDetails: [],
    country_code: "",
    country_id: "",
    isFatchingCountry: false,
    countries: [],
    handleErrors: {},
    branchCreating: false,
    showmodal: false,
    add_type: "",
    input_val: "",
    addingInfo: false,
    country_name: '',
    type: '',
    state_id1: '',
    country_id1: '',
    no_of_digits: '',
    userObj:'',
    fresh_login:false
  };
  componentDidMount() {
    let user='';
    const login_user = getLoggedInUser("user");
    
    const { fetchSaleAgents, fetchTeamLeads } = this.props;
    this.setState({ isFatchingCities: true });
    this.getStatesList();
    this.getCitiesList();
    this.getCountryList();

    fetchSaleAgents();
    fetchTeamLeads();


    if (login_user) {
      user = JSON.parse(login_user);
      if(user.last_login == null) {
        this.setState({
          // userObj : JSON.parse(login_user)
          fresh_login : true
        })
      }
 
     
    } // End if


  }

  /**
   * Get all countries
   */
  getCountryList = () => {
    getCountries()
      .then((res) => {
        console.log("all getCountries = ", res);
        let countries = res.data.data;

        const getOptions = countries.map((item, index) => {
          return {
            key: index,
            text: `${item.country_code} - ${item.country_name}`,
            value: item.id,
          };
        });

        this.setState({
          isFatchingCountry: false,
          errorMsg: "",
          countries: getOptions,
        });
      })
      .catch((err) => {

        this.setState({
          isFatchingCountry: false,
          errorMsg: err.response.data.message,
        });
      });
  };

  /**
   * Get all States
   */
  getStatesList = () => {
    getStates()
      .then((res) => {
        console.log("all states = ", res);
        this.setState({
          isFatchingState: false,
          errorMsg: "",
          states: res.data.data,
        });
      })
      .catch((err) => {

        this.setState({
          isFatchingState: true,
          errorMsg: err.response.data.message,
        });
      });
  };

  /**
   * Fetch all cities for current bank
   */
  getCitiesList = () => {
    // Call get city api
    getCitiesList()
      .then((res) => {
        console.log("all cities list = ", res);
        this.setState({
          isFatchingCities: false,
          errorMsg: "",
          cities: res.data.data,
        });
      })
      .catch((err) => {

        this.setState({
          isFatchingCities: true,
          errorMsg: err.response ? err.response.data.message : 'Somthing went wrong !',
        });
      });
  };

  handleInputValues = (e) => {
    const { name, value } = e.target;

    console.log("getCityByState name = ", name);
    console.log("getCityByState value = ", value);
    if (name == "city_id") {

      if (value != '') {
        this.setState({ fetchingDetails: true });
        this.getCityDetails(value, true);
        this.setState({ [name]: value, is_change: true });
      } else {


        this.setState({ [name]: value, is_change: true, country_id: '', state_id: '' });
      }

    } else {
      if (name == PIN_CODE) {
        this.testValidateNumber(name, value, PIN_CODE);
      } else if (name == PHONE) {
        this.testValidateNumber(name, value, PHONE);
      } else if (name == INPUT_VAL) {
        this.testValidateField(name, value, INPUT_VAL);
      } else if (name == COUNTRY_NAME) {
        this.testValidateField(name, value, COUNTRY_NAME);
      } else if (name == NO_OF_DIGITS) {
        this.testValidateNumber(name, value, NO_OF_DIGITS);

      } else {
        this.setState({ [name]: value, is_change: true });
      }
    }
  };

  /**
   * Get city related data
   * @param  city_id
   */
  getCityDetails = (city_id) => {
    getCityByName(city_id)
      .then((res) => {
        console.log("getCityByState response = ", res);
        if (res.data.status == 200) {
          let data = res.data.data;
          console.log("getCityByState data = ", data.state);
          this.setState({
            errorMsg: "",
            citiDetails: data,
            fetchingDetails: false,
            country_id: data.state
              ? data.state.country
                ? data.state.country.id
                : "NA"
              : "NA",
            state_id: data.state ? data.state.id : "NA",
          });
        } else {
          this.setState({
            errorMsg: res.data.message,
            citiDetails: [],
            fetchingDetails: false,
          });
        }
      })
      .catch((err) => {
        console.log("response  getCityByState err = ", err);
        this.setState({
          errorMsg: err.response
            ? err.response.data.message
            : "Sonthing went wrong !",
          citiDetails: [],
          fetchingDetails: false,
        });
      });
  };

  /**
   * Test validate number inputs
   */
  testValidateNumber = (name, value, field_name) => {
    let handleErrors = {};

    if (!/^[0-9]+$/.test(value)) {
      handleErrors[field_name] = ERROR_MSGS.VALID_NUMBER;
      this.setState({ [name]: value, handleErrors });
    } else {
      handleErrors[field_name] = "";
      this.setState({ [name]: value, handleErrors });
    }

    return handleErrors;
  };

  /**
   * Test validate number inputs
   */
  testValidateField = (name, value, field_name) => {
    let handleErrors = {};

    if (value.length < 1) {
      handleErrors[field_name] = ERROR_MSGS.TEXT_ERROR;
      this.setState({ [name]: value, handleErrors });
    } else {
      handleErrors[field_name] = "";
      this.setState({ [name]: value, handleErrors });
    }

    return handleErrors;
  };

  /**
   * Handle bank validation
   */
  handleAddBranchValidation = () => {
    const handleErrors = {};
    const {
      branch_name,
      area,
      address,
      locality,
      phone,
      pin_code,
      branch_manager_name,
    } = this.state;

    if (branch_name.length < 1) {
      handleErrors.branch_name = ERROR_MSGS.REQUIRED_FIELD;
    } else if (!/^[a-zA-Z]*$/g.test(branch_name)) {
      handleErrors.branch_name = "Only letters are allowd";
    } else if (address.length < 1) {
      handleErrors.address = ERROR_MSGS.REQUIRED_FIELD;
    } else if (area.length < 1) {
      handleErrors.area = ERROR_MSGS.REQUIRED_FIELD;
    } else if (locality.length < 1) {
      handleErrors.locality = ERROR_MSGS.REQUIRED_FIELD;
    } else if (pin_code.length < 1) {
      handleErrors.pin_code = ERROR_MSGS.REQUIRED_FIELD;
    } else if (branch_manager_name.length < 1) {
      handleErrors.branch_manager_name = ERROR_MSGS.REQUIRED_FIELD;
    } else if (phone.length < 1) {
      handleErrors.phone = ERROR_MSGS.REQUIRED_FIELD;
    }

    this.setState({ handleErrors });
    return handleErrors;
  };

  /**
   * Handle add branch api
   */
  handleAddBranch = () => {
    const payload = CreateReuqestPayload(this.state);
    const errors = this.handleAddBranchValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      // Call branch api
      this.createNewBranch(payload);
    }
  };

  /**
   * Create branch api define
   * @param } data
   */
  createNewBranch = (data) => {
    this.setState({ branchCreating: true });
    createBranch(data)
      .then((res) => {
        console.log("addedd rsponse = ", res);
        let _this = this;

        if (res.data.status == 200) {
          console.log("response success = ", res.data);
          this.setState({
            branchCreating: false,
            successMsg: res.data.msg,
            branch_name: "",
            area: "",
            address: "",
            city_id: "",
            state_id: "",
            locality: "",
            phone: "",
            sale_agent_id: "",
            team_lead_id: "",
            pin_code: "",
            branch_manager_name: "",
          });

          setTimeout(() => {
            _this.props.history.push("/manage-branches");
          }, 6000);
        } else {
          console.log("response error = ", res.data);
          this.setState({
            errorMsg: ERROR_MSGS.DEFAULT_ERROR_MSG,
            branchCreating: false,
            successMsg: "",
          });
        }
        setTimeout(() => {
          this.setState({ errorMsg: "", successMsg: "" });
        }, 4000);
      })
      .catch((err) => {
        console.log("error => ", err);
        console.log("error response = ", err.response);
        this.setState({
          errorMsg: err.response
            ? err.response.data.msg
            : ERROR_MSGS.DEFAULT_ERROR_MSG,
          branchCreating: false,
          successMsg: "",
        });

        setTimeout(() => {
          this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  /**
   * Show add branch modal
   * @param } type
   */
  showAddModal = (type) => {
    console.log('showAddModalshowAddModal = ', type)
    if (type == CITY) {
      this.setState({
        showmodal: true,
        add_type: TYPE_CITY,
        type: CITY
      });
    } else if (type == STATE) {

      this.setState({
        showmodal: true,
        add_type: TYPE_STATE,
        type: STATE
      });
    } else if (type == COUNTRY) {

      this.setState({
        showmodal: true,
        add_type: TYPE_COUNTRY,
        type: COUNTRY
      });
    } else if ("close") {

      this.setState({
        showmodal: false,
        input_val: '',
        country_name: '',
        handleErrors: {},
        country_id: '',
        state_id: '',
        isCountry: false,
        isCity: false,
        isState: false
      });
    }
  };

  /**
   * Handle add new branch api
   */ 
  handleAddInfo = () => {
    const { add_type, input_val, country_name, country_id1, state_id1, handleErrors, type, no_of_digits } = this.state;
    const login_user = getLoggedInUser("user");
    let user = '';

    console.log('no_of_digits ')
    console.log(no_of_digits)

    console.log('TYPE_CITY TYPE_CITY ')
    console.log(TYPE_CITY)


    if (login_user) {
      user = JSON.parse(login_user);
    } // End if

    let noError = false;
    if (input_val == '') {
      noError = true;
      handleErrors['input_val'] = "Field is required, Enter at  least 1 chars !";
      this.setState({ handleErrors });
    } else {
      noError = false;
      handleErrors['input_val'] = "";
      this.setState({ handleErrors });
    } // End if-else

    if (country_name == '') {
      noError = true;
      handleErrors['country_name'] = "Field is required, Enter at  least 1 chars !";
      this.setState({ handleErrors });
    } else {
      noError = false;
      handleErrors['country_name'] = "";
      this.setState({ handleErrors });
    } // End if-else


    if (add_type == TYPE_CITY) {
      if (state_id1 == '') {
        noError = true;
        handleErrors['state_id1'] = STATE_ERROR_MSG;
        this.setState({ handleErrors });
      } else {
        noError = false;
        handleErrors['state_id1'] = "";
        this.setState({ handleErrors });
      } // End if-else

      if (no_of_digits == '') {
        noError = true;
        handleErrors['no_of_digits'] = DIGIT_ERROR_MSG;
        this.setState({ handleErrors });
      } else {
        noError = false;
        handleErrors['no_of_digits'] = "";
        this.setState({ handleErrors });
      } // End if-else


    } else if (add_type == TYPE_STATE) {
      if (country_id1 == '') {
        noError = true;
        handleErrors['country_id1'] = COUNTRY_ERROR_MSG;
        this.setState({ handleErrors });
      } else {
        noError = false;
        handleErrors['country_id1'] = "";
        this.setState({ handleErrors });
      } // End if-else
    }

    // Validate errors
    if (!noError) {
      this.setState({ isSaving: true })

       // Create custom payload for api call
      const payload = {
        type,
        state_id: state_id1,
        country_id: country_id1,
        country_name,
        input_val,
        bank_id: user.bank_id,
        no_of_digits
      }

      // Add api call to db
      addBranchInfo(payload).then(response => {
 
        if (response.data.status == 200) {

          this.setState({
            successMsg: response.data.msg,
            isSaving: false,
            showmodal: false,
            city_id: '',
            input_val: '',
            state_id1: '',
            country_id1: ''
          })

          // Call api for 
          this.getStatesList();
          this.getCitiesList();
          this.getCountryList();
          let _this = this;

          setTimeout(() => {
            _this.setState({
              successMsg: '',
              showmodal: false,
            })

          }, 6000);

        } else {
          this.setState({
            errorMsg: response.data.msg,
            isSaving: false,
            showmodal: false,
          })

          let _this = this;

          setTimeout(() => {
            _this.setState({
              errorMsg: '',

            })
          }, 6000);

        }

      })
    } else {
      console.log("Errors found ");
      console.log(noError)
      console.log(handleErrors)

    }
    // this.setState({ handleErrors });
    // return handleErrors;
  };



  render() {
    const { sale_agents, team_leads } = this.props;
    const {
      isFatchingState,
      errorMsg,
      branch_name,
      area,
      address,
      city_id,
      state_id,
      locality,
      phone,
      sale_agent_id,
      team_lead_id,
      pin_code,
      branch_manager_name,
      states,
      cities,
      isFatchingCities,
      addBranchErrors,
      citiDetails,
      fetchingDetails,
      country_code,
      country_id,
      isFatchingCountry,
      countries,
      handleErrors,
      branchCreating,
      successMsg,
      showmodal,
      add_type,
      input_val,
      addingInfo,
      country_name,
      isSaving,
      country_id1,
      state_id1,
      no_of_digits,
      fresh_login
    } = this.state;

    console.log("handleErrors iddd");
    console.log(handleErrors);
    return (
      <Fragment>
        <AddBranchModal
          isFatchingState={isFatchingState}
          errorMsg={errorMsg}
          branch_name={branch_name}
          area={area}
          address={address}
          city_id={city_id}
          state_id={state_id}
          locality={locality}
          phone={phone}
          sale_agent_id={sale_agent_id}
          team_lead_id={team_lead_id}
          pin_code={pin_code}
          branch_manager_name={branch_manager_name}
          states={states}
          team_leads={team_leads}
          cities={cities}
          sale_agents={sale_agents}
          handleInputValues={this.handleInputValues}
          isFatchingCities={isFatchingCities}
          addBranchErrors={addBranchErrors}
          citiDetails={citiDetails}
          fetchingDetails={fetchingDetails}
          country_id={country_id}
          country_code={country_code}
          countries={countries}
          isFatchingCountry={isFatchingCountry}
          handleAddBranch={this.handleAddBranch}
          handleErrors={handleErrors}
          branchCreating={branchCreating}
          successMsg={successMsg}
          showAddModal={this.showAddModal}
          add_type={add_type}
          fresh_login={fresh_login}

        />

        {showmodal && (
          <AddNewBranchModal
            addingInfo={addingInfo}
            input_val={input_val}
            showmodal={showmodal}
            add_type={add_type}
            country_name={country_name}
            handleInputValues={this.handleInputValues}
            handleErrors={handleErrors}
            handleAddInfo={this.handleAddInfo}
            showAddModal={this.showAddModal}
            states={states}
            countries={countries}
            country_id1={country_id1}
            city_id={city_id}
            state_id1={state_id1}
            fetchingDetails={fetchingDetails}
            isFatchingCountry={isFatchingCountry}
            isSaving={isSaving}
            errorMsg={errorMsg}
            successMsg={successMsg}
            no_of_digits={no_of_digits}
          />
        )}
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddBranch);
