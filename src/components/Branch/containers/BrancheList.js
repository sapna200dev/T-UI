import React, { Component, Fragment } from "react";
import { Branches } from "../components/index";
import { getAllBranches } from "../services/index";

class BrancheList extends Component {
  state = {
    branches: [],
    isFatching: false,
    errorMsg: "",
    filterBranchLists: [],
    sort_by:'branch_name',
    pagination_count:10
  };

  componentDidMount() {
    this.setState({ isFatching: true });
    this.getBranches();
  }

  /**
   *  Get branches
   */
  getBranches = () => {
    const { sort_by } = this.state;

    getAllBranches() 
      .then((res) => {
        if (res.data.status == 200) {
          let sorted_data = res.data.data;

          sorted_data.sort(this.dynamicSort(sort_by));

          this.setState({
            isFatching: false,
            branches: sorted_data,
            filterBranchLists: sorted_data,
          });
        } else {
          this.setState({
            isFatching: false,
            branches: [],
            errorMsg: res.data.message,
          });
        }
      })
      .catch((err) => {
        console.log("Error response = ", err);
        this.setState({
          isFatching: false,
          branches: [],
          errorMsg: err.response
            ? err.response.data.message
            : "Somthing went wrong !",
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { branches } = this.state;
    let arrayBrancheLists = [];

    if (data) {
      arrayBrancheLists = this.handleFilterChangeVal(branches, data);
    } else {
      arrayBrancheLists = branches;
    }

    this.setState({
      filterBranchLists: arrayBrancheLists,
      searchValue: data,
    });
  };

  /**
   * Handle search event
   */
  handleFilterChangeVal = (branches, value) => {
    let branchList = [];
    branchList = branches.filter((item) => {
      return (
        (item.branch_name &&
          item.branch_name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.area &&
          item.area.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.locality &&
          item.locality.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.address &&
          item.address.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.pin_code &&
          item.pin_code.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return branchList;
  };




      /**
   * Handle Bank inputs
   */
    handleInputValues = (e, result, type) => {
      const {filterBranchLists} = this.state;

      if(type == 'pagination') {
        const { name, value } = e.target;

        this.setState({ [name] : value });
      } else if(type == 'sort') {

        const { name, value } = e.target;
    
        filterBranchLists.sort(this.dynamicSort(value));
  
  
        this.setState({ filterBranchLists, sort_by:value });
      }

    };
 
       /**
        * Sort list
        * @param  property 
        * @returns 
        */
    dynamicSort = (property) =>  {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            /* next line works with strings and numbers, 
            * and you may want to customize it to your needs
            */
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }

  render() {
    const {
      branches,
      isFatching,
      errorMsg,
      searchValue,
      filterBranchLists,
      sort_by,
      pagination_count
    } = this.state;
    return (
      <Fragment>
        <Branches
          branches={filterBranchLists}
          isFatching={isFatching}
          errorMsg={errorMsg}
          searchValue={searchValue}
          handleFilterChange={this.handleFilterChange}
          sort_by={sort_by}
          handleInputValues={this.handleInputValues}
          pagination_count={pagination_count}
        />
      </Fragment>
    );
  }
}

export default BrancheList;
