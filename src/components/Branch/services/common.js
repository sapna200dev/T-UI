import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'


/**
 * Get all branches api
 *
 */
export const getAllBranches = () =>
axios.get(`${URL_HOST}/api/branches/all`, );

/**
 * Get states api
 */
export const getStates = () =>
axios.get(`${URL_HOST}/api/common/states`);


/**
 * Get city list api 
 *
 */

export const getCitiesList = () =>
axios.get(`${URL_HOST}/api/branches/cities`);


/**
 * Get city by name api
 */
export const getCityByName = (name) =>
axios.get(`${URL_HOST}/api/branches/city/${name}`);


/**
 * Get countries api
 */
export const getCountries = () =>
axios.get(`${URL_HOST}/api/common/get-countries`);


/**
 * Create new branch api
 */
export const createBranch = (data) =>
axios.post(`${URL_HOST}/api/branches/create`, data);


/**
 * Get branch by id
 */
export const getBranchById = (id) =>
axios.get(`${URL_HOST}/api/branches/${id}`);



/**
 * Get all cities by id api
 */
export const getCities = (id) =>
axios.get(`${URL_HOST}/api/common/get-cities/${id}`);

/**
 * Add additional branch info
 */
export const addBranchInfo = (data) =>
axios.post(`${URL_HOST}/api/branches/add/info`, data);