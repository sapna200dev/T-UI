import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip,
  CSelect,
  CLabel
} from "@coreui/react";
import { ContentLoading } from "../../../Comman/components";
import "../../containers/emaployee/employee.css";
import { COULUMN_NAMES, PAGINATION_COUNT, config } from "../../constant";
import ReactDatatable from '@ashvin27/react-datatable';



const getBadge = (status) => {
  switch (status) {
    case "Active":
      return "success";
    case "Inactive":
      return "secondary";
    case "Pending":
      return "warning";
    case "Banned":
      return "danger";
    default:
      return "primary";
  }
};
const fields = ["name", "branch_name", "username", "role", "status", "Action"];

export const EmployeeList = (props) => {
  const {
    employeeLists,
    handleDeleteItem,
    errorMsg,
    successMsg,
    loading,
    handleFilterChange,
    handleInputValues,
    setOrder,
    sort_by,
    pagination_count
  } = props;

  console.log("employeeLists = ", employeeLists);


  const columns = [
    {
      key: "name",
      text: "Name",
      className: "name",
      align: "left",
      sortable: true,
    },
    {
      key: "branch_name",
      text: "Branch Name",
      className: "branch_name",
      align: "left",
      sortable: true
    },
    {
      key: "username",
      text: "Username",
      className: "username",
      align: "left",
      sortable: true
    },
    {
      key: "role",
      text: "Role",
      className: "role",
      align: "left",
      sortable: true
    },
    {
      key: "status",
      text: "Status",
      className: "status",
      sortable: true,
      align: "left"
    },
    {
      key: "view_more",
      text: "View",
      className: "view_more",
      sortable: false,
      align: "left",
      cell: record => {
        return (
          <Fragment>
            <a
              href={`#/employee-view/${record.id}`}
              className="button_style"
            >
              <CButton
                type="submit"
                size="sm"
                color="primary"
                className="remove_button view_btn"
              >
                <CTooltip content={`View Record`} placement={"top-start"}>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </CTooltip>

              </CButton>
            </a>
          </Fragment>
        )
      }
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 100,
      align: "left",
      sortable: false,
      cell: record => {
        return (
          <Fragment>
            <a
              href={`#/employee-edit/${record.id}`}
              className="button_style"
            >
              <CTooltip content={`Edit Record`} placement={"top-start"}>
                <CButton type="submit" size="sm" color="primary">
                  <i className="fa fa-edit" aria-hidden="true"></i>

                </CButton>
              </CTooltip>
            </a>

            {/* <button
                      className="btn btn-primary btn-sm"
                      onClick={() => {
                          console.log('record = ', record)
                      }}
                      style={{marginRight: '5px'}}>
                      <i className="fa fa-edit"></i>
                  </button> */}
            <CButton
              type="submit"
              size="sm"
              color="danger"
              className="remove_button"
              onClick={() => handleDeleteItem(record)}
            >
              <CTooltip content={`Delete Record`} placement={"top-start"}>
                <i className="fa fa-trash" aria-hidden="true"></i>
              </CTooltip>


            </CButton>

          </Fragment>
        );
      }
    }
  ];


  // let string = "/*/*5*?*/6****";
  // let last_chars =  string.substring(0, string.length - 2);
  // let first_chars = last_chars.substring(2);
  // let replace_ques =  first_chars.replace(/[?=]/g, "/");

  // console.log('Original string = ', string)
  // console.log('Remove last 2 chars from string = ', last_chars)
  // console.log('Remove first 2 chars from string = ', first_chars)
  // console.log('Remove question mark from string = ', replace_ques)
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> Employee List</b>
          <span>
            <a href="#/employee-add" className="button_style1 button_style">
              <CButton
                type="submit"
                size="sm"
                color="primary"
              // className="add_employee_button"
              // onClick={() => handleDeleteItem(item)}
              >
                {" "}
                <i className="fa fa-plus" aria-hidden="true"></i> New Employee
                </CButton>
            </a>
            {/* <span className="pagination_count">
            <CLabel htmlFor="last_name">
            Per page -
                </CLabel> {"   "}
          <CSelect
            custom
            name="pagination_count"
            id="pagination_count"
            width={50}
            onChange={(e, result) => handleInputValues(e, result, 'pagination')}
            value={pagination_count}
            className="view-text-color showing_pagination_count "
          >
            <option value="" style={{display:'none'}}>Please select</option>
            {PAGINATION_COUNT && PAGINATION_COUNT.length > 0 ? (
              PAGINATION_COUNT.map((item) => (
                <option value={item.value}  >{item.text} </option>
              ))
            ) : (
              <option value="">No Branches</option>
            )}
          </CSelect>
            </span> */}
          </span>
        </CCardHeader>

        <span className="sorting_lebal">
          {/* <CLabel htmlFor="last_name">
            Sort By - 
                </CLabel> {"   "}
          <CSelect
            custom
            name="sort_by"
            id="sort_by"
            width={50}
            onChange={(e, result) => handleInputValues(e, result, 'sort')}
            value={sort_by}
            className="view-text-color showing_column"
          >
            <option value="">Please select</option>
            {COULUMN_NAMES && COULUMN_NAMES.length > 0 ? (
              COULUMN_NAMES.map((item) => (
                <option value={item.value}  >{item.text} {item.value == sort_by ? ' - ( Selected ) ' : ''}  </option>
              ))
            ) : (
              <option value="">No Branches</option>
            )}
          </CSelect> */}

          {/* <div className="md-form mt-3">
          <input
              className="form-control search_list"
              type="text"
              placeholder="Search"
              aria-label="Search"
              onChange={(e) => {
                handleFilterChange(e);
              }}

            />
        
          </div> */}
        </span>
        <CCardBody>
          {successMsg && (
            <CAlert color="success" className="msg_div">
              {successMsg}
            </CAlert>
          )}
          <ReactDatatable
            config={config}
            records={employeeLists}
            columns={columns}
            extraButtons={[]}
            loading={loading}
          />
          {/* 
          <CDataTable
            items={employeeLists}
            fields={fields}
            itemsPerPage={pagination_count}
            loading={loading}
            sort={true}
            pagination
            scopedSlots={{
              status: (item) => (
                <td>
                  {item.status}
             
                </td>
              ),
              Action: (item) => (
                <td>
                  <a
                    href={`#/employee-edit/${item.id}`}
                    className="button_style"
                  >
                    <CTooltip content={`Edit Record`} placement={"top-start"}>
                      <CButton type="submit" size="sm" color="primary">
                        <i className="fa fa-edit" aria-hidden="true"></i>
                    
                      </CButton>
                    </CTooltip>
                  </a>

                  <CButton
                    type="submit"
                    size="sm"
                    color="danger"
                    className="remove_button"
                    onClick={() => handleDeleteItem(item)}
                  >
                    <CTooltip content={`Delete Record`} placement={"top-start"}>
                      <i className="fa fa-trash" aria-hidden="true"></i>
                    </CTooltip>

                 
                  </CButton>
                  <a
                    href={`#/employee-view/${item.id}`}
                    className="button_style"
                  >
                    <CButton
                      type="submit"
                      size="sm"
                      color="primary"
                      className="remove_button"
                    >
                      <CTooltip content={`View Record`} placement={"top-start"}>
                        <i className="fa fa-eye" aria-hidden="true"></i>
                      </CTooltip>

                    </CButton>
                  </a>
                </td>
              ),
            }}
          /> */}
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
