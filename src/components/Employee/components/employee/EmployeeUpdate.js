import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { InvertContentLoader, ContentLoading } from "../../../Comman/components";

export const EmployeeUpdate = (props) => {
  const {
    first_name,
    last_name,
    branch_id,
    status,
    role_id,
    mobile_number,
    email,
    access_level,
    team_lead,
    username,
    password,
    confirm_password,
    handleInputValues,
    handleErrors,
    handleUpdateEmployee,
    all_branches,
    all_roles,
    successMesg,
    errorMsg,
    statuses,
    communication_address,
    location,
    loading,
    isFatching
  } = props;

  let arrayUniqueRoles = [];

  if (all_roles) {
    arrayUniqueRoles = [
      ...new Map(all_roles.map((item) => [item["name"], item])).values(),
    ];
  }


  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> Edit Employee </b>
          </h4>
        </CCardHeader>
        {successMesg && (
          <CAlert color="success" className="msg_div">
            {successMesg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
              {isFatching && <ContentLoading content="Fetching record.." />}
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="Bank Name"
                  onChange={handleInputValues}
                  invalid={handleErrors.first_name}
                  className="view-text-color"
                />
                <CInvalidFeedback>{handleErrors.first_name}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder=" Last Name "
                  onChange={handleInputValues}
                  invalid={handleErrors.last_name}
                  className="view-text-color"
                />
                <CInvalidFeedback>{handleErrors.last_name}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="branch_id">
                  Branch <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="branch_id"
                  id="branch_id"
                  invalid={handleErrors.branch_id}
                  onChange={handleInputValues}
                  value={branch_id}
                  className="view-text-color"
                >
                  <option value="">Please select</option>
                  {all_branches.length > 0 ? (
                    all_branches.map((item) => (
                      <option value={item.id} >{item.branch_name}</option>
                    ))
                  ) : (
                    <option value="">No Branches</option>
                  )}
                </CSelect>

                <CInvalidFeedback>{handleErrors.branch_name}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
              <CLabel htmlFor="status">
                  Status <small className="required_lable">*</small>
                </CLabel>
              <CSelect
                  custom
                  name="status"
                  id="status"
                  invalid={handleErrors.status}
                  onChange={handleInputValues}
                  value={status}
                  className="view-text-color"
                >
                  <option value="">Please select</option>
                  {statuses.length > 0 ? (
                    statuses.map((item) => (
                      <option value={item.name}>{item.display_name}</option>
                    ))
                  ) : (
                    <option value="">No Statuses</option>
                  )}
                </CSelect>

                <CInvalidFeedback>{handleErrors.status}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="role_id">
                  Roles <small className="required_lable">*</small>
                </CLabel>

                <CSelect
                  custom
                  name="role_id"
                  id="role_id"
                  invalid={handleErrors.role_id}
                  onChange={handleInputValues}
                  value={role_id}
                  className="view-text-color"
                >
                  <option value="">Please select</option>
                  {arrayUniqueRoles.map((item) => (
                    <option value={item.id} >{item.display_name}</option>
                  ))}
                </CSelect>

                <CInvalidFeedback>{handleErrors.role}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="mobile_number">
                  Mobile Number <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="mobile_number"
                  name="mobile_number"
                  value={mobile_number}
                  placeholder=" Mobile Number"
                  onChange={handleInputValues}
                  invalid={handleErrors.mobile_number}
                  className="view-text-color"
                />
                <CInvalidFeedback>
                  {handleErrors.mobile_number}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="email">
                  Email Address <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="email"
                  name="email"
                  value={email}
                  placeholder="Email Address"
                  onChange={handleInputValues}
                  invalid={handleErrors.email}
                  className="view-text-color"
                />
                <CInvalidFeedback>{handleErrors.email}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="access_level">
                  Access Level <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="access_level"
                  name="access_level"
                  placeholder=" Access Level"
                  onChange={handleInputValues}
                  disabled
                  className="view-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="team_lead">
                  Team Lead <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="team_lead"
                  name="team_lead"
                  placeholder=" Team Lead"
                  disabled
                  className="view-text-color"
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="communication_address">
                  Communication Address{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CTextarea 
                  id="communication_address"
                  name="communication_address" 
                  placeholder=" Communication Address"
                  className="view-text-color"
                  onChange={handleInputValues}
                  invalid={handleErrors.communication_address}
                  value= {communication_address ? communication_address : ''}
                />
                
                
                <CInvalidFeedback>
                  {handleErrors.communication_address}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="location">
                  Location <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="location"
                  name="location"
                  value={location?location : ''}
                  placeholder="Location"
                  onChange={handleInputValues}
                  invalid={handleErrors.location}
                  className="view-text-color"
                />
                <CInvalidFeedback>{handleErrors.location}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>


            <CCardFooter className="divider_layout"></CCardFooter>
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="username">
                  Username <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="username"
                  name="username"
                  value={username}
                  placeholder="Username"
                  onChange={handleInputValues}
                  invalid={handleErrors.Username}
                  disabled
                  className="view-text-color"
                />
                <CInvalidFeedback>{handleErrors.Username}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="password">
                  Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  placeholder="Password"
                  onChange={handleInputValues}
                  invalid={handleErrors.password}
                  className="view-text-color"
                />
                <CInvalidFeedback>{handleErrors.password}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="confirm_password">
                  Confirm Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="confirm_password"
                  name="confirm_password"
                  value={confirm_password}
                  placeholder="Confirm Password"
                  onChange={handleInputValues}
                  invalid={handleErrors.confirm_password}
                  className="view-text-color"
                />
                <CInvalidFeedback>
                  {handleErrors.confirm_password}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>
        <CCardFooter>
        <span className="buttonRight">
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleUpdateEmployee}
          >
            <CIcon name="cil-scrubber" /> Update  {loading && <InvertContentLoader />}
          </CButton>
          <a href="#/employee" className="button_style">
          <CButton
            type="reset"
            size="sm"
            color="danger"
            className="remove_button"
          >
           
              <CIcon name="cil-ban" /> Cancel
              </CButton>
            </a>
                    </span>
          {/* <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
          >
            <CIcon name="cil-ban" /> Reset
          </CButton> */}
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
