import React, { Fragment } from "react";

export const EMPLOYEE_STATE = {
    first_name: '',
    last_name: '',
    branch_id: '',
    status: '',
    role_id: '',
    mobile_number: '',
    communication_address: '',
    location: '',
    email: '',
    access_level: '',
    team_lead: '',
    username: '',
    password: '',
    confirm_password: '',
    handleErrors: {},
    all_roles: [],
    all_branches: [],
    successMesg: '',
    errorMsg: '',
    id: '',
    statuses: [],
    isFatching: false

};


export const EMPLOYEE_FORM_VALIDATIONS = {
    FIRST_NAME: 'First name is required',
    LAST_NAME: 'Last name is required',
    BRANCH_NAME: 'Branch name is required',
    STATUS: 'Status is required',
    ROLE: 'Role is required ',
    MO_NUMBER: 'Mobile number is required',
    EMAIL_ADDRESS: 'Email Address is required',
    USERNAME: 'Username is required',
    PASSWORD: 'Password is required',
    CONFIRM_PASS: 'Confirm password is required',
    CONFIRM_PASS_LENGTH: 'Confirm password must match with password',
    LOCATION: 'Location is required',
    COMMUNICATION_ADDRESS: 'Communication adddress is required',
    FIRST_NAME_VALID: 'Only letters are allowd',
    LAST_NAME_VALID: 'Only letter are allowd',
    MO_NUMBER_VALID: 'Number must be 10 digits only'
}


export const COULUMN_NAMES = [
    { id: 0, text: 'Name', value: 'name' },
    { id: 1, text: 'Branch Name', value: 'branch_name' },
    { id: 2, text: 'Username', value: 'username' },
    { id: 3, text: 'Role', value: 'role' },
    { id: 4, text: 'Status', value: 'status' },
];


export const PAGINATION_COUNT = [
    { id: 0, text: '5', value: '5' },
    { id: 1, text: '10', value: '10' },
    { id: 2, text: '15', value: '15' },
    { id: 3, text: '20', value: '20' },
];


// export const columns = [
//     {
//         key: "name",
//         text: "Name",
//         className: "name",
//         align: "left",
//         sortable: true,
//     },
//     {
//         key: "branch_name",
//         text: "Branch Name",
//         className: "branch_name",
//         align: "left",
//         sortable: true
//     },
//     {
//         key: "username",
//         text: "Username",
//         className: "username",
//         sortable: true
//     },
//     {
//         key: "role",
//         text: "Role",
//         className: "role",
//         align: "left",
//         sortable: true
//     },
//     {
//         key: "status",
//         text: "Status",
//         className: "status",
//         sortable: true,
//         align: "left"
//     },
//     {
//         key: "action",
//         text: "Action",
//         className: "action",
//         width: 100,
//         align: "left",
//         sortable: false,
//         cell: record => { 
//             return (
//                 <Fragment>
                    
//                     <button
//                         className="btn btn-primary btn-sm"
//                         onClick={() => {
//                             console.log('record = ', record)
//                         }}
//                         style={{marginRight: '5px'}}>
//                         <i className="fa fa-edit"></i>
//                     </button>
//                     <button 
//                         className="btn btn-danger btn-sm" 
//                         onClick={() => this.deleteRecord(record)}>
//                         <i className="fa fa-trash"></i>
//                     </button>
//                 </Fragment>
//             );
//         }
//     }
// ];


export const config = {
    page_size: 10,
    // length_menu: [10, 20, 50],
    button: {
        excel: true,
        print: true,
        csv: true
    },
    show_filter:true,
    filename:'turant_employees',
    language: {
        loading_text: "Please be patient while data loads..."
    }
    // pagination:'advance'
};


