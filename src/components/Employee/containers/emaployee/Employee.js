import React, { Component, Fragment } from "react";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from "@coreui/react";
import { EmployeeList } from "../../components/employee";
import { InvertContentLoader } from "../../../Comman/components";
import { employeeLists, deleteEmployee } from "../../services/common";

export class Employee extends Component {
  state = {
    employeeLists: [],
    isDelete: false,
    deleteItem: "",
    successMsg: "",
    loading: false,
    errorMsg: "",
    searchValue: "",
    filterEmployeeLists: [],
    isLoading:false,
    sort_by:'name',
    pagination_count:10
  };

  componentDidMount() {
    this.getAllEmployess();
  }

  getAllEmployess = () => {
    const { sort_by } = this.state;
    this.setState({
      loading: true,
    });
    employeeLists()
      .then((res) => {
        let sorted_data = res.data.data;

        sorted_data.sort(this.dynamicSort(sort_by));
        this.setState({
          employeeLists: res.data.data,
          filterEmployeeLists: sorted_data,
          loading: false,
        });
      })
      .catch((err) => {
        this.setState({
          employeeLists: "",
          loading: false,
        });
      });
  };

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    this.setState({ isLoading : true })
    const { deleteItem } = this.state;

    const data = {
      username: deleteItem.username,
      role_id: deleteItem.role_id,
    }; 

    deleteEmployee(deleteItem.id, data)
      .then((res) => { 
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
            isLoading:false
          });
          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
          }); 
        }

        this.getAllEmployess();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isLoading:false
        });
      });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle search event
   */ 
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { employeeLists } = this.state;
    let arrayEmployeeLists = [];

    if (data) {
      arrayEmployeeLists = this.handleFilterChangeVal(employeeLists, data);
    } else {
      arrayEmployeeLists = this.state.employeeLists;
    }

    this.setState({
      filterEmployeeLists: arrayEmployeeLists,
      searchValue: data,
    });
  };

  handleFilterChangeVal = (employeeLists, value) => {
    let employeeList = [];
    employeeList = employeeLists.filter((item) => {
      return (
        (item.name &&
          item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.branch_name &&
          item.branch_name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.username &&
          item.username.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.role &&
          item.role.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.status &&
          item.status.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return employeeList;
  };

    /**
   * Handle Bank inputs
   */
     handleInputValues = (e, result, type) => {
       const {filterEmployeeLists} = this.state;

      if(type == 'pagination') {
        const { name, value } = e.target;

        this.setState({ [name] : value });
      } else if(type == 'sort') {
        const { name, value } = e.target;
        filterEmployeeLists.sort(this.dynamicSort(value));
  
        this.setState({ filterEmployeeLists, sort_by:value });
      }

    };

      /**
       * Sort list
       * @param  property 
       * @returns 
       */
    dynamicSort = (property) =>  {
          var sortOrder = 1;
          if(property[0] === "-") {
              sortOrder = -1;
              property = property.substr(1);
          }
          return function (a,b) {
              /* next line works with strings and numbers, 
              * and you may want to customize it to your needs
              */
              var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
              return result * sortOrder;
          }
     }

  render() {
    const {
      filterEmployeeLists,
      isDelete,
      deleteItem,
      errorMsg,
      successMsg,
      loading,
      searchValue,
      isLoading,
      sort_by,
      pagination_count
    } = this.state;

    return (
      <Fragment>
        <div>
          <EmployeeList
            employeeLists={filterEmployeeLists}
            errorMsg={errorMsg}
            successMsg={successMsg}
            loading={loading}
            handleDeleteItem={this.handleDeleteItem}
            searchValue={searchValue}
            handleFilterChange={this.handleFilterChange}
            handleInputValues={this.handleInputValues}
            sort_by={sort_by}
            pagination_count={pagination_count}
          />
        {isDelete && (
          <CModal show={isDelete} onClose={this.handleCancelModal} color="danger">
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this Employee <b>{deleteItem.name}</b>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={this.handleDeleteConfirm}>
                Delete {isLoading && ( <InvertContentLoader /> )}
              </CButton>{" "}
              <CButton color="secondary" onClick={this.handleCancelModal}>
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}
        </div>
      </Fragment>
    );
  }
}
