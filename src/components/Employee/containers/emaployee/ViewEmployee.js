import React, { Component, Fragment } from "react";
import { ViewEmployeeModal } from "../../components/employee";
import { getEmployeeDetails } from "../../services/common";
import { EMPLOYEE_STATE } from "../../constant";
import { getLoggedInUser } from "../../../Comman/functions";
import {  getBranches, getRoles, addEmployee } from "../../services/common";

export class ViewEmployee extends Component {
  state = EMPLOYEE_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({
      id,
      isFatching:true
    })
    this.getBankDetails(id);

    const userObj = getLoggedInUser('loggedInUser')
    let user='';

    if (userObj) {
        user = JSON.parse(userObj);
      }

    this.getUserRoles();
    this.getUserBranches();
    console.log("getEmployeeDetails detailsdddddd");
  }

  getBankDetails = (id) => {
    getEmployeeDetails(id)
      .then((res) => {

        console.log("getEmployeeDetails response ", res.data);
        if (res.data && res.data.status == 200) {
          let data = res.data.data;
          console.log("getEmployeeDetails data ", data);
          this.setState({
            first_name: data.first_name,
            last_name: data.last_name,
            branch_name:data.user_branch.id,
            status:data.status,
            role:data.role_id,
            mobile_number:data.mobile_number,
            email:data.email,
            username:data.username,
            isFatching:false
          });
        } else {
          console.log("bankDetail eeee => ", res.data.data);
          this.setState({
            errorMsg: res.data.msg,
            isFatching:false
          });
        }
       
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          isFatching:false
        });
      });
  };



    
  getUserRoles = () => {
    getRoles().then(res => {
      console.log('res roles => ', res.data.data)
      this.setState({
        all_roles : res.data.data
      })
    }).catch(err => {
      console.log('errror roles => ', err)
    })
  }

  getUserBranches = () => {
    getBranches().then(res => {
      this.setState({
        all_branches : res.data.data
      })
    }).catch(err => {

    })
  }



  render() {
    const { 
        first_name,
        last_name,
        branch_name,
        status,
        role,
        mobile_number,
        email,
        access_level,
        team_lead,
        username,
        password,
        confirm_password,
        handleErrors,
        all_roles,
        all_branches,
        errorMsg,
        successMesg,
        isFatching
    } = this.state;

    console.log('res all_branches => ', all_branches)
    console.log('res branch_name => ', branch_name)
   
    return (
      <Fragment>
        <div>
          <ViewEmployeeModal
              first_name={first_name}
              last_name={last_name}
              branch_name={branch_name}
              status={status}
              role={role}
              mobile_number={mobile_number}
              email={email}
              access_level={access_level}
              team_lead={team_lead}
              username={username}
              password={password}
              confirm_password={confirm_password}
              handleInputValues={this.handleInputValues}
              handleErrors={handleErrors}
              handleAddEmployee={this.handleAddEmployee}
              all_roles={all_roles}
              all_branches={all_branches}
              successMesg={successMesg}
              errorMsg={errorMsg}
              isFatching={isFatching}
          />
        </div>
      </Fragment>
    );
  }
}
