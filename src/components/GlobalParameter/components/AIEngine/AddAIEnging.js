import React, { Fragment } from "react";
import {
  InvertContentLoader,
  PageNotFound,
} from "../../../Comman/components/index";

import {
  CWidgetDropdown,
  CRow,
  CCol,
  CCardHeader,
  CCard,
  CCardBody,
  CBadge,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ENGINE_IP_ADDRESS, ENGINE_PORT_NUMBER, ENGINE_PROTOCOL, ENGINE_SERVICE } from '../../constants'


import { FIELDS } from "../../constants";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile",
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

const AIEngineLists = (props) => {
  const {
    ai_engine_list,
    isLoading,
    handleDeleteItem,
    successMsg,
    errorMsg,
    handleFilterChange,
    searchValue,
    protocol,
    ip_address,
    port_number,
    service_engine,
    handleInputValues,
    service_engine_errors,
    handleAddAIEngine,
    isSaving,
    bankList,
    bank_id,
    isFatchingBanks
  } = props;
  
  return (
    <Fragment>
      <CCard> 
        <CCardHeader>
          <b style={{ fontSize: "15px" }}> Add Settings</b>
        </CCardHeader>
        {successMsg && (
          <CAlert
            color="success"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert
            color="danger"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Protocol <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="protocol"
                  id="protocol"
                  invalid={service_engine_errors.protocol}
                  onChange={handleInputValues}
                  value={protocol}
                >
                  <option value="">Please select</option>
                  {ENGINE_PROTOCOL ? (
                    ENGINE_PROTOCOL.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Protocols</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.protocol}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  IP Address <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom 
                  name="ip_address"
                  id="ip_address"
                  invalid={service_engine_errors.ip_address}
                  onChange={handleInputValues}
                  value={ip_address}
                >
                  <option value="">Please select</option>
                  {ENGINE_IP_ADDRESS ? (
                    ENGINE_IP_ADDRESS.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No IP Address</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.ip_address}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="port_number">
                  Port Number <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="port_number"
                  id="port_number"
                  invalid={service_engine_errors.port_number}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  {ENGINE_PORT_NUMBER ? (
                    ENGINE_PORT_NUMBER.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Port Number</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.port_number}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="service_engine">
                  Service Engine <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="service_engine"
                  id="service_engine"
                  invalid={service_engine_errors.service_engine}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  {ENGINE_SERVICE ? (
                    ENGINE_SERVICE.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Service Engine</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.service_engine}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3"></CCol>
              {/* <CCol xs="12" md="2"></CCol> */}
            </CFormGroup>

            <CFormGroup row>
            <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Banks <small className="required_lable">*</small>
                  {isFatchingBanks && <InvertContentLoader color={"black"} />}
              
                </CLabel> 
                <CSelect
                  custom
                  name="bank_id"
                  id="bank_id"
                  invalid={service_engine_errors.bank_id}
                  onChange={handleInputValues}
                  value={bank_id}
                >
                  <option value="">Please select</option>
                  {bankList ? (
                    bankList.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Banks</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.bank_id}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                </CCol>
                <CCol xs="12" md="3"></CCol>
            </CFormGroup>
          </CForm>

          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddAIEngine}
          >
            <CIcon name="cil-scrubber" /> Save
            {isSaving && <InvertContentLoader />}
          </CButton>
          <a href="#/global-parameter-list" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
        </CCardBody>
      </CCard>
    </Fragment>
  );
};

export default AIEngineLists;
