import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip
} from "@coreui/react";
import { FIELDS } from "../../constants";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile", 
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

const AIEngineLists = (props) => {
  const {
    ai_engine_list,
    handleDeleteItem,
    successMsg,
    errorMsg,
    ai_engine_list_filter,
    isLoading, 
    searchValue,
    handleFilterChange
  } = props;


  const convertCaptilize = (str) =>  {
    var i, frags = str.split('_');
    for (i=0; i<frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }
  
  const capitalizeFirstLetter = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "15px" }}> Settings</b>
          <span>
            <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />
 
              <a href="#/global-parameter-add" className="button_style">
                <CButton
                  type="submit"
                  size="sm"
                  color="primary"
                  className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                >
                  {" "}
                  <i className="fa fa-plus" aria-hidden="true"></i>  AI Engine
                </CButton>
              </a>
            </div>
          </span>
          {successMsg && (
            <CAlert color="success" className="msg_div">
              {successMsg}
            </CAlert>
          )}
          {errorMsg && (
            <CAlert color="danger" className="msg_div">
              {errorMsg}
            </CAlert>
          )}
          <CCardBody>
            <CDataTable
              items={ai_engine_list_filter}
              fields={FIELDS}
              itemsPerPage={5}
              loading={isLoading}
              pagination
              scopedSlots={{
                protocol: (item) => <td>{capitalizeFirstLetter(item.protocol)}</td>,
                service_engine: (item) => <td>{convertCaptilize(item.service_engine)}</td>,
                bank_name: (item) => <td>{capitalizeFirstLetter(item.bank_name)}</td>,
                Action: (item) => (
                  <td>
                    <a
                      href={`#/global-parameter-edit/${item.id}`}
                      className="cancel_bt"
                    >
                     <CTooltip
                        content={`Edit Record`}
                        placement={"top-start"}
                      >
                        <CButton type="submit" size="sm" color="primary">
                          <i className="fa fa-edit" aria-hidden="true"></i>
                          {/* Edit */}
                          {/* {loading && <ContentLoading />} */}
                        </CButton>
                      </CTooltip>


                    </a>

                    <CButton
                      type="submit"
                      size="sm"
                      color="danger"
                      className="remove_button"
                      onClick={() => handleDeleteItem(item)}
                    >
                        <CTooltip content={`Delete Record`} placement={"top-start"}>
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </CTooltip>
                    </CButton>
                    <a
                      href={`#/global-parameter-detail/${item.id}`}
                      className="cancel_bt"
                    >
                      <CButton
                        type="submit"
                        size="sm"
                        color="primary"
                        className="remove_button"
                      >
                        <CTooltip content={`View Record`} placement={"top-start"}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </CTooltip>
                      </CButton>
                    </a>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCardHeader>
      </CCard>
    </Fragment>
  );
};

export default AIEngineLists;
