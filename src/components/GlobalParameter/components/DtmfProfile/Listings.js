import React, { Component, Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from "@coreui/react";
import { getDTMFProfileLists, deleteDTMFProfile } from "../../services/index";
import { InvertContentLoader } from "../../../Comman/components/index";

import { DTMF_FIELDS } from "../../constants";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile", 
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

class Listings extends Component {
  state = {
    errorMsg: "",
    successMsg: "",
    dtmf_list: [],
    dtmf_list_filter: [],
    isLoading: false,
    isDelete: false,
    deleteItem: "",
    isDeleteLoading: false,
    searchValue: "",
  };


  componentDidMount() {
    console.log("response componentDidMount Listings => ");
    this.setState({ isLoading: true });

    // Fetch dtmf profiles
    this.fetchDTMFProfiles();
  }

  /**
   * Fetch ai settings
   */
  fetchDTMFProfiles = () => {
    getDTMFProfileLists()
      .then((response) => {
        console.log("response => ", response);
        if (response.data.status == 200) {
          this.setState({
            dtmf_list: response.data.data,
            dtmf_list_filter: response.data.data,
            isLoading: false,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          isLoading: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    this.setState({ isDeleteLoading: true });
    const { deleteItem } = this.state;

    deleteDTMFProfile(deleteItem.bankId, deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
            isDeleteLoading: false,
          });

          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
            isDeleteLoading: false,
          });
        }
        this.fetchDTMFProfiles();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isDeleteLoading: false,
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { dtmf_list } = this.state;

    let arrayDTMFProfiles = [];
    if (data) {
      arrayDTMFProfiles = this.handleFilterChangeVal(dtmf_list, data);
    } else {
      arrayDTMFProfiles = this.state.dtmf_list;
    }

    this.setState({
      dtmf_list_filter: arrayDTMFProfiles,
      searchValue: data,
    });
  };
  /**
   * Handle search event
   */
  handleFilterChangeVal = (dtmf_list, value) => {
    let dtmf_profile = [];
    dtmf_profile = dtmf_list.filter((item) => {
      return (
        (item.dtmf_status &&
          item.dtmf_status.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||

        (item.valid_code &&
          item.valid_code.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.invalid_code &&
          item.invalid_code.toLowerCase().indexOf(value.toLowerCase()) !==
          -1) ||
        (item.bank_name &&
          item.bank_name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return dtmf_profile;
  };


  /**
   * Remove underscroe and capitalize first letter
   * @param  str 
   * @returns 
   */
  convertCaptilize = (str) => {
    var i, frags = str.split('_');
    for (i = 0; i < frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }

  /**
   * Capital first letter
   * @param  s 
   * @returns 
   */
  capitalizeFirstLetter = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  /**
   * Handle route by id
   * @param  id 
   */
  handleRouteId = (id) => {
    localStorage.setItem("dtmf_profile_id", id);
    window.location.href = `#/dtmf-detail/${id}`
  }


  render() {
    const {
      successMsg,
      errorMsg,
      dtmf_list_filter,
      isLoading,
      searchValue,
      isDelete,
      deleteItem,
      isDeleteLoading
    } = this.state;

    return (
      <Fragment>

        <CCard>
          <CCardHeader>
            <b style={{ fontSize: "15px" }}> Settings</b>
            <span>
              <div className="md-form mt-3">
                <input
                  className="form-control search_list"
                  type="text"
                  placeholder="Search"
                  aria-label="Search"
                  value={searchValue}
                  onChange={(e) => {
                    this.handleFilterChange(e);
                  }}
                />

                <a href="#/add-dtmf-profile" className="button_style">
                  <CButton
                    type="submit"
                    size="sm"
                    color="primary"
                    className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                  >
                    {" "}
                    <i className="fa fa-plus" aria-hidden="true"></i>  DTMF Profile
                        </CButton>
                </a>
              </div>
            </span>
            {successMsg && (
              <CAlert color="success" className="msg_div">
                {successMsg}
              </CAlert>
            )}
            {errorMsg && (
              <CAlert color="danger" className="msg_div">
                {errorMsg}
              </CAlert>
            )}
            <CCardBody>
              <CDataTable
                items={dtmf_list_filter}
                fields={DTMF_FIELDS}
                itemsPerPage={5}
                loading={isLoading}
                pagination
                scopedSlots={{
                  dtmf_status: (item) => <td>{this.convertCaptilize(item.dtmf_status)}</td>,

                  dtmf: (item) => <td>{item.is_dtmf == 1 ? 'True' : 'False'}</td>,
                  bank_name: (item) => <td>{this.capitalizeFirstLetter(item.bank_name)}</td>,
                  Action: (item) => (
                    <td>
                      <a
                        href={`#/dtmf-edit/${item.id}`}
                        className="cancel_bt"
                      >
                        <CTooltip
                          content={`Edit Record`}
                          placement={"top-start"}
                        >
                          <CButton type="submit" size="sm" color="primary">
                            <i className="fa fa-edit" aria-hidden="true"></i>
                          </CButton>
                        </CTooltip>
                      </a>

                      <CButton
                        type="submit"
                        size="sm"
                        color="danger"
                        className="remove_button"
                        onClick={() => this.handleDeleteItem(item)}
                      >
                        <CTooltip content={`Delete Record`} placement={"top-start"}>
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </CTooltip>
                      </CButton>
                      <CButton
                        type="submit"
                        size="sm"
                        color="primary"
                        className="remove_button"
                        onClick={() => this.handleRouteId(item.id)}
                      >
                        <CTooltip content={`View Record`} placement={"top-start"}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </CTooltip>
                      </CButton>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCardHeader>
        </CCard>


        {isDelete && (
          <CModal
            show={isDelete}
            onClose={this.handleCancelModal}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this Profile{" "}
              <b>{this.convertCaptilize(deleteItem.dtmf_status)}</b>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={this.handleDeleteConfirm}>
                Delete {isDeleteLoading && <InvertContentLoader />}
              </CButton>{" "}
              <CButton color="secondary" onClick={this.handleCancelModal}>
                Cancel
                    </CButton>
            </CModalFooter>
          </CModal>
        )}


      </Fragment>
    );
  }
}


export default Listings;
