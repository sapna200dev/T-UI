import React, { Fragment } from "react";
import {
    ContentLoading,
  InvertContentLoader,
} from "../../../Comman/components/index";

import {
  CCol,
  CCardHeader,
  CCard,
  CCardBody,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CAlert,
  CSelect,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { IS_DTMF_VALUES, DTMF_STATUSES } from '../../constants'


const DTMFDetails = (props) => {
  const {
    successMsg,
    errorMsg,
    bankList,
    bank_id,
    isFatchingBanks,
    invalid_code,
    dtmf,
    is_dtmf,
    valid_code,
    isFatching
  } = props;
  
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "15px" }}> Details</b>
        </CCardHeader>
        {successMsg && (
          <CAlert
            color="success"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert
            color="danger"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="3">
              {isFatching && (
                  <ContentLoading
                    isFromAISetting={true}
                    content={"Fetching Record...!"}
                  />
                )}
                <CLabel htmlFor="alternate_phone">
                Profile Type <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="dtmf"
                  id="dtmf"
                  value={dtmf}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {DTMF_STATUSES ? (
                    DTMF_STATUSES.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No DTMF Profile</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                DTMF Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="is_dtmf"
                  id="is_dtmf"
                  value={is_dtmf}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {IS_DTMF_VALUES ? (
                    IS_DTMF_VALUES.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No DTMF</option>
                  )}
                </CSelect>

              </CCol>
              <CCol md="3">
                <CLabel htmlFor="valid_code">
                 Valid INput <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="valid_code"
                  name="valid_code"
                  value={valid_code}
                  placeholder="Enter Valid Code"
                  className="ai_setting_details"
                  // className="detail-text-color"
                />
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="invalid_code">
                  Invalid Input <small className="required_lable">*</small>
                  </CLabel>
                  <CInput
                    id="invalid_code"
                    name="invalid_code"
                    value={invalid_code}
                    placeholder="Enter Invalid Code"
                    className="ai_setting_details"
                  />
              </CCol>

              <CCol xs="12" md="3"></CCol>
              {/* <CCol xs="12" md="2"></CCol> */}
            </CFormGroup>

            <CFormGroup row>
            <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Bank <small className="required_lable">*</small>
                  {isFatchingBanks && <InvertContentLoader color={"black"} />}
              
                </CLabel>
                <CSelect
                  custom
                  name="bank_id"
                  id="bank_id"
                  value={bank_id}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {bankList ? (
                    bankList.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Banks</option>
                  )}
                </CSelect>

              </CCol>
              <CCol xs="12" md="3">
                </CCol>
                <CCol xs="12" md="3"></CCol>
            </CFormGroup>
          </CForm>

          <a href="#/global-parameter-list" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
        </CCardBody>
      </CCard>
    </Fragment>
  );
};

export default DTMFDetails;
