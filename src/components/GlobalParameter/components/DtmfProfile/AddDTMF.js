import React, { Fragment } from "react";
import {
  InvertContentLoader,
  PageNotFound,
} from "../../../Comman/components/index";

import {
  CWidgetDropdown,
  CRow,
  CCol,
  CCardHeader,
  CCard,
  CCardBody,
  CBadge,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { IS_DTMF_VALUES, DTMF_STATUSES, ENGINE_PROTOCOL, ENGINE_SERVICE, ENGINE_IP_ADDRESS, ENGINE_PORT_NUMBER } from '../../constants'


import { FIELDS } from "../../constants";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile",
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

const AddDTMF = (props) => {
  const {
    ai_engine_list,
    isLoading,
    handleDeleteItem,
    successMsg,
    errorMsg,
    handleFilterChange,
    searchValue,
    protocol,
    ip_address,
    port_number,
    service_engine,
    handleInputValues,
    service_engine_errors,
    handleAddAIEngine,
    isSaving,
    bankList,
    bank_id,
    isFatchingBanks,
    invalid_code,
    dtmf,
    is_dtmf,
    valid_code
  } = props;
  
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "15px" }}> Add Settings</b>
        </CCardHeader>
        {successMsg && (
          <CAlert
            color="success"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert
            color="danger"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                 Profile Type <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="dtmf"
                  id="dtmf"
                  invalid={service_engine_errors.dtmf}
                  onChange={handleInputValues}
                  value={dtmf}
                >
                  <option value="">Please select</option>
                  {DTMF_STATUSES ? (
                    DTMF_STATUSES.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No DTMF Profile</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.dtmf}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                DTMF Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="is_dtmf"
                  id="is_dtmf"
                  invalid={service_engine_errors.is_dtmf}
                  onChange={handleInputValues}
                  value={is_dtmf}
                >
                  <option value="">Please select</option>
                  {IS_DTMF_VALUES ? (
                    IS_DTMF_VALUES.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No DTMF</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.is_dtmf}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="valid_code">
                 Valid Input <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="valid_code"
                  name="valid_code"
                  value={valid_code}
                  placeholder="Enter Valid Code"
                  onChange={handleInputValues}
                  invalid={service_engine_errors.valid_code}
                  // className="detail-text-color"
                />

                <CInvalidFeedback>
                  {service_engine_errors.valid_code}
                </CInvalidFeedback>
              </CCol>
              <CCol md="3">
                <CLabel htmlFor="invalid_code">
                  Invalid Input <small className="required_lable">*</small>
                  </CLabel>
                  <CInput
                    id="invalid_code"
                    name="invalid_code"
                    value={invalid_code}
                    placeholder="Enter Invalid Code"
                    onChange={handleInputValues}
                    invalid={service_engine_errors.invalid_code}
                    // className="detail-text-color"
                  />

                  <CInvalidFeedback>
                    {service_engine_errors.invalid_code}
                  </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3"></CCol>
              {/* <CCol xs="12" md="2"></CCol> */}
            </CFormGroup>

            <CFormGroup row>
            <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Bank <small className="required_lable">*</small>
                  {isFatchingBanks && <InvertContentLoader color={"black"} />}
              
                </CLabel>
                <CSelect
                  custom
                  name="bank_id"
                  id="bank_id"
                  invalid={service_engine_errors.bank_id}
                  onChange={handleInputValues}
                  value={bank_id}
                >
                  <option value="">Please select</option>
                  {bankList ? (
                    bankList.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Banks</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.bank_id}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                </CCol>
                <CCol xs="12" md="3"></CCol>
            </CFormGroup>
          </CForm>

          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddAIEngine}
          >
            <CIcon name="cil-scrubber" /> Save
            {isSaving && <InvertContentLoader />}
          </CButton>
          <a href="#/global-parameter-list" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
        </CCardBody>
      </CCard>
    </Fragment>
  );
};

export default AddDTMF;
