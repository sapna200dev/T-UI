import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip
} from "@coreui/react";
import { AT_FIELDS } from "../../constants";


const Listings = (props) => {
  const {

    handleDeleteItem,
    successMsg,
    errorMsg,
    at_lists_filter,
    isLoading, 
    searchValue,
    handleFilterChange
  } = props;



  /**
   * Capital first letter
   * @param  s 
   * @returns 
   */
  const capitalizeFirstLetter = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "15px" }}>AT  Settings</b>
          <span>
            <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />
 
              <a href="#/add-at-setting" className="button_style">
                <CButton
                  type="submit"
                  size="sm"
                  color="primary"
                  className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                >
                  {" "}
                  <i className="fa fa-plus" aria-hidden="true"></i>  AT Settings
                </CButton>
              </a>
            </div>
          </span>
          {successMsg && (
            <CAlert color="success" className="msg_div">
              {successMsg}
            </CAlert>
          )}
          {errorMsg && (
            <CAlert color="danger" className="msg_div">
              {errorMsg}
            </CAlert>
          )}
          <CCardBody>
            <CDataTable
              items={at_lists_filter}
              fields={AT_FIELDS}
              itemsPerPage={5}
              loading={isLoading}
              pagination
              scopedSlots={{
                protocol: (item) => <td>{capitalizeFirstLetter(item.host)}</td>,
                bank_name: (item) => <td>{capitalizeFirstLetter(item.bank_name)}</td>,
                environment: (item) => <td>{item.environment != null ? capitalizeFirstLetter(item.environment) : 'NA'}</td>,
                port: (item) => <td>{item.port != null ? item.port : 'NA'}</td>,
                Action: (item) => (
                  <td>
                    <a
                      href={`#/asterisks-detail/${item.id}`}
                      className="cancel_bt"
                    >
                     <CTooltip
                        content={`Edit Record`}
                        placement={"top-start"}
                      >
                        <CButton type="submit" size="sm" color="primary">
                          <i className="fa fa-edit" aria-hidden="true"></i>
                          {/* Edit */}
                          {/* {loading && <ContentLoading />} */}
                        </CButton>
                      </CTooltip>


                    </a>

                    <CButton
                      type="submit"
                      size="sm"
                      color="danger"
                      className="remove_button"
                      onClick={() => handleDeleteItem(item)}
                    >
                        <CTooltip content={`Delete Record`} placement={"top-start"}>
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </CTooltip>
                    </CButton>
                    <a
                      href={`#/asterisks-view/${item.id}`}
                      className="cancel_bt"
                    >
                      <CButton
                        type="submit"
                        size="sm"
                        color="primary"
                        className="remove_button"
                      >
                        <CTooltip content={`View Record`} placement={"top-start"}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </CTooltip>
                      </CButton>
                    </a>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCardHeader>
      </CCard>
    </Fragment>
  );
};

export default Listings;
