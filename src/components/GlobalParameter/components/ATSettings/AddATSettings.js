import React, { Fragment } from "react";
import {
  InvertContentLoader,
  PageNotFound,
} from "../../../Comman/components/index";

import {
  CWidgetDropdown,
  CRow,
  CCol,
  CCardHeader,
  CCard,
  CCardBody,
  CBadge,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ENGINE_IP_ADDRESS, ENGINE_PORT_NUMBER, ENGINE_PROTOCOL, ENGINE_SERVICE } from '../../constants'
import {AT_ENVIORNMENT, AT_IP_ADDRESS} from '../../constants'

import { FIELDS } from "../../constants";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile",
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

const AddATSettings = (props) => {
  const {

    successMsg,
    errorMsg,

    protocol,
    ip_address,

    handleInputValues,
    service_engine_errors,
    handleAddATSettings,
    isSaving,
    bankList,
    bank_id,
    isFatchingBanks,
    at_environment,
    ip_addresses,
    envirnments,
    addressess,
    ports,
    protocols,
    portsVal
  } = props;
  
  console.log('_this_this = error ', errorMsg)
  return (
    <Fragment>
      <CCard> 
        <CCardHeader>
          <b style={{ fontSize: "15px" }}> Add AT Settings</b>
        </CCardHeader>
        {successMsg && (
          <CAlert
            color="success"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert
            color="danger"
            className="msg_div"
            style={{ marginTop: "10px", marginBottom: "1px" }}
          >
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Protocol <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="protocol"
                  id="protocol"
                  invalid={service_engine_errors.protocol}
                  onChange={handleInputValues}
                  value={protocol}
                >
                  <option value="">Please select</option>
                  {/* {ENGINE_PROTOCOL ? (
                    ENGINE_PROTOCOL.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Protocols</option>
                  )} */}
                  {protocols ? (
                    protocols.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Protocols</option>
                  )}

                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.protocol}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Ports <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="portsVal"
                  id="portsVal"
                  // invalid={service_engine_errors.ports}
                  onChange={handleInputValues}
                  value={portsVal}
                >
                  <option value="">Please select</option>
                  {ports ? (
                    ports.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Prots</option>
                  )}
                </CSelect>
                {/* <CInvalidFeedback>
                  {service_engine_errors.ports}
                </CInvalidFeedback> */}
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ip_address">
                  IP Address <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="ip_address"
                  id="ip_address"
                  invalid={service_engine_errors.ip_address}
                  onChange={handleInputValues}
                  value={ip_address}
                >
                  <option value="">Please select</option>
                  {/* {AT_IP_ADDRESS ? (
                    AT_IP_ADDRESS.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No IP Address</option>
                  )} */}

                  {addressess ? (
                    addressess.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No IP Address</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.ip_address}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="at_environment">
                Environment <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="at_environment"
                  id="at_environment"
                  invalid={service_engine_errors.at_environment}
                  onChange={handleInputValues}
                  value={at_environment}
                >
                  <option value="">Please select</option>
                  {/* {AT_ENVIORNMENT ? (
                    AT_ENVIORNMENT.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Environment</option>
                  )} */}

                  {envirnments ? (
                    envirnments.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Environment</option>
                  )}


                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.at_environment}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="bank_id">
                  Banks <small className="required_lable">*</small>
                  {isFatchingBanks && <InvertContentLoader color={"black"} />}
              
                </CLabel>
                <CSelect
                  custom
                  name="bank_id"
                  id="bank_id"
                  invalid={service_engine_errors.bank_id}
                  onChange={handleInputValues}
                  value={bank_id}
                >
                  <option value="">Please select</option>
                  {bankList ? (
                    bankList.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Banks</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {service_engine_errors.bank_id}
                </CInvalidFeedback>
              </CCol>
     

              <CCol xs="12" md="3"></CCol>
              {/* <CCol xs="12" md="2"></CCol> */}
            </CFormGroup>

            <CFormGroup row>

              <CCol xs="12" md="3">
                </CCol>
                <CCol xs="12" md="3"></CCol>
            </CFormGroup>
          </CForm>

          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddATSettings}
          >
            <CIcon name="cil-scrubber" /> Save
            {isSaving && <InvertContentLoader />}
          </CButton>
          <a href="#/global-parameter-list" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>
        </CCardBody>
      </CCard>
    </Fragment>
  );
};

export default AddATSettings;
