import React, { Fragment } from "react";
import {
  InvertContentLoader,
  ContentLoading
} from "../../../Comman/components/index";


import {
  CCol,
  CCardHeader,
  CCard,
  CCardBody,
  CForm,
  CFormGroup,
  CLabel,

  CSelect,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ENGINE_PROTOCOL } from '../../constants'
import { AT_ENVIORNMENT, AT_IP_ADDRESS } from '../../constants'

const DetailATSetting = (props) => {
  const {
    protocol,
    ip_address,
    service_engine_errors,
    bankList,
    bank_id,
    isFatchingBanks,
    at_environment,
    isFatching,
    envirnments,
    addressess,
    ports,
    protocols,
    portsVal
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "15px" }}> View AT Settings</b>
        </CCardHeader>

        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
               
            <CFormGroup row>
              <CCol xs="12" md="3">
              {isFatching && (
                  <ContentLoading
                    isFromAISetting={true}
                    content={"Fetching Record...!"}
                  />
                )}
                <CLabel htmlFor="alternate_phone">
                  Protocol <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="protocol"
                  id="protocol"
                  invalid={service_engine_errors.protocol}
                  value={protocol}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {/* {ENGINE_PROTOCOL ? (
                    ENGINE_PROTOCOL.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Protocols</option>
                  )} */}
                  {protocols ? (
                    protocols.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Protocols</option>
                  )}

                </CSelect>
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Ports <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="portsVal"
                  id="portsVal"
                  className="ai_setting_details"
                  invalid={service_engine_errors.ports}
                  value={portsVal}
                >
                  <option value="">Please select</option>
                  {ports ? (
                    ports.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Prots</option>
                  )}
                </CSelect>

              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="ip_address">
                  IP Address <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="ip_address"
                  id="ip_address"
                  invalid={service_engine_errors.ip_address}
                  value={ip_address}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {/* {AT_IP_ADDRESS ? (
                    AT_IP_ADDRESS.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No IP Address</option>
                  )} */}

                  {addressess ? (
                    addressess.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No IP Address</option>
                  )}
                </CSelect>

              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="at_environment">
                  Environment <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="at_environment"
                  id="at_environment"
                  invalid={service_engine_errors.at_environment}

                  value={at_environment}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {/* {AT_ENVIORNMENT ? (
                    AT_ENVIORNMENT.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Environment</option>
                  )} */}

                  {envirnments ? (
                    envirnments.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Environment</option>
                  )}
                </CSelect>

              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="bank_id">
                  Banks <small className="required_lable">*</small>
                  {isFatchingBanks && <InvertContentLoader color={"black"} />}

                </CLabel>
                <CSelect
                  custom
                  name="bank_id"
                  id="bank_id"
                  invalid={service_engine_errors.bank_id}

                  value={bank_id}
                  className="ai_setting_details"
                >
                  <option value="">Please select</option>
                  {bankList ? (
                    bankList.map((item) => (
                      <option value={item.text}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Banks</option>
                  )}
                </CSelect>

              </CCol>


              <CCol xs="12" md="3"></CCol>
              {/* <CCol xs="12" md="2"></CCol> */}
            </CFormGroup>

            <CFormGroup row>

              <CCol xs="12" md="3">
              </CCol>
              <CCol xs="12" md="3"></CCol>
            </CFormGroup>
          </CForm>
          <a href="#/global-parameter-list" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Back
            </CButton>
          </a>
        </CCardBody>
      </CCard>
    </Fragment>
  );
};

export default DetailATSetting;
