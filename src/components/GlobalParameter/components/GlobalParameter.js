import React, { Fragment } from "react";
import {

  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
 
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import { AIEngineLists } from './AIEngine'
import { Listings } from './DtmfProfile'
import ATListing from '../containers/ATProfile/ATListing'

export const GlobalParameter = (props) => {
  const { 
    errorMsg,
    successMsg,
    ai_engine_list_filter,
    ai_engine_list,
    isLoading,
    handleDeleteItem,
    searchValue,
    handleFilterChange
  } = props

 
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> Global Parameter List</b>
        </CCardHeader>

        <CCardBody>
          <CTabs>
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink>AI Engine</CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink>DTMF Profile</CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink>AT Setting</CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane>
                <AIEngineLists 
                  ai_engine_list_filter={ai_engine_list_filter}
                  ai_engine_list={ai_engine_list}
                  isLoading={isLoading}
                  handleDeleteItem={handleDeleteItem}
                  successMsg={successMsg}
                  errorMsg={errorMsg}
                  handleFilterChange={handleFilterChange}
                  searchValue={searchValue}
                />
              </CTabPane>
              <CTabPane>
                <Listings 
                    ai_engine_list_filter={ai_engine_list_filter}
                    ai_engine_list={ai_engine_list}
                    isLoading={isLoading}
                    handleDeleteItem={handleDeleteItem}
                    successMsg={successMsg}
                    errorMsg={errorMsg}
                    handleFilterChange={handleFilterChange}
                    searchValue={searchValue}
                  />
              </CTabPane>
              <CTabPane>
                <ATListing 
                    ai_engine_list_filter={ai_engine_list_filter}
                    ai_engine_list={ai_engine_list}
                    isLoading={isLoading}
                    handleDeleteItem={handleDeleteItem}
                    successMsg={successMsg}
                    errorMsg={errorMsg}
                    handleFilterChange={handleFilterChange}
                    searchValue={searchValue}
                  />
              </CTabPane>
            </CTabContent>
          </CTabs>

          {/* <PageNotFound /> */}
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
