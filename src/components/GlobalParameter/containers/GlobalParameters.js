import React, { Component, Fragment } from "react";
import { GlobalParameter } from "../components/index";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from "@coreui/react";

import { getAIEngineList, deleteAIEngine } from "../services/index";
import { InvertContentLoader } from "../../Comman/components/index";

class GlobalParameters extends Component {
  state = {
    protocol: "",
    ip_address: "",
    port_number: "",
    service_engine: "",
    isSaving: false,
    service_engine_errors: {},
    errorMsg: "",
    successMsg: "",
    ai_engine_list: [],
    ai_engine_list_filter: [],
    isLoading: false,
    isDelete: false,
    deleteItem: "",
    isDeleteLoading: false,
    searchValue: "",
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    this.fetchAIEngineList();
  }

  /**
   * Fetch ai settings
   */
  fetchAIEngineList = () => {
    getAIEngineList()
      .then((response) => {
        console.log("response => ", response);
        if (response.data.status == 200) {
          this.setState({
            ai_engine_list: response.data.data,
            ai_engine_list_filter: response.data.data,
            isLoading: false,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          isLoading: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    this.setState({ isDeleteLoading: true });
    const { deleteItem } = this.state;

    deleteAIEngine(deleteItem.bankId, deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
            isDeleteLoading: false,
          });

          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
            isDeleteLoading: false,
          });
        }
        this.fetchAIEngineList();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isDeleteLoading: false,
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { ai_engine_list } = this.state;

    let arrayAISettings = [];
    if (data) {
      arrayAISettings = this.handleFilterChangeVal(ai_engine_list, data);
    } else {
      arrayAISettings = this.state.ai_engine_list;
    }

    this.setState({
      ai_engine_list_filter: arrayAISettings,
      searchValue: data,
    });
  };
  /**
   * Handle search event
   */
  handleFilterChangeVal = (ai_engine_list, value) => {
    let ai_settings = [];
    ai_settings = ai_engine_list.filter((item) => {
      return (
        (item.protocol &&
          item.protocol.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.ip_address &&
          item.ip_address.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.port_number &&
          item.port_number.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.service_engine &&
          item.service_engine.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.bank_name &&
          item.bank_name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return ai_settings;
  };

  render() {
    const {
      errorMsg,
      successMsg,
      ai_engine_list_filter,
      ai_engine_list,
      isLoading,
      isDelete,
      deleteItem,
      isDeleteLoading,
      searchValue,
    } = this.state;

    return (
      <Fragment>
        <GlobalParameter
          errorMsg={errorMsg}
          successMsg={successMsg}
          ai_engine_list_filter={ai_engine_list_filter}
          ai_engine_list={ai_engine_list}
          isLoading={isLoading}
          handleDeleteItem={this.handleDeleteItem}
          handleFilterChange={this.handleFilterChange}
          searchValue={searchValue}
        />
        {/* <PageNotFound /> */}

        {isDelete && (
          <CModal
            show={isDelete}
            onClose={this.handleCancelModal}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this AI Engine{" "}
              <b>{deleteItem.service_engine}</b>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={this.handleDeleteConfirm}>
                Delete {isDeleteLoading && <InvertContentLoader />}
              </CButton>{" "}
              <CButton color="secondary" onClick={this.handleCancelModal}>
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}
      </Fragment>
    );
  }
}

export default GlobalParameters;
