import React, { Component, Fragment } from "react";
// import { GlobalParameter } from "../components/index";
import {
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
} from "@coreui/react";

import {
  validateRequestDTMFProfileParams,
  CreateReuqestPayloadForDTMFProfile,
} from "../functions";
import {AddDTMF } from '../../components/DtmfProfile/index'
import { updateDTMFProfile, getDTMFProfileREcord } from "../../services/index";
import { EditDtmf } from "../../components/DtmfProfile/index";
import { bankLists } from "../../../Bank/services";

class EditPage extends Component {
  state = {

    dtmf: "",
    is_dtmf: "",
    valid_code: "",
    invalid_code: "",
    isSaving: false,
    service_engine_errors: {},
    errorMsg: "",
    successMsg: "",
    ai_engine_list: [],
    bankList: [],
    bank_id: "",
    isFatchingBanks:false
  };

  componentDidMount() {
    this.setState({ isFatchingBanks:true })

    const { id } = this.props.match.params;
    // const { id } = this.props.id;
    this.setState({ isFatching: true, id });

        console.log('update id = ', id)

    this.getDTMFProfileById(id);
    this.getAllBanks();
  }

  /**
   * Get all banks
   */
  getAllBanks = () => {
    bankLists()
      .then((res) => {
        console.log("bank list  res", res);
        if (res.data.status == 200) {
          const getOptions = res.data.data.map((item, index) => {
            return {
              key: index,
              value: `${item.bank_name}`,
              text: item.id,
            };
          });

          this.setState({
            bankList: getOptions,
            filterBankLists: res.data.data,
            isFatchingBanks:false
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
          });
        }
      })
      .catch((err) => {
        console.log("bank list  error", err);
        this.setState({
          bankList: "",
          filterBankLists: "",
          isFatchingBanks:false
        });
      });
  };

  /**
   *  Get all ai_engine list
   */
   getDTMFProfileById = (id) => {
    getDTMFProfileREcord(id)
      .then((response) => {
        if (response.data.status == 200) {
          let bank = response.data.data;

          this.setState({
            dtmf: bank.dtmf_status,
            is_dtmf: bank.is_dtmf,
            valid_code: bank.valid_code,
            invalid_code: bank.invalid_code,
            bank_id: bank.bankId,
            ai_engine_list: response.data.data,
            isFatching: false,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
            isFatching: false,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          isFatching: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  handleInputValues = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleUpdateDtmfValidation = () => {
    // Request validate
    let service_engine_errors = validateRequestDTMFProfileParams(this.state);
    this.setState({ service_engine_errors });
    return service_engine_errors;
  };

  handleUpdateDTMFProfile = () => {
    console.log("handleAddEndUser = ");
    const errors = this.handleUpdateDtmfValidation();
    console.log("handleAddEndUser errors = ", errors);
    if (Object.getOwnPropertyNames(errors).length === 0) {
      console.log(" all done u can add");
      this.modifyDTMFProfileSetting();
    }
  };

  /**
   * Add ai engine settings
   */
  modifyDTMFProfileSetting = () => {
    this.setState({ isSaving: true });
    const { id } = this.state;
    const payload = CreateReuqestPayloadForDTMFProfile(this.state);

    updateDTMFProfile(id, payload)
      .then((response) => {
        console.log("response found = ", response);
        let _this = this;
        if (response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            isSaving: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } else {
          this.setState({
            isSaving: false,
            errorMsg: response.data.msg,
            successMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        }
      })
      .catch((error) => {
        let _this = this;
        console.log("error found = ", error);
        console.log("error found = ", error.response);

        this.setState({
          isSaving: false,
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          successMsg: "",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };
  render() {
    const {
      dtmf,
      is_dtmf,
      valid_code,
      invalid_code,
      service_engine_errors,
      isSaving,
      errorMsg,
      successMsg,
      ai_engine_list,
      bankList,
      bank_id,
      isFatchingBanks
    } = this.state;
    console.log("service_engine_errors bankList = ", bankList);

    return (
      <Fragment>
           <CCard>
          <CCardHeader>
            <b style={{ fontSize: "20px" }}> Global Parameter List</b>
          </CCardHeader>

          <CCardBody> 
            <CTabs>
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink>DTMF Profile</CNavLink>
                </CNavItem>
                {/* <CNavItem>
                  <CNavLink>DTMF Profile</CNavLink>
                </CNavItem> */}
                <CNavItem>{/* <CNavLink>Messages</CNavLink> */}</CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane>
                    <EditDtmf 
                        ai_engine_list={ai_engine_list}
                        errorMsg={errorMsg}
                        successMsg={successMsg}
                        dtmf={dtmf}
                        is_dtmf={is_dtmf}
                        valid_code={valid_code}
                        invalid_code={invalid_code}
                        isSaving={isSaving}
                        handleUpdateDTMFProfile={this.handleUpdateDTMFProfile}
                        service_engine_errors={service_engine_errors}
                        bankList={bankList}
                        bank_id={bank_id}
                        handleInputValues={this.handleInputValues}
                        isFatchingBanks={isFatchingBanks}
                        />
                </CTabPane>
                {/* <CTabPane>Other global params Edit</CTabPane> */}
              </CTabContent>
            </CTabs>

            {/* <PageNotFound /> */}
          </CCardBody>
        </CCard>

     
      </Fragment>
    );
  }
}

export default EditPage;
