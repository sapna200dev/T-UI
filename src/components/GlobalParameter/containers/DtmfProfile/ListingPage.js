import React, { Component, Fragment } from "react";
import { GlobalParameter } from "../components/index";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from "@coreui/react";

import { getDTMFProfileLists, deleteDTMFProfile } from "../../services/index";
import { InvertContentLoader } from "../../../Comman/components/index";

class ListingPage extends Component {
  state = {
    errorMsg: "",
    successMsg: "",
    dtmf_list: [],
    dtmf_list_filter: [],
    isLoading: false,
    isDelete: false,
    deleteItem: "",
    isDeleteLoading: false,
    searchValue: "",
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    this.fetchDTMFProfiles();
  }

  /**
   * Fetch ai settings
   */
  fetchDTMFProfiles = () => {
    getDTMFProfileLists()
      .then((response) => {
        console.log("response => ", response);
        if (response.data.status == 200) {
          this.setState({
            dtmf_list: response.data.data,
            dtmf_list_filter: response.data.data,
            isLoading: false,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          isLoading: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    this.setState({ isDeleteLoading: true });
    const { deleteItem } = this.state;

    deleteDTMFProfile(deleteItem.bankId, deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
            isDeleteLoading: false,
          });

          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
            isDeleteLoading: false,
          });
        }
        this.fetchAIEngineList();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isDeleteLoading: false,
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { dtmf_list } = this.state;

    let arrayDTMFProfiles = [];
    if (data) {
      arrayDTMFProfiles = this.handleFilterChangeVal(dtmf_list, data);
    } else {
      arrayDTMFProfiles = this.state.dtmf_list;
    }

    this.setState({
    dtmf_list_filter: arrayDTMFProfiles,
      searchValue: data,
    });
  };
  /**
   * Handle search event
   */
  handleFilterChangeVal = (dtmf_list, value) => {
    let dtmf_profile = [];
    dtmf_profile = dtmf_list.filter((item) => {
      return (
        (item.dtmf_status &&
          item.dtmf_status.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.is_dtmf &&
          item.is_dtmf.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.valid_code &&
          item.valid_code.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.invalid_code &&
          item.invalid_code.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.bank_name &&
          item.bank_name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return dtmf_profile;
  };

  render() {
    const {
      errorMsg,
      successMsg,
    dtmf_list_filter,
      dtmf_list,
      isLoading,
      isDelete,
      deleteItem,
      isDeleteLoading,
      searchValue,
    } = this.state;

    return (
      <Fragment>
        <GlobalParameter
          errorMsg={errorMsg}
          successMsg={successMsg}
        dtmf_list_filter={dtmf_list_filter}
          dtmf_list={dtmf_list}
          isLoading={isLoading}
          handleDeleteItem={this.handleDeleteItem}
          handleFilterChange={this.handleFilterChange}
          searchValue={searchValue}
        />
        {/* <PageNotFound /> */}

        {isDelete && (
          <CModal
            show={isDelete}
            onClose={this.handleCancelModal}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this End User{" "}
              <b>{deleteItem.dtmf_status}</b>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={this.handleDeleteConfirm}>
                Delete {isDeleteLoading && <InvertContentLoader />}
              </CButton>{" "}
              <CButton color="secondary" onClick={this.handleCancelModal}>
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}
      </Fragment>
    );
  }
}

export default ListingPage;
