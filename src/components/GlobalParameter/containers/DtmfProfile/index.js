
import AddPage from './AddPage'
import DetailPage from './DetailPage'
import EditPage from './EditPage'
export {
    AddPage,
    DetailPage,
    EditPage
}
