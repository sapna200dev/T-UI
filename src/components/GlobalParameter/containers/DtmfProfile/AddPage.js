import React, { Component, Fragment } from "react";
// import { GlobalParameter } from "../components/index";
import {
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
} from "@coreui/react";

import {
  validateRequestDTMFProfileParams,
  CreateReuqestPayloadForDTMFProfile,
} from "../functions";
import {AddDTMF } from '../../components/DtmfProfile/index'
import { createDTMFProfileLists, getAIEngineList } from "../../services/index";
import { AddAIEnging } from "../../components/AIEngine";
import { bankLists } from "../../../Bank/services";

class AddPage extends Component {
  state = {

    dtmf: "",
    is_dtmf: "",
    valid_code: "",
    invalid_code: "",
    isSaving: false,
    service_engine_errors: {},
    errorMsg: "",
    successMsg: "",
    ai_engine_list: [],
    bankList: [],
    bank_id: "",
    isFatchingBanks:false
  };

  componentDidMount() {
    this.setState({ isFatchingBanks:true })
    this.fetchAIEngineList();
    this.getAllBanks();
  }

  /**
   * Get all banks
   */
  getAllBanks = () => {
    bankLists()
      .then((res) => {
        console.log("bank list  res", res);
        if (res.data.status == 200) {
          const getOptions = res.data.data.map((item, index) => {
            return {
              key: index,
              value: `${item.bank_name}`,
              text: item.id,
            };
          });

          this.setState({
            bankList: getOptions,
            filterBankLists: res.data.data,
            isFatchingBanks:false
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
          });
        }
      })
      .catch((err) => {
        console.log("bank list  error", err);
        this.setState({
          bankList: "",
          filterBankLists: "",
          isFatchingBanks:false
        });
      });
  };

  /**
   *  Get all ai_engine list
   */
  fetchAIEngineList = () => {
    getAIEngineList()
      .then((response) => {
        console.log("response => ", response);
        if (response.data.status == 200) {
          this.setState({
            ai_engine_list: response.data.data,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;
        console.log("error found = ", error);
        console.log("error found = ", error.response);

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  handleInputValues = (e) => {
    const { name, value } = e.target;
    console.log("change name = ", name);
    console.log("change value = ", value);
    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleAddAIEngineValidation = () => {
    // Request validate
    let service_engine_errors = validateRequestDTMFProfileParams(this.state);
    this.setState({ service_engine_errors });
    return service_engine_errors;
  };

  handleAddAIEngine = () => {
    console.log("handleAddEndUser = ");
    const errors = this.handleAddAIEngineValidation();
    console.log("handleAddEndUser errors = ", errors);
    if (Object.getOwnPropertyNames(errors).length === 0) {
      console.log(" all done u can add");
      this.addDTMFProfileSetting();
    }
  };

  /**
   * Add ai engine settings
   */
  addDTMFProfileSetting = () => {
    this.setState({ isSaving: true });
    const payload = CreateReuqestPayloadForDTMFProfile(this.state);
    console.log("payload  = ", payload);
    createDTMFProfileLists(payload)
      .then((response) => {
        console.log("response found = ", response);
        let _this = this;
        if (response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            isSaving: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } else {
          this.setState({
            isSaving: false,
            errorMsg: response.data.msg,
            successMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        }
      })
      .catch((error) => {
        let _this = this;
        console.log("error found = ", error);
        console.log("error found = ", error.response);

        this.setState({
          isSaving: false,
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          successMsg: "",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };
  render() {
    const {
      dtmf,
      is_dtmf,
      valid_code,
      invalid_code,
      service_engine_errors,
      isSaving,
      errorMsg,
      successMsg,
      ai_engine_list,
      bankList,
      bank_id,
      isFatchingBanks
    } = this.state;
    console.log("service_engine_errors bankList = ", bankList);

    return (
      <Fragment>

<CCard>
          <CCardHeader>
            <b style={{ fontSize: "20px" }}> Global Parameter List</b>
          </CCardHeader>

          <CCardBody>
            <CTabs>
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink>DTMF Profile</CNavLink>
                </CNavItem>
                <CNavItem>{/* <CNavLink>Messages</CNavLink> */}</CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane>
                  <AddDTMF 
                      ai_engine_list={ai_engine_list}
                      errorMsg={errorMsg}
                      successMsg={successMsg}
                      dtmf={dtmf}
                      is_dtmf={is_dtmf}
                      valid_code={valid_code}
                      invalid_code={invalid_code}
                      isSaving={isSaving}
                      handleAddAIEngine={this.handleAddAIEngine}
                      service_engine_errors={service_engine_errors}
                      bankList={bankList}
                      bank_id={bank_id}
                      handleInputValues={this.handleInputValues}
                      isFatchingBanks={isFatchingBanks}
                    />
                </CTabPane>
         
              </CTabContent>
            </CTabs>

            {/* <PageNotFound /> */}
          </CCardBody>
        </CCard>


       <div> 
       
       </div>
      </Fragment>
    );
  }
}

export default AddPage;
