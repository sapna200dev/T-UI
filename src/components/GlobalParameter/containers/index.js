import GlobalParameters from './GlobalParameters'
import  AIEngineList from './AIEngine/AIEngineList'
import AddNewAIEngine from './AIEngine/AddNewAIEngine'
import EditAIEngine from './AIEngine/EditAIEngine'
import AIEngineDetails from './AIEngine/AIEngineDetails'
import DetailPage from './DtmfProfile/DetailPage'
import EditPage from './DtmfProfile/EditPage'
import AddPage from './DtmfProfile/AddPage'
import ATListing from './ATProfile/ATListing'
import AddNewAT from './ATProfile/AddNewAT'
import EditATSettings from './ATProfile/EditATSettings'
import ViewATSetting from './ATProfile/ViewATSetting'

export {
    GlobalParameters,
    AIEngineList,
    AddNewAIEngine,
    EditAIEngine,
    AIEngineDetails,

    DetailPage,
    EditPage,
    AddPage,


    ATListing,
    AddNewAT,
    EditATSettings,
    ViewATSetting
}
