import React, { Component, Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from "@coreui/react";
import { getATSetttingsLists, deleteATSetttings } from "../../services/index";
import { InvertContentLoader } from "../../../Comman/components/index";
import { Listings } from '../../components/ATSettings/index'

import { DTMF_FIELDS } from "../../constants";
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile", 
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

 class ATListing extends Component {
    state = {
        errorMsg: "", 
        successMsg: "",
        at_lists: [],
        at_lists_filter: [],
        isLoading: false,
        isDelete: false,
        deleteItem: "",
        isDeleteLoading: false,
        searchValue: "",
      };


    componentDidMount() {
        console.log("response componentDidMount Listings => ");
        this.setState({ isLoading: true });

        // Fetch dtmf profiles
        this.fetchATSettings();
    }

    /**
     * Fetch ai settings
     */
   fetchATSettings = () => {
    getATSetttingsLists()
      .then((response) => {
        console.log("response ddddddd => ", response);
        if (response.data.status == 200) {
          this.setState({
            at_lists: response.data.data,
            at_lists_filter: response.data.data,
            isLoading: false,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          isLoading: false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    this.setState({ isDeleteLoading: true });
    const { deleteItem } = this.state;

    // Delete record api call
    deleteATSetttings(deleteItem.bankId, deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
            isDeleteLoading: false,
          });

          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);

          // Fetch dtmf profiles
          this.fetchATSettings();

        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
            isDeleteLoading: false,
          });
        }
        this.fetchATSettings();
      })
      .catch((err) => {
        console.log('delete record err = ', err.response)
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
          isDeleteLoading: false,
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { at_lists } = this.state;

    let arrayDTMFProfiles = [];
    if (data) {
      arrayDTMFProfiles = this.handleFilterChangeVal(at_lists, data);
    } else {
      arrayDTMFProfiles = this.state.at_lists;
    }

    this.setState({
        at_lists_filter: arrayDTMFProfiles,
      searchValue: data,
    });
  };

  /**
   * Handle search event
   */
  handleFilterChangeVal = (at_lists, value) => {
    let dtmf_profile = [];
    dtmf_profile = at_lists.filter((item) => {
      return (
        (item.bank_name &&
          item.bank_name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
  
        (item.host &&
          item.host.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.ip_address &&
          item.ip_address.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.environment &&
          item.environment.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return dtmf_profile;
  };


  /**
   * Remove underscroe and capitalize first letter
   * @param  str 
   * @returns 
   */
  convertCaptilize = (str) =>  {
    var i, frags = str.split('_');
    for (i=0; i<frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }
  
  /**
   * Capital first letter
   * @param  s 
   * @returns 
   */
  capitalizeFirstLetter = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }


  /**
   * Handle route by id
   * @param  id 
   */
  handleRouteId = (id) => {
    localStorage.setItem("dtmf_profile_id", id);
    window.location.href=`#/dtmf-detail/${id}`
  }


    render() {
        const {
            successMsg,
            errorMsg,
            at_lists_filter,
            isLoading, 
            searchValue,
            isDelete,
            deleteItem,
            isDeleteLoading
          } = this.state;
          console.log('at_lists_filterat_lists_filter = ', at_lists_filter)
        return (
            <Fragment>
                                           
                <Listings 
                  at_lists_filter={at_lists_filter}
                  isLoading={isLoading}
                  handleDeleteItem={this.handleDeleteItem}
                  successMsg={successMsg}
                  errorMsg={errorMsg}
                  handleFilterChange={this.handleFilterChange}
                  searchValue={searchValue}
                />
              {isDelete && (
                <CModal
                    show={isDelete}
                    onClose={this.handleCancelModal}
                    color="danger"
                >
                    <CModalHeader closeButton>
                    <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                    Are you sure to delete this Settings {" "} <b>{deleteItem.environment} -  {deleteItem.ip_address} </b>
                    <b></b>
                    </CModalBody>
                    <CModalFooter>
                    <CButton color="danger" onClick={this.handleDeleteConfirm}>
                        Delete {isDeleteLoading && <InvertContentLoader />}
                    </CButton>{" "}
                    <CButton color="secondary" onClick={this.handleCancelModal}>
                        Cancel
                    </CButton>
                    </CModalFooter>
                </CModal>
                )}
        

            </Fragment>
          );
    }
}



export default ATListing