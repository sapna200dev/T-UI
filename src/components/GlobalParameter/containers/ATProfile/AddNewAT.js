import React, { Component, Fragment } from "react";
// import { GlobalParameter } from "../components/index";
import {
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
} from "@coreui/react";

import {
  validateRequestATSettingParams,
  CreateReuqestPayloadForATSetting,
} from "../functions";
import { addNewATSetttings, getATSetttingsLists } from "../../services/index";
import { AddAIEnging } from "../../components/AIEngine";
import { bankLists, getATPortsAPI,
  getATIpAddressessAPI, 
  getATEnvironmentAPI,
  getATProtocolsAPI } from "../../../Bank/services";
import AddATSettings from '../../components/ATSettings/AddATSettings'

class AddNewAT extends Component {
  state = {
    protocol: "",
    ip_address: "",
    host:'',
    port_number: "",
    service_engine: "",
    isSaving: false,
    service_engine_errors: {},
    errorMsg: "",
    successMsg: "",
    at_settings_list: [],
    bankList: [],
    bank_id: "",
    isFatchingBanks:false,
    ip_addresses:[],
    at_environment:'',
    envirnments:[],
    addressess:[],
    ports:[],
    protocols:[],
    isFatchingATConfigs:false,
    portsVal:''
  };

  componentDidMount() {
    this.setState({ isFatchingBanks:true, isFatchingATConfigs:true })
    // Fetch all AT settings
    this.fetchATSettingsList();
 
    // Fetch all banks
    this.getAllBanks();
    this.getATPorts()
    this.getATEnvironments();
    this.getATIpAddress();
    this.getATProtocols();
  }

  /**
   * Get all banks
   */
  getAllBanks = () => {
    bankLists()
      .then((res) => {

        if (res.data.status == 200) {
          const getOptions = res.data.data.map((item, index) => {
            return {
              key: index,
              value: `${item.bank_name}`,
              text: item.id,
            };
          });

          this.setState({
            bankList: getOptions,
            filterBankLists: res.data.data,
            isFatchingBanks:false
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
          });
        }
      })
      .catch((err) => {
        console.log("bank list  error", err);
        this.setState({
          bankList: "",
          filterBankLists: "",
          isFatchingBanks:false
        });
      });
  };

  /**
   * Get all banks
   */
   getATPorts = () => {
    getATPortsAPI()
      .then((res) => {
        if (res.data.status == 200) {
          const getPortsOptions = res.data.data.map((item, index) => {
            if(item.ports != null  && item.ports != undefined) {
              return {
                key: item.id,
                value: `${item.ports}`,
                text:`${item.ports}`,
              };
            }
          });

          this.setState({
            ports: getPortsOptions,
            // filterBankLists: res.data.data,
            isFatchingATConfigs:false
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
            isFatchingATConfigs:false
          });
        }
      })
      .catch((err) => {
        console.log("bank list  error", err);
        this.setState({
          bankList: "",
          filterBankLists: "",
          isFatchingATConfigs:false
        });
      });
  };


    /**
   * Get protocols
   */
     getATProtocols = () => {
      getATProtocolsAPI()
        .then((res) => {
          if (res.data.status == 200) {
            const getProtocolOptions = res.data.data.map((item, index) => {
              if(item.protocols != null && item.protocols != undefined) {
                return {
                  key: item.id,
                  value: `${item.protocols}`,
                  text: `${item.name}`,
                };
              }
          
            });
          
            this.setState({
              protocols: getProtocolOptions,
              // filterBankLists: res.data.data,
              isFatchingATConfigs:false
            });
          } else {
            this.setState({
              errorMsg: res.data.msg,
              isFatchingATConfigs:false
            });
          }
        })
        .catch((err) => {
          console.log("bank list  error", err);
          this.setState({
            bankList: "",
            filterBankLists: "",
            isFatchingATConfigs:false
          });
        });
    };



    /**
   * Get Ip addressess
   */
     getATIpAddress = () => {
      getATIpAddressessAPI()
        .then((res) => {
          if (res.data.status == 200) {
            const getIpAddressOptions = res.data.data.map((item, index) => {
              if(item.ip_address != null && item.ip_address != undefined ) {
                return {
                  key: item.id,
                  value: `${item.ip_address}`,
                  text: `${item.ip_address}`,
                };
              }
          
            });
          
            this.setState({
              addressess: getIpAddressOptions,
              // filterBankLists: res.data.data,
              isFatchingATConfigs:false
            });
          } else {
            this.setState({
              errorMsg: res.data.msg,
              isFatchingATConfigs:false
            });
          }
        })
        .catch((err) => {
          console.log("bank list  error", err);
          this.setState({
            bankList: "",
            filterBankLists: "",
            isFatchingATConfigs:false
          });
        });
    };

    

       /**
   * Get getATEnvironments
   */
    getATEnvironments = () => {
          getATEnvironmentAPI()
            .then((res) => {
              if (res.data.status == 200) {

                const getEnvironmentOptions = res.data.data.map((item, index) => {
                  if(item.environment != null && item.environment != undefined) {
                    return {
                      key: item.id,
                      value: `${item.environment}`,
                      text: `${item.environment}`,
                    };
                  }
              
                });
              
                this.setState({
                  envirnments: getEnvironmentOptions,
                  // filterBankLists: res.data.data,
                  isFatchingATConfigs:false
                });
              } else {
                this.setState({
                  errorMsg: res.data.msg,
                  isFatchingATConfigs:false
                });
              }
            })
            .catch((err) => {
              console.log("bank list  error", err);
              this.setState({
                bankList: "",
                filterBankLists: "",
                isFatchingATConfigs:false
              });
            });
        };

        

  /**
   *  Get all ai_engine list
   */
  fetchATSettingsList = () => {
    getATSetttingsLists()
      .then((response) => {
      
        if (response.data.status == 200) {

          const getOptions = response.data.data.map((item, index) => {
            return {
              key: index,
              value: `${item.ip_address}`,
              text:`${item.ip_address}`,
            };
          });
          this.setState({
            at_settings_list: response.data.data,
            ip_addresses : getOptions,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;


        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  /**
   * Handle inputs
   * @param  e 
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleAddAIEngineValidation = () => {
    // Request validate
    let service_engine_errors = validateRequestATSettingParams(this.state);
    this.setState({ service_engine_errors });
    return service_engine_errors;
  };

  /**
   * Handle at settings
   */
  handleAddATSettings = () => {
    const errors = this.handleAddAIEngineValidation();

    if (Object.getOwnPropertyNames(errors).length === 0) {
      // Add new AT settings
      this.addATSetting();
    }
  };

  /**
   * Add ai engine settings
   */
  addATSetting = () => {
    this.setState({ isSaving: true });
    const payload = CreateReuqestPayloadForATSetting(this.state);
  
    addNewATSetttings(payload)
      .then((response) => { 
        console.log("response found at settings = ", response.data);
        let _this = this;
        if (response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            isSaving: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);

          this.fetchATSettingsList();
        } else {
          this.setState({
            isSaving: false,
            errorMsg: response.data.msg,
            successMsg: "",
          });


          setTimeout(function () {
            _this.setState({ errorMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);

          this.fetchATSettingsList();
        }
      })
      .catch((error) => {
        let _this = this;
        console.log("error found = ", error);
        console.log("error found = ", error.response);

        this.setState({
          isSaving: false,
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          successMsg: "",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };
  render() {
    const {
      protocol,
      ip_address,
      port_number,
      service_engine,
      service_engine_errors,
      isSaving,
      errorMsg,
      successMsg,
      at_settings_list,
      bankList,
      bank_id,
      isFatchingBanks,
      host,
      ip_addresses,
      isFatchingATConfigs,
      envirnments,
      addressess,
      ports,
      protocols,
      portsVal
    } = this.state;
    console.log("service_engine_errors addressess = ", addressess);
    console.log("service_engine_errors envirnments = ", envirnments);
    console.log("service_engine_errors ports = ", ports);
    console.log("service_engine_errors protocols = ", protocols);




    return (
      <Fragment>
        <CCard>
          <CCardHeader>
            <b style={{ fontSize: "20px" }}> Global Parameter List</b>
          </CCardHeader>

          <CCardBody>
            <CTabs>
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink>AT Settings</CNavLink>
                </CNavItem>
                {/* <CNavItem> 
                  <CNavLink>DTMF Profile</CNavLink>
                </CNavItem>
                <CNavItem></CNavItem> */}
              </CNav>
              <CTabContent>
                <CTabPane>
                  <AddATSettings
                    at_settings_list={at_settings_list}
                    errorMsg={errorMsg}
                    successMsg={successMsg}
                    protocol={protocol}
                    ip_address={ip_address}
                    port_number={port_number}
                    service_engine={service_engine}
                    isSaving={isSaving}
                    handleAddATSettings={this.handleAddATSettings}
                    service_engine_errors={service_engine_errors}
                    bankList={bankList}
                    bank_id={bank_id}
                    handleInputValues={this.handleInputValues}
                    isFatchingBanks={isFatchingBanks}
                    host={host}
                    ip_addresses={ip_addresses}
                    envirnments={envirnments}
                    addressess={addressess}
                    ports={ports}
                    protocols={protocols}
                    portsVal={portsVal}
                  />
                </CTabPane>
                {/* <CTabPane>
                  <AddPage />
                </CTabPane> */}
              </CTabContent>
            </CTabs>

            {/* <PageNotFound /> */}
          </CCardBody>
        </CCard>
      </Fragment>
    );
  }
}

export default AddNewAT;
