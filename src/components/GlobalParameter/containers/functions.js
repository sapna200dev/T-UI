import { AI_ENGINE_FORM_VALIDATIONS, DTMF_PROFILE_FORM_VALIDATIONS } from "../constants";

/**
 * Handle request
 * @param  props
 */
export const validateRequestAIEngineParams = (props) => {
  const addAIEngineErrors = {};
  const { protocol, ip_address, port_number, service_engine, bank_id } = props;

  if (protocol.length < 1) {
    addAIEngineErrors.protocol = AI_ENGINE_FORM_VALIDATIONS.PROTOCOL;
  } else if (ip_address.length < 1) {
    addAIEngineErrors.ip_address = AI_ENGINE_FORM_VALIDATIONS.IP_ADDRESS;
  } else if (port_number.length < 1) {
    addAIEngineErrors.port_number = AI_ENGINE_FORM_VALIDATIONS.PORT_NUMBER;
  } else if (service_engine.length < 1) {
    addAIEngineErrors.service_engine =
      AI_ENGINE_FORM_VALIDATIONS.SERVICE_ENGINE;
  } else if (bank_id.length < 1) {
    addAIEngineErrors.bank_id =
      AI_ENGINE_FORM_VALIDATIONS.BANK_ID;
  }

  return addAIEngineErrors;
};


/**
 * Handle request
 * @param  props
 */
 export const validateRequestATSettingParams = (props) => {
  const addAIEngineErrors = {};
  const { protocol, ip_address,bank_id, at_environment, ports } = props;

  if (protocol.length < 1) {
    addAIEngineErrors.protocol = AI_ENGINE_FORM_VALIDATIONS.PROTOCOL;
  }  else if (ip_address.length < 1) {
    addAIEngineErrors.ip_address = AI_ENGINE_FORM_VALIDATIONS.IP_ADDRESS;
  } else if (at_environment.length < 1) {
    addAIEngineErrors.at_environment =
      AI_ENGINE_FORM_VALIDATIONS.BANK_ID;
  } else if (bank_id.length < 1) {
    addAIEngineErrors.bank_id =
      AI_ENGINE_FORM_VALIDATIONS.BANK_ID;
  }

  return addAIEngineErrors;
};




/**
 * Handle DTMF Profile request validation
 * @param  props
 */
 export const validateRequestDTMFProfileParams = (props) => {
  const addDTMFProfileErrors = {};
  const { dtmf, is_dtmf, valid_code, invalid_code, bank_id } = props;

  if (dtmf.length < 1) {
    addDTMFProfileErrors.dtmf = DTMF_PROFILE_FORM_VALIDATIONS.DTMF;
  } else if (is_dtmf.length < 1) {
    addDTMFProfileErrors.is_dtmf = DTMF_PROFILE_FORM_VALIDATIONS.IS_DTMF;
  } else if (valid_code.length < 1) {
    addDTMFProfileErrors.valid_code = DTMF_PROFILE_FORM_VALIDATIONS.VALID_CODE;
  } else if (invalid_code.length < 1) {
    addDTMFProfileErrors.invalid_code =
      DTMF_PROFILE_FORM_VALIDATIONS.INVALID_CODE;
  } else if (bank_id.length < 1) {
    addDTMFProfileErrors.bank_id =
      DTMF_PROFILE_FORM_VALIDATIONS.BANK_ID;
  }

  return addDTMFProfileErrors;
};


/**
 * Create request payload
 * @param props
 */
export const CreateReuqestPayloadForAIEngine = (props) => {
  const { protocol, ip_address, port_number, service_engine, bank_id } = props;

  const payload = {
    protocol:protocol,
    ip_address:ip_address,
    port_number:port_number,
    service_engine:service_engine,
    bank_id:bank_id
  };

  return payload;
};


/**
 * Create request payload for AT settings
 * @param props
 */
 export const CreateReuqestPayloadForATSetting = (props) => {
  const { protocol, ip_address, bank_id, at_environment, portsVal } = props;

  const payload = {
    host:protocol,
    ip_address:ip_address,
    bank_id:bank_id,
    environment:at_environment,
    port:portsVal
  };

  return payload;
};



/**
 * Create request payload for DTMF Profile
 * @param props
 */
 export const CreateReuqestPayloadForDTMFProfile = (props) => {
  const { dtmf, is_dtmf, valid_code, invalid_code, bank_id } = props;

  const payload = {
    dtmf_status:dtmf,
    is_dtmf:is_dtmf,
    valid_code:valid_code,
    invalid_code:invalid_code,
    bank_id:bank_id
  };

  return payload;
};