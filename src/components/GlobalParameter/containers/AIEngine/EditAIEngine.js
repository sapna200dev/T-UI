import React, { Component, Fragment } from "react";
// import { GlobalParameter } from "../components/index";
import {
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
} from "@coreui/react";

import {
  validateRequestAIEngineParams,
  CreateReuqestPayloadForAIEngine,
} from "../functions";
import { updateAIEngineSettings1, getAIEngineById } from "../../services/index";
import { UpdateAIEngine } from "../../components/AIEngine";
import { bankLists } from "../../../Bank/services";

class EditAIEngine extends Component {
  state = {
    protocol: "",
    ip_address: "",
    port_number: "",
    service_engine: "",
    isSaving: false,
    service_engine_errors: {},
    errorMsg: "",
    successMsg: "",
    ai_engine_list: [],
    bankList: [],
    bank_id: "",
    id:''
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ isFatching:true, id })

    this.getAIEngineSettingById(id);
    this.getAllBanks();
  }

  /**
   * Get all banks
   */
  getAllBanks = () => {
    bankLists()
      .then((res) => {
        console.log("bank list  res", res);
        if (res.data.status == 200) {
          const getOptions = res.data.data.map((item, index) => {
            return {
              key: index,
              value: `${item.bank_name}`,
              text: item.id,
            };
          });

          this.setState({
            bankList: getOptions,
            filterBankLists: res.data.data,
            loading: false,
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
          });
        }
      })
      .catch((err) => {
        console.log("bank list  error", err);
        this.setState({
          bankList: "",
          filterBankLists: "",
          loading: false,
        });
      });
  };

  /**
   *  Get all ai_engine list
   */
  getAIEngineSettingById = (id) => {
    getAIEngineById(id)
      .then((response) => {
        console.log("response dddd=> ", response);
        if (response.data.status == 200) {
            let bank = response.data.data;

          this.setState({
            protocol: bank.protocol,
            ip_address: bank.ip_address,
            port_number: bank.port_number,
            service_engine: bank.service_engine,
            bank_id : bank.bankId,
            ai_engine_list: response.data.data,
            isFatching:false
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
            isFatching:false
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
            isFatching:false,
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  handleInputValues = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleUpdateAIEngineValidation = () => {
    // Request validate
    let service_engine_errors = validateRequestAIEngineParams(this.state);
    this.setState({ service_engine_errors });
    return service_engine_errors;
  };

  /**
   * Handle update ai settings
   */
  handleUpdateAIEngine = () => {
    const errors = this.handleUpdateAIEngineValidation();

    if (Object.getOwnPropertyNames(errors).length === 0) {
      console.log(" all done u can add");
      this.UpdateEngineSetting();
    }
  };

  /**
   * Update ai engine settings
   */
  UpdateEngineSetting = () => {
      const {id} = this.state;
    this.setState({ isSaving: true })

    // Create request ai engine settign payload
    const payload = CreateReuqestPayloadForAIEngine(this.state);

    // Call update ai setting api
    updateAIEngineSettings1(payload, id)
      .then((response) => {
        console.log("response found = ", response);
        let _this = this;
        if (response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            isSaving: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } else {
          this.setState({
            isSaving: false,
            errorMsg: response.data.msg,
            successMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } // End if
      })
      .catch((error) => {
        let _this = this;
        this.setState({
          isSaving: false,
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          successMsg: "",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };
  render() {
    const {
      protocol,
      ip_address,
      port_number,
      service_engine,
      service_engine_errors,
      isSaving,
      errorMsg,
      successMsg,
      ai_engine_list,
      bankList,
      bank_id,
      isFatching
    } = this.state;

    return (
      <Fragment>
        <CCard>
          <CCardHeader>
            <b style={{ fontSize: "20px" }}> Global Parameter List</b>
          </CCardHeader>

          <CCardBody> 
            <CTabs>
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink>AI Engine</CNavLink>
                </CNavItem>
                {/* <CNavItem>
                  <CNavLink>DTMF Profile</CNavLink>
                </CNavItem> */}
                <CNavItem>{/* <CNavLink>Messages</CNavLink> */}</CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane>
                  <UpdateAIEngine
                    ai_engine_list={ai_engine_list}
                    errorMsg={errorMsg}
                    successMsg={successMsg}
                    protocol={protocol}
                    ip_address={ip_address}
                    port_number={port_number}
                    service_engine={service_engine}
                    isSaving={isSaving}
                    handleUpdateAIEngine={this.handleUpdateAIEngine}
                    service_engine_errors={service_engine_errors}
                    bankList={bankList}
                    bank_id={bank_id}
                    handleInputValues={this.handleInputValues}
                    isFatching={isFatching}
                  />
                </CTabPane>
                {/* <CTabPane>Other global params Edit</CTabPane> */}
              </CTabContent>
            </CTabs>

            {/* <PageNotFound /> */}
          </CCardBody>
        </CCard>
      </Fragment>
    );
  }
}

export default EditAIEngine;

