
import AIEngineList from './AIEngineList'
import  AddNewAIEngine from './AddNewAIEngine'
import EditAIEngine from './EditAIEngine'
import AIEngineDetails from './AIEngineDetails'
export {
    AIEngineList,
    AddNewAIEngine,
    EditAIEngine,
    AIEngineDetails
}
