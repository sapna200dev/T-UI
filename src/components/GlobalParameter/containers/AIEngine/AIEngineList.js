import React, { Component, Fragment } from "react";
// import { GlobalParameter } from "../components/index";
import {
  validateRequestAIEngineParams,
  CreateReuqestPayloadForAIEngine,
} from "../functions";
import { addAIEngineSettings, getAIEngineList } from "../../services/index";
import {AIEngineLists} from '../../components/AIEngine/index'

class AIEngineList extends Component {
  state = {
    protocol: "",
    ip_address: "",
    port_number: "",
    service_engine: "",
    isSaving: false,
    service_engine_errors: {},
    errorMsg: "",
    successMsg: "",
    ai_engine_list: [],
  };

  componentDidMount() {
    this.fetchAIEngineList();
  }

  fetchAIEngineList = () => {
    getAIEngineList()
      .then((response) => {
        console.log("response => ", response);
        if (response.data.status == 200) {
          this.setState({
            ai_engine_list: response.data.data,
          });
        } else {
          this.setState({
            errorMsg: response.data.msg,
          });
        }
      })
      .catch((error) => {
        console.log("error => ", error.response);
        let _this = this;
        console.log("error found = ", error);
        console.log("error found = ", error.response);

        this.setState({
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  handleInputValues = (e) => {
    const { name, value } = e.target;
    console.log("change name = ", name);
    console.log("change value = ", value);
    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleAddAIEngineValidation = () => {
    // Request validate
    let service_engine_errors = validateRequestAIEngineParams(this.state);
    this.setState({ service_engine_errors });
    return service_engine_errors;
  };

  handleAddAIEngine = () => {
    console.log("handleAddEndUser = ");
    const errors = this.handleAddAIEngineValidation();
    console.log("handleAddEndUser errors = ", errors);
    if (Object.getOwnPropertyNames(errors).length === 0) {
      console.log(" all done u can add");
      this.UpdateEngineSetting();
    }
  };

  UpdateEngineSetting = () => {
    this.setState({ isSaving: true });
    const payload = CreateReuqestPayloadForAIEngine(this.state);
    console.log("payload  = ", payload);
    addAIEngineSettings(payload)
      .then((response) => {
        console.log("response found = ", response);
        let _this = this;
        if (response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            isSaving: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        } else {
          this.setState({
            isSaving: false,
            errorMsg: response.data.msg,
            successMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
            // _this.props.history.push("/end-user");
          }, 4000);
        }
      })
      .catch((error) => {
        let _this = this;
        console.log("error found = ", error);
        console.log("error found = ", error.response);

        this.setState({
          isSaving: false,
          errorMsg: error.response
            ? error.response.data.msg
            : "Somthing went wrong !",
          successMsg: "",
        });

        setTimeout(function () {
          _this.setState({ errorMsg: "" });
          // _this.props.history.push("/end-user");
        }, 4000);
      });
  };

  render() {
    const {
      protocol,
      ip_address,
      port_number,
      service_engine,
      service_engine_errors,
      isSaving,
      errorMsg,
      successMsg,
      ai_engine_list
    } = this.state;
    console.log("service_engine_errors errors = ", service_engine_errors);

    return (
      <Fragment>
        <AIEngineLists
          ai_engine_list={ai_engine_list}
          errorMsg={errorMsg}
          successMsg={successMsg}
        />
      </Fragment>
    );
  }
}

export default AIEngineList;
