

export const AI_ENGINE_FORM_VALIDATIONS = {
    PROTOCOL: "This field is mandatory",
    IP_ADDRESS:'This field is mandatory',
    PORT_NUMBER: "This field is mandatory",
    SERVICE_ENGINE: "This field is mandatory",
    BANK_ID: "This field is mandatory",
    PORTS:"This field is mandatory",
  };
  

  export const DTMF_PROFILE_FORM_VALIDATIONS = {
    DTMF: "This field is mandatory",
    IS_DTMF:'This field is mandatory',
    VALID_CODE: "This field is mandatory",
    INVALID_CODE: "This field is mandatory",
    BANK_ID: "This field is mandatory",
  };
  
  
export const ENGINE_PROTOCOL = [
  { key: 0, text: "http", value: "HTTP" },
  { key: 1, text: "https", value: "HTTPS" },
];
 
export const ENGINE_IP_ADDRESS = [
  { key: 0, text: "146.71.79.71", value: "146.71.79.71" },
  { key: 1, text: "75.25.130.10", value: "75.25.130.10" },
  { key: 2, text: "164.52.220.28", value: "164.52.220.28" },
  { key: 3, text: "73.231.203.214", value: "73.231.203.214" },
  { key: 4, text: "13.126.202.81", value: "13.126.202.81" },
  { key: 5, text: "75.18.249.168", value: "75.18.249.168" }, 
  { key: 6, text: "34.83.103.250", value: "34.83.103.250" }, 
  { key: 7, text: "34.93.136.121", value: "34.93.136.121" }, 
];

export const ENGINE_PORT_NUMBER = [{ key: 0, text: "4600", value: "4600" }];

export const ENGINE_SERVICE = [
  { key: 0, text: "engine_service2", value: "Service Engine 2" },
  { key: 1, text: "engine_service3", value: "Service Engine 3" },
  { key: 3, text: "AI-GCP", value: "AI-GCP" },
];

// $2y$10$ndrilonB3eQ0VMnO4eH5EeTSXXT9msVV/SEm3rSgH8pQDT6t6ZTZe -> new FOR turant.worl.co 
// $2y$10$Pc56jXMKzwl1.9EA7pt4oe2J3mtzE4Pyd0iT9pBSOP1Ai2fcKi4gG -> old FOR turant.worl.co TurantSA@123
export const FIELDS = [ 
  "protocol",
  "ip_address",
  "port_number",
  "service_engine",
  "bank_name",
  "Action",
];


/**************************************** DTMF Profile Section ***************************************/
export const DTMF_FIELDS = [
  "dtmf_status",
  "dtmf",
  "valid_code",
  "invalid_code",
  "bank_name",
  "Action",
];

export const IS_DTMF_VALUES = [
  { key: 0, text: 0, value: 'False' },
  { key: 1, text: 1, value: 'True' },
];

export const DTMF_STATUSES = [
  { key: 0, text: "dtmf", value: "DTMF" },
  { key: 1, text: "non_dtmf", value: "NON DTMF" },
]; 


/**************************************** AT Setting Section ***************************************/

export const AT_IP_ADDRESS = [
  { key: 0, text: "164.52.209.133", value: "164.52.209.133" },
  { key: 1, text: "65.0.235.53", value: "65.0.235.53" },
  { key: 2, text: "75.18.249.168", value: "75.18.249.168" },
  { key: 3, text: "13.126.202.81", value: "13.126.202.81" },
];

export const AT_ENVIORNMENT = [
  { key: 0, text: "AT-Prod", value: "AT-Prod" },
  { key: 1, text: "AT-Dev", value: "AT-Dev" },
  { key: 2, text: "Python-Ivr", value: "Python-Ivr" },
  { key: 3, text: "Python-Ivr-2", value: "Python-Ivr-2" },

];


export const AT_FIELDS = [
  "protocol",
  "ip_address",
  'environment',
  'port',
  "bank_name",

  "Action",
];




