

export {
  /** AI Engine calls */
  addAIEngineSettings,
  getAIEngineList,
  getAIEngineById,
  updateAIEngineSettings1,
  deleteAIEngine,

  /** DTMF Profile calls */
  getDTMFProfileREcord,
  getDTMFProfileLists,
  createDTMFProfileLists,
  updateDTMFProfile,
  deleteDTMFProfile,

  /** Astrisk api calls */
  getATSetttingsLists,
  addNewATSetttings,
  getATSetttingsListById,
  updateATSetttings,
  deleteATSetttings
  } from "./common";
  