import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'

/**
 * Add new ai_engine 
 * @param data 
 */
export const addAIEngineSettings = (data) =>
axios.post(`${URL_HOST}/api/ai_engine_setting/add`, data);

/**
 * UPdate ai_engine 
 * @param data 
 */
export const updateAIEngineSettings1 = (data, id) =>
axios.post(`${URL_HOST}/api/ai_engine_setting/update/${id}`, data);


/**
 * get ai engine
 * @param data 
 */
export const getAIEngineList = () =>
axios.get(`${URL_HOST}/api/ai_engine_setting/list`);


/**
 * get ai engine
 * @param data 
 */
export const getAIEngineById = (id) =>
axios.get(`${URL_HOST}/api/ai_engine_setting/id/${id}`);

/**
 * delete ai engine
 * @param data 
 */
export const deleteAIEngine = (bankId, id) =>
axios.get(`${URL_HOST}/api/ai_engine_setting/delete/${bankId}/${id}`);



/******************** DTMF Profile API's ******************************/

/**
 * get ai engine
 * @param data 
 */
 export const getDTMFProfileREcord = (id) =>
    axios.get(`${URL_HOST}/api/dtmf_profile/id/${id}`);

/**
 * get all dtmf profiles api
 * @param data 
 */
 export const getDTMFProfileLists = () =>
    axios.get(`${URL_HOST}/api/dtmf_profile/list`);

/**
 * add dtmf profile api
 * @param data 
 */
 export const createDTMFProfileLists = (data) =>
    axios.post(`${URL_HOST}/api/dtmf_profile/add`, data);

/**
 * Update dtmf profile
 * @param data, id
 */
 export const updateDTMFProfile = (id, data) =>
    axios.post(`${URL_HOST}/api/dtmf_profile/update/${id}`, data);


 /**
 * delete DTMF profile
 * @param data 
 */
export const deleteDTMFProfile = (bankId, id) =>
    axios.get(`${URL_HOST}/api/dtmf_profile/delete/${bankId}/${id}`);


 /**
 * Fetch AT settings lists
 * @param data 
 */
  export const getATSetttingsLists = () =>
   axios.get(`${URL_HOST}/api/at_settings/list`);


 /**
 * Add new AT settings
 * @param data 
 */
export const addNewATSetttings = (payload) =>
    axios.post(`${URL_HOST}/api/at_settings/add`, payload);

/**
 * Fetch AT setting by id
 * @param data 
 */
export const getATSetttingsListById = (id) =>
  axios.get(`${URL_HOST}/api/at_settings/id/`+id);

/**
 * Update AT setting
 * @param data 
 */
 export const updateATSetttings = (id, data) =>
 axios.post(`${URL_HOST}/api/at_settings/update/`+id, data);


/**
 * Delete AT setting
 * @param bank_id, id 
 */
 export const deleteATSetttings = (bank_id, id) =>
 axios.get(`${URL_HOST}/api/at_settings/delete/`+bank_id+'/'+id);

  
    
    