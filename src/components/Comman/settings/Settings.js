import React, { Component, Fragment } from 'react'
import { CirclePicker } from 'react-color'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CListGroup,
  CListGroupItem,
  CRow,
  CTabContent,
  CTabPane,
  CCardFooter,
  CButton
} from '@coreui/react'
import { PageNotFound } from '../components'


export class Settings extends Component {
  state = {
    activeTab: 0
  }

  /**
   * Set active tab
   * @param  index 
   */
  setActiveTab = (index) => {
    this.setState({
      activeTab: index
    })
  }

  /**
   * Handle change layout
   * @param  color 
   * @param  event 
   */
  handleChange = (color, event) => {
    console.log('handleChange')
    console.log(color.hex)

    // Set in local storage
    localStorage.setItem("layout_color", color.hex);
    window.location.reload();
  }



  render() {
    const { activeTab } = this.state;
    const { isFromProfile } = this.props;

    return (
      <Fragment>
        <CCard>
          <CCardHeader>

            <b style={{ fontSize: "20px" }}>System Settings</b>
          </CCardHeader>
          <CCardBody>
            {!isFromProfile ? <PageNotFound /> :
              <CRow>
                <CCol xs="4">
                  <CListGroup id="list-tab" role="tablist">
                    <CListGroupItem onClick={() => this.setActiveTab(0)} action active={activeTab === 0} >Layout Color</CListGroupItem>
                    <CListGroupItem onClick={() => this.setActiveTab(1)} action active={activeTab === 1} >Profile</CListGroupItem>
                    <CListGroupItem onClick={() => this.setActiveTab(2)} action active={activeTab === 2} >Messages</CListGroupItem>
                    <CListGroupItem onClick={() => this.setActiveTab(3)} action active={activeTab === 3} >Settings</CListGroupItem>
                  </CListGroup>
                </CCol>
                <CCol xs="8">
                  <CTabContent>
                    <CTabPane active={activeTab === 0} >
                      <Fragment>
                        <CCard color="gradient-secondary">
                          <CCardHeader>
                            Choose Color
                            </CCardHeader>
                          <CCardBody> 
                            <CirclePicker  onChange={this.handleChange} />
                            <span style={{ float: 'right' }}>
                              {/* <CButton
                                color="primary"
                                className="px-4 login_bt"
                              >
                                Save
                                </CButton> */}
                            </span>
                          </CCardBody>
                        </CCard>
                      </Fragment>
                    </CTabPane>
                    <CTabPane active={activeTab === 1}>
                      <p>Cupidatat quis ad sint excepteur laborum in esse qui. Et excepteur consectetur ex nisi eu do cillum ad laborum. Mollit et eu officia
                      dolore sunt Lorem culpa qui commodo velit ex amet id ex. Officia anim incididunt laboris deserunt
                      anim aute dolor incididunt veniam aute dolore do exercitation. Dolor nisi culpa ex ad irure in elit eu dolore. Ad laboris ipsum
                        reprehenderit irure non commodo enim culpa commodo veniam incididunt veniam ad.</p>
                    </CTabPane>
                    <CTabPane active={activeTab === 2}>
                      <p>Ut ut do pariatur aliquip aliqua aliquip exercitation do nostrud commodo reprehenderit aute ipsum voluptate. Irure Lorem et laboris
                      nostrud amet cupidatat cupidatat anim do ut velit mollit consequat enim tempor. Consectetur
                      est minim nostrud nostrud consectetur irure labore voluptate irure. Ipsum id Lorem sit sint voluptate est pariatur eu ad cupidatat et
                      deserunt culpa sit eiusmod deserunt. Consectetur et fugiat anim do eiusmod aliquip nulla
                        laborum elit adipisicing pariatur cillum.</p>
                    </CTabPane>
                    <CTabPane active={activeTab === 3}>
                      <p>Irure enim occaecat labore sit qui aliquip reprehenderit amet velit. Deserunt ullamco ex elit nostrud ut dolore nisi officia magna
                      sit occaecat laboris sunt dolor. Nisi eu minim cillum occaecat aute est cupidatat aliqua labore
                      aute occaecat ea aliquip sunt amet. Aute mollit dolor ut exercitation irure commodo non amet consectetur quis amet culpa. Quis ullamco
                      nisi amet qui aute irure eu. Magna labore dolor quis ex labore id nostrud deserunt dolor
                        eiusmod eu pariatur culpa mollit in irure.</p>
                    </CTabPane>
                  </CTabContent>
                </CCol>
              </CRow>
            }


          </CCardBody>
        </CCard>
      </Fragment>
    )
  }
}