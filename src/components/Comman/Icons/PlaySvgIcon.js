import React, { Fragment } from "react";

export const PlaySvgIcon = (props) => {
  const { width, height, handleAudioPlay, src,type,value ,index,dyna, audio} = props;

  return (
    <Fragment>
        <span onClick={() => handleAudioPlay(false, src,type,value,index,dyna, audio)} style={{ cursor: 'pointer'}}>
    <svg
      style={{width:'19px', marginTop:'-7px'}}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        class="c-icon c-icon-2xl"
        role="img"
      >
        <path
          fill="var(--ci-primary-color, currentColor)"
          d="M444.4,235.236,132.275,49.449A24,24,0,0,0,96,70.072V434.214a24.017,24.017,0,0,0,35.907,20.839L444.03,276.7a24,24,0,0,0,.367-41.461ZM128,420.429V84.144L416.244,255.718Z"
          class="ci-primary"
        ></path>
      </svg>
    </span>
    </Fragment>
  );
};
