export { BankSvgIcon } from "./BankSvgIcon";
export { CustomerSvgIcon } from "./CustomerSvgIcon";
export { DashboardSvgIcon } from "./DashboardSvgIcon";
export { AddBranchSvgIcon } from "./AddBranchSvgIcon";
export {UploadDocSvgIcon} from './UploadDocSvgIcon'
export {DownloadSampleDoc} from './DownloadSampleDoc'
export {GlobalSvgIcon} from './GlobalSvgIcon'
export {ManageBranchSvgIcon} from './ManageBranchSvgIcon'
export {PlaySvgIcon} from './PlaySvgIcon'
export {StopSvgIcon} from './StopSvgIcon'

export {ShowMoreSvgIcon} from './ShowMoreSvgIcon'