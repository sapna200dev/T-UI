import React, { Fragment } from "react";

const svg_css = {
  width: '22px',
  height: '31px',
  /* font-size: 2rem; */
  marginLeft: '6px',
  cursor: 'pointer'
}

export const AddBranchSvgIcon = () => {
  return (
    <Fragment>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        className="c-icon c-icon-2xl"
        role="img"
        style={svg_css}
      >
        <polygon
          fill="var(--ci-primary-color, currentColor)"
          points="440 240 272 240 272 72 240 72 240 240 72 240 72 272 240 272 240 440 272 440 272 272 440 272 440 240"
          className="ci-primary"
        ></polygon>
      </svg>
    </Fragment>
  );
};
