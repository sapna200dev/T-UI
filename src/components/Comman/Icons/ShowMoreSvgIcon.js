import React, { Fragment } from "react";

export const ShowMoreSvgIcon = (props) => {


  return (
    <Fragment>
      <span
     
        style={{ cursor: "pointer",  }}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 512 512"
          className="c-icon c-icon-2xl"
          role="img"
          style={{ cursor: "pointer",  height: '21px', width:'16px', marginLeft:'10px' }}
        >
          <rect
            width="352"
            height="32"
            x="80"
            y="96"
            fill="var(--ci-primary-color, currentColor)"
            class="ci-primary"
          ></rect>
          <rect
            width="352"
            height="32"
            x="80"
            y="240"
            fill="var(--ci-primary-color, currentColor)"
            className="ci-primary"
          ></rect>
          <rect
            width="352"
            height="32"
            x="80"
            y="384"
            fill="var(--ci-primary-color, currentColor)"
            className="ci-primary"
          ></rect>
        </svg>
      </span>
    </Fragment>
  );
};
