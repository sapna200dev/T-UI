import React, { Fragment } from "react";

export const StopSvgIcon = (props) => {
  const { width, height, handleAudioPlay, src,type,value ,index,dyna, audio} = props;

  console.log('va;lue = ', value)
  return (
    <Fragment>
 
        <span onClick={() => handleAudioPlay(false, src,type,value,index,dyna, audio)} style={{ cursor: 'pointer'}}>
     <svg
        style={{ width: "19px", marginTop: "-7px" }}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        class="c-icon c-icon-2xl"
        role="img"
      >
        <path
          fill="var(--ci-primary-color, currentColor)"
          d="M200,48H72A24.028,24.028,0,0,0,48,72V440a24.028,24.028,0,0,0,24,24H200a24.028,24.028,0,0,0,24-24V72A24.028,24.028,0,0,0,200,48Zm-8,384H80V80H192Z"
          class="ci-primary"
        ></path>
        <path
          fill="var(--ci-primary-color, currentColor)"
          d="M440,48H312a24.028,24.028,0,0,0-24,24V440a24.028,24.028,0,0,0,24,24H440a24.028,24.028,0,0,0,24-24V72A24.028,24.028,0,0,0,440,48Zm-8,384H320V80H432Z"
          class="ci-primary"
        ></path>
      </svg>
     </span>
   
    </Fragment>
  );
};
