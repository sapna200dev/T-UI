import React, { Fragment } from "react";

export const DownloadSampleDoc = (props) => {
  const {width, height} = props;

  return (
    <Fragment>
  <svg
                  style={{ width: "1rem", height: "1rem" }}
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                  className="c-icon c-icon-2xl"
                  role="img"
                >
                  <polygon
                    fill="var(--ci-primary-color, currentColor)"
                    points="272 434.744 272 209.176 240 209.176 240 434.744 188.118 382.862 165.49 405.489 256 496 346.51 405.489 323.882 382.862 272 434.744"
                    className="ci-primary"
                  ></polygon>
                  <path
                    fill="var(--ci-primary-color, currentColor)"
                    d="M400,161.176c0-79.4-64.6-144-144-144s-144,64.6-144,144a96,96,0,0,0,0,192h80v-32H112a64,64,0,0,1,0-128h32v-32a112,112,0,0,1,224,0v32h32a64,64,0,0,1,0,128H320v32h80a96,96,0,0,0,0-192Z"
                    className="ci-primary"
                  ></path>
                </svg>
    </Fragment>
  );
};
