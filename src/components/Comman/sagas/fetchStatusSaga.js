import React, { Component, Fragment } from 'react';
import { take, put, call, all } from 'redux-saga/effects';
import * as actions from '../actions';
import axios, { setAuthHeader } from '../../Comman/axiosConfig';
// import { SHOW_TOAST } from '../../Common/actions';
// import { CONST_LANG } from '../../Common/constant_language';
import { URL_HOST } from '../../Comman/constants'




export const fetchAllStatus = async () => {
    try {
      const response = await axios.get(`${URL_HOST}/api/status/all`);

    //   setAuthHeader(response.data.access_token);
      // setCookie('ytel_token', response.data.access_token, 7);
      return response.data;
    } catch (err) {
      return err.response.data;
    }
  };
  

  export function* fetchDefaultStatus() {
    try {
      const response = yield fetchAllStatus();
        console.log('role response = ', response)
      if (response && response) {

        yield all([
          put({
            type: actions.FETCH_STATUS_SUCCESS, 
            response : response.data,
          })
       
        ]);
      } else {
  
        yield all([
          put({
            type: actions.FETCH_STATUS_FAILURE,
            response,

          }),
          put({
            type: 'ERROR',
            msgType: 'error',
            header: (response && response.msg) ? response.msg : 'CONST_LANG.LOGIN_ERROR_CONTENT', //CONST_LANG.ERROR_HEADER,
            content: 'errorMessageContent',
          }),
        ]);
      }
    } catch (err) {
      yield all([
        put({ type: actions.FETCH_STATUS_FAILURE, err }),
        put({
          type: 'ERROR',
          msgType: 'error',
          header: 'Error',
          content: 'CONST_LANG.LOGIN_FAIL_MSG',
        }),
      ]);
    }
  }



  export function* watchFetchStatus() {
    while (true) {
      const state = yield take(actions.FETCH_STATUS);
      yield call(fetchDefaultStatus);
    }
  }
  