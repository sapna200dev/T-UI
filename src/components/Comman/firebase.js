import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

 const firebaseConfig = {
    apiKey: "AIzaSyClzpmoF5uSAzNFRzg4-_GQplp13KZDqIM",
    authDomain: "ytel-a2eb8.firebaseapp.com",
    databaseURL: "https://ytel-a2eb8.firebaseio.com",
    projectId: "ytel-a2eb8",
    storageBucket: "ytel-a2eb8.appspot.com",
    messagingSenderId: "280499229280",
    appId: "1:280499229280:web:39c8ac2fbfb284ba296905",
    measurementId: "G-JZBY4MTSMB"
  };


firebase.initializeApp(firebaseConfig);


export default firebase;
 