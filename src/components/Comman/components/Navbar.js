import React, { Component, Fragment } from "react";
import "./navbarStyle.css";

import {
  CAlert,
} from "@coreui/react";

import {
  BankSvgIcon,
  CustomerSvgIcon,
  DashboardSvgIcon,
  UploadDocSvgIcon,
  GlobalSvgIcon,
  ManageBranchSvgIcon,
} from "../../Comman/Icons";
import CIcon from "@coreui/icons-react";
import { getUserObj, redirectToLoginPage, getLoggedInUser } from "../../../components/Comman/functions";
export class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "",
      routeMatch: "",
      id: "",
      showDrop : '',
      userObj:{},
      userRegistration:false
    };
  }

  componentDidMount() {
    const { id } = this.state;
    let user='';
    const login_user = getLoggedInUser("user");
    if (login_user) {
      user = JSON.parse(login_user)
      this.setState({
        userObj : JSON.parse(login_user)
      })
      console.log('user registration ', user)
      if(user.isUserRegister != null && user.isUserRegister == 1 ) {
        this.setState({
          userRegistration:true
        })
      
      }
    }
    console.log("route = ", id);
    console.log("route  id = ", this.props);
    console.log(window.location);
    this.setState({ routeMatch: window.location.hash });
    // const { id } = this.props.match.params;

    const user_role = getUserObj("role");
    if (user_role) {
      this.setState({
        role: JSON.parse(user_role),
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { routeMatch } = this.state;
    console.log("route  window.location.hash = ", window.location.hash);
    if (prevProps.routeMatch != routeMatch) {
      console.log("route  window.location.hash = ", window.location.hash);
    }
  }

  handleDropDwon = () => {
    const {showDrop} = this.state
      if(showDrop!='') {
        this.setState({
          showDrop : ''
        })
      } else {
        this.setState({
          showDrop : 'c-show'
        })
      }
  
    console.log('handleDropDwon')
  }

  render() {
    const { role, routeMatch, showDrop, userRegistration } = this.state;
    const super_admin = ["Super Admin"];
    const admin = ["Admin"];
    const customer = ["Super Admin", "Customer"]; 
    const employees = ["Sales Agent", "Team Lead"]; 

    let pathname = window.location.hash; 

    var employee = pathname.substring(0, 10);
    const onEmployee = /\/employee/.test(employee);
    console.log('showDrop = ', showDrop)
    return (
      <ul
        className="c-sidebar-nav h-100 ps ps--active-y "
      
        // style={{ position: "relative", overflowY: 'auto !important', height:'50px !important' }}
      >
        <li className="c-sidebar-nav-item" >
          <a
            aria-current="page"
            className={`c-sidebar-nav-link   ${
              routeMatch == "#/dashboard" ? "c-active" : ""
            }`}
            href="#/dashboard"
            // style={{color: 'black'}}
          >
            <DashboardSvgIcon />
            Dashboard
            {/* <span className="badge badge-info">NEW</span> */}
          </a>
        </li>
        <li className="c-sidebar-nav-title">Details</li>
        {!super_admin.includes(role.display_name) ? (
          <Fragment>
            {admin.includes(role.display_name) ? (
              <Fragment>
                
             <span className="c-side-hover ">

              <div className="container">
              <li className={`c-sidebar-nav-item  `} style={userRegistration ? {pointerEvents: 'none'}:{}} data-hover="Hello, this is the tooltip">
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/employee" ? "c-active" : ""
                    }`}
                    href="#/employee"
                  >
                    <CustomerSvgIcon />
                    Manage Employee
                  </a>
                </li>
                <li className={`c-sidebar-nav-item  `} style={userRegistration ? {pointerEvents: 'none'}:{}}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/end-user"
                  >
                    <CustomerSvgIcon />
                    Manage End User
                  </a>
                </li>
                <li className={`c-sidebar-nav-item  `} style={userRegistration ? {pointerEvents: 'none'}:{}}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/upload-file"
                  >
                    <UploadDocSvgIcon />
                    Scheduler
                  </a>
                </li>

                <li className={`c-sidebar-nav-item  `} style={userRegistration ? {pointerEvents: 'none'}:{}}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/manage-branches"
                  >
                    <ManageBranchSvgIcon />
                    <span>Manage Branches</span>
                  </a>
                </li>
                <li className="c-sidebar-nav-title">Others</li>


                <li className={`c-sidebar-nav-item  `} style={userRegistration ? {pointerEvents: 'none'}:{}}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/manage-settings"
                  >
                    <CIcon
                      name="cil-settings"
                      className="mfe-2 c-sidebar-nav-icon"
                      style={{ width: "1.4rem", height: "1.5rem" }}
                    />
                    <span>Settings</span>
                  </a>
                </li>
                {/* <li className={`c-sidebar-nav-item  `}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/ivr-settings"
                  >
                    <CIcon
                      name="cil-settings"
                      className="mfe-2 c-sidebar-nav-icon"
                      style={{ width: "1.4rem", height: "1.5rem" }}
                    />
                    <span>IVR Settings</span>
                  </a>
                </li>
                <li className={`c-sidebar-nav-item  `}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/ivr-verification-settings"
                  >
                    <CIcon
                      name="cil-settings"
                      className="mfe-2 c-sidebar-nav-icon"
                      style={{ width: "1.4rem", height: "1.5rem" }}
                    />
                    <span>IVR Verification Rules</span>
                  </a>
                </li> */}
               
             
                <li className={`c-sidebar-nav-dropdown ${showDrop}`} onClick={this.handleDropDwon} style={userRegistration ? {pointerEvents: 'none'}:{}}>
                  <a className="c-sidebar-nav-dropdown-toggle">
                          <CIcon
                            name="cil-settings"
                            className="mfe-2 c-sidebar-nav-icon"
                            style={{ width: "1.4rem", height: "1.5rem" }}
                          />
                    IVR Settings
                  </a>
                  <ul className="c-sidebar-nav-dropdown-items">
                    <li className="c-sidebar-nav-item">
                      <a className="c-sidebar-nav-link"  href="#/ivr-settings">
                         
                          <span>IVR Registration</span>
                      </a>
                    </li>
                    <li className="c-sidebar-nav-item">
                      <a className="c-sidebar-nav-link"   href="#/ivr-verification-settings">
                       
                        <span>IVR Verification</span>
                      </a>
                    </li>

                  </ul>
                </li>
                <li className={`c-sidebar-nav-item  `} style={userRegistration ? {pointerEvents: 'none'}:{}}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/api-management" ? "c-active" : ""
                    }`}
                    href="#/api-management"
                  >
                    <ManageBranchSvgIcon />
                    <span>API's</span>
                  </a>
                </li>
                {userRegistration && (
                  <div className="middle">
                    <div className="text">
                     <b> Warning !</b>
                      <hr />
                      <span>
                      You are new login please click on above logo to fill your form ! 
                      </span>
                     </div>
                  </div>
                )}

              </div>

                </span>
              </Fragment>
            ) : (
              <Fragment> 
                {employees.includes(role.display_name) ? (
                  <Fragment>
                    <li className={`c-sidebar-nav-item  `}>
                      <a
                       className={`c-sidebar-nav-link   ${
                          routeMatch == "#/end-user" ? "c-active" : ""
                        }`}
                        href="#/end-user"
                      >
                        <CustomerSvgIcon />
                         Manage End Users
                      </a>
                    </li>
                   </Fragment>
                ) :(
                  <Fragment></Fragment>
                ) }
              </Fragment>
            )}
          </Fragment>
        ) : (
          <Fragment>
            {customer.includes(role.display_name) ? (
              <Fragment>
                <li className="c-sidebar-nav-title">Details</li>
                <li className="c-sidebar-nav-item c-active">
                  <a className="c-sidebar-nav-link" href="#/bank-list">
                    <BankSvgIcon />
                    Bank
                  </a>
                </li>
                <li className={`c-sidebar-nav-item  `}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/global-parameter-list"
                  >
                    <GlobalSvgIcon />
                    Global Parameters
                  </a>
                </li>
                {/* <li className={`c-sidebar-nav-item  `}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/manage-rating"
                  >
                    <CIcon
                      name="cil-star"
                      className="mfe-2 c-sidebar-nav-icon"
                      style={{ width: "1.4rem", height: "1.5rem" }}
                    />
                    Score Management
                  </a>
                </li> */}
              </Fragment>
            ) : (
              <Fragment> 
            
            </Fragment>
            )}
          </Fragment>
        )}

        <li className="c-sidebar-nav-divider m-2" />
        <div className="ps__rail-x" style={{ left: "0px", bottom: "-318px" }}>
          <div
            className="ps__thumb-x"
            tabIndex={0}
            style={{ left: "0px", width: "0px" }}
          />
        </div>
        <div
          className="ps__rail-y"
          style={{ top: "318px", right: "0px", height: "374px" }}
        >
          <div
            className="ps__thumb-y"
            tabIndex={0}
            style={{ top: "137px", height: "161px" }}
          />
        </div>
      </ul>
    );
  }
}
