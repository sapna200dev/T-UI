export const FETCH_ENDUSER = 'FETCH_ENDUSER';
export const FETCH_ENDUSER_SUCCESS = 'FETCH_ENDUSER_SUCCESS';
export const FETCH_ENDUSER_FAILURE = 'FETCH_ENDUSER_FAILURE';
// export const RESET_LOGIN = 'RESET_LOGIN';

export function fetchEndUsersInfo(logint_type) {
    return { type: FETCH_ENDUSER, logint_type };
  }