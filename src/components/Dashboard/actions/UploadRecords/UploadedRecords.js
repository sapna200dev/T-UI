export const FETCH_UPLOADERECORDS = 'FETCH_UPLOADERECORDS';
export const FETCH_UPLOADERECORDS_SUCCESS = 'FETCH_UPLOADERECORDS_SUCCESS';
export const FETCH_UPLOADERECORDS_FAILURE = 'FETCH_UPLOADERECORDS_FAILURE';
// export const RESET_LOGIN = 'RESET_LOGIN';

export function fetchUploadedRecordInfo() {
    return { type: FETCH_UPLOADERECORDS };
  }