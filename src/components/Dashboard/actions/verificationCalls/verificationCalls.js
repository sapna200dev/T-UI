export const FETCH_VERIFICATION_CALLS = 'FETCH_VERIFICATION_CALLS';
export const FETCH_VERIFICATION_CALLS_SUCCESS = 'FETCH_VERIFICATION_CALLS_SUCCESS';
export const FETCH_VERIFICATION_CALLS_FAILURE = 'FETCH_VERIFICATION_CALLS_FAILURE';
// export const RESET_LOGIN = 'RESET_LOGIN';

export function fetchVerificationCallsInfo() {
    return { type: FETCH_VERIFICATION_CALLS };
  }