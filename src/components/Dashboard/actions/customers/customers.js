export const FETCH_CUSTOMER = 'FETCH_CUSTOMER';
export const FETCH_CUSTOMER_SUCCESS = 'FETCH_CUSTOMER_SUCCESS';
export const FETCH_CUSTOMER_FAILURE = 'FETCH_CUSTOMER_FAILURE';
// export const RESET_LOGIN = 'RESET_LOGIN';

export function fetchCustomersInfo() {
    return { type: FETCH_CUSTOMER };
  }