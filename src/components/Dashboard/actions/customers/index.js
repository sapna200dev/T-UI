export {
    FETCH_CUSTOMER,
    FETCH_CUSTOMER_SUCCESS,
    FETCH_CUSTOMER_FAILURE,
    fetchCustomersInfo

  } from './customers';

