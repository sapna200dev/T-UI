export const FETCH_TODAY_CUSTOMER = 'FETCH_TODAY_CUSTOMER';
export const FETCH_TODAY_CUSTOMER_SUCCESS = 'FETCH_TODAY_CUSTOMER_SUCCESS';
export const FETCH_TODAY_CUSTOMER_FAILURE = 'FETCH_TODAY_CUSTOMER_FAILURE';
// export const RESET_LOGIN = 'RESET_LOGIN';

export function fetchTodayCustomerInfo() {
    return { type: FETCH_TODAY_CUSTOMER };
  }