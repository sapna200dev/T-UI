export const FETCH_TODAY_ENDUSER = 'FETCH_TODAY_ENDUSER';
export const FETCH_TODAY_ENDUSER_SUCCESS = 'FETCH_TODAY_ENDUSER_SUCCESS';
export const FETCH_TODAY_ENDUSER_FAILURE = 'FETCH_TODAY_ENDUSER_FAILURE';
// export const RESET_LOGIN = 'RESET_LOGIN';

export function fetchTodayEndUsersInfo(login_type) {
    return { type: FETCH_TODAY_ENDUSER, login_type };
  }