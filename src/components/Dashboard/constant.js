
export const GRAPH_OPTIONS = [
    {id:0, text:'Line Chart', value:'line-chart'},
    {id:1, text:'Pie Chart', value:'pie-chart'},
    {id:2, text:'Bar Chart', value:'bar-chart'},    
];
  
    
  export const ERROR_CODES = {
      CITY_ERROR : 'This field is mandatory',
      MOBILE_NUMBER_ERROR : 'This field is mandatory and Please only enter numeric values',
  }