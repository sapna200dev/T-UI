import React, { Component, Fragment } from "react";
import { DashboardModal } from "../components/Dashboard/DashboardModal";
import {LOGOUT_WARNING_TIME, LOGOUT_TIME_OUT, URL_HOST} from '../../Comman/constants'
import { connect } from "react-redux";
import { logout } from "../../Auth/servicers/common";
import { getUserObj, redirectToLoginPage, getLoggedInUser, setLoggedInUser } from "../../Comman/functions";
import {
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../EndUser/actions/EndUser/index";
import {ERROR_CODES} from '../constant'

import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CLabel,
  CFormGroup,
  CForm,
  CSelect,
  CCol,
  CInput,
  CInvalidFeedback,
  CAlert
} from "@coreui/react";



import {
  fetchCustomersInfo,
  fetchEndUsersInfo,
  fetchUploadedRecordInfo,
  fetchCallsAttemptedInfo,
  fetchRegistrationCallsInfo,
  fetchVerificationCallsInfo,
  fetchTodayCustomerInfo,
  fetchTodayEndUsersInfo,
  fetchTodayUploadedRecordInfo,
  fetchTodyCallsAttemptedInfo,
  fetchTodayRegistrationCallsInfo,
  fetchTodayVerificationCallsInfo
} from "../actions/index";
import { InvertContentLoader } from "../../Comman/components";
import { fetchCities, addNewUserDetails  } from '../services/common'
import { AddBranchModal, AddNewBranchModal } from "../../Branch/components";
import { getCityByName } from "../../Branch/services";

var warningTimeout = LOGOUT_WARNING_TIME;
var timoutNow = LOGOUT_TIME_OUT;
var warningTimerID,timeoutTimerID;

let role='';

class Dashboard extends Component {
  state = {
    graph_type:'line-chart',
    fetchingInfo:false,
    userObj:'',
    showDefaultModal:false,
    branch_name: "",
    area: "",
    address: "",
    city_id: "",
    state_id: "",
    locality: "",
    phone: "",
    sale_agent_id: "",
    team_lead_id: "",
    pin_code: "",
    branch_manager_name: "",
    isAdding: false,
    errorMsg: "",
    successMsg: "",
    isFatchingState: false,
    states: [],
    addBranchErrors: {},
    cities: [],
    fetchingDetails: false,
    citiDetails: [],
    country_code: "",
    country_id: "",
    isFatchingCountry: false,
    countries: [],
    handleErrors: {},
    branchCreating: false,
    showmodal: false,
    add_type: "",
    input_val: "",
    addingInfo: false,
    country_name: '',
    type: '',
    state_id1: '',
    country_id1: '',
    no_of_digits: '',
    city_id:'',
    mobile_no:'',
    addErrors:{},
    adding:false,
    digit_fetching:false
  };

  componentDidMount() {
    let user='';
    const login_user = getLoggedInUser("user");

    this.setState({ fetchingInfo:true })


    if (login_user) {
      user = JSON.parse(login_user)
      this.setState({
        userObj : JSON.parse(login_user)
      })

      if(user.isUserRegister != null && user.isUserRegister == 1 ) {

        let _this = this;
        // alert('add branch')
        // _this.props.history.push("/add-new-branch");
        this.setState({
          showDefaultModal : true,
        })

        fetchCities().then(response => {
          console.log('city response = ', response.data.data)
          this.setState({
            cities:response.data.data,
            fetchingInfo:false
          })
        }).catch(err => {
          let error = err.data ? err.data.response.msg : err;
          console.log('error response = ', error)
          this.setState({
            errorMsg: error,
            fetchingInfo:false
          })
        })
      } // End if
    } // End if

    // Fetch dashboard info
    this.fetchDashboardInfo();

    // Reset timer
    this.setupTimers();
  } // End function componentDidMount


  /**
   * Fetch Dashboard info
   */
  fetchDashboardInfo = () => {
    const user_role = getUserObj("role");

    if (user_role) { role = JSON.parse(user_role); } // end if
    let login_type = ( role.name == 'sales_agent') ? 'sales_agent' : ( role.name == 'team_lead') ? 'team_lead' :'admin';

    const {
      fetchCustomersInfo,
      fetchEndUsersInfo,
      fetchUploadedRecordInfo,
      fetchCallsAttemptedInfo,
      fetchVerificationCallsInfo,
      fetchRegistrationCallsInfo,
      fetchTodayCustomerInfo,
      fetchTodayEndUsersInfo,
      fetchTodayUploadedRecordInfo,
      fetchTodyCallsAttemptedInfo,
      fetchTodayRegistrationCallsInfo,
      fetchTodayVerificationCallsInfo
    } = this.props;


    fetchCustomersInfo();
    fetchEndUsersInfo(login_type);
    fetchUploadedRecordInfo();
    fetchCallsAttemptedInfo();
    fetchRegistrationCallsInfo();
    fetchVerificationCallsInfo();

    fetchTodayCustomerInfo();
    fetchTodayEndUsersInfo(login_type);
    fetchTodayUploadedRecordInfo();
    fetchTodyCallsAttemptedInfo();
    fetchTodayRegistrationCallsInfo();
    fetchTodayVerificationCallsInfo()
    this.setState({ fetchingInfo:false })

  } // End function fetchDashboardInfo


  /**
   * Set timer
   */
  startTimer = () => {
      // window.setTimeout returns an Id that can be used to start and stop a timer
      warningTimerID = window.setTimeout(this.warningInactive, warningTimeout);
  }
  
  warningInactive = () =>  {
      window.clearTimeout(warningTimerID);
      timeoutTimerID = window.setTimeout(this.IdleTimeout, timoutNow);
      console.log("Log out pop up warningInactive  ")
  } // End function warningInactive
  

  /**
   * Handle reset timer
   */
  resetTimer = () =>  {
   console.log("resetTimer ") 
      window.clearTimeout(timeoutTimerID);
      window.clearTimeout(warningTimerID);
      this.startTimer();
  } // End function resetTimer
  
  /**
   * Handle idle timout
   */
  IdleTimeout = () =>  {
   console.log("Log out  ")
   logout()
    // this.logoutActive()
    redirectToLoginPage(true);
  } // End function logout
  
  setupTimers  = () =>  {
   console.log("setupTimers ")
      document.addEventListener("mousemove", this.resetTimer, false);
      document.addEventListener("mousedown", this.resetTimer, false);
      document.addEventListener("keypress", this.resetTimer, false);
      document.addEventListener("touchmove", this.resetTimer, false);
      document.addEventListener("onscroll", this.resetTimer, false);
      this.startTimer();
  } // End function setupTimers
  
  
  /**
   * Handle input values
   * @param  e 
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    console.log('handleInputValues name = ', name);
    console.log('handleInputValues value = ', value)
    if(name ==  'mobile_no') {
      this.testValidateNumber(name, value, 'mobile_no')
    }  else {
      this.getCityDetails(value)
      this.setState({
        [name] : value
      })
    } // End if-else
 
  } // End function handleInputValues

  /**
   * Dashboard apis
   */
  handleREfreshDashboardData = () => {
    this.setState({ ccccccccccc:true })
    this.fetchDashboardInfo();
  } // End function handleREfreshDashboardData

  /**
   * Handle cancel modal
   */
  handleCancelModal = () => {
    this.setState({
      showDefaultModal:false
    })
  } // End function handleCancelModal

  handleConfirm = () => {
    console.log('handleConfirm = ')
  }

    /**
   * Handle bank validation
   */
     handleAddValidation = () => { 
      const {city_id, mobile_no} = this.state;

      // Request validate
      let addErrors = {};

      if (city_id.length < 1 ) {
        addErrors.city_id = ERROR_CODES.CITY_ERROR;
      }  else if (mobile_no.length < 1 ) {
        addErrors.mobile_no = ERROR_CODES.MOBILE_NUMBER_ERROR;
      }  else if (mobile_no.length < 10 && mobile_no.length > 11 ) {
        addErrors.mobile_no = ERROR_CODES.MOBILE_NUMBER_ERROR;
      } // End if-else

      this.setState({ addErrors });
      return addErrors;
    }; // End function handleAddValidation

  /**
   * Handle branch modal
   */
  handleBranchModal = () => {

    // Handle form validation before insert into system
    const errors = this.handleAddValidation();

    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.setState({
        adding : true
      })

      // Handle add form
      this.addNewForm();
    } // End if

  } // End function handleBranchModal

   /**
   * Test validate number inputs
   */
    testValidateNumber = (name, value, field_name) => {
      let addErrors ={}
      const {no_of_digits} = this.state;

      if(!/^[0-9]+$/.test(value)){
        addErrors[field_name] = ERROR_CODES.MOBILE_NUMBER_ERROR
        this.setState({ [name]: value,addErrors, is_change: false });
      } else if(value != no_of_digits) {
        addErrors[field_name] = `Please enter ${no_of_digits} digits mobile number `;
        this.setState({ [name]: value,addErrors, is_change: false });
      } else {
        addErrors[field_name]='';
        this.setState({ [name]: value, is_change: true, addErrors });
      } // End if-else
  
        return addErrors;
    } // End function testValidateNumber
  
    /**
     * Add default branch / user / employee
     */
    addNewForm = () => {
      const { city_id, mobile_no, userObj } = this.state;
      const payload = { 
        city_id,
        mobile_no
      }

      console.log('payload of ruser = ', payload)

      // API call for add user form
      addNewUserDetails(payload).then(response => {
        console.log('response payload = ', response)
        console.log('response payload user = ', response.data.user)

        if(response.data.status == 200) {
          this.setState({
            successMsg :response.data.msg,
            adding:false
          })

          const loggedInUser = {
            username: userObj.username,
            email: userObj.email,
            id: userObj.id,
            profile: userObj.profile,
            bank_id :userObj.bank_id,
            bank_name: userObj.bank_name,
            name: userObj.name,
            last_login : userObj.last_login,
            isUserRegister : 0,
            city : response.data.user.city,
            state : response.data.user.state,
            country : response.data.user.country,
            mobile_no : response.data.user.mobile_no,
          };

          setLoggedInUser(JSON.stringify(loggedInUser));
          
          let _this = this
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 5000);
  
          setTimeout(function () {
            _this.props.history.push("/end-user");
            window.location.reload();
          }, 1000);

        } else {
          this.setState({
            errorMsg : response.data.msg,
            adding:false
          })
          let _this = this
          setTimeout(function () {
            _this.setState({ errorMsg : "" });
          }, 5000);

        }


      }).catch(error => {
        let err =error.response ? error.response.data.msg  : error;
        console.log('error response = ', err)
        this.setState({
          errorMsg :err,
          adding:false
        })
        let _this = this
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 5000);
      })
    } // End function addNewForm


      /**
   * Get city related data
   * @param  city_id
   */
  getCityDetails = (city_id) => {
    this.setState({
      digit_fetching:true
    })
    getCityByName(city_id)
      .then((res) => {
        console.log("getCityByState response = ", res);
        if (res.data.status == 200) {
          let data = res.data.data;
          console.log("getCityByState data = ", data.state);
          this.setState({
            no_of_digits : data.no_of_digits,
            errorMsg: "",
            citiDetails: data,
            fetchingDetails: false,
            country_id: data.state
              ? data.state.country
                ? data.state.country.id
                : "NA"
              : "NA",
            state_id: data.state ? data.state.id : "NA",
            digit_fetching:false
          });
        } else {
          this.setState({
            errorMsg: res.data.message,
            citiDetails: [],
            fetchingDetails: false,
            digit_fetching:false
          });
        }
      })
      .catch((err) => {
        console.log("response  getCityByState err = ", err);
        this.setState({
          errorMsg: err.response
            ? err.response.data.message
            : "Sonthing went wrong !",
          citiDetails: [],
          fetchingDetails: false,
          digit_fetching:false
        });
      });
  };



  render() {

    const {
      customersDetails,
      isFatching,
      endUserDetails,
      uploadedRecords,
      attempted_calls,
      registration_calls,
      verification_calls,

      todays_customer,
      today_enduser,
      today_uploaded_records,
      today_attempts_call,
      today_register_calls,
      today_verification_calls,

      isTotalCustomerFatching,
      isTotalEnduserFatching,
      isTotalRecordsUploadFatching,
      isCallsFatching,
      isRegisterCallFatching,
      isVerifyCallsFatching,
      isTodayEnduserFatching,
      isTodayCustomerFatching,
      isTodayUploadedRecordsFatching,
      isTodayAttemptesCallsFatching,
      isTodayRegisterCallFatching,
      isTodayVerifyCallFatching,
      sale_agents, team_leads
  
    } = this.props;

    const {graph_type, fetchingInfo,     showDefaultModal, 
    
      isFatchingState,
      errorMsg,
      branch_name,
      area,
      address,
      city_id,
      state_id,
      locality,
      phone,
      sale_agent_id,
      team_lead_id,
      pin_code,
      branch_manager_name,
      states,
      isFatchingCities,
      addBranchErrors,
      citiDetails,
      fetchingDetails,
      country_code,
      country_id,
      isFatchingCountry,
      countries,
      handleErrors,
      branchCreating,
      successMsg,
      showmodal,
      add_type,
      input_val,
      addingInfo,
      country_name,
      isSaving,
      country_id1,
      state_id1,
      no_of_digits,
      cities,
      mobile_no,
      addErrors,
      adding,
      digit_fetching
    } = this.state
    console.log('showDefaultModal value digit_fetching = ', digit_fetching);
    return ( 
      <Fragment>
              {showDefaultModal && (
                <Fragment>
                      {/* <CModal visible={true} >
                        <CModalHeader>
                          <CModalTitle>Modal title</CModalTitle>
                        </CModalHeader>
                        <CModalBody>
                          I will not close if you click outside me. Don't even try to press escape key.
                        </CModalBody>
                        <CModalFooter>
                          <CButton color="secondary" >
                            Close
                          </CButton>
                          <CButton color="primary">Save changes</CButton>
                        </CModalFooter>
                      </CModal> */}

                <CModal show={showDefaultModal} id="myModal" onClose={this.handleCancelModal} color="primary" 
                  backdrop={true}
                  keyboard={true}
                  // portal={false} cities
                 
                >
                  <CModalHeader closeButton>
                    <CModalTitle style={{ color: "white" }}>Add Form</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                      <span>
                        {successMsg && (
                          <CAlert color="success" className="msg_div">
                            {successMsg}
                          </CAlert>
                        )}
                        {errorMsg && (
                          <CAlert color="danger" className="msg_div">
                            {errorMsg}
                          </CAlert> 
                        )}
                      </span>
                      <span>
                        {/* Please create branch first, Click on <b>OK</b> to create <b>Branch</b> */}
                        <CForm
                          action=""
                          method="post"
                          encType="multipart/form-data"
                          className="form-horizontal"
                        >
                          <CFormGroup row> 
                          <CCol xs="12" md="1"></CCol>
                          <CCol xs="12" md="10">
                            <CLabel htmlFor="middle_name">
                              Select City <small className="required_lable">*</small>
                              {digit_fetching && <InvertContentLoader color={"black"} />}
                            </CLabel>
                            <CSelect
                              custom
                              name="city_id"
                              id="city_id"
                              invalid={addErrors.city_id}
                              onChange={this.handleInputValues}
                              value={city_id}
                            >
                              <option value="">Please Select City</option>
                              {cities ? (
                                cities.map((item) => (
                                  <option value={item.id}>{item.name}</option>
                                ))
                              ) : (
                                <option value="">No City</option>
                              )}
                            </CSelect>
                            <CInvalidFeedback>
                              {addErrors.city_id}
                            </CInvalidFeedback>
                          </CCol>
                          <CCol xs="12" md="1"></CCol>
                          </CFormGroup>
                          <CFormGroup row> 
                          <CCol xs="12" md="1"></CCol>
                          <CCol xs="12" md="10">
                            <CLabel htmlFor="middle_name">
                              Mobile Number <small className="required_lable">*</small>
                              {digit_fetching && <InvertContentLoader color={"black"} />}
                            </CLabel>
                            <CInput
                              id="mobile_no"
                              name="mobile_no"
                              value={mobile_no} 
                              placeholder={`Enter ${no_of_digits} Mobile Number`}
                              onChange={this.handleInputValues}
                              invalid={addErrors.mobile_no}
                            />
                            <CInvalidFeedback>
                              {addErrors.mobile_no}
                            </CInvalidFeedback>
                          </CCol>
                          <CCol xs="12" md="1"></CCol>
                          </CFormGroup>

                        </CForm>
                      </span>
                  </CModalBody>
                  <CModalFooter>
                    <CButton color="secondary" onClick={this.handleCancelModal}>
                      Cancel                 
                    </CButton>
                    {" "}
                    <CButton color="primary" onClick={this.handleBranchModal}>
                      Save {adding && (<InvertContentLoader />)}
                    </CButton>
                  </CModalFooter>
                </CModal>
                </Fragment>
              )}
              
        <DashboardModal
          customersDetails={customersDetails}
          isFatching={isFatching}
          endUserDetails={endUserDetails}
          uploadedRecords={uploadedRecords}
          attempted_calls={attempted_calls}
          registration_calls={registration_calls}
          verification_calls={verification_calls}

          todays_customer={todays_customer}
          today_enduser={today_enduser}
          today_uploaded_records={today_uploaded_records}
          today_attempts_call={today_attempts_call}
          today_register_calls={today_register_calls}
          today_verification_calls={today_verification_calls}

          isTotalCustomerFatching={isTotalCustomerFatching}
          isTotalEnduserFatching={isTotalEnduserFatching}
          isTotalRecordsUploadFatching={isTotalRecordsUploadFatching}
          isCallsFatching={isCallsFatching}
          isRegisterCallFatching={isRegisterCallFatching}
          isVerifyCallsFatching={isVerifyCallsFatching}
          isTodayEnduserFatching={isTodayEnduserFatching}
          isTodayCustomerFatching={isTodayCustomerFatching}
          isTodayUploadedRecordsFatching={isTodayUploadedRecordsFatching}
          isTodayRegisterCallFatching={isTodayRegisterCallFatching}
          isTodayVerifyCallFatching={isTodayVerifyCallFatching}
          isTodayAttemptesCallsFatching={isTodayAttemptesCallsFatching}
          handleREfreshDashboardData={this.handleREfreshDashboardData}
          graph_type={graph_type}
          handleInputValues={this.handleInputValues}
          fetchingInfo={fetchingInfo}

        />


      </Fragment>
    );
  }
}

export function mapStateToProps(state) {
  return {
    isTotalCustomerFatching: state.CustomerReducer.isTotalCustomerFatching,
    isTotalEnduserFatching: state.CustomerReducer.isTotalEnduserFatching,
    isTotalRecordsUploadFatching: state.CustomerReducer.isTotalRecordsUploadFatching,
    isCallsFatching: state.CustomerReducer.isCallsFatching,
    isRegisterCallFatching: state.CustomerReducer.isRegisterCallFatching,
    isVerifyCallsFatching: state.CustomerReducer.isVerifyCallsFatching,

    isTodayEnduserFatching: state.CustomerReducer.isTodayEnduserFatching,
    isTodayCustomerFatching: state.CustomerReducer.isTodayCustomerFatching,
    isTodayUploadedRecordsFatching: state.CustomerReducer.isTodayUploadedRecordsFatching,
    isTodayAttemptesCallsFatching: state.CustomerReducer.isTodayAttemptesCallsFatching,
    isTodayRegisterCallFatching: state.CustomerReducer.isTodayRegisterCallFatching,
    isTodayVerifyCallFatching: state.CustomerReducer.isTodayVerifyCallFatching,
  
    customersDetails: state.CustomerReducer.customersDetails,
    endUserDetails: state.CustomerReducer.endUserDetails,
    uploadedRecords: state.CustomerReducer.uploadedRecords,
    attempted_calls: state.CustomerReducer.attempted_calls,
    registration_calls: state.CustomerReducer.registration_calls,
    verification_calls: state.CustomerReducer.verification_calls,

    todays_customer: state.CustomerReducer.todays_customer,
    today_enduser: state.CustomerReducer.today_enduser,
    today_uploaded_records: state.CustomerReducer.today_uploaded_records,
    today_attempts_call: state.CustomerReducer.today_attempts_call,
    today_register_calls: state.CustomerReducer.today_register_calls,
    today_verification_calls: state.CustomerReducer.today_verification_calls,
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    branches: state.fetchBranchesReducer.branches,

  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchCustomersInfo: () => dispatch(fetchCustomersInfo()),
    fetchEndUsersInfo: (logint_type) => dispatch(fetchEndUsersInfo(logint_type)),
    fetchUploadedRecordInfo: () => dispatch(fetchUploadedRecordInfo()),
    fetchCallsAttemptedInfo: () => dispatch(fetchCallsAttemptedInfo()),
    fetchRegistrationCallsInfo: () => dispatch(fetchRegistrationCallsInfo()),
    fetchVerificationCallsInfo: () => dispatch(fetchVerificationCallsInfo()),

    fetchTodayCustomerInfo: () => dispatch(fetchTodayCustomerInfo()),
    fetchTodayEndUsersInfo: (login_type) => dispatch(fetchTodayEndUsersInfo(login_type)),
    fetchTodayUploadedRecordInfo: () => dispatch(fetchTodayUploadedRecordInfo()),
    fetchTodyCallsAttemptedInfo: () => dispatch(fetchTodyCallsAttemptedInfo()),
    fetchTodayRegistrationCallsInfo: () => dispatch(fetchTodayRegistrationCallsInfo()),
    fetchTodayVerificationCallsInfo: () => dispatch(fetchTodayVerificationCallsInfo()),
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

// export default Dashboard;
