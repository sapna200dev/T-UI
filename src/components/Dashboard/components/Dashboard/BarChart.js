import React, { Fragment } from 'react'
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCardHeader
} from '@coreui/react'
import {
  CChartBar,
  CChartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs'
import moment from "moment";
import DocsLink from './DocsLink'

const BarChart = (props) => {
    const {calls_count, isToday} = props;

    var success_count =  calls_count.success &&
    calls_count.success.count ;

    var failed_count = calls_count.failed &&
    calls_count.failed.count ;

    var total_calls = success_count + failed_count;

    // moment(). format('dddd'); 
  return (
    <CCardGroup  >
      <CCard>
        <CCardHeader>
          Bar Chart
          <DocsLink href="http://www.chartjs.org"/>
        </CCardHeader>
        <CCardBody>
          {failed_count ==0 && success_count == 0 ? (
            <Fragment>
              <span className="error_handle">
               <b>Today's no call data found !</b>
               </span>
            </Fragment>
          ) : (
            <CChartBar
              datasets={[
                {
                  label:[ 'Call History'],
                  // backgroundColor: '#f87979',
                  backgroundColor: [(failed_count > 0 && success_count == 0) ? '#f87979' : '#41B883','#f87979' ],
                  data: [success_count, failed_count, 100]
                }
              ]}
              labels={[
                  'Success', 'Failed'
              ]}
        
              options={{
                tooltips: {
                  enabled: true
                }
              }}
            />
          )}
         
        </CCardBody>
      </CCard>



    </CCardGroup>
  )
}

export default BarChart
