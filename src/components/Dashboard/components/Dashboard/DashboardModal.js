import React, { lazy, Fragment, useEffect, useState } from "react";
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CSelect,
  CRow,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CTabs,
  CNav,
} from "@coreui/react";
import moment from "moment";
import { getUserObj } from "../../../../components/Comman/functions";
import MainChartExample from "../../../../views/charts/MainChartExample.js";
import BarChart from './BarChart'
import PieChart from './PieChart'
import { ContentLoading } from "../../../Comman/components";
import { GRAPH_OPTIONS } from '../../constant'

const WidgetsDropdown = lazy(() =>
  import("../../../../views/widgets/WidgetsDropdown.js")
);

const SuperAdminDashboard = lazy(() =>
  import("./SuperAdminDashboard.js")
);

const WidgetsBrand = lazy(() =>
  import("../../../../views/widgets/WidgetsBrand.js")
);

export const DashboardModal = (props) => {
  const {
    fetchCustomersInfo,
    customersDetails,
    isFatching,
    endUserDetails,
    uploadedRecords,
    attempted_calls,
    verification_calls,
    registration_calls,
    todays_customer,
    today_enduser,
    today_uploaded_records,
    today_attempts_call,
    today_register_calls,
    today_verification_calls,
    isTotalCustomerFatching,
    isTotalEnduserFatching,
    isTotalRecordsUploadFatching,
    isCallsFatching,
    isRegisterCallFatching,
    isVerifyCallsFatching,
    isTodayEnduserFatching,
    isTodayCustomerFatching,
    isTodayUploadedRecordsFatching,
    isTodayAttemptesCallsFatching,
    isTodayRegisterCallFatching,
    isTodayVerifyCallFatching,
    graph_type,
    handleInputValues,
    handleREfreshDashboardData,
    fetchingInfo
  } = props;

  const [role, setRole] = useState("");

  useEffect(() => {
    const user_role = getUserObj("role");

    if (user_role) {
      setRole(JSON.parse(user_role));
    }

  }, []);

  console.log('graph_type name = ', graph_type);
  return (
    <Fragment>
      {role.name == 'super_admin' ? (
        <SuperAdminDashboard />
      ) : (
        <WidgetsDropdown
          customersDetails={customersDetails}
          endUserDetails={endUserDetails}
          uploadedRecords={uploadedRecords}
          attempted_calls={attempted_calls}
          verification_calls={verification_calls}
          registration_calls={registration_calls}
          todays_customer={todays_customer}
          today_enduser={today_enduser}
          today_uploaded_records={today_uploaded_records}
          today_attempts_call={today_attempts_call}
          today_register_calls={today_register_calls}
          today_verification_calls={today_verification_calls}

          isTotalCustomerFatching={isTotalCustomerFatching}
          isTotalEnduserFatching={isTotalEnduserFatching}
          isTotalRecordsUploadFatching={isTotalRecordsUploadFatching}
          isCallsFatching={isCallsFatching}
          isRegisterCallFatching={isRegisterCallFatching}
          isVerifyCallsFatching={isVerifyCallsFatching}
          isTodayEnduserFatching={isTodayEnduserFatching}
          isTodayCustomerFatching={isTodayCustomerFatching}
          isTodayUploadedRecordsFatching={isTodayUploadedRecordsFatching}
          isTodayRegisterCallFatching={isTodayRegisterCallFatching}
          isTodayVerifyCallFatching={isTodayVerifyCallFatching}
          isTodayAttemptesCallsFatching={isTodayAttemptesCallsFatching}
        />

      )}


      <CCard>
        <CCardHeader>
          
          <span>
            <span style={{float:'left'}}>
            Call Graphs
        
            </span>
            <div className="class1"   >
              <span className="class2">
             {isTotalCustomerFatching && 'Loading...' } 
              </span>
            <span onClick={handleREfreshDashboardData}>
          {' '}  <i className="fa fa-refresh refresh_icon dashboard_refresh" style={{fontSize:'16px'}}></i>
          
            </span>
            </div>
          </span>
          
          </CCardHeader>
        <CCardBody>
          <CTabs>
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink>Systemwide</CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink>Today</CNavLink>
              </CNavItem>
              <CNavItem>{/* <CNavLink>Messages</CNavLink> */}</CNavItem>
            </CNav>

            <CTabContent>

              <CTabPane>

                <CRow>

                  <CCol sm="10">
                    <h4 id="traffic" className="card-title mb-0" style={{ marginTop: '22px' }}>
                      Systemwide  Traffic
                    </h4>
                    <div className="small text-muted">

                      <b> {moment().format("MMM  YY")}</b>
                   
                    </div>
                  </CCol>
                  <CCol sm="2" className="d-none d-md-block">
                    <CSelect
                      custom
                      name="graph_type"
                      id="graph_type"
                      style={{ marginTop: '22px' }}

                      onChange={handleInputValues}
                      value={graph_type}

                      className="view-text-color"
                    >
                      <option value="">Please select</option>
                      {GRAPH_OPTIONS ? (
                        GRAPH_OPTIONS.map((item) => (
                          <option value={item.value}>{item.text}</option>
                        ))
                      ) : (
                        <option value="">No Loan Types</option>
                      )}
                    </CSelect>

                    {/* <CButton color="primary" className="float-right" style={{marginTop:'22px'}}>
                      <CIcon name="cil-cloud-download" />
                    </CButton> */}
                    {/* <CButtonGroup className="float-right mr-3">
              {["Day", "Month", "Year"].map((value) => (
                <CButton
                  color="outline-secondary"
                  key={value}
                  className="mx-0"
                  active={value === "Month"}
                >
                  {value}
                </CButton>
              ))}
            </CButtonGroup> */}
                  </CCol>
                </CRow>
              
                {graph_type == 'line-chart' ?
                  (
                    <MainChartExample style={{ height: "300px", marginTop: "40px" }} />
                  )
                  : graph_type == 'pie-chart' ? (

                    <PieChart calls_count={attempted_calls} isToday={true} />
                  ) :

                    (<BarChart calls_count={attempted_calls} isToday={true} />)}




              </CTabPane>
              <CTabPane>



                <CRow>

                  <CCol sm="10">
                    <h4 id="traffic" className="card-title mb-0" style={{ marginTop: '22px' }}>
                      Today Traffic
            </h4>
                    <div className="small text-muted">November 2017</div>
                  </CCol>
                  <CCol sm="2" className="d-none d-md-block" >
                  <CSelect
                      custom
                      name="graph_type"
                      id="graph_type"
                      style={{ marginTop: '22px' }}

                      onChange={handleInputValues}
                      value={graph_type}

                      className="view-text-color"
                    >
                      <option value="">Please select</option>
                      {GRAPH_OPTIONS ? (
                        GRAPH_OPTIONS.map((item) => (
                          <option value={item.value}>{item.text}</option>
                        ))
                      ) : (
                        <option value="">No Loan Types</option>
                      )}
                    </CSelect>

                    {/* <CButtonGroup className="float-right mr-3">
              {["Day", "Month", "Year"].map((value) => (
                <CButton
                  color="outline-secondary"
                  key={value}
                  className="mx-0"
                  active={value === "Month"}
                >
                  {value}
                </CButton>
              ))}
            </CButtonGroup> */}
                  </CCol>
                </CRow>
                {graph_type == 'line-chart' ?
                  (
                    <MainChartExample style={{ height: "300px", marginTop: "40px" }} />
                  )
                  : graph_type == 'pie-chart' ? (

                    <PieChart calls_count={today_attempts_call} isToday={false} />
                  ) :

                    (<BarChart calls_count={today_attempts_call} isToday={false} />)}


              </CTabPane>

            </CTabContent>
          </CTabs>

        </CCardBody>


        <CCardFooter>
          {/* <CRow className="text-center">
            <CCol md sm="12" className="mb-sm-2 mb-0">
              <div className="text-muted">Visits</div>
              <strong>29.703 Users (40%)</strong>
              <CProgress
                className="progress-xs mt-2"
                precision={1}
                color="success"
                value={40}
              />
            </CCol>
            <CCol md sm="12" className="mb-sm-2 mb-0 d-md-down-none">
              <div className="text-muted">Unique</div>
              <strong>24.093 Users (20%)</strong>
              <CProgress
                className="progress-xs mt-2"
                precision={1}
                color="info"
                value={40}
              />
            </CCol>
            <CCol md sm="12" className="mb-sm-2 mb-0">
              <div className="text-muted">Pageviews</div>
              <strong>78.706 Views (60%)</strong>
              <CProgress
                className="progress-xs mt-2"
                precision={1}
                color="warning"
                value={40}
              />
            </CCol>
            <CCol md sm="12" className="mb-sm-2 mb-0">
              <div className="text-muted">New Users</div>
              <strong>22.123 Users (80%)</strong>
              <CProgress
                className="progress-xs mt-2"
                precision={1}
                color="danger"
                value={40}
              />
            </CCol>
            <CCol md sm="12" className="mb-sm-2 mb-0 d-md-down-none">
              <div className="text-muted">Bounce Rate</div>
              <strong>Average Rate (40.15%)</strong>
              <CProgress
                className="progress-xs mt-2"
                precision={1}
                value={40}
              />
            </CCol>
          </CRow> */}
        </CCardFooter>
      </CCard>

      {/* <WidgetsBrand withCharts /> */}
    </Fragment>
  );
};
