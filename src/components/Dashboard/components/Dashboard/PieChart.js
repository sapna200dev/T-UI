import React, { Fragment } from 'react'
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCardHeader
} from '@coreui/react'
import {
  CChartBar,
  CChartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs'
import moment from "moment";
import DocsLink from './DocsLink'

const PieChart = (props) => {
    const {calls_count, isToday} = props;
    console.log('calls_countcalls_count =00');
    console.log(calls_count)
    // moment(). format('dddd'); 

    var success_count =  calls_count.success &&
    calls_count.success.count ;

    var failed_count = calls_count.failed &&
    calls_count.failed.count ;

    var total_calls = success_count + failed_count;

  return (
    <CCardGroup  > 

      <CCard>
        <CCardHeader>
        {isToday ? 'All' : 'Today\'s' + '( ' +  moment(). format('dddd') +' )'}  Calls
        </CCardHeader>
        <CCardBody>
          {calls_count.success.count == 0 && calls_count.failed.count == 0 ? (
            <Fragment>
               <span className="error_handle">
               <b>Today's no call data found !</b>
               </span>
             </Fragment>

          ) : (
            <CChartPie
              className="pie-chart"
                datasets={[
                  {
                    backgroundColor: [
                      '#41B883',
                      '#DD1B16',
                
                    ],
                    data: [calls_count.success &&
                        calls_count.success.count , calls_count.failed &&
                        calls_count.failed.count, ]
                  }
                ]}
                labels={[
                    'Success ' , 'Failed ' ,  'Total - ' + total_calls
                ]}
                options={{
                  tooltips: {
                    enabled: true
                  }
                }}
              />
          )}
          
        </CCardBody>
      </CCard>


    </CCardGroup>
  )
}

export default PieChart
