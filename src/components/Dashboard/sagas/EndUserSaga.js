import React, { Component, Fragment } from 'react';
import { take, put, call, all } from 'redux-saga/effects';
import * as actions from '../actions';
import axios, { setAuthHeader } from '../../Comman/axiosConfig';
// import { SHOW_TOAST } from '../../Common/actions';
// import { CONST_LANG } from '../../Common/constant_language';
import { URL_HOST } from '../../Comman/constants'




export const fetchEndUserData = async (logint_type) => {
    try {
      const response = await axios.post(`${URL_HOST}/api/dashboard/total-endusers`, {
        login_user:logint_type
      });

    //   setAuthHeader(response.data.access_token);
      // setCookie('ytel_token', response.data.access_token, 7);
      return response.data;
    } catch (err) {
      return err.response.data;
    }
  };
  

  export function* getEndUser(logint_type) {
    try {
      const response = yield fetchEndUserData(logint_type);
        // console.log('getEndUser response = ', response.data)
        // console.log('getEndUser response status = ', response.status)
      if (response && response.status == 200) {

        yield all([
          put({
            type: actions.FETCH_ENDUSER_SUCCESS, 
            response: response.data,
          })
       
        ]);
      } else {
  
        yield all([
          put({
            type: actions.FETCH_ENDUSER_FAILURE,
            response,

          }),
          put({
            type: 'ERROR',
            msgType: 'error',
            header: (response && response.msg) ? response.msg : 'CONST_LANG.LOGIN_ERROR_CONTENT', //CONST_LANG.ERROR_HEADER,
            content: 'errorMessageContent',
          }),
        ]);
      }
    } catch (err) {
      yield all([
        put({ type: actions.FETCH_ENDUSER_FAILURE, err }),
        put({
          type: 'ERROR',
          msgType: 'error',
          header: 'Error',
          content: 'CONST_LANG.LOGIN_FAIL_MSG',
        }),
      ]);
    }
  }



  export function* watchEndUserSaga() {
    while (true) {
      const state = yield take(actions.FETCH_ENDUSER);
      yield call(getEndUser, state.logint_type);
    }
  }
  