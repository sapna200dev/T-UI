import React, {Component, Fragment} from 'react'
import { render } from 'enzyme'
import {IVRSettingsList} from '../componenets/index'
import {getIvrRule} from '../services/index'


class IVRSettings extends Component {
    state = {
        ivr_rules :[],
        isFatching:false,
        errorMsg:'',
        filterRegisterRuleLists:[],
        searchValue:''
    }


    componentDidMount() {
        this.setState({ isFatching : true })
        getIvrRule().then(res => {
            if(res.data.status == 200 ) {
                this.setState({
                    ivr_rules : res.data.data,
                    filterRegisterRuleLists: res.data.data,
                    isFatching:false
                })
            } else {
                this.setState({
                    ivr_rules : [],
                    errorMsg : res.data.msg,
                    isFatching:false
                })

                setTimeout(() => {
                    this.setState({ errorMsg:'' })
                }, 5000)
            }
        }).catch(err => {
            console.log('error', err)
            this.setState({
                isFatching:false
            })
        })
    }

    /**
   * Handle search event
   */ 
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { ivr_rules } = this.state;
    let ivr_registration_obj = [];

    if (data) {
      ivr_registration_obj = this.handleFilterChangeVal(ivr_rules, data);
    } else {
      ivr_registration_obj = this.state.ivr_rules;
    }

    this.setState({
      filterRegisterRuleLists: ivr_registration_obj,
      searchValue: data,
    });
  };

  /**
   * Handle filter change
   * @param  ivr_rules 
   * @param  value 
   */
  handleFilterChangeVal = (ivr_rules, value) => {
    let ivr_rule_lists = [];
    ivr_rule_lists = ivr_rules.filter((item) => {
      return (
        (item.activation_date &&
          item.activation_date.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.ivr_rule_status &&
          item.ivr_rule_status.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.deactivation_date &&
          item.deactivation_date.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.ivr_rule_set_name &&
          item.ivr_rule_set_name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.total_recordings && parseInt(item.total_recordings) === parseInt(value)) || 
        (item.language &&
            item.language.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return ivr_rule_lists;
  };




    render() {
        const {ivr_rules, isFatching, errorMsg, filterRegisterRuleLists, searchValue} = this.state;
 
        return (
            <Fragment>
                <IVRSettingsList 
                    ivr_rules={filterRegisterRuleLists}
                    isFatching={isFatching}
                    errorMsg={errorMsg}
                    handleFilterChange={this.handleFilterChange}
                    searchValue={searchValue}
                />
            </Fragment>
        )
    }
}


export default IVRSettings