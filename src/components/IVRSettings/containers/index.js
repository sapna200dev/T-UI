import IVRSettings from './IVRSettings'
import IVRSettingsRules from './IVRSettingsRules'
import IvrSettingsEdit from './IvrSettingsEdit'
import IvrSettingsDetails from './IvrSettingsDetails'
export {
    IVRSettings,
    IVRSettingsRules,
    IvrSettingsEdit,
    IvrSettingsDetails
}
