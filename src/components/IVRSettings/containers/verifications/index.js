import VerificationList from './VerificationList'
import AddIvrVerification from './AddIvrVerification'
import IvrVerificationDetails from './IvrVerificationDetails';
import IvrVerificationEdit from './IvrVerificationEdit'
export {
    VerificationList,
    AddIvrVerification,
    IvrVerificationDetails,
    IvrVerificationEdit
}
