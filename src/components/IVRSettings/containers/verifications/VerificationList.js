import React, { Component, Fragment } from "react";
import { render } from "enzyme";
import { IVRVerificationSettingsList } from "../../componenets/verification/index";
import { getIvrVerificationRules } from "../../services/index";

class VerificationList extends Component {
  state = {
    ivr_verification_rules: [],
    isFatching: false,
    errorMsg: "",
    searchValue: "",
    filterVerifyRuleLists: [],
  };

  componentDidMount() {
    this.setState({ isFatching: true });
    getIvrVerificationRules()
      .then((res) => {
        if (res.data.status == 200) {
          this.setState({
            ivr_verification_rules: res.data.data,
            filterVerifyRuleLists: res.data.data,
            isFatching: false,
          });
        } else {
          this.setState({
            ivr_verification_rules: [],
            errorMsg: res.data.msg,
            isFatching: false,
          });

          setTimeout(() => {
            this.setState({ errorMsg: "" });
          }, 5000);
        }
      })
      .catch((err) => {
        console.log("error", err);
        this.setState({
          ivr_verification_rules: [],
          errorMsg: err.response.data.msg,
          isFatching: false,
        });
      });
  }

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    console.log("fillter value = ", data);

    const { ivr_verification_rules } = this.state;
    let ivr_verification_obj = [];

    if (data) {
      ivr_verification_obj = this.handleFilterChangeVal(
        ivr_verification_rules,
        data
      );
    } else {
      ivr_verification_obj = this.state.ivr_verification_rules;
    }

    this.setState({
      filterVerifyRuleLists: ivr_verification_obj,
      searchValue: data,
    });
  };

  /**
   * Handle filter change
   * @param  ivr_rules
   * @param  value
   */
  handleFilterChangeVal = (ivr_rules, value) => {
    let ivr_rule_lists = [];
    ivr_rule_lists = ivr_rules.filter((item) => {
      return (
        (item.activation_date &&
          item.activation_date.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.ivr_rule_status &&
          item.ivr_rule_status.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.deactivation_date &&
          item.deactivation_date.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.ivr_rule_set_name &&
          item.ivr_rule_set_name.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.total_recordings &&
          parseInt(item.total_recordings) === parseInt(value)) ||
        (item.language &&
          item.language.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return ivr_rule_lists;
  };

  render() {
    const {
      ivr_verification_rules,
      isFatching,
      errorMsg,
      searchValue,
      filterVerifyRuleLists,
    } = this.state;

    return (
      <Fragment>
        <IVRVerificationSettingsList
          ivr_verification_rules={filterVerifyRuleLists}
          isFatching={isFatching}
          errorMsg={errorMsg}
          handleFilterChange={this.handleFilterChange}
          searchValue={searchValue}
        />
      </Fragment>
    );
  }
}

export default VerificationList;
