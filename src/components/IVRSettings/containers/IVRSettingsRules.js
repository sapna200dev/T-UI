import React, { Component, Fragment } from "react";
import { AddIVRSettingsList } from "../componenets/index";
import { connect } from "react-redux"; 
import { Ivr_Fields } from "../contants";
import { ValidateRequestParams, prepareFormRequestParams } from "../functions";
import { ERROR_VALIDATIONS } from "../contants";
import { addIvrRule } from "../services/index";
import { getLoggedInUser } from "../../Comman/functions";
import {
  fetchLanguages,
} from "../../EndUser/actions/EndUser/index";


let data = [];
let audio = document.querySelector("#audio");

class IVRSettingsRules extends Component {
  state = {
    total_recording: 3,
    audio_lang:'english',
    bank_name: "",
    ivr_rules_set: "",
    activation_status: "testing",
    activation_date: "",
    check_audio: "",
    check_upload_recording: "",
    ai_dm_pass_text: "",
    ai_dm_pass_text_uploading_recording: "",
    ai_dm_fail_text: "",
    ai_dm_fail_text_uploading_recording: "",
    welcome_text_audio_recording: "",
    Ivr_recording_Fields: [],
    handleErrors: {},
    field_2_value: "",
    deactivation_date: "",
    file_obj: [],
    playAudio: true,
    stopAudio: false,
    isSaving: false,
    successMsg: "",
    isNotEmpty: false,
    playAudio1: false,
    playAudio2: false,
    playAudio3: false,
    playAudio4: false,
    disabledAudio: false,
    welcome_text: "",
  };

  componentDidMount() {
    let arrays = [];
    const getLoggedUser = getLoggedInUser("user");
    let user = "";
    if (getLoggedUser) {
      user = JSON.parse(getLoggedUser);
    }
    audio = document.querySelector("#audio");

    let new_array = [];
    new_array = Ivr_Fields.map((item, index) => {
      let item_row = item;
      item_row["field_1_value"] = "";
      item_row["field_2_value"] = "";
      item_row["field_4_value"] = index > 0 ? '9' : "4";

      item_row["field_1_check"] = false;
      item_row["field_2_check"] = false;
      item_row["field_4_check"] = false;
      return item_row;
    });

    this.setState({
      bank_name: user.bank_name,
      playAudio1: false,
      playAudio2: false,
      playAudio3: false,
      playAudio4: false,
      playAudio5: false,
      handleErrors: {},
      ai_dm_pass_text_uploading_recording: "",
      ai_dm_fail_text_uploading_recording: "",
      welcome_text_audio_recording: "",
      check_upload_recording: "",
      Ivr_recording_Fields: new_array,
    });

    const {
      fetchLanguages,
    } = this.props;
    fetchLanguages();
  }

  /**
   * Handle inputs based on keys
   */
  handleInputValuesNew = (e, index, coulmn) => {
    const { name, value } = e.target;
    const { Ivr_recording_Fields } = this.state;

    let finalResult = Ivr_recording_Fields;
    finalResult[index][coulmn] = value;

    let custom_index = name.replace("value", "check");
    if (finalResult[index][coulmn] != "") {
      finalResult[index][custom_index] = false;
    } else {
      finalResult[index][custom_index] = true;
    }

    finalResult[index][coulmn] = value;
    this.setState({ Ivr_recording_Fields: finalResult });
  };

  /**
   * Handle inputs
   */
  handleInputValues = (e) => {
    const { Ivr_recording_Fields, file_obj } = this.state;
    const { name, value } = e.target;
    let finalRsult = [];

    if (name == "total_recording") {
      if (value != "") {
        let validate = this.testValidateNumber(name, value, "total_recording");
        let existingCount = Ivr_recording_Fields.length;

        if (parseInt(existingCount) > parseInt(value)) {
          console.log("lets reduxe the row  ", value);
          const index = file_obj.indexOf(value);
          if (index > -1) {
            file_obj.splice(index, 1);
          }
          let rotaionCount = parseInt(value);
          for (var i = 0; i < rotaionCount; i++) {
            let obj = Ivr_recording_Fields[i];
            finalRsult.push(obj);
          }

          this.setState({ Ivr_recording_Fields: finalRsult });
        } else if (parseInt(existingCount) < parseInt(value)) {
          console.log("lets add the row   ", value);

          finalRsult = Ivr_recording_Fields;
          let rotaionCount = parseInt(value) - parseInt(existingCount);
          console.log("lets reduxe the row rotaionCount  ", rotaionCount);

          for (var i = 0; i < rotaionCount; i++) {
            let obj = {
              id: rotaionCount + 1,
              field_1: "Recording Text-" + i,
              field_1_value: "",
              field_1_check: false,
              field_2: "Playback Recording",
              field_2_value: "",
              field_2_check: false,
              field_3: "Beep at start of Recording",
              field_3_value: "",
              field_3_check: false,
              field_4: "Recording Duration (sec)",
              field_4_value: "9",
              field_4_check: false,
            };
            finalRsult.push(obj);
          }

          this.setState({ Ivr_recording_Fields: finalRsult });
        }
        this.setState({ [name]: value });
      } else {
        this.setState({ [name]: value });
      }
    } else if (name == "ivr_rules_set") {
      let validate = this.testValidateNumber(name, value, "ivr_rules_set");
    } else if (name == "welcome_text") {
      let validate = this.testValidateNumber(name, value, "welcome_text");
    } else {
      this.setState({ [name]: value });
    }
  };

  /**
   * Test validate number inputs
   */
  testValidateNumber = (name, value, field_name) => {
    let handleErrors = {};
    if (field_name == "total_recording") {
      if (!/^[0-9]+$/.test(value)) {
        handleErrors[field_name] = ERROR_VALIDATIONS.VALID_FIELD;

        this.setState({ [name]: value, handleErrors, is_change: false });
      } else {
        handleErrors[field_name] = "";
        this.setState({ [name]: value, handleErrors });
      }
    } else if (field_name == "ivr_rules_set") {
      var re = /^[a-zA-Z0-9-_]+$/;
      if (!re.test(value)) {
        handleErrors[field_name] =
          "Only allowed chars, numbers, underscor and dashes in rule set name";

        this.setState({ [name]: value, handleErrors, is_change: false });
      } else {
        handleErrors[field_name] = "";
        this.setState({ [name]: value, handleErrors });
      }
    } else if (field_name == "welcome_text") {
      var re = /^[a-zA-Z0-9- _]+$/;
      if (!re.test(value)) {
        handleErrors[field_name] =
          "Only allowed chars, numbers, underscor and dashes in rule set name";

        this.setState({ [name]: value, handleErrors, is_change: false });
      } else {
        handleErrors[field_name] = "";
        this.setState({ [name]: value, handleErrors });
      }
    }

    return handleErrors;
  };

  /**
   * Handle dynamic upload docs
   */
  handleInputValuesUpload2 = (event, index, uploadType = null) => {
    const { files } = event.target;
    const { Ivr_recording_Fields, file_obj } = this.state;
    console.log("createObjectURL" , files[0]);
    console.log("uploadType" , uploadType);

    // if (audio) {
    //   audio.src = URL.createObjectURL(files[0]);
    // }

    if (files[0]) {
      let finalResult = Ivr_recording_Fields;
      finalResult[index][uploadType] = files[0];

      data[index] = files[0];

      file_obj[uploadType] = data;

      console.log("createObjectUR finalResult L", finalResult);

      this.setState({
        Ivr_recording_Fields: finalResult,
        file_obj: data,
        isNotEmpty: false,
      });
    } else {
      this.setState({ isNotEmpty: true });
    }
  };

  /**
   *
   * Handle upload events
   */
  handleInputValuesUpload1 = (event, uploadType = null) => {
    // audio.src = URL.createObjectURL(event.target.files[0]);

    if (uploadType == "check_upload_recording") {
      this.setState({
        check_upload_recording: event.target.files[0],
      });
    } else if (uploadType == "ai_dm_pass_text_uploading_recording") {
      this.setState({
        ai_dm_pass_text_uploading_recording: event.target.files[0],
      });
    } else if (uploadType == "ai_dm_fail_text_uploading_recording") {
      this.setState({
        ai_dm_fail_text_uploading_recording: event.target.files[0],
      });
    } else if (uploadType == "welcome_text_audio_recording") {
      this.setState({
        welcome_text_audio_recording: event.target.files[0],
      });
    }
  };

  /**
   * Handle bank validation
   */
  handleAddRecordingValidation = () => {
    const { Ivr_recording_Fields } = this.state;
    let finalResult = Ivr_recording_Fields;
    let error = 0;

    finalResult.map((item, index) => {
      let _this = this;

      console.log("value check QQQQQ= ", finalResult[index]["field_2_value"]);

      console.log("value check index= ", index);

      if (
        finalResult[index]["field_1_value"] == "" ||
        finalResult[index]["field_2_value"] == "" ||
        finalResult[index]["field_3_value"] == "" ||
        finalResult[index]["field_4_value"] == ""
      ) {
        console.log(
          "value check dddddd= ",
          finalResult[index]["field_2_value"]
        );

        if (finalResult[index]["field_1_value"] == "") {
          finalResult[index]["field_1_check"] = true;

          this.setState({ Ivr_recording_Fields: finalResult });
        } else {
          finalResult[index]["field_1_check"] = false;

          this.setState({ Ivr_recording_Fields: finalResult });
        }

        if (finalResult[index]["field_3_value"] == "") {
          finalResult[index]["field_3_check"] = true;

          this.setState({ Ivr_recording_Fields: finalResult });
        } else {
          finalResult[index]["field_3_check"] = false;

          this.setState({ Ivr_recording_Fields: finalResult });
        }

        if (finalResult[index]["field_4_value"] == "") {
          finalResult[index]["field_4_check"] = true;

          this.setState({ Ivr_recording_Fields: finalResult });
        } else {
          finalResult[index]["field_4_check"] = false;

          this.setState({ Ivr_recording_Fields: finalResult });
        }

        return (error = 1);
      } else {
        return (error = 0);
      }
    });
  };

  /**
   * Handle bank validation
   */
  handleAddIVRRulesValidation = () => {
    // Request validate
    let handleErrors = ValidateRequestParams(this.state);

    this.setState({ handleErrors });
    return handleErrors;
  };

  /**
   * Handle add irv event with validations
   */
  handleAddIVRRule = () => {
    const {
      Ivr_recording_Fields,
      check_upload_recording,
      ai_dm_pass_text_uploading_recording,
      ai_dm_fail_text_uploading_recording,
      welcome_text_audio_recording,
    } = this.state;

    const recordings = this.handleAddRecordingValidation();

    let errorField = false;
    let errorMsgText = "";

    let finalResult = Ivr_recording_Fields;

    finalResult.map((item, i) => {
      if (item.field_2_value == "") {
        errorField = true;
        errorMsgText = "Select Playback Recording !";
      } else {
        if (item.field_1_check || item.field_2_check || item.field_4_check) {
          errorField = true;
        } else {
          errorField = false;
        }
      }
    });

    if (
      check_upload_recording == "" ||
      ai_dm_pass_text_uploading_recording == "" ||
      ai_dm_fail_text_uploading_recording == "" ||
      welcome_text_audio_recording == ""
    ) {
      errorField = true;
      errorMsgText = "Select Playback Recording !";
    }

    const errors = this.handleAddIVRRulesValidation();

    if (Object.getOwnPropertyNames(errors).length === 0) {
      if (errorField) {
        this.setState({
          errorMsg: errorMsgText,
        });

        setTimeout(() => {
          this.setState({
            errorMsg: "",
          });
        }, 5000);
      } else {
        console.log("all done field");
        this.handleReuquest();
      }
    }
  };

  /**
   * Handle add requests
   */
  handleReuquest = () => {
    this.setState({ isSaving: true });
    let params = prepareFormRequestParams(this.state);
    console.log("payload = ", params);
    addIvrRule(params)
      .then((res) => {
        let _this = this;
        if(res.data.status == 200 ) {
          let new_array = [];
          new_array = this.state.Ivr_recording_Fields.map((item, index) => {
            let item_row = item;
            item_row["field_1_value"] = "";
            item_row["field_2_value"] = "";
            item_row["field_4_value"] = "";
      
            item_row["field_1_check"] = false;
            item_row["field_2_check"] = false;
            item_row["field_4_check"] = false;
            return item_row;
          });
      
          console.log('response success', new_array)
  
          _this.setState({
            successMsg: res.data.msg,
            isSaving: false,
            total_recording: 3,
            ivr_rules_set: "",
            activation_status: "testing",
            activation_date: "",
            check_audio: "",
            check_upload_recording: "",
            ai_dm_pass_text: "",
            ai_dm_pass_text_uploading_recording: "",
            ai_dm_fail_text: "",
            ai_dm_fail_text_uploading_recording: "",
            Ivr_recording_Fields: new_array,
            welcome_text_audio_recording: "",
            field_2_value: "",
            deactivation_date: "",
            file_obj: [],
            errorMsg: "",
            playAudio1: false,
            playAudio2: false,
            playAudio3: false,
            playAudio4: false,
            disabledAudio: true,
            welcome_text: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            _this.props.history.push("/ivr-settings");
          }, 4000);
  
          console.log("response = ", res);
        } else {
          _this.setState({
            errorMsg : res.data.msg,
            isSaving:false,
            successMsg:''
          })

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);

        }

      })
      .catch((err) => {
        let _this = this;
        console.log("error =>", err.response);
        this.setState({
          successMsg: "",
          errorMsg: err.response.data.message,
          isSaving: false,
        });
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };
 
  handleAudioPlay = (play = true, data = null, type, value, index, dyna) => {
    let {
      check_upload_recording,
      ai_dm_pass_text_uploading_recording,
      ai_dm_fail_text_uploading_recording,
      Ivr_recording_Fields,
      welcome_text_audio_recording,
    } = this.state;

    if (type == "check_upload_recording") {
      if (value) {
        if (audio) {
          audio.src = URL.createObjectURL(check_upload_recording);
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = URL.createObjectURL(check_upload_recording);
          audio.autoplay = true;
        }
      }

      this.setState({
        playAudio1: !value,
        playAudio2: false,
        playAudio3: false,
        playAudio4: false,
      });
    }
    if (type == "ai_dm_pass_text_uploading_recording") {
      if (value) {
        if (audio) {
          audio.src = URL.createObjectURL(ai_dm_pass_text_uploading_recording);
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = URL.createObjectURL(ai_dm_pass_text_uploading_recording);
          audio.autoplay = true;
        }
      }
      this.setState({
        playAudio2: !value,
        playAudio1: false,
        playAudio3: false,
        playAudio4: false,
      });
    }
    if (type == "ai_dm_fail_text_uploading_recording") {
      if (value) {
        if (audio) {
          audio.src = URL.createObjectURL(ai_dm_fail_text_uploading_recording);
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = URL.createObjectURL(ai_dm_fail_text_uploading_recording);
          audio.autoplay = true;
        }
      }

      this.setState({
        playAudio3: !value,
        playAudio1: false,
        playAudio2: false,
        playAudio4: false,
      });
    }

    if (type == "welcome_text_audio_recording") {
      if (value) {
        if (audio) {
          audio.src = URL.createObjectURL(welcome_text_audio_recording);
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = URL.createObjectURL(welcome_text_audio_recording);
          audio.autoplay = true;
        }
      }

      this.setState({
        playAudio4: !value,
        playAudio3: false,
        playAudio1: false,
        playAudio2: false,
      });
    }

    let Ivr_recording_FieldsRecords = Ivr_recording_Fields;
    let row = Ivr_recording_FieldsRecords;
    row = row.map((x) => {
      let xvalue = x;
      xvalue["field_2_play"] = false;
      return xvalue;
    });
    if (dyna == "true") {
      row[index]["field_2_play"] = !value;

      if (value) {
        if (audio) {
          audio.src = URL.createObjectURL(row[index]["field_2_value"]);
          audio.pause();
        }
      } else {
        if (audio) {
          audio.src = URL.createObjectURL(row[index]["field_2_value"]);
          audio.autoplay = true;
        }
      }

      // if (audio) {
      //   audio.src = URL.createObjectURL(row[index]["field_2_value"]);
      //   audio.autoplay = true;
      // }
      this.setState({
        playAudio3: false,
        playAudio1: false,
        playAudio1: false,
        playAudio4: false,
      });
    }
    this.setState({ Ivr_recording_Fields: row });

    // audio.autoplay = true;
    // console.log('handleAudioPlay', audio)
  };

  render() {
    const {
      total_recording,
      bank_name,
      ivr_rules_set,
      activation_status,
      activation_date,
      check_audio,
      check_upload_recording,
      ai_dm_pass_text,
      ai_dm_pass_text_uploading_recording,
      ai_dm_fail_text,
      ai_dm_fail_text_uploading_recording,
      handleErrors,
      deactivation_date,
      Ivr_recording_Fields,
      file_obj,
      playAudio,
      stopAudio,
      successMsg,
      isSaving,
      errorMsg,
      isNotEmpty,
      playAudio1,
      playAudio2,
      playAudio3,
      playAudio4,
      disabledAudio,
      welcome_text,
      welcome_text_audio_recording,
      audio_lang,
    } = this.state;


    const {languages} = this.props;

    console.log('languageslanguages = ', languages)

    return (
      <Fragment>
        <AddIVRSettingsList
          total_recording={total_recording}
          bank_name={bank_name}
          ivr_rules_set={ivr_rules_set}
          activation_status={activation_status}
          activation_date={activation_date}
          check_audio={check_audio}
          welcome_text={welcome_text}
          check_upload_recording={check_upload_recording}
          disabledAudio={disabledAudio}
          ai_dm_pass_text={ai_dm_pass_text}
          ai_dm_pass_text_uploading_recording={
            ai_dm_pass_text_uploading_recording
          }
          ai_dm_fail_text={ai_dm_fail_text}
          ai_dm_fail_text_uploading_recording={
            ai_dm_fail_text_uploading_recording
          }
          Ivr_recording_Fields={Ivr_recording_Fields}
          deactivation_date={deactivation_date}
          handleInputValues={this.handleInputValues}
          handleInputValuesNew={this.handleInputValuesNew}
          handleAddIVRRule={this.handleAddIVRRule}
          handleErrors={handleErrors}
          handleInputValuesUpload2={this.handleInputValuesUpload2}
          handleInputValuesUpload1={this.handleInputValuesUpload1}
          handleAudioPlay={this.handleAudioPlay}
          playAudio={playAudio}
          stopAudio={stopAudio}
          successMsg={successMsg}
          isSaving={isSaving}
          errorMsg={errorMsg}
          isNotEmpty={isNotEmpty}
          playAudio1={playAudio1}
          playAudio2={playAudio2}
          playAudio3={playAudio3}
          playAudio4={playAudio4}
          welcome_text_audio_recording={welcome_text_audio_recording}
          languages={languages}
          audio_lang={audio_lang}
        />
      </Fragment>
    );
  }
}



export function mapStateToProps(state) {
  return {
    languages: state.fetchEndUserReducer.languages,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: () => dispatch(fetchLanguages()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IVRSettingsRules);


// export default IVRSettingsRules;
