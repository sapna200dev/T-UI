export const Ivr_Fields = [
  {
    id: 0,
    field_1: "Recording Text-1",
    field_1_value: "",
    field_1_check:false,
    field_2: "Playback Recording",
    field_2_value: "",
    field_2_check:false,
    field_3: "Beep at start of Recording",
    field_3_value: "no",
    field_3_check:false,
    field_4: "Recording Duration (sec)",
    field_4_value: 4,
    field_4_check:false,
    field_2_play:false
  },
  {
    id: 1, 
    field_1: "Recording Text-2", 
    field_1_value: "",
    field_1_check:false,
    field_2: "Playback Recording",
    field_2_value: "",
    field_2_check:false,
    field_3: "Beep at start of Recording",
    field_3_value: "no",
    field_3_check:false,
    field_4: "Recording Duration (sec)",
    field_4_value: 9,
    field_4_check:false,
    field_2_play:false
  },
  {
    id: 2,
    field_1: "Recording Text-3",
    field_1_value: "",
    field_1_check:false,
    field_2: "Playback Recording",
    field_2_value: "",
    field_2_check:false,
    field_3: "Beep at start of Recording",
    field_3_value: "no",
    field_3_check:false,
    field_4: "Recording Duration (sec)",
    field_4_value: 9,
    field_4_check:false,
    field_2_play:false
  },
];

export const activation_statuses_update = [
  { key: 0, text: "active", value: "Active" },
  { key: 1, text: "testing", value: "Testing" },
];

export const activation_statuses = [
  { key: 0, text: "testing", value: "Testing" },
];

export const recording_beep_start = [
  { key: 0, text: "yes", value: "Yes" },
  { key: 1, text: "no", value: "No" },
];

export const audio_check = [
  { key: 0, text: "1", value: "Recording -1" },
  { key: 1, text: "2", value: "Recording -2" },
  { key: 2, text: "3", value: "Recording -3" },
  { key: 3, text: "4", value: "Do Not Send" },
];


export const ivr_info = [
  { key: 0, text: "1", value: "Recording -1" },
  { key: 1, text: "2", value: "Recording -2" },
  { key: 2, text: "3", value: "Recording -3" },
  // { key: 3, text: "4", value: "Do Not Send" },
];


export const ERROR_VALIDATIONS = {
  // REGEX__EMAIL : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  TEXT__VALIDATION_ERROR: 'This is mandatory field',
  TEXT__VALIDATION_ERROR__PASSWORD_EMPTY : 'Mandatory Field !',
  TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_EMPTY : 'Mandatory Field !',
  TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH :'Confirm password must match with password.',
  TEXT__VALIDATION_ERROR__USERNAME_EMPTY : 'Please fill username',
  VALID_FIELD:'This field is mandatory and Please only enter numeric characters',
}


