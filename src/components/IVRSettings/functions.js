import { ERROR_VALIDATIONS } from "./contants";
import moment from "moment";

export const ValidateRequestParams = (params) => {
  const addBankErrors = {};
  const {
    total_recording,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    ai_dm_pass_text,
    ai_dm_fail_text,
    deactivation_date,
    welcome_text,
    check_upload_recording,
    audio_lang
  } = params;

  if (ivr_rules_set.length < 1) {
    addBankErrors.ivr_rules_set = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (bank_name.length < 1) {
    addBankErrors.bank_name = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (!/^[a-zA-Z]*$/g.test(bank_name)) {
    addBankErrors.bank_name = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (total_recording.length < 1) {
    addBankErrors.total_recording = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (audio_lang.length < 1) {
    addBankErrors.audio_lang = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (activation_status.length < 1) {
    addBankErrors.activation_status = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (welcome_text == "") {
    addBankErrors.welcome_text = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (ai_dm_pass_text.length < 1) {
    addBankErrors.ai_dm_pass_text = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (ai_dm_fail_text.length < 1) {
    addBankErrors.ai_dm_fail_text = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  }

  return addBankErrors;
};

/**
 * Handle Add Verification params
 * @param  props
 */
export const ValidateVerificationRequestParams = (params) => {
  const addBankErrors = {};
  const {
    total_recording,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    ai_dm_pass_text,
    ai_dm_fail_text,
    deactivation_date,
    welcome_text,
    audio_lang
  } = params;

  if (ivr_rules_set.length < 1) {
    addBankErrors.ivr_rules_set = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (bank_name.length < 1) {
    addBankErrors.bank_name = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (!/^[a-zA-Z]*$/g.test(bank_name)) {
    addBankErrors.bank_name = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (total_recording.length < 1) {
    addBankErrors.total_recording = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  }  else if (audio_lang.length < 1) {
    addBankErrors.audio_lang = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (activation_status.length < 1) {
    addBankErrors.activation_status = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (welcome_text == "") {
    addBankErrors.welcome_text = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  }  else if (ai_dm_pass_text.length < 1) {
    addBankErrors.ai_dm_pass_text = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  } else if (ai_dm_fail_text.length < 1) {
    addBankErrors.ai_dm_fail_text = ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR;
  }

  return addBankErrors;
};

/**
 * Makde form request params
 * @param  props
 */
export const prepareVerificationFormRequestParams = (props) => {
  const {
    total_recording,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    check_upload_recording,
    ai_dm_pass_text,
    ai_dm_pass_text_uploading_recording,
    ai_dm_fail_text,
    ai_dm_fail_text_uploading_recording,
    ivr_info_recording,
    ivr_info,
    Ivr_recording_Fields,
    deactivation_date,
    file_obj,
    welcome_text,
    welcome_text_audio_recording,
    audio_lang
  } = props;

  var formDataRequest = new FormData();

  // console.log('Ivr_recording_Fields Ivr_recording_Fields = ', Ivr_recording_Fields)
  formDataRequest.append(
    "ai_dm_pass_text_uploading_recording",
    ai_dm_pass_text_uploading_recording
  );
  formDataRequest.append(
    "ai_dm_fail_text_uploading_recording",
    ai_dm_fail_text_uploading_recording
  );
  formDataRequest.append("check_upload_recording", check_upload_recording);
  formDataRequest.append(
    "recording_fields",
    JSON.stringify(Ivr_recording_Fields)
  );
  formDataRequest.append("bank_name", bank_name);
  formDataRequest.append("total_recording", total_recording);
  formDataRequest.append("ivr_rules_set", ivr_rules_set);
  formDataRequest.append("activation_status", activation_status);
  formDataRequest.append("activation_date", activation_date);
  formDataRequest.append("check_audio", check_audio);
  formDataRequest.append("ai_dm_pass_text", ai_dm_pass_text);
  formDataRequest.append("ai_dm_fail_text", ai_dm_fail_text);
  formDataRequest.append("deactivation_date", deactivation_date);
  formDataRequest.append("welcome_text", welcome_text);
  formDataRequest.append("audio_lang", audio_lang);
  

  formDataRequest.append(
    "welcome_text_audio_recording",
    welcome_text_audio_recording
  );
  formDataRequest.append("idUpdate", false);
  formDataRequest.append("is_registration", 0);
  formDataRequest.append("ivr_info_recording", ivr_info_recording);
  formDataRequest.append("ivr_info", ivr_info);

  file_obj.forEach((element, index) => {
    formDataRequest.append("recording_fields_file_" + index, element);
  });
  // formDataRequest.append("files", JSON.stringify(file_obj));
  return formDataRequest;
};

/**
 * Makde form request params
 * @param  props
 */
export const prepareFormRequestParams = (props) => {
  const {
    total_recording,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    check_upload_recording,
    ai_dm_pass_text,
    ai_dm_pass_text_uploading_recording,
    ai_dm_fail_text,
    ai_dm_fail_text_uploading_recording,
    Ivr_recording_Fields,
    deactivation_date,
    file_obj,
    welcome_text,
    welcome_text_audio_recording,
    audio_lang
  } = props;

  console.log('audio language = ', audio_lang)
  var formDataRequest = new FormData();

  // console.log('Ivr_recording_Fields Ivr_recording_Fields = ', Ivr_recording_Fields)
  formDataRequest.append(
    "ai_dm_pass_text_uploading_recording",
    ai_dm_pass_text_uploading_recording
  );
  formDataRequest.append(
    "ai_dm_fail_text_uploading_recording",
    ai_dm_fail_text_uploading_recording
  );
  formDataRequest.append("check_upload_recording", check_upload_recording);
  formDataRequest.append(
    "recording_fields",
    JSON.stringify(Ivr_recording_Fields)
  );
  formDataRequest.append("bank_name", bank_name);
  formDataRequest.append("total_recording", total_recording);
  formDataRequest.append("ivr_rules_set", ivr_rules_set);
  formDataRequest.append("activation_status", activation_status);
  formDataRequest.append("activation_date", activation_date);
  formDataRequest.append("check_audio", check_audio);
  formDataRequest.append("ai_dm_pass_text", ai_dm_pass_text);
  formDataRequest.append("ai_dm_fail_text", ai_dm_fail_text);
  formDataRequest.append("deactivation_date", deactivation_date);
  formDataRequest.append("welcome_text", welcome_text);
  formDataRequest.append("audio_lang", audio_lang);
  formDataRequest.append(
    "welcome_text_audio_recording",
    welcome_text_audio_recording
  );
  formDataRequest.append("idUpdate", false);
  formDataRequest.append("is_registration", 1);

  file_obj.forEach((element, index) => {
    formDataRequest.append("recording_fields_file_" + index, element);
  });
  // formDataRequest.append("files", JSON.stringify(file_obj));
  return formDataRequest;
};

/**
 * Makde form request params
 * @param  props
 */
export const prepareUpdateVerificationFormRequestParams = (props) => {
  const {
    total_recording,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    check_upload_recording,
    ai_dm_pass_text,
    ai_dm_pass_text_uploading_recording,
    ai_dm_fail_text,
    ai_dm_fail_text_uploading_recording,
    Ivr_recording_Fields,
    deactivation_date,
    file_obj,
    welcome_text,
    welcome_text_audio_recording,
    id,
    ivr_info_recording,
    ivr_info,
    audio_lang,
  } = props;

  let activation_date_param = activation_date && activation_date != 'Invalid date'
    ? moment(activation_date).format("YYYY-MM-DD HH::mm")
    : null;
  let deactivation_date_param = deactivation_date && deactivation_date != null && deactivation_date != 'Invalid date'
    ? moment(deactivation_date).format("YYYY-MM-DD HH::mm")
    : null;

  var formDataRequest = new FormData();
  
  formDataRequest.append(
    "ai_dm_pass_text_uploading_recording",
    ai_dm_pass_text_uploading_recording
  );
  formDataRequest.append(
    "ai_dm_fail_text_uploading_recording",
    ai_dm_fail_text_uploading_recording
  );
  formDataRequest.append("check_upload_recording", check_upload_recording); 
  formDataRequest.append(
    "recording_fields",
    JSON.stringify(Ivr_recording_Fields)
  );
  formDataRequest.append("bank_name", bank_name);
  formDataRequest.append("total_recording", total_recording);
  formDataRequest.append("ivr_rules_set", ivr_rules_set);
  formDataRequest.append("activation_status", activation_status);
  formDataRequest.append("activation_date", activation_date_param);
  formDataRequest.append("check_audio", check_audio);
  formDataRequest.append("ai_dm_pass_text", ai_dm_pass_text);
  formDataRequest.append("ai_dm_fail_text", ai_dm_fail_text);
  formDataRequest.append("deactivation_date", deactivation_date_param);
  formDataRequest.append("welcome_text", welcome_text);
  formDataRequest.append("ivr_info_recording", ivr_info_recording);
  formDataRequest.append("ivr_info", ivr_info);
  formDataRequest.append("audio_lang", audio_lang);
  
  formDataRequest.append(
    "welcome_text_audio_recording",
    welcome_text_audio_recording
  );
  formDataRequest.append("id", id);
  formDataRequest.append("isUpdate", true);
  formDataRequest.append("is_registration", 0);

  file_obj.forEach((element, index) => {
    formDataRequest.append("recording_fields_file_" + index, element);
         formDataRequest.append("row_index" , element.row_index);

  });
  // formDataRequest.append("files", JSON.stringify(file_obj));
  return formDataRequest; 
};

/**
 * Makde form request params
 * @param  props
 */
export const prepareUpdateFormRequestParams = (props) => {
  const {
    total_recording,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    check_upload_recording,
    ai_dm_pass_text,
    ai_dm_pass_text_uploading_recording,
    ai_dm_fail_text,
    ai_dm_fail_text_uploading_recording,
    Ivr_recording_Fields,
    deactivation_date,
    file_obj,
    welcome_text,
    welcome_text_audio_recording,
    id,
    audio_lang
  } = props;

console.log('file_obj = ', file_obj)
  let activation_date_param = activation_date && activation_date != 'Invalid date'
    ? moment(activation_date).format("YYYY-MM-DD HH::mm")
    : null;
  let deactivation_date_param = deactivation_date && deactivation_date != null && deactivation_date != 'Invalid date'
    ? moment(deactivation_date).format("YYYY-MM-DD HH::mm")
    : null;

    console.log('update rules file_obj= ', file_obj)

  var formDataRequest = new FormData();
  formDataRequest.append(
    "ai_dm_pass_text_uploading_recording",
    ai_dm_pass_text_uploading_recording
  );
  formDataRequest.append(
    "ai_dm_fail_text_uploading_recording",
    ai_dm_fail_text_uploading_recording
  );
  formDataRequest.append("check_upload_recording", check_upload_recording);
  formDataRequest.append(
    "recording_fields",
    JSON.stringify(Ivr_recording_Fields)
  );
  formDataRequest.append("bank_name", bank_name);
  formDataRequest.append("total_recording", total_recording);
  formDataRequest.append("ivr_rules_set", ivr_rules_set);
  formDataRequest.append("activation_status", activation_status);
  formDataRequest.append("activation_date", activation_date_param);
  formDataRequest.append("check_audio", check_audio);
  formDataRequest.append("ai_dm_pass_text", ai_dm_pass_text);
  formDataRequest.append("ai_dm_fail_text", ai_dm_fail_text);
  formDataRequest.append("deactivation_date", deactivation_date_param);
  formDataRequest.append("welcome_text", welcome_text);
  formDataRequest.append("audio_lang", audio_lang);
  
  formDataRequest.append(
    "welcome_text_audio_recording",
    welcome_text_audio_recording
  );
  formDataRequest.append("id", id);
  formDataRequest.append("isUpdate", true);
  formDataRequest.append("is_registration", 1);

  file_obj.forEach((element, index) => {
    console.log('update rules file_obj element = ', element)

    formDataRequest.append("recording_fields_file_" + index, element);
    formDataRequest.append("row_index" , element.row_index);
  });
  // formDataRequest.append("files", JSON.stringify(file_obj));
  return formDataRequest;
};
