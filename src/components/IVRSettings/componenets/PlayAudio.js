import React, { Fragment } from "react";
import "./PlayStyles.css";

const audiocss = {
  // position:'fixed',
  // bottom:'0',
  // width:'100%'
  position: "fixed",
  // height: "200px",
  left: "98%",
  top: "83%",
  marginLeft: "-300px",
};

export const PlayAudio = (props) => {
  const { audio } = props;
  return (
    <Fragment>
     <span style={audiocss}>
       <h4 style={{fontSize:'16px', fontWeight:'800'}}>Recording Preview</h4>
       <audio controls preload="metadata" id="audio" >
        <source type="audio/mp3" id="cusomaudios" />
        
      </audio>
      {/* <audio controls style={audiocss} id="audio">
        <source type="audio/mp3" />
      </audio> */}
     </span>
    </Fragment>
  );
};
