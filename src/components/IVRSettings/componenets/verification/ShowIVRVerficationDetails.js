import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import {
  ContentLoading,
  InvertContentLoader,
} from "../../../Comman/components";
import {
  activation_statuses_update,
  Ivr_Fields,
  recording_beep_start,
  audio_check,
  ivr_info,
} from "../../contants";
import { PlayAudio } from "./../PlayAudio";
import { PlaySvgIcon, StopSvgIcon } from "../../../Comman/Icons";

const label_css = { 
  display: "block",
  width: "100%",
  height: "calc(1.5em + 0.75rem + 2px)",
  padding: "0.375rem 0.75rem",
  fontSize: "0.875rem",
  fontWeight: "400",
  lineHeight: "1.5",
  backgroundClip: "padding-box",
  border: "0px solid",
  color: "#768192",
  backgroundColor: "#fff",
  borderColor: "#d8dbe0",
  borderRadius: "0.25rem",
  transition: "border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out",
};
const stylePlay = {
  display: "flex",
};

const error_msg = {
  fontSize: "10px",
  color: "red",
  fontFamily: "Roboto",
};
export const ShowIVRVerficationDetails = (props) => {
  const {
    total_recording,
    handleInputValues,
    Ivr_recording_Fields,
    handleInputValuesNew,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    check_upload_recording,
    ai_dm_pass_text,
    ai_dm_pass_text_uploading_recording,
    ai_dm_fail_text,
    ai_dm_fail_text_uploading_recording,
    handleErrors,
    handleInputValuesUpload1,
    handleAudioPlay,
    successMsg,
    errorMsg,
    isNotEmpty,
    playAudio1,
    playAudio2,
    playAudio3,
    disabledAudio,
    welcome_text,
    playAudio4,
    welcome_text_audio_recording,
    isFatching,
    ivr_info_recording,
    playAudio5,
    ivr_info_val,
    languages,
    audio_lang,
  } = props;

  let audio_msg = "Check Audio";

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}>Verification - Details IVR Rules</b>
        </CCardHeader>
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="3">
                {isFatching && <ContentLoading content="Fetching record.." />}
                <CLabel htmlFor="ivr_rules_set">
                  IVR Rules Set<small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ivr_rules_set"
                  name="ivr_rules_set"
                  value={ivr_rules_set}
                  placeholder="IVR Rules Set"
                  onChange={handleInputValues}
                  invalid={handleErrors.ivr_rules_set}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="bank_name">
                  Bank Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="bank_name"
                  name="bank_name"
                  value={bank_name}
                  placeholder="Bank Name"
                  onChange={handleInputValues}
                  invalid={handleErrors.bank_name}
                  style={{ pointerEvents: "none" }}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="total_recording">
                  Total Recordings <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="total_recording"
                  name="total_recording"
                  value={total_recording}
                  placeholder="Total Recordings"
                  onChange={handleInputValues}
                  invalid={handleErrors.total_recording}
                  className="detail-text-color"
                />
              </CCol>

              <CCol xs="12" md="3">
              <CLabel htmlFor="audio_lang">
              Audio Language <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="audio_lang"
                  id="audio_lang"
                  invalid={handleErrors.audio_lang}
                  onChange={handleInputValues}
                  value={audio_lang}
                  // className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {languages ? (
                    languages.map((item) => (
                      <option value={item.id}>{item.language}</option>
                    ))
                  ) : (
                    <option value="">No Languages</option>
                  )}
                </CSelect>

              
                <CInvalidFeedback>
                  {handleErrors.audio_lang}
                </CInvalidFeedback>
              </CCol>

            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="activation_status">
                  Activation Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="activation_status"
                  id="activation_status"
                  invalid={handleErrors.activation_status}
                  onChange={handleInputValues}
                  // value={activation_status}
                  className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {activation_statuses_update.length > 0 ? (
                    activation_statuses_update.map((item) => (
                      <option
                        value={item.text}
                        selected={
                          item.text == activation_status ? "selected" : ""
                        }
                      >
                        {item.value}
                      </option>
                    ))
                  ) : (
                    <option value="">No Statuses</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="activation_date">
                  {activation_status == "active" && (
                    <Fragment>
                      Activation Date{" "}
                      <small className="required_lable">*</small>
                    </Fragment>
                  )}{" "}
                </CLabel>
                {activation_status == "active" && (
                  <Fragment>
                    <CInput
                      type="date"
                      id="activation_date"
                      name="activation_date"
                      value={activation_date}
                      placeholder="Activation Date"
                      onChange={handleInputValues}
                      invalid={activation_date == "" ? true : false}
                      className="detail-text-color"
                    />
                  </Fragment>
                )}
              </CCol>
              <CCol xs="12" md="3">
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="welcome_text">
                  Welcome Text <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="welcome_text"
                  name="welcome_text"
                  value={welcome_text}
                  placeholder="Welcome Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.welcome_text}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                <span style={label_css}>
                    <a
                      href={welcome_text_audio_recording}
                      target="_blank"
                    >
                      {audio_msg}
                    </a>
                  </span>
                  {welcome_text_audio_recording ? (
                    playAudio4 ? (
                      <StopSvgIcon
                        type="welcome_text_audio_recording"
                        src={welcome_text_audio_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio4}
                      />
                    ) : (
                      <PlaySvgIcon
                        type="welcome_text_audio_recording"
                        src={welcome_text_audio_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio4}
                      />
                    )
                  ) : null}
                </span>
              </CCol>
            </CFormGroup>

            {Ivr_recording_Fields &&
              Ivr_recording_Fields.map((fields, index) => {
                return (
                  <Fragment>
                    <CFormGroup row>
                      <CCol xs="12" md="3">
                        <CLabel htmlFor="recording_text">
                          {fields.field_1}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <CInput
                          // id="field_1_value"
                          name="field_1_value"
                          value={fields.field_1_value}
                          placeholder={fields.field_1}
                          onChange={(e) =>
                            handleInputValuesNew(e, index, "field_1_value")
                          }
                          invalid={fields.field_1_check}
                          className="detail-text-color"
                        />
                      </CCol>
                      <CCol xs="12" md="3">
                        <CLabel>
                          {" "}
                          {fields.field_2}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <span>
                          <span style={label_css}>
                            <a href={fields.field_2_value} target="_blank">
                              {audio_msg}
                            </a>
                          </span>
                        </span>

                        <div style={{ float: "right", marginTop: "-25px" }}>
                          {fields.field_2_value ? (
                            fields.field_2_play ? (
                              <StopSvgIcon
                                type={fields.field_2_play}
                                index={index}
                                dyna={"true"}
                                src={fields.field_1_value}
                                handleAudioPlay={handleAudioPlay}
                                value={fields.field_2_play}
                              />
                            ) : (
                              <PlaySvgIcon
                                type={fields.field_2_play}
                                index={index}
                                dyna={"true"}
                                src={fields.field_1_value}
                                handleAudioPlay={handleAudioPlay}
                                value={fields.field_2_play}
                              />
                            )
                          ) : null}
                        </div>
                      </CCol>
                      <CCol xs="12" md="3">
                        <CLabel htmlFor="recording_beep_start">
                          {fields.field_3}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <CSelect
                          custom
                          name="field_3_value"
                          // value={fields.field_3_value}
                          placeholder={fields.field_3}
                          onChange={(e) =>
                            handleInputValuesNew(e, index, "field_3_value")
                          }
                          invalid={fields.field_3_check}
                          className="detail-text-color"
                        >
                          <option value="">Please select</option>
                          {recording_beep_start.length > 0 ? (
                            recording_beep_start.map((item) => (
                              <option
                                value={item.key}
                                selected={
                                  item.text == fields.field_3_value
                                    ? "selected"
                                    : ""
                                }
                              >
                                {item.value}
                              </option>
                            ))
                          ) : (
                            <option value="">No Beeps</option>
                          )}
                        </CSelect>
                      </CCol>
                      <CCol xs="12" md="3">
                        <CLabel htmlFor="recording_duration">
                          {fields.field_4}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <CInput
                          type="text"
                          name="field_4_value"
                          value={fields.field_4_value}
                          placeholder={fields.field_4}
                          onChange={(e) =>
                            handleInputValuesNew(e, index, "field_4_value")
                          }
                          invalid={fields.field_4_check}
                          className="detail-text-color"
                        />
                      </CCol>
                    </CFormGroup>
                  </Fragment>
                );
              })}

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="check_audio">
                  Check Audio <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="check_audio"
                  id="check_audio"
                  invalid={handleErrors.check_audio}
                  onChange={handleInputValues}
                  className="detail-text-color"
                  value={check_audio}
                >
                  <option value="">Please select</option>
                  {audio_check.length > 0 ? (
                    audio_check.map((item) => (
                      <option value={item.key}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Audios</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                {check_audio != "do_not_send" && (
                  <Fragment>
                    <CLabel>
                      Playback Recording{" "}
                      <small className="required_lable">*</small>
                    </CLabel>
                    <span style={stylePlay}>
                      <span style={label_css}>
                        <a href={check_upload_recording} target="_blank">
                          {audio_msg}
                        </a>
                      </span>

                      {check_upload_recording ? (
                        playAudio1 ? (
                          <StopSvgIcon
                            type="check_upload_recording"
                            src={check_upload_recording}
                            handleAudioPlay={handleAudioPlay}
                            value={playAudio1}
                          />
                        ) : (
                          <PlaySvgIcon
                            type="check_upload_recording"
                            src={check_upload_recording}
                            handleAudioPlay={handleAudioPlay}
                            value={playAudio1}
                          />
                        )
                      ) : null}
                    </span>
                  </Fragment>
                )}

                <CInvalidFeedback>
                  {handleErrors.check_upload_recording}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>

    

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ivr_info">
                IVR Info <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="ivr_info"
                  id="ivr_info"
                  invalid={handleErrors.ivr_info}
                  onChange={handleInputValues}
                  value={ivr_info_val}
                >
                  <option value="">Please select</option>
                  {ivr_info.length > 0 ? (
                    ivr_info.map((item) => (
                      <option value={ivr_info_val}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Audios</option>
                  )}
                </CSelect>

                {/* <CInput
                  id="ivr_info"
                  name="ivr_info"
                  value={ivr_info_val}
                  placeholder="AI-DM Pass Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.ivr_info}
                  className="detail-text-color"
                /> */}
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                  <span style={label_css}>
                    <a
                      href={ivr_info_recording}
                      target="_blank"
                    >
                      {audio_msg}
                    </a>
                  </span>
                  <div>
                    {ivr_info_recording ? (
                      playAudio5 ? (
                        <StopSvgIcon
                          type="ivr_info_recording"
                          src={ivr_info_recording}
                          handleAudioPlay={handleAudioPlay}
                          value={playAudio5}
                        />
                      ) : (
                        <PlaySvgIcon
                          type="ivr_info_recording"
                          src={ivr_info_recording}
                          handleAudioPlay={handleAudioPlay}
                          value={playAudio5}
                        />
                      )
                    ) : null}
                  </div>
                </span>
              </CCol>
            </CFormGroup>











            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ai_dm_pass_text">
                  AI-DM Success Text <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ai_dm_pass_text"
                  name="ai_dm_pass_text"
                  value={ai_dm_pass_text}
                  placeholder="AI-DM Pass Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.ai_dm_pass_text}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                  <span style={label_css}>
                    <a
                      href={ai_dm_pass_text_uploading_recording}
                      target="_blank"
                    >
                      {audio_msg}
                    </a>
                  </span>
                  <div>
                    {ai_dm_pass_text_uploading_recording ? (
                      playAudio2 ? (
                        <StopSvgIcon
                          type="ai_dm_pass_text_uploading_recording"
                          src={ai_dm_pass_text_uploading_recording}
                          handleAudioPlay={handleAudioPlay}
                          value={playAudio2}
                        />
                      ) : (
                        <PlaySvgIcon
                          type="ai_dm_pass_text_uploading_recording"
                          src={ai_dm_pass_text_uploading_recording}
                          handleAudioPlay={handleAudioPlay}
                          value={playAudio2}
                        />
                      )
                    ) : null}
                  </div>
                </span>
              </CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ai_dm_fail_text">
                  AI-DM Failure Text <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ai_dm_fail_text"
                  name="ai_dm_fail_text"
                  value={ai_dm_fail_text}
                  placeholder="AI-DM Fail Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.ai_dm_fail_text}
                  className="detail-text-color"
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                  <span style={label_css}>
                    <a
                      href={ai_dm_fail_text_uploading_recording}
                      target="_blank"
                    >
                      {audio_msg}
                    </a>
                  </span>
                  {ai_dm_fail_text_uploading_recording ? (
                    playAudio3 ? (
                      <StopSvgIcon
                        type="ai_dm_fail_text_uploading_recording"
                        src={ai_dm_fail_text_uploading_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio3}
                      />
                    ) : (
                      <PlaySvgIcon
                        type="ai_dm_fail_text_uploading_recording"
                        src={ai_dm_fail_text_uploading_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio3}
                      />
                    )
                  ) : null}
                </span>
              </CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>
        <CCardFooter>
          <a href="#/ivr-verification-settings" className="cancel_bt">
            <CButton
              type="reset"
              size="sm"
              color="primary"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> GO BACK
            </CButton>
          </a>

          {!disabledAudio && <PlayAudio />}
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
