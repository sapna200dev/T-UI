import React, { Fragment } from "react";
import moment from 'moment';
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
  CTooltip
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import { ContentLoading, InvertContentLoader } from "../../../Comman/components";
import {
  activation_statuses_update,
  recording_beep_start,
  audio_check,
  ivr_info
} from "../../contants";
import { PlayAudio } from "./../PlayAudio";
import { PlaySvgIcon, StopSvgIcon } from "../../../Comman/Icons";


const stylePlay = {
  display: "flex",
  
};

const error_msg = {
  fontSize: "10px",
  color: "red",
  fontFamily: "Roboto",
};
export const EditIvrVerification = (props) => {
  const {
    total_recording,
    handleInputValues,
    Ivr_recording_Fields,
    handleInputValuesNew,
    bank_name,
    ivr_rules_set,
    activation_status,
    activation_date,
    check_audio,
    check_upload_recording,
    ai_dm_pass_text,
    ai_dm_pass_text_uploading_recording,
    ai_dm_fail_text,
    ai_dm_fail_text_uploading_recording,
    deactivation_date,
    handleAddIVrVerificationRule,
    handleErrors,
    handleInputValuesUpload2,
    handleInputValuesUpload1,
    handleAudioPlay,
    playAudio,
    stopAudio,
    setAudioSrc,
    isSaving,
    successMsg,
    errorMsg,
    isNotEmpty,
    playAudio1,
    playAudio2,
    playAudio3,
    disabledAudio,
    welcome_text,
    playAudio4,
    playAudio5,
    ivr_info_recording,
    isFatching,
    welcome_text_audio_recording,
    ivr_info_val,
    languages,
    audio_lang
  } = props;


  const dateAndTime = moment().add(1, 'day').endOf('day').toISOString().split('T')
  const time = dateAndTime[1].split(':')

  // dateAndTime[0]+'T'+time[0]+':'+time[1]
  const tomorrow =  dateAndTime[0]+'T23:59'

  console.log(
    "tomorrow = ",
    tomorrow
  );
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}>Verification - Update IVR Rules</b>
        </CCardHeader>
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post" 
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="3">
              {isFatching && <ContentLoading content="Fetching record.." />}
                <CLabel htmlFor="ivr_rules_set">
                  IVR Rules Set <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ivr_rules_set"
                  name="ivr_rules_set"
                  value={ivr_rules_set}
                  placeholder="IVR Rules Set"
                  onChange={handleInputValues}
                  invalid={handleErrors.ivr_rules_set}
                />
                <CInvalidFeedback>
                  {handleErrors.ivr_rules_set}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
             
                <CLabel htmlFor="bank_name">
                  Bank Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="bank_name"
                  name="bank_name"
                  value={bank_name}
                  placeholder="Bank Name"
                  onChange={handleInputValues}
                  invalid={handleErrors.bank_name}
                  style={{ pointerEvents: "none" }}
                />
                <CInvalidFeedback>{handleErrors.bank_name}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="total_recording">
                  Total Recordings <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="total_recording"
                  name="total_recording"
                  value={total_recording}
                  placeholder="Total Recordings"
                  onChange={handleInputValues}
                  invalid={handleErrors.total_recording}
                />
                <CInvalidFeedback>
                  {handleErrors.total_recording}
                </CInvalidFeedback> 
              </CCol>

              <CCol xs="12" md="3">
              <CLabel htmlFor="audio_lang">
              Audio Language <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="audio_lang"
                  id="audio_lang"
                  invalid={handleErrors.audio_lang}
                  onChange={handleInputValues}
                  value={audio_lang}
                  // className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {languages ? (
                    languages.map((item) => (
                      <option value={item.id}>{item.language}</option>
                    ))
                  ) : (
                    <option value="">No Languages</option>
                  )}
                </CSelect>

              
                <CInvalidFeedback>
                  {handleErrors.audio_lang}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="activation_status">
                  Activation Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="activation_status"
                  id="activation_status"
                  invalid={handleErrors.activation_status}
                  onChange={handleInputValues}
                  // value={activation_status}
                >
                  <option value="">Please select</option>
                  {activation_statuses_update.length > 0 ? (
                    activation_statuses_update.map((item) => (
                      <option
                        value={item.text}
                        selected={
                          item.text == activation_status ? "selected" : ""
                        }
                      >
                        {item.value}
                      </option>
                    ))
                  ) : (
                    <option value="">No Statuses</option>
                  )}
                </CSelect>

                <CInvalidFeedback>
                  {handleErrors.activation_status}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="activation_date">
                  {activation_status == "active"
                    &&  (
                      <Fragment>
                            Activation Date     <small className="required_lable">*</small>
                      </Fragment>
                    )}{" "}
                   
                </CLabel>
                {activation_status == "active" &&  (
                  <Fragment>
                    <input 
                      className="form-control" 
                      id="party"
                      type="datetime-local" 
                      // name="partydate"
                      // min="2020-11-04T08:30" 
                      min={tomorrow}
                      
                      //  max="2017-06-30T16:30"
                      onChange={handleInputValues}
                      invalid={activation_date == "" ? true : false}
                      value={activation_date}
                      id="activation_date"
                      name="activation_date"
                    />
                    {/* <CInput
                      type="date"
                      id="activation_date"
                      name="activation_date"
                      value={activation_date}
                      placeholder="Activation Date"
                      onChange={handleInputValues}
                      invalid={activation_date == "" ? true : false}
                    /> */}
                    <CInvalidFeedback>
                      {activation_date == "" ? "This is mandatory field" : ""}
                    </CInvalidFeedback>
                  </Fragment>
                ) }
              </CCol>
              <CCol xs="12" md="3"></CCol>
            </CFormGroup>
            <CFormGroup row>
            <CCol xs="12" md="3">
                <CLabel htmlFor="welcome_text">
                  Welcome ffffText <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="welcome_text"
                  name="welcome_text"
                  value={welcome_text}
                  placeholder="Welcome Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.welcome_text}
                />
                <CInvalidFeedback>
                  {handleErrors.welcome_text}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                  <CInputFile
                    // custom
                    id="custom-file-input"
                    name="welcome_text_audio_recording"
                    onChange={(e) =>
                      handleInputValuesUpload1(
                        e,
                        "welcome_text_audio_recording"
                      )
                    }
                  />
                  {welcome_text_audio_recording ? (
                    playAudio4 ? (
                      <StopSvgIcon
                        type="welcome_text_audio_recording"
                        src={welcome_text_audio_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio4}
                      />
                    ) : (
                      <PlaySvgIcon
                        type="welcome_text_audio_recording"
                        src={welcome_text_audio_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio4}
                      />
                    )
                  ) : null}
                  {/* <CLabel
                    htmlFor="custom-file-input"
                    variant="custom-file"
                    style={label_css}
                  >
                    Upload Recording
                  </CLabel> */}
                </span>
                <span style={error_msg}>Upload mp3 files only !</span>
                <CInvalidFeedback>
                  {handleErrors.ai_dm_fail_text_uploading_recording}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>



            {Ivr_recording_Fields &&
              Ivr_recording_Fields.map((fields, index) => {
                return (
                  <Fragment>
                    <CFormGroup row>
                      <CCol xs="12" md="3">
                        <CLabel htmlFor="recording_text">
                          {fields.field_1}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <CInput
                          // id="field_1_value"
                          name="field_1_value"
                          value={fields.field_1_value}
                          placeholder={fields.field_1}
                          onChange={(e) =>
                            handleInputValuesNew(e, index, "field_1_value")
                          }
                          invalid={fields.field_1_check}
                        />

                        <CInvalidFeedback>
                          {fields.field_1_check
                            ? "This is mandatory field"
                            : ""}
                        </CInvalidFeedback>
                      </CCol>
                      <CCol xs="12" md="3">
                        <CLabel>
                          {" "}
                          {fields.field_2}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <span>
                          <CInputFile
                            // custom
                            id="custom-file-input"
                            name="field_2_value"
                            // value={fields.field_2_value}
                            placeholder={fields.field_2}
                            onChange={(e) =>
                              handleInputValuesUpload2(
                                e,
                                index,
                                "field_2_value"
                              )
                            }
                            invalid={fields.field_2_check}
                          />
                        </span>
                        
                        <div style={{ float: "right", marginTop: '-25px' }}>
                          {fields.field_2_value ? (
                            fields.field_2_play ? (
                              <StopSvgIcon
                                type={fields.field_2_play}
                                index={index}
                                dyna={"true"}
                                src={fields.field_1_value}
                                handleAudioPlay={handleAudioPlay}
                                value={fields.field_2_play}
                              />
                            ) : (
                              <PlaySvgIcon
                                type={fields.field_2_play}
                                index={index}
                                dyna={"true"}
                                src={fields.field_1_value}
                                handleAudioPlay={handleAudioPlay}
                                value={fields.field_2_play}
                              />
                            )
                          ) : null}
                        </div>
                       
                        <span style={{ display: "-webkit-box" }}>
                          <span style={error_msg}>
                            Upload mp3 files only !
                          </span>
                          <CInvalidFeedback
                            style={
                              isNotEmpty
                                ? {
                                    display: "block",
                                    width: "100%",
                                    marginTop: " 3px",
                                    fontSize: "80%",
                                    color: "#e55353",
                                    fontFamily: "Roboto",
                                    marginLeft: "6px",
                                  }
                                : {}
                            }
                          >
                            {isNotEmpty ? "This is mandatory field" : ""}
                          </CInvalidFeedback>
                        </span>
                      </CCol>
                      <CCol xs="12" md="3">
                        <CLabel htmlFor="recording_beep_start">
                          {fields.field_3}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <CSelect
                          custom
                          name="field_3_value"
                          // value={fields.field_3_value}
                          placeholder={fields.field_3}
                          onChange={(e) =>
                            handleInputValuesNew(e, index, "field_3_value")
                          }
                          invalid={fields.field_3_check}
                          className="detail-text-color"
                        >
                          <option value="">Please select</option>
                          {recording_beep_start.length > 0 ? (
                            recording_beep_start.map((item) => (
                              <option
                                value={item.text}
                                selected={
                                  item.text == fields.field_3_value
                                    ? "selected"
                                    : ""
                                }
                              >
                                {item.value}
                              </option>
                            ))
                          ) : (
                            <option value="">No Beeps</option>
                          )}
                        </CSelect>
                        <CInvalidFeedback>
                          {fields.field_3_check
                            ? "This is mandatory field"
                            : ""}
                        </CInvalidFeedback>
                      </CCol>
                      <CCol xs="12" md="3">
                        <CLabel htmlFor="recording_duration">
                          {fields.field_4}{" "}
                          <small className="required_lable">*</small>
                        </CLabel>
                        <CInput
                          type="text"
                          name="field_4_value"
                          value={fields.field_4_value}
                          placeholder={fields.field_4}
                          onChange={(e) =>
                            handleInputValuesNew(e, index, "field_4_value")
                          }
                          invalid={fields.field_4_check}
                          className="detail-text-color"
                        />
                        <CInvalidFeedback>
                          {fields.field_4_check
                            ? "This is mandatory field"
                            : ""}
                        </CInvalidFeedback>
                      </CCol>
                    </CFormGroup>
                  </Fragment>
                );
              })}

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="check_audio">
                  Check Audio <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="check_audio"
                  id="check_audio"
                  invalid={handleErrors.check_audio}
                  onChange={handleInputValues}
                  value={2}
                  className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {audio_check.length > 0 ? (
                    audio_check.map((item) => (
                      <option value={item.key}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Audios</option>
                  )}
                </CSelect>
                <CInvalidFeedback>{handleErrors.check_audio}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                {check_audio != 'do_not_send' && (
                  <Fragment>
                    <CLabel>
                        Playback Recording <small className="required_lable">*</small>
                      </CLabel>
                      <span style={stylePlay}>
                        <CInputFile
                          // custom
                          id="custom-file-input"
                          name="check_upload_recording"
                          onChange={(e) =>
                            handleInputValuesUpload1(e, "check_upload_recording")
                          }
                        />
                        
                        {check_upload_recording ? (
                          playAudio1 ? (
                            <StopSvgIcon
                              type="check_upload_recording"
                              src={check_upload_recording}
                              handleAudioPlay={handleAudioPlay}
                              value={playAudio1}
                            />
                          ) : (
                            <PlaySvgIcon 
                              type="check_upload_recording"
                              src={check_upload_recording}
                              handleAudioPlay={handleAudioPlay}
                              value={playAudio1}
                            />
                          )
                        ) : null}
                       
                        {/* <CLabel
                          htmlFor="custom-file-input"
                          variant="custom-file"
                          style={label_css}
                        >
                          Upload Recording
                        </CLabel> */}
                      </span>
                      <span style={error_msg}>Upload mp3 files only !</span>
                  </Fragment>
                )}
  
                <CInvalidFeedback>
                  {handleErrors.check_upload_recording}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>


            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ivr_info">
                 IVR Info <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="ivr_info"
                  id="ivr_info"
                  invalid={handleErrors.ivr_info}
                  onChange={handleInputValues}
                  value={2}
                  className="detail-text-color"
                >
                  <option value="">Please select</option>
                  {ivr_info.length > 0 ? (
                    ivr_info.map((item) => ( 
                      <option value={item.key}>{item.value}</option>
                    ))
                  ) : (
                    <option value="">No Audios</option>
                  )}
                </CSelect>
                <CInvalidFeedback>{handleErrors.ivr_info}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <Fragment>
                    <CLabel>
                      Playback Recording{" "}
                      <small className="required_lable">*</small>
                    </CLabel>
                    <span style={stylePlay}>
                      <CInputFile
                        // custom
                        id="custom-file-input"
                        name="ivr_info_recording"
                        onChange={(e) =>
                          handleInputValuesUpload1(e, "ivr_info_recording")
                        }
                      />

                      {ivr_info_recording ? (
                        playAudio5 ? (
                          <StopSvgIcon
                            type="ivr_info_recording"
                            src={ivr_info_recording}
                            handleAudioPlay={handleAudioPlay}
                            value={playAudio5}
                          />
                        ) : (
                          <PlaySvgIcon
                            type="ivr_info_recording"
                            src={ivr_info_recording}
                            handleAudioPlay={handleAudioPlay}
                            value={playAudio5}
                          />
                        )
                      ) : null}

                      {/* <CLabel
                          htmlFor="custom-file-input"
                          variant="custom-file"
                          style={label_css}
                        >
                          Upload Recording
                        </CLabel> */}
                    </span>
                    <span style={error_msg}>Upload mp3 files only !</span>
                  </Fragment>

                <CInvalidFeedback>
                  {handleErrors.check_upload_recording}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ai_dm_pass_text">
                  AI-DM Success Text <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ai_dm_pass_text"
                  name="ai_dm_pass_text"
                  value={ai_dm_pass_text}
                  placeholder="AI-DM Pass Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.ai_dm_pass_text}
                />
                <CInvalidFeedback>
                  {handleErrors.ai_dm_pass_text}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                  <CInputFile
                    // custom
                    id="custom-file-input"
                    name="ai_dm_pass_text_uploading_recording"
                    // value={ai_dm_pass_text_uploading_recording}
                    onChange={(e) =>
                      handleInputValuesUpload1(
                        e,
                        "ai_dm_pass_text_uploading_recording"
                      )
                    }
                  />
                   
                    <div>
                    {ai_dm_pass_text_uploading_recording ? (
                    playAudio2 ? (
                      <StopSvgIcon
                        type="ai_dm_pass_text_uploading_recording"
                        src={ai_dm_pass_text_uploading_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio2}
                      />
                    ) : (
                      <PlaySvgIcon
                        type="ai_dm_pass_text_uploading_recording"
                        src={ai_dm_pass_text_uploading_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio2}
                      />
                    )
                  ) : null}
                    </div>
                 
                  {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="c-icon c-icon-2xl" role="img"><path fill="var(--ci-primary-color, currentColor)" d="M444.4,235.236,132.275,49.449A24,24,0,0,0,96,70.072V434.214a24.017,24.017,0,0,0,35.907,20.839L444.03,276.7a24,24,0,0,0,.367-41.461ZM128,420.429V84.144L416.244,255.718Z" class="ci-primary"></path></svg> */}
                  {/* <CLabel
                    htmlFor="custom-file-input"
                    variant="custom-file"
                    style={label_css}
                  >
                    Upload Recording
                  </CLabel> */}
                </span>
                <span>
                  <span style={error_msg}>Upload mp3 files only !</span>
                  <CInvalidFeedback>
                    {handleErrors.ai_dm_pass_text_uploading_recording}
                  </CInvalidFeedback>
                </span>
              </CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="3">
                <CLabel htmlFor="ai_dm_fail_text">
                  AI-DM Failure Text <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="ai_dm_fail_text"
                  name="ai_dm_fail_text"
                  value={ai_dm_fail_text}
                  placeholder="AI-DM Fail Text"
                  onChange={handleInputValues}
                  invalid={handleErrors.ai_dm_fail_text}
                />
                <CInvalidFeedback>
                  {handleErrors.ai_dm_fail_text}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel>
                  Playback Recording <small className="required_lable">*</small>
                </CLabel>
                <span style={stylePlay}>
                  <CInputFile
                    // custom
                    id="custom-file-input"
                    name="ai_dm_fail_text_uploading_recording"
                    onChange={(e) =>
                      handleInputValuesUpload1(
                        e,
                        "ai_dm_fail_text_uploading_recording"
                      )
                    }
                  />
                  {ai_dm_fail_text_uploading_recording ? (
                    playAudio3 ? (
                      <StopSvgIcon
                        type="ai_dm_fail_text_uploading_recording"
                        src={ai_dm_fail_text_uploading_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio3}
                      />
                    ) : (
                      <PlaySvgIcon
                        type="ai_dm_fail_text_uploading_recording"
                        src={ai_dm_fail_text_uploading_recording}
                        handleAudioPlay={handleAudioPlay}
                        value={playAudio3}
                      />
                    )
                  ) : null}
                  {/* <CLabel
                    htmlFor="custom-file-input"
                    variant="custom-file"
                    style={label_css}
                  >
                    Upload Recording
                  </CLabel> */}
                </span>
                <span style={error_msg}>Upload mp3 files only !</span>
                <CInvalidFeedback>
                  {handleErrors.ai_dm_fail_text_uploading_recording}
                </CInvalidFeedback>
              </CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>
        <CCardFooter>
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddIVrVerificationRule}
          >
            <CIcon name="cil-scrubber" /> Update
            {isSaving && <InvertContentLoader />}
          </CButton>
          <a href="#/ivr-verification-settings" className="cancel_bt">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>

          {/* <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
            // onClick={handleResetFormEvent}
          >
            <CIcon name="cil-ban" /> Reset
          </CButton> */}
            {!disabledAudio && (
             <PlayAudio  />
          )}
        </CCardFooter>
      </CCard>
      {/* <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> Recording preview</b>
        </CCardHeader>
        <CCardBody>
          {!disabledAudio && (
             <PlayAudio  />
          )}
        </CCardBody>
      </CCard> */}
     
    </Fragment>
  );
};
