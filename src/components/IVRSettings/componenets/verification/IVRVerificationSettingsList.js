import React, { Fragment } from "react";
import moment from "moment";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip
} from "@coreui/react";
// import { ContentLoading } from "../../Comman/components";

const fields = [
  "IVR_rules_status",
  "activation_date",
  "deactivation_date",
  "IVR_rule_set_name",
  "total_recordings",
  "language",
  "Action",
];

export const IVRVerificationSettingsList = (props) => {
  const { ivr_verification_rules, isFatching, errorMsg, handleFilterChange, searchValue } = props;
  let scheduled_date = "";
  let display_msg = "";

  return ( 
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}>Verification - IVR Rules</b>

          <span>
            <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />
              <a href="#/add-ivr-verification" className="button_style">
                <CButton
                  type="submit"
                  size="sm"
                  color="primary"
                  className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                >
                  {" "}
                  <i className="fa fa-plus" aria-hidden="true"></i> New IVR Rule
                </CButton>
              </a>
            </div>
          </span>
        </CCardHeader>
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          {/* {successMsg && (
            <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
          )} */}
          <CDataTable
            items={ivr_verification_rules}
            fields={fields}
            itemsPerPage={10}
            loading={isFatching}
            sort={true}
            pagination={true}
            scopedSlots={{
              IVR_rules_status: (item) => (
                <Fragment>
                  <td>
                    {
                      ((scheduled_date =
                        item.activation_date != null &&
                        item.activation_date != "null"
                          ? item.activation_date
                          : "-"),
                      (display_msg =
                        scheduled_date > moment().format("YYYY-MM-DD HH::mm")
                          ? "Scheduled"
                          : ""),
                      item.ivr_rule_status + " - " + "( " + display_msg + " )"),
                      display_msg!='' ? item.ivr_rule_status + " - " + "( " + display_msg + " )" : item.ivr_rule_status
                    }
                  </td>
                </Fragment>
              ),
              activation_date: (item) => (
                <td>
                  {item.activation_date != null && 
                  item.activation_date != "null"
                    ? item.activation_date
                    : "-"}
                </td>
              ),
              deactivation_date: (item) => (
                <td> 
                  {item.deactivation_date != null &&
                  item.deactivation_date != "null"
                    ? item.deactivation_date
                    : "-"}
                </td>
              ),
              IVR_rule_set_name: (item) => (
                <td>
                  {item.ivr_rule_set_name != "null" && item.ivr_rule_set_name != "null" ? item.ivr_rule_set_name : '-'}
                </td>
              ),
              langauge: (item) => (
                <td>
                  {item.langauge != "null" && item.langauge != "null" ? item.langauge : '-'}
                </td>
              ),
              Action: (item) => (
                <td>
                  <a
                    href={`#/ivr-verification-edit/${item.id}`}
                    className="button_style"
                    // style={
                    //   item.rule_status == "testing"
                    //     ? {}
                    //     : { visibility: "hidden" }
                    // }
                  >

                    <CTooltip content={`View Record`} placement={"top-start"}>
                      <CButton type="submit" size="sm" color="primary">
                        <i className="fa fa-edit" aria-hidden="true"></i>
                      </CButton>
                    </CTooltip>
                  </a>

                  <a
                    href={`#/ivr-verification-details/${item.id}`}
                    className="button_style"
                  >
                    <CButton
                      type="submit"
                      size="sm"
                      color="primary"
                      className="remove_button"
                    >
                      <CTooltip content={`View Record`} placement={"top-start"}>
                        <i className="fa fa-eye" aria-hidden="true"></i>
                      </CTooltip>
                    </CButton>
                  </a>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
