import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'

// Manage Registration API
export const addIvrRule = (data) =>
axios.post(`${URL_HOST}/api/ivr_setting/add-new`, data);

export const getIvrRule = () =>
axios.get(`${URL_HOST}/api/ivr_setting/all`);

export const fetchIvrRuleDetail = (id) =>
axios.get(`${URL_HOST}/api/ivr_setting/details/${id}`);


// Manage Verification API
export const addIvrVerificationRule = (data) =>
axios.post(`${URL_HOST}/api/ivr_setting_verify/add-new-ivr-verification`, data);

export const getIvrVerificationRules = () =>
axios.get(`${URL_HOST}/api/ivr_setting_verify/all`);

export const fetchIvrVerificationRuleDetail = (id) =>
axios.get(`${URL_HOST}/api/ivr_setting_verify/details/${id}`);
