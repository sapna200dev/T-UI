export {
  addIvrRule,
  getIvrRule,
  fetchIvrRuleDetail,
  addIvrVerificationRule,
  getIvrVerificationRules,
  fetchIvrVerificationRuleDetail,
} from "./comman";
