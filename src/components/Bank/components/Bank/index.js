export { AddBank } from "./AddBank";

export { BankList } from "./BankList";

export { UpdateBank } from "./UpdateBank";
export {ViewBank} from './ViewBank'