import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
  CTooltip
} from "@coreui/react";
import { ContentLoading } from "../../../Comman/components";

const fields = [
  "bank_name",
  "location",
  "address",
  "billing_status",
  "billing_cycle",
  "status",
  "portal",
  "Action",
];

export const BankList = (props) => {
  const {
    bankList,
    handleDeleteItem,
    errorMsg,
    successMsg,
    loading,
    handleFilterChange,
    searchValue,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> Bank List</b>
          <span>
            <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />
               <a href="#/bank-add" className="button_style">
              <CButton
                type="submit"
                size="sm"
                color="primary"
                className="add_employee_button"
                // onClick={() => handleDeleteItem(item)}
              >
               
                  {" "}
                  Add New
                  </CButton>
                </a>
             
            </div>
          </span>
        </CCardHeader>

        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CDataTable
            items={bankList}
            fields={fields}
            itemsPerPage={5}
            loading={loading}
            pagination
            scopedSlots={{
              status: (item) => (
                <td>
                  {item.status}
                  {/* <CBadge color="success">{item.status}</CBadge> */}
                </td>
              ),
              portal: () => (
                <td>
                  {'Asterisk'}
                  {/* <CBadge color="success">{item.status}</CBadge> */}
                </td>
              ),
              Action: (item) => (
                <td>
                  <a href={`#/bank-update/${item.id}`} className="cancel_bt">
                      <CTooltip
                        content={`Edit Record`}
                        placement={"top-start"}
                      >
                        <CButton type="submit" size="sm" color="primary">
                          <i className="fa fa-edit" aria-hidden="true"></i>
                          {/* Edit */}
                          {/* {loading && <ContentLoading />} */}
                        </CButton>
                      </CTooltip>

                   
                  </a>

                  <CButton
                      type="submit"
                      size="sm"
                      color="danger"
                      className="remove_button"
                      onClick={() => handleDeleteItem(item)}
                    >
                        <CTooltip content={`Delete Record`} placement={"top-start"}>
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </CTooltip>
                      {/* Delete */}
                    </CButton>

                 
                  <a href={`#/bank-view/${item.id}`} className="cancel_bt">
                      <CButton
                        type="submit"
                        size="sm"
                        color="primary"
                        className="remove_button"
                      >
                        <CTooltip content={`View Record`} placement={"top-start"}>
                          <i className="fa fa-eye" aria-hidden="true"></i>
                        </CTooltip>
                        {/* View */}
                      </CButton>
                 
                  </a>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
