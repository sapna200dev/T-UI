import React, { Fragment } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
} from "@coreui/react";

const fields = ["name", "email", "account_no", "account_type", "Action"];

export const BankCustomerList = (props) => {
  const { customers } = props;
  console.log("nom nsdfsdf", customers);
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b>Customers</b>
        </CCardHeader>

        <CCardBody>
          <CDataTable
            items={customers}
            fields={fields}
            itemsPerPage={5}
            pagination
            scopedSlots={{
              Action: (item) => (
                <td>
                  <a
                    // href={`#/customer-view/${item.id}`}
                    style={{ color: "white" }}
                  >
                    <CButton
                      type="submit"
                      size="sm"
                      color="primary"
                      // onClick={() => handleEditItem(item)}
                    >
                      View
                    </CButton>
                  </a>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
