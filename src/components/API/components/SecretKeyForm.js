import React, { Fragment } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CButton,
  CCol,
  CForm,
  CRow,
  CAlert,
} from "@coreui/react";
import { InvertContentLoader } from "../../Comman/components";


const key = {
  marginBottom: '24px',
}

const token_key = {
  color: '#1b1616',
  backgroundColor: '#d2d2d2',
  borderColor: '#d2d2d2'
}

export const SecretKeyForm = (props) => {
  const {
    handleGenerateSecretKey,
    isGenrating,
    errorMsg,
    secret_key,
    handleOpenApis
  } = props;
  return (
    <Fragment>
      <CCard>
        <CCardHeader>

          <b style={{ fontSize: "20px" }}>Generate Secret Key</b>
        </CCardHeader>
        <CCardBody>
          <span>
            {errorMsg && (
              <CAlert color="danger" className="msg_div">
                {errorMsg}
              </CAlert>
            )}
          </span>
          <span>
            <h4>  <small>Before you access our api's you need to create secret key </small></h4>
          </span>

          <CForm>
            <CRow>
              <CCol xs="12" className="text-right">
                <CButton color="primary" className="px-0 secret_key" onClick={handleGenerateSecretKey} style={key}>
                  Generate Secret Key {isGenrating && <InvertContentLoader />}
                </CButton>
              </CCol>
            </CRow>
          </CForm>
          {secret_key && (
            <Fragment>
              <CAlert color="success" className="msg_div" style={token_key}>
                <span>
                  {secret_key}
                </span>
              </CAlert>
              <span style={{ marginLeft: '17px' }}>
                Great, By using this secret key you can access our api's in your application
              </span>
              <br />
              <span style={{ marginLeft: '17px' }}>
                <a  onClick={handleOpenApis}>Click</a> here to open api's
              </span>

            </Fragment>
          )}

        </CCardBody>
      </CCard>
    </Fragment>
  );
};
