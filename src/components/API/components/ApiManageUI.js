import React, { Fragment } from "react";
import { CCard, CCardBody, CCardHeader } from "@coreui/react";
import {PageNotFound} from '../../Comman/components/index'
import { SecretKeyForm } from "./SecretKeyForm";


export const ApiManageUI = (props) => {
  const {
    handleGenerateSecretKey, 
    isGenrating,
    errorMsg, 
    secret_key,
    handleOpenApis
  } = props;
  return (
    <Fragment>
      <CCard>
        <CCardHeader>
            
        <b style={{ fontSize: "20px" }}> Open API's</b>
           </CCardHeader>
        <CCardBody>

          <SecretKeyForm
            handleGenerateSecretKey={handleGenerateSecretKey}
            isGenrating={isGenrating}
            errorMsg={errorMsg}
            secret_key={secret_key}
            handleOpenApis={handleOpenApis}
          />

            {/* <PageNotFound /> */}
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
