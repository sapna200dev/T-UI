import React, { Component, Fragment } from 'react'
import { getSecretKey } from '../../Comman/functions'
import {ApiDocumentationUI} from '../components/index'
import {generateSecretKey} from '../services/common'
import {PageNotFound} from '../../Comman/components/index'
class ApiDocumentation extends Component {
    state = {
        isGenrating:false,
        secret_key:'',
        errorMsg:'',
        successMsg:''
    }


    componentDidMount() {
       let secret_key =  getSecretKey();
       this.setState({secret_key: secret_key ? secret_key : null})
    }


    render() {
        const {isGenrating, errorMsg, secret_key} = this.state;;
        return (
            <Fragment>
                    <ApiDocumentationUI />
            </Fragment>
        )
    }
}

export default ApiDocumentation