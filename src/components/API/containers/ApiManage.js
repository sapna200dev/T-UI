import React, { Component, Fragment } from 'react'
import { getSecretKey } from '../../Comman/functions'
import {ApiManageUI} from '../components/index'
import {generateSecretKey} from '../services/common'

class ApiManage extends Component {
    state = {
        isGenrating:false,
        secret_key:'',
        errorMsg:'',
        successMsg:''
    }


    componentDidMount() {
       let secret_key =  getSecretKey();
       this.setState({secret_key: secret_key ? secret_key : null})
    }

    /**
     * Handle generate secret key
     */
    handleGenerateSecretKey = () => {
        this.setState({ isGenrating: true })
        // Generate secret Api 
        generateSecretKey().then(response => {
            console.log('response secrete key = ', response)
            if(response.data.status == 200) {
                this.setState({ 
                    secret_key : response.data.data, 
                    isGenrating: false 
                })
            } else {
                this.setState({ 
                    errorMsg : response.data.msg,
                    isGenrating: false 
                })
            }
           
        }).catch(err => {
            console.log('response error = ', err)
            const error = err.response ? err.response.data.msg : "";
            this.setState({ 
                errorMsg : error,
                isGenrating: false 
            })
        })
        console.log('Generating = ')
    } // End function


    handleOpenApis = () => {
        console.log('handleOpenApis')
        const newWindow = window.open('/#/500', '_blank')
    }

    render() {
        const {isGenrating, errorMsg, secret_key} = this.state;;
        return (
            <Fragment>
                <ApiManageUI
                    handleGenerateSecretKey={this.handleGenerateSecretKey}
                    isGenrating={isGenrating}
                    errorMsg={errorMsg}
                    secret_key={secret_key}
                    handleOpenApis={this.handleOpenApis}
                />
            </Fragment>
        )
    }
}

export default ApiManage