import React, {useEffect, useState} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import logo from '../assets/images/logo_1.jpeg'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import {Navbar} from '../components/Comman/components/Navbar'

// sidebar nav config
import navigation from './_nav'

const TheSidebar = (props) => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)

    const [layout_color, setLayoutColor] = useState('');

  useEffect(() => {
    let color = localStorage.getItem("layout_color");
    setLayoutColor(color)
  }, []);

  console.log('layout_color')
  console.log(layout_color)
   return ( 
    <CSidebar
      show={props.mobileToggel}
      // onShowChange={(val) => dispatch({type: 'set', sidebarShow: val })}
      onShowChange={false}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/* <h4>TURANT APP</h4> */}
        <img src={logo} style={{ height: '59px',    width: '100%' }}/>
         {/* <CIcon
          className="c-sidebar-brand-full"
          name="logo"
          height={35}
        />
        <CIcon
          className="c-sidebar-brand-minimized" #3c4b64
          name="logo"
          height={35}
        />  */}
      </CSidebarBrand>
      <CSidebarNav style={{background: layout_color , color:'black'}}>

        {/* <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle 
          }}
        />  */}
        <Navbar />
      </CSidebarNav>
      <CSidebarMinimizer  style={{background:  layout_color, color:'black'}} className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
