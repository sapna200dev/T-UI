import React, {useState} from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'

const TheLayout = () => {
  console.log("TheLayout")
  const [mobileToggel, setMobileToggel] = useState(true);

  const showToggleSidebar = () => {
    console.log('showToggleSidebarshowToggleSidebar ')
    setMobileToggel(!mobileToggel)

  }

  const showToggleSidebarMobile = () => {
    console.log('showToggleSidebarMobile showToggleSidebarMobile ')
    setMobileToggel(!mobileToggel)
  }

  return (
    <div className="c-app c-default-layout" >
      <TheSidebar route="/bank-list"
        mobileToggel={mobileToggel}
      />
      <div className="c-wrapper">
        <TheHeader
          showToggleSidebar= { showToggleSidebar}
          showToggleSidebarMobile={showToggleSidebarMobile}
        />
        <div className="c-body">
          <TheContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
} 

export default TheLayout
 