import React, { useEffect, useState, Fragment } from "react";
import { getUserObj, getLoggedInUser } from "../components/Comman/functions";

const TheHeaderUserRole = () => {
  const [role, setRole] = useState("");
  const [user, setUser] = useState(""); 

  useEffect(() => {
    const user_role = getUserObj("role");
    const loggedInUser = getLoggedInUser("user");
    if (user_role) { 
      setRole(JSON.parse(user_role));
    }
    if (loggedInUser) { 
      setUser(JSON.parse(loggedInUser));
    }
  }, []);
 

  const  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }


  return (
    <Fragment>
      <div className="text-truncate font-weight-bold">
        {/* <span className="fa fa-exclamation text-danger"></span> */}
        {user.bank_name ? capitalizeFirstLetter(user.bank_name.toLowerCase()) : 'Turant'} {role.display_name} 
        {/* ( {user.bank_name} ) */}
      </div>
    </Fragment>
  );
}; 

export default TheHeaderUserRole;
