import React from "react";
import {
  CWidgetDropdown,
  CRow,
  CCol,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CDropdownToggle,
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
  CBadge,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ChartLineSimple from "../charts/ChartLineSimple";
import ChartBarSimple from "../charts/ChartBarSimple";
import {
  CustomerSvgIcon,
  UploadDocSvgIcon,
} from "../../components/Comman/Icons";

const WidgetsDropdown = (props) => {
  const {
    customersDetails,
    endUserDetails,
    uploadedRecords,
    attempted_calls,
    verification_calls,
    registration_calls,

    todays_customer,
    today_enduser,
    today_uploaded_records,
    today_attempts_call,
    today_register_calls,
    today_verification_calls,

    isTotalCustomerFatching,
    isTotalEnduserFatching,
    isTotalRecordsUploadFatching,
    isCallsFatching,
    isRegisterCallFatching,
    isVerifyCallsFatching,
    isTodayEnduserFatching,
    isTodayCustomerFatching,
    isTodayUploadedRecordsFatching,
    isTodayAttemptesCallsFatching,
    isTodayRegisterCallFatching,
    isTodayVerifyCallFatching,
  } = props;

 
  const lorem =
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.";

  // render
  return (
    <CRow>
      <CCol xs="12" md="12" className="mb-4">
        <CCard>
          <CCardHeader>System Summary</CCardHeader>
          <CCardBody>
            <CTabs>
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink>Systemwide</CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink>Today</CNavLink>
                </CNavItem>
                <CNavItem>{/* <CNavLink>Messages</CNavLink> */}</CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane>
                  <CRow style={{ marginTop: "17px" }}>
                    {/* <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-primary"
                        header={customersDetails && customersDetails.bank_count > 0 ? customersDetails.bank_count : 0}
                        text="Total Customers ( Banks )"
                        // footerSlot="ssssss"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="c-chart-wrapper mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[65, 59, 84, 84, 51, 55, 40]}
                            dataPoints={customersDetails && customersDetails.banks ? customersDetails.banks.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="primary"
                            label="Customers"
                            labels="months"
                          />
                        }
                       
                      >
                        {isTotalCustomerFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle color="transparent">
                           
                            <CustomerSvgIcon />
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                              Active
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {customersDetails.active &&
                                  customersDetails.active.count}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                              In-active
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count_inactive"
                              >
                                {customersDetails.inactive &&
                                  customersDetails.inactive.count}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>Poc</CDropdownItem>
                           
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol> */}

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-info"
                        header={endUserDetails && endUserDetails.end_user_count}
                        text="Total End Users"
                        footerSlot={
                        //   <ChartBarSimple
                        //   className="mt-3 mx-3"
                        //   style={{height: '70px'}}
                        //   backgroundColor="rgb(250, 152, 152)"
                        //   label="Members"
                        //   labels="months"
                        // />
                          <ChartBarSimple
                            pointed
                            className="c-chart-wrapper mt-3 mx-3"
                            style={{ height: "70px" }}
                        
                            dataPoints={endUserDetails && endUserDetails.endUsers ? endUserDetails.endUsers.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="End User"
                            labels="months"
                          />
                        }
                      >
                        {isTotalEnduserFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle caret={true} color="transparent">
                            {/* <CIcon name="cil-location-pin"/> */}
                            <CustomerSvgIcon />
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                              Active
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {endUserDetails && endUserDetails.active &&
                                  endUserDetails.active.count}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                              In-active
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {endUserDetails && endUserDetails.inactive &&
                                  endUserDetails.inactive.count}
                              </CBadge>
                            </CDropdownItem>
                            {/* <CDropdownItem>Poc</CDropdownItem> */}
                            {/* <CDropdownItem disabled>Disabled action</CDropdownItem> */}
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-warning"
                        header={uploadedRecords && uploadedRecords.total_count > 0 ? uploadedRecords.total_count : 0}
                        text="Total Records Uploaded"
                        footerSlot={
                          // <ChartLineSimple
                          //   className="mt-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgba(255,255,255,.2)"
                          //   dataPoints={[78, 81, 80, 45, 34, 12, 40]}
                          //   options={{
                          //     elements: { line: { borderWidth: 2.5 } },
                          //   }}
                          //   pointHoverBackgroundColor="warning"
                          //   label="Members"
                          //   labels="months"
                          // />
                          <ChartBarSimple
                          
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                         
                            dataPoints={uploadedRecords && uploadedRecords.records_count ? uploadedRecords.records_count.map((item, index) => {
                              return index+1;
                             }) : []}

                            
                            pointHoverBackgroundColor="warning"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="Record"
                            labels="months"
                          />
                        }
                      >
                        {isTotalRecordsUploadFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle color="transparent">
                            {/* <CIcon name="cil-settings"/> */}
                            <UploadDocSvgIcon />
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                             <CDropdownItem>
                              Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {uploadedRecords && uploadedRecords.success_count &&
                                  uploadedRecords.success_count > 0 ? uploadedRecords.success_count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {uploadedRecords && uploadedRecords.failed_count &&
                                  uploadedRecords.failed_count > 0 ? uploadedRecords.failed_count : 0}
                              </CBadge>
                            </CDropdownItem>

                          
                            {/* <CDropdownItem>Something else here...</CDropdownItem> */}
                            {/* <CDropdownItem disabled>Disabled action</CDropdownItem> */}
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-danger"
                        header={attempted_calls && attempted_calls.total_count > 0 ? attempted_calls.total_count : 0}
                        text="Total Calls Attempted"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                            dataPoints={attempted_calls && attempted_calls.total ? attempted_calls.total.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="Calls"
                            labels="months"
                          />
                          // <ChartBarSimple
                          //   className="mt-3 mx-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgb(250, 152, 152)"
                          //   label="Members"
                          //   labels="months"
                          // />
                        }
                      >
                        {isCallsFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}

                        <CDropdown>
                          <CDropdownToggle
                            caret
                            className="text-white"
                            color="transparent"
                          >
                            {/* <CIcon name="cil-settings"/> */}
                            <span className="call_css">
                              <i className="fa fa-phone" aria-hidden="true"></i>
                            </span>
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                              Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {attempted_calls.success &&
                                  attempted_calls.success.count}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {attempted_calls.failed &&
                                  attempted_calls.failed.count}
                              </CBadge>
                            </CDropdownItem>
                            {/* <CDropdownItem>Something else here...</CDropdownItem> */}
                            {/* <CDropdownItem disabled>Disabled action</CDropdownItem> */}
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>
                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-info"
                        header={registration_calls && registration_calls.total_count >0 ? registration_calls.total_count : 0}
                        text="Total Registration Calls Attemtped"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                            dataPoints={registration_calls && registration_calls.total ? registration_calls.total.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="Calls"
                            labels="months"
                          />
                          // <ChartBarSimple
                          //   className="mt-3 mx-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgb(250, 152, 152)"
                          //   label="Members"
                          //   labels="months"
                          // />
                        }
                      >
                        {isRegisterCallFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle
                            caret
                            className="text-white"
                            color="transparent"
                          >
                            {/* <CIcon name="cil-settings"/> */}
                            <span className="call_css">
                              <i className="fa fa-phone" aria-hidden="true"></i>
                            </span>
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                              Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {registration_calls.success &&
                                  registration_calls.success.count}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {registration_calls.failed &&
                                  registration_calls.failed.count}
                              </CBadge>
                            </CDropdownItem>
                            {/* <CDropdownItem>
                              Something else here...
                            </CDropdownItem>
                            <CDropdownItem disabled>
                              Disabled action
                            </CDropdownItem> */}
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>

                  </CRow>

                  <CRow style={{ marginTop: "17px" }}>
        
                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-info"
                        header={verification_calls && verification_calls.total_count > 0 ? verification_calls.total_count : 0}
                        text="Total Verification Calls Attemtped"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                            dataPoints={verification_calls && verification_calls.total ? verification_calls.total.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="Calls"
                            labels="months"
                          />
                          // <ChartBarSimple
                          //   className="mt-3 mx-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgb(250, 152, 152)"
                          //   label="Members"
                          //   labels="months"
                          // />
                        }
                      >
                        {isVerifyCallsFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle
                            caret
                            className="text-white"
                            color="transparent"
                          >
                            {/* <CIcon name="cil-settings"/> */}
                            <span className="call_css">
                              <i className="fa fa-phone" aria-hidden="true"></i>
                            </span>
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                              Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {verification_calls.success &&
                                  verification_calls.success.count}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {verification_calls.failed &&
                                  verification_calls.failed.count}
                              </CBadge>
                            </CDropdownItem>
                            {/* <CDropdownItem>Something else here...</CDropdownItem> */}
                            {/* <CDropdownItem disabled>Disabled action</CDropdownItem> */}
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>
                  </CRow>
                </CTabPane>

               <CTabPane>
                  <CRow style={{ marginTop: "17px" }}>
                    {/* <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-primary"
                        header={todays_customer.total_count > 0 ? todays_customer.total_count : 0}
                        text="Today Customers ( Banks )"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="c-chart-wrapper mt-3 mx-3"
                            style={{ height: "70px" }}
                            dataPoints={todays_customer.total ? todays_customer.total.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="primary"
                            label="Customers"
                            labels="days"
                          />
                        }
                      >
                        {isTodayCustomerFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle color="transparent">
                          <CustomerSvgIcon />
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                              <CDropdownItem>
                            Active
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {todays_customer.active &&
                                  todays_customer.active.count  > 0 ?  todays_customer.active.count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            In-active
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {todays_customer.inactive &&
                                  todays_customer.inactive.count > 0 ? todays_customer.inactive.count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem></CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol> */}

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-info"
                        header={today_enduser.total_count > 0 ? today_enduser.total_count : 0}
                        text="End Users"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                            dataPoints={today_enduser.total ? today_enduser.total.map((item, index) => {
                              return index+1;
                            }) : [0]}

                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="End Users"
                            labels="days"
                          />
                        }
                      >
                        {isTodayEnduserFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle caret={true} color="transparent">
                          <CustomerSvgIcon />
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                             <CDropdownItem>
                            Active
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {today_enduser && today_enduser.active &&
                                  today_enduser.active.count  > 0 ?  today_enduser.active.count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            In-active
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {today_enduser.inactive &&
                                  today_enduser.inactive.count > 0 ? today_enduser.inactive.count : 0}
                              </CBadge>
                            </CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-warning"
                        // header={today_uploaded_records}
                        header={today_uploaded_records.records_count > 0 ? today_uploaded_records.records_count : 0}
                        text="Records Uploaded"
                        footerSlot={
                          <ChartLineSimple
                          pointed
                          className="mt-3 mx-3"
                          style={{ height: "70px" }}
                          // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                          dataPoints={today_uploaded_records.total ? today_uploaded_records.total.map((item, index) => {
                            return index+1;
                          }) : []}
                          pointHoverBackgroundColor="warning"
                          options={{
                            elements: { line: { tension: 0.00001 } },
                          }}
                          label="Record"
                          labels="months"
                        />
                          // <ChartLineSimple
                          //   className="mt-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgba(255,255,255,.2)"
                          //   // dataPoints={[78, 81, 80, 45, 34, 12, 40]}
                          //   // dataPoints={today_uploaded_records.total ? today_uploaded_records.total.map((item, index) => {
                          //   //   return index+1;
                          //   // }) : [0]}
                          //   options={{
                          //     elements: { line: { borderWidth: 2.5 } },
                          //   }}
                          //   pointHoverBackgroundColor="warning"
                          //   label="Members"
                          //   labels="months"
                          // />
                        }
                      >
                        {isTodayUploadedRecordsFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle color="transparent">
                          <UploadDocSvgIcon />
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >

                            <CDropdownItem>
                            Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {today_uploaded_records && today_uploaded_records.success_count &&
                                  today_uploaded_records.success_count  > 0 ?  today_uploaded_records.success_count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {today_uploaded_records && today_uploaded_records.failed_count &&
                                  today_uploaded_records.failed_count > 0 ? today_uploaded_records.failed_count : 0}
                              </CBadge>
                            </CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-danger"
                        header={today_attempts_call && today_attempts_call.total_count > 0 ? today_attempts_call.total_count : 0}
                        text="Calls Attempted"
                        footerSlot={
                          <ChartLineSimple
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                            dataPoints={today_attempts_call && today_attempts_call.total ? today_attempts_call.total.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="Record"
                            labels="months"
                          />
                          // <ChartBarSimple
                          //   dataPoints={today_uploaded_records.total ? today_uploaded_records.total.map((item, index) => {
                          //     return index+1;
                          //   }) : [0]}
                          //   className="mt-3 mx-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgb(250, 152, 152)"
                          //   label="Members"
                          //   labels="months"
                          // />
                        }
                      >
                        {isTodayAttemptesCallsFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle
                            caret
                            className="text-white"
                            color="transparent"
                          >
                          <span className="call_css">
                              <i className="fa fa-phone" aria-hidden="true"></i>
                            </span>
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                             <CDropdownItem>
                            Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {today_attempts_call.success &&
                                  today_attempts_call.success.count  > 0 ?  today_attempts_call.success.count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {today_attempts_call.failed &&
                                  today_attempts_call.failed.count > 0 ? today_attempts_call.failed.count : 0}
                              </CBadge>
                            </CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>
                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-info"
                        header={today_register_calls && today_register_calls.total_count > 0 ? today_register_calls.total_count : 0}
                        text="Registeration Calls"
                        footerSlot={
                          <ChartLineSimple
                          pointed
                          className="mt-3 mx-3"
                          style={{ height: "70px" }}
                          // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                          dataPoints={today_register_calls && today_register_calls.total ? today_register_calls.total.map((item, index) => {
                            return index+1;
                          }) : []}
                          pointHoverBackgroundColor="info"
                          options={{
                            elements: { line: { tension: 0.00001 } },
                          }}
                          label="Record"
                          labels="months"
                        />

                          // <ChartLineSimple
                          //   className="mt-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgba(255,255,255,.2)"
                          //   // dataPoints={[78, 81, 80, 45, 34, 12, 40]}
                          //   dataPoints={today_register_calls.total && today_register_calls.total.map((item, index) => {
                          //     return index;
                          //   })}
                          //   options={{
                          //     elements: { line: { borderWidth: 2.5 } },
                          //   }}
                          //   pointHoverBackgroundColor="warning"
                          //   label="Members"
                          //   labels="months"
                          // />
                        }
                      >
                        {isTodayRegisterCallFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle color="transparent">
                          <span className="call_css">
                              <i className="fa fa-phone" aria-hidden="true"></i>
                            </span>
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                            Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {today_register_calls.success &&
                                  today_register_calls.success.count  > 0 ?  today_register_calls.success.count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {today_register_calls.failed &&
                                  today_register_calls.failed.count > 0 ? today_register_calls.failed.count : 0}
                              </CBadge>
                            </CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>

                  </CRow>

                  <CRow style={{ marginTop: "17px" }}>

                    <CCol sm="6" lg="3">
                      <CWidgetDropdown
                        color="gradient-info"
                        header={today_verification_calls.total_count >0 ? today_verification_calls.total_count : 0}
                        text="Verification Calls"
                        
                        footerSlot={
                          // <ChartBarSimple
                          //   className="mt-3 mx-3"
                          //   style={{ height: "70px" }}
                          //   backgroundColor="rgb(250, 152, 152)"
                          //   label="Calls"
                          //   labels="months"
                          //   today_register_calls={today_register_calls.total}
                          // />
                          <ChartLineSimple
                            pointed
                            className="mt-3 mx-3"
                            style={{ height: "70px" }}
                            // dataPoints={[1, 18, 9, 17, 34, 22, 11]}
                            dataPoints={today_verification_calls.total ? today_verification_calls.total.map((item, index) => {
                              return index+1;
                            }) : []}
                            pointHoverBackgroundColor="info"
                            options={{
                              elements: { line: { tension: 0.00001 } },
                            }}
                            label="Record"
                            labels="months"
                          />
                        }
                      >
                        {isTodayVerifyCallFatching && (
                            <div class="spinner-border spinner-border-sm dashboard_loading" role="status" >
                              <span class="sr-only">Loading...</span>
                            </div>
                        )}
                        <CDropdown>
                          <CDropdownToggle
                            caret
                            className="text-white"
                            color="transparent"
                          >
                            <span className="call_css">
                              <i className="fa fa-phone" aria-hidden="true"></i>
                            </span>
                          </CDropdownToggle>
                          <CDropdownMenu
                            className="pt-0"
                            placement="bottom-end"
                          >
                            <CDropdownItem>
                            Success
                              <CBadge
                                color="primary"
                                className="mfs-auto lebal_count"
                              >
                                {today_verification_calls.success &&
                                  today_verification_calls.success.count  > 0 ?  today_verification_calls.success.count : 0}
                              </CBadge>
                            </CDropdownItem>
                            <CDropdownItem>
                            Failed
                              <CBadge
                                color="danger"
                                className="mfs-auto lebal_count"
                              >
                                {today_verification_calls.failed &&
                                  today_verification_calls.failed.count > 0 ? today_verification_calls.failed.count : 0}
                              </CBadge>
                            </CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </CWidgetDropdown>
                    </CCol>
                  </CRow>
                </CTabPane>
                <CTabPane>
                  <span style={{ marginLeft: "42%" }}>Under Construction</span>
                </CTabPane>
              </CTabContent>
            </CTabs>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default WidgetsDropdown;
