import React from "react";
import {
  admin,
  super_admin,
  customer_care,
  ROUTE_VALIDATE,
  employees
} from "./components/Comman/constants";
import { getUserObj } from "./components/Comman/functions";
import {
  Bank,
  Banks,
  BankDetail,
  ShowBank,
} from "./components/Bank/containers";
// import { Profile } from "./components/Profile/containers";
// import { ProfilePage1 } from "./components/Profile/containers";
import {MainProfilePage } from './components/Profile/containers';
import { Dashboard } from "./components/Dashboard/containers";
import AddEmployee from "./components/Employee/containers/emaployee/AddEmployee";
import { Employee } from "./components/Employee/containers/emaployee/Employee";
import { ViewEmployee } from "./components/Employee/containers/emaployee/ViewEmployee";
import EmployeeDetails from "./components/Employee/containers/emaployee/EmployeeDetails";
import {EndUsers, AddEndUser, EditEndUser, EndUserDetails} from './components/EndUser/container/EndUser/index'
import {UploadNewDoc} from './components/UploadDoc/container/UploadDoc/index'
import {BrancheList} from './components/Branch/containers/index'
import { GlobalParameters , AddNewAIEngine, EditAIEngine, AIEngineDetails, DetailPage, EditPage, AddPage, 
  EditATSettings, AddNewAT, ViewATSetting} from "./components/GlobalParameter/containers/index";

  import { ManageRating, ManageRatingEdit } from './components/ManageRating/containers/index'
import {Settings} from './components/Comman/settings/Settings'
// import {AIEngineList} from './components/GlobalParameter/containers/index'
import {SettingUI} from "./components/Settings/containers";
import {IVRSettings, IVRSettingsRules, IvrSettingsEdit, IvrSettingsDetails} from './components/IVRSettings/containers'
import {VerificationList, AddIvrVerification, IvrVerificationDetails, IvrVerificationEdit} from './components/IVRSettings/containers/verifications/index'
import {AddBranch, BranchViewDetails} from './components/Branch/containers/index'

import {ApiManage, ApiDocumentation} from './components/API/containers/index'
import {PageNotFound} from './components/Comman/components/index'



let is_admin = false;
let is_super_admin = false; 
let is_customer_care = false;
let is_emmployee = false;

const user_role = getUserObj("role");
let role = "";
if (user_role) {
  role = JSON.parse(user_role);
}

if (admin.includes(role.display_name)) {
  is_admin = ROUTE_VALIDATE.IS_ADMIN;
} else if (super_admin.includes(role.display_name)) {
  is_super_admin = ROUTE_VALIDATE.IS_SUPER_ADMIN;
} else if (customer_care.includes(role.display_name)) {
  is_customer_care = ROUTE_VALIDATE.IS_CUSTOMER_CARE;
} else if (employees.includes(role.display_name)) {
  is_emmployee = ROUTE_VALIDATE.IS_EMPLOYEE;
}


console.log('is_adminis_adminis_adminis_adminis_admin = ')
console.log(is_admin)


const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/profile", name: "Profile", component: MainProfilePage },


 /********************** Bank Routes   ******************************/
  {
    path: `${is_super_admin ? "/bank-list" : "/dashboard"}`,
    name: "Bank List",
    component: Banks,
  },
  {
    path: `${is_super_admin ? "/bank-add" : "/dashboard"}`,
    name: "Bank Add",
    component: Bank,
  },

  {
    path: `${is_super_admin ? "/bank-update/:id" : "/dashboard"}`,
    name: "Bank Edit",
    component: BankDetail,
  },

  {
    path: `${is_super_admin ? "/bank-view/:id" : "/dashboard"}`,
    name: "Bank View",
    component: ShowBank,
  },

  {
    path: `${is_super_admin ? "/global-parameter-add" : "/dashboard"}`,
    name: "Add Global Parameter",
    component: AddNewAIEngine,
  },
  {
    path: `${is_super_admin ? "/global-parameter-list" : "/dashboard"}`,
    name: "Global Parameter",
    component: GlobalParameters,
  },

  {
    path: `${is_super_admin ? "/global-parameter-edit/:id" : "/dashboard"}`,
    name: "Edit Global Parameter",
    component: EditAIEngine,
  },
  {
    path: `${is_super_admin ? "/global-parameter-detail/:id" : "/dashboard"}`,
    name: "Global Parameter Details",
    component: AIEngineDetails,
  },

  {
    path: `${is_super_admin ? "/manage-rating" : "/dashboard"}`,
    name: "Score Management",
    component: ManageRating,
  },


  {
    path: `${is_super_admin ? "/rating-update/:id" : "/dashboard"}`,
    name: "Rating Edit",
    component: ManageRatingEdit,
  },
  
  
  

 /********************** DTMF Profile Routes   ******************************/

 {
  path: `${is_super_admin ? "/add-dtmf-profile" : "/dashboard"}`,
  name: "Add Global Parameter",
  component: AddPage,
},
{
  path: `${is_super_admin ? "/dtmf-detail/:id" : "/dashboard"}`,
  name: "DTMF Profile Details",
  component: DetailPage,
},
{
  path: `${is_super_admin ? "/dtmf-edit/:id" : "/dashboard"}`,
  name: "DTMF Profile Edit",
  component: EditPage,
},
 /********************** Employee Routes   ******************************/
  {
    path: `${is_admin ? "/employee" : "/dashboard"}`,
    name: "Employee List",
    component: Employee,
  },
  {
    path: `${is_admin ? "/employee-add" : "/dashboard"}`,
    name: "Employee Add",
    component: AddEmployee,
  },
  {
    path: `${is_admin ? "/employee-view/:id" : "/dashboard"}`,
    name: "Employee View",
    component: ViewEmployee,
  },
  {
    path: `${is_admin ? "/employee-edit/:id" : "/dashboard"}`,
    name: "Employee Edit",
    component: EmployeeDetails,
  },


 /********************** End Users Routes   ******************************/

  {
    path: `${(is_admin || is_emmployee) ? "/end-user" : "/dashboard"}`,
    name: "End Users",
    component: EndUsers,
  },
  {
    path: `${(is_admin || is_emmployee) ? "/end-user-add" : "/dashboard"}`,
    name: "Add End User",
    component: AddEndUser,
  },

  {
    path: `${(is_admin || is_emmployee) ? "/end-user-edit/:id" : "/dashboard"}`,
    name: "Edit End User",
    component: EditEndUser,
  },
  {
    path: `${(is_admin || is_emmployee) ? "/end-user-view/:id" : "/dashboard"}`,
    name: "End User Details",
    component: EndUserDetails,
  },


 /********************** Upload docs Routes   ******************************/
  {
    path: `${is_admin ? "/upload-file" : "/dashboard"}`,
    name: "Upload File",
    component: UploadNewDoc,
  },

  {
    path: `${is_admin ? "/manage-branches" : "/dashboard"}`,
    name: "Manage Branch",
    component: BrancheList,
  },

  {
    path: `${is_admin ? "/manage-settings" : "/dashboard"}`,
    name: "Settings",
    component: SettingUI,
  },


 /********************** IVR Registration Routes   ******************************/
  {
    path: `${is_admin ? "/ivr-settings" : "/dashboard"}`,
    name: "IVR Settings",
    component: IVRSettings,
  },

  {
    path: `${is_admin ? "/add-ivr-rule" : "/dashboard"}`,
    name: "Add IVR Rules",
    component: IVRSettingsRules,
  },

  {
    path: `${is_admin ? "/ivr-rule-edit/:id" : "/dashboard"}`,
    name: "Edit IVR Rules",
    component: IvrSettingsEdit,
  },

 
  {
    path: `${is_admin ? "/ivr-rule-detail/:id" : "/dashboard"}`,
    name: "IVR Rules Details",
    component: IvrSettingsDetails,
  },

 /********************** IVR Verification Routes   ******************************/
  {
    path: `${is_admin ? "/ivr-verification-settings" : "/dashboard"}`,
    name: "IVR Verification Settings",
    component: VerificationList,
  },

  {
    path: `${is_admin ? "/add-ivr-verification" : "/dashboard"}`,
    name: "Add IVR Verification Rule",
    component: AddIvrVerification,
  },
  {
    path: `${is_admin ? "/ivr-verification-edit/:id" : "/dashboard"}`,
    name: "Edit IVR Verification",
    component: IvrVerificationEdit,
  },

  {
    path: `${is_admin ? "/ivr-verification-details/:id" : "/dashboard"}`,
    name: "IVR Verification Details",
    component: IvrVerificationDetails,
  },

  /********************** Brranch Routes   ******************************/
  {
    path: `${is_admin ? "/add-new-branch" : "/dashboard"}`,
    name: "Add New Branch",
    component: AddBranch,
  },

  {
    path: `${is_admin ? "/branch-details/:id" : "/dashboard"}`,
    name: "Branch Detail View",
    component: BranchViewDetails,
  },

  
  /********************** AT Settings   ******************************/
  {
    path: `${is_super_admin ? "/add-at-setting" : "/dashboard"}`,
    name: "Add New AT Settings",
    component: AddNewAT,
  },
  {
    path: `${is_super_admin ? "/asterisks-detail/:id" : "/dashboard"}`,
    name: "Edit AT Settings",
    component: EditATSettings,
  },
  {
    path: `${is_super_admin ? "/asterisks-view/:id" : "/dashboard"}`,
    name: "View AT Settings",
    component: ViewATSetting,
  },

 
  {
    path: `/system-settings`,
    name: "Settings",
    component: Settings,
  },


    
  /********************** API Management   ******************************/
  {
    path: `/api-management`,
    name: "API Management",
    component: ApiManage,
  },
  {
    path: `/api-documentation`,
    component: ApiDocumentation,
  },
  {
    path: `/not-found`,
    component: PageNotFound,
  },
  
  

 
  // {
  //   path: `${is_admin ? "/add-ivr-verification" : "/dashboard"}`,
  //   name: "Add IVR Verification Rule",
  //   component: AddIvrVerification,
  // },
  // {
  //   path: `${is_admin ? "/ivr-verification-edit/:id" : "/dashboard"}`,
  //   name: "Edit IVR Verification",
  //   component: IvrVerificationEdit,
  // },

  // {
  //   path: `${is_admin ? "/ivr-verification-details/:id" : "/dashboard"}`,
  //   name: "IVR Verification Details",
  //   component: IvrVerificationDetails,
  // },


  


  // { path: '/bank/add', name: 'Add', component: Bank },
  // { path: '/bank/update/:id', name: 'Edit', component: BankDetail },
  // { path: '/bank/view/:id', name: 'View', component: ShowBank },
  // { path: '/customer', name: 'Customer', component: Customer },
  // { path: '/customer-add', name: 'Add', component: AddCustomer },
  // { path: "/customer-view/:id", name: "View", component: ShowCustomer },
  // { path: '/customer-update/:id', name: 'Update', component: CustomerDetails },


];

export default routes;
